import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class FollowSuggestionBloc {
  final _following = BehaviorSubject<bool>();
  final _currentUser = BehaviorSubject<User>();

  Stream<bool> get following => _following.stream;

  Function(bool) get setFollowing => _following.sink.add;

  Stream<User> get currentUser => _currentUser.stream;

  Function(User) get setCurrentUser => _currentUser.sink.add;

  MemecistUser user;

  initializeFollowSuggestionsBloc(MemecistUser user) async {
    this.user = user;
    User currentUser = await repository.getLoggedInUser();
    bool isFollowing =
        await repository.isFollowingUser(user.userID, currentUser.uid);
    setFollowing(isFollowing);
    setCurrentUser(currentUser);
  }

  follow() async {
    setFollowing(true);
    User currentUser = await repository.getLoggedInUser();
    MemecistUser user = await repository.getMemecistUser(currentUser.uid);
    repository.followUser(this.user.userID, user);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.follow_user, parameters: {
      FirebaseAnalyticsConstants.source:
          FirebaseAnalyticsConstants.follow_suggestion_card
    });
  }

  unFollow() async {
    setFollowing(false);
    User currentUser = await repository.getLoggedInUser();
    repository.unFollowUser(this.user.userID, currentUser.uid);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.unfollow_user, parameters: {
      FirebaseAnalyticsConstants.source:
          FirebaseAnalyticsConstants.follow_suggestion_card
    });
  }

  void dispose() async {
    await _following.drain();
    _following.close();

    await _currentUser.drain();
    _currentUser.close();
  }
}
