import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_messaging_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/keywords_generator.dart';
import 'package:rxdart/rxdart.dart';

class GroupSuggestionBloc {
  final _isMember = BehaviorSubject<bool>();

  Stream<bool> get member => _isMember.stream;

  Function(bool) get setMember => _isMember.sink.add;

  Group group;

  initializeGroupSuggestionsBloc(Group group) async {
    this.group = group;
    User currentUser = await repository.getLoggedInUser();
    bool isMember = await repository.isMember(group.groupID, currentUser.uid);
    bool isAdmin = await repository.isAdmin(group.groupID, currentUser.uid);

    if (isMember || isAdmin) {
      setMember(true);
    } else {
      setMember(false);
    }
  }

  joinGroup() async {
    setMember(true);
    User currentUser = await repository.getLoggedInUser();
    MemecistUser user = await repository.getMemecistUser(currentUser.uid);

    repository.joinGroup(this.group.groupID, user);

    repository
        .subscribeTopic(FirebaseMessagingConstants.group + "-" + group.groupID);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.join_group, parameters: {
      FirebaseAnalyticsConstants.source:
          FirebaseAnalyticsConstants.group_suggestion_card
    });
  }

  leaveGroup() async {
    setMember(false);
    User currentUser = await repository.getLoggedInUser();

    bool isAdmin = await repository.isAdmin(group.groupID, currentUser.uid);
    repository.leaveGroup(currentUser.uid, group.groupID, isAdmin);

    if (isAdmin)
      repository.unSubscribeTopic(FirebaseMessagingConstants.group +
          "-" +
          group.groupID +
          "-" +
          FirebaseMessagingConstants.admin);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.leave_group, parameters: {
      FirebaseAnalyticsConstants.source:
          FirebaseAnalyticsConstants.group_suggestion_card
    });
  }

  void dispose() async {
    await _isMember.drain();
    _isMember.close();
  }
}
