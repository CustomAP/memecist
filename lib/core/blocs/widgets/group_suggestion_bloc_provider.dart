import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/widgets/group_suggestion_bloc.dart';

class GroupSuggestionBlocProvider extends InheritedWidget {
  final GroupSuggestionBloc groupSuggestionBloc;

  GroupSuggestionBlocProvider({Key key, Widget child})
      : groupSuggestionBloc = GroupSuggestionBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static GroupSuggestionBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<GroupSuggestionBlocProvider>()
        .groupSuggestionBloc;
  }
}
