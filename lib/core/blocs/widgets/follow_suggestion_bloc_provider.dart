import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/widgets/follow_suggestion_bloc.dart';

class FollowSuggestionBlocProvider extends InheritedWidget {
  final FollowSuggestionBloc followSuggestionBloc;

  FollowSuggestionBlocProvider({Key key, Widget child})
      : followSuggestionBloc = FollowSuggestionBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static FollowSuggestionBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<FollowSuggestionBlocProvider>()
        .followSuggestionBloc;
  }
}
