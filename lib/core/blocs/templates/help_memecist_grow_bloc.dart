import 'dart:io';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class HelpMemecistGrowBloc {
  final _imageUploadTask = PublishSubject<StorageUploadTask>();
  final _showLoading = BehaviorSubject<bool>();
  final _isSSUploaded = BehaviorSubject<bool>();

  Stream<StorageUploadTask> get imageUploadTask => _imageUploadTask.stream;

  Function(StorageUploadTask) get setImageUploadTask =>
      _imageUploadTask.sink.add;

  Stream<bool> get showLoading => _showLoading.stream;

  Function(bool) get setShowLoading => _showLoading.sink.add;

  Stream<bool> get isSSUploaded => _isSSUploaded.stream;

  Function(bool) get setIsSSUploaded => _isSSUploaded.sink.add;

  uploadScreenshot(File file) async {
    setShowLoading(true);
    User currentUser = await repository.getLoggedInUser();
    var result =
        await FlutterImageCompress.compressWithList(file.readAsBytesSync());
    setImageUploadTask(repository.uploadMemecistStoryScreenshot(
        Uint8List.fromList(result), currentUser.uid));
  }

  shareImage() async {
    final file = await DefaultCacheManager()
        .getSingleFile(FirebaseStorageConstants.memecistStoryShare);

    await repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.memecist_story_share);

    await Share.file(
        'Share', 'memecist_story.png', file.readAsBytesSync(), 'image/png', text: "Available on Android and IOS");
  }

  uploadToDb(StorageReference storageReference) async {
    User currentUser = await repository.getLoggedInUser();
    await repository.uploadMcStorySSDetails(
        currentUser.uid, await storageReference.getDownloadURL());

    setShowLoading(false);
    setImageUploadTask(null);
    setIsSSUploaded(true);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.mc_story_ss_upload);
  }

  HelpMemecistGrowBloc() {
    _imageUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        imageUploadTask.events.listen((eventReceived) {
          if (imageUploadTask.isComplete) {
            uploadToDb(eventReceived.snapshot.ref);
          }
        });
      }
    });
  }

  void dispose() async {
    await _imageUploadTask.drain();
    _imageUploadTask.close();

    await _showLoading.drain();
    _showLoading.close();

    await _isSSUploaded.drain();
    _isSSUploaded.close();
  }
}
