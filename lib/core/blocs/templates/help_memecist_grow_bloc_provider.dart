import 'package:memecist/core/blocs/templates/help_memecist_grow_bloc.dart';
import 'package:flutter/material.dart';

class HelpMemecistGrowBlocProvider extends InheritedWidget {
  final HelpMemecistGrowBloc helpMemecistGrowBloc;

  HelpMemecistGrowBlocProvider({Key key, Widget child})
      : helpMemecistGrowBloc = HelpMemecistGrowBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static HelpMemecistGrowBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<HelpMemecistGrowBlocProvider>()
        .helpMemecistGrowBloc;
  }
}
