import 'package:memecist/core/blocs/templates/templates_bloc.dart';
import 'package:flutter/material.dart';

class TemplatesBlocProvider extends InheritedWidget {
  final TemplatesBloc templatesBloc;

  TemplatesBlocProvider({Key key, Widget child})
      : templatesBloc = TemplatesBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static TemplatesBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<TemplatesBlocProvider>()
        .templatesBloc;
  }
}
