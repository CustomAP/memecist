import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/ip_model.dart';
import 'package:memecist/core/models/template_category_model.dart';
import 'package:memecist/core/models/template_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:ui' as ui;

import 'package:save_in_gallery/save_in_gallery.dart';

enum TemplateUploadStatus { Uploading, Complete }

class TemplatesBloc {
  final _categories = BehaviorSubject<List<TemplateCategory>>();
  final _templatesList = PublishSubject<List<Template>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _isSearching = BehaviorSubject<bool>();
  final _imageFile = BehaviorSubject<File>();
  final _imageUploadTask = PublishSubject<StorageUploadTask>();
  final _templateUploadStatus = PublishSubject<TemplateUploadStatus>();
  final _imageUploadProgress = PublishSubject<double>();

  int runCount = 0;
  int searchRunCount = 0;
  int docLimit = 10;
  bool hasMore = true;
  bool hasMoreSearch = true;
  bool isSearchingTemplates = false;

  String templateName, categoryName, subCategoryName;

  List<TemplateCategory> templateCategories = [];
  List<Template> templates = [];
  List<Template> searchedTemplates = [];

  Stream<List<TemplateCategory>> get categories => _categories.stream;

  Function(List<TemplateCategory>) get setCategories => _categories.sink.add;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreTemplates => _hasMore.stream;

  Function(bool) get setHasMoreTemplates => _hasMore.sink.add;

  Stream<List<Template>> get templatesList => _templatesList.stream;

  Function(List<Template>) get setTemplatesList => _templatesList.sink.add;

  Stream<bool> get isSearching => _isSearching.stream;

  Function(bool) get setIsSearching => _isSearching.sink.add;

  Stream<File> get imageFile => _imageFile.stream;

  Function(File) get setImageFile => _imageFile.sink.add;

  Stream<StorageUploadTask> get imageUploadTask => _imageUploadTask.stream;

  Function(StorageUploadTask) get setImageUploadTask =>
      _imageUploadTask.sink.add;

  Stream<TemplateUploadStatus> get templateUploadStatus =>
      _templateUploadStatus.stream;

  Function(TemplateUploadStatus) get setTemplateUploadStatus =>
      _templateUploadStatus.sink.add;

  Stream<double> get imageUploadProgress => _imageUploadProgress.stream;

  Function(double) get setImageUploadProgress => _imageUploadProgress.sink.add;

  getCategories() async {
    try {
      String country = await repository.getUserCountry();
      if (country == null || country == "") {
        IP ip = await repository.fetchIPDetails();
        country = ip.countryCodeIso3;
        await repository.setUserCountry(ip.countryCodeIso3);
      }
      templateCategories = await repository.getTemplateCategories(country);

      List<TemplateCategory> allTemplateCategories =
          await repository.getAllTemplateCategories();

      allTemplateCategories.forEach((category) {
        templateCategories.add(category);
      });

      LoggerUtil.log("Templates Bloc", "get categories",
          "received: " + templateCategories.length.toString());

      setCategories(templateCategories);
    } catch (e) {
      templateCategories = await repository.getAllTemplateCategories();

      LoggerUtil.log("Templates Bloc", "get categories",
          "received: " + templateCategories.length.toString());

      setCategories(templateCategories);

      Crashlytics.instance.log(e.toString());
    }
  }

  searchCategories(String searchTerm) {
    if (searchTerm == "") {
      setCategories(templateCategories);
      return;
    }

    List<TemplateCategory> newTemplateCategories = templateCategories
        .where((category) => category.templateCategoryName
            .toLowerCase()
            .contains(searchTerm.toLowerCase()))
        .toList();

    setCategories(newTemplateCategories);

    repository.firebaseAnalytics.logSearch(
        searchTerm: searchTerm,
        origin: FirebaseAnalyticsConstants.template_category);
  }

  searchTemplates(String searchTerm, {bool firstRun = true}) async {
    if (searchTerm == "") {
      setTemplatesList(templates);
      setIsSearching(false);
      isSearchingTemplates = false;
      setHasMoreTemplates(hasMore);
      return;
    }

    hasMoreSearch = true;
    setHasMoreTemplates(true);
    setLoading(true);
    isSearchingTemplates = true;
    setIsSearching(true);

    if (firstRun) {
      searchRunCount = 0;
      searchedTemplates.clear();
    }

    List<Template> newTemplates = [];

    if (searchRunCount == 0) {
      newTemplates = await repository.getInitialSearchTemplates(searchTerm);
    } else {
      newTemplates = await repository.getMoreSearchTemplates(searchTerm);
    }

    LoggerUtil.log(
        "Templates Bloc",
        "search templates",
        "found : " +
            newTemplates.length.toString() +
            " search term : " +
            searchTerm);

    newTemplates.forEach((template) {
      searchedTemplates.add(template);
    });

    /* without this if backspace is triggered fast, is searching is set off,
     but the request is already made and hence will populate the templates with
      last search query*/
    if (isSearchingTemplates) {
      setTemplatesList(searchedTemplates);
      if (newTemplates.length < 0) {
        setHasMoreTemplates(false);
        hasMoreSearch = false;
      }
      searchRunCount++;
    }

    setLoading(false);

    repository.firebaseAnalytics.logSearch(
        searchTerm: searchTerm, origin: FirebaseAnalyticsConstants.template);
  }

  Future<bool> saveImageWithoutWatermark(Template template) async {
    final file =
        await DefaultCacheManager().getSingleFile(template.templatePic);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.template_download,
        parameters: {FirebaseAnalyticsConstants.is_without_watermark: true});

    String randomName = generateRandomName();

    return ImageSaver().saveImage(
        imageBytes: file.readAsBytesSync(),
        imageName: randomName + ".png",
        directoryName: "Memecist");
  }

  saveImageWithWatermark(GlobalKey key) {
    return new Future.delayed(const Duration(milliseconds: 20), () async {
      try {
        RenderRepaintBoundary boundary = key.currentContext.findRenderObject();
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);
        ByteData byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);

        repository.firebaseAnalytics.logEvent(
            name: FirebaseAnalyticsConstants.template_download,
            parameters: {
              FirebaseAnalyticsConstants.is_without_watermark: false
            });

        String randomName = generateRandomName();

        return ImageSaver().saveImage(
            imageBytes: byteData.buffer.asUint8List(),
            imageName: randomName + ".png",
            directoryName: "Memecist");
      } catch (e) {
        print(e);
        return false;
      }
    });
  }

  generateRandomName() {
    var rand = new Random();
    var codeUnits = new List.generate(20, (index) {
      return rand.nextInt(33) + 89;
    });
    return new String.fromCharCodes(codeUnits);
  }

  getTemplates(String categoryID, {bool isNewCategory = false}) async {
    setLoading(true);

    if (isNewCategory) {
      runCount = 0;
      hasMore = true;
      setHasMoreTemplates(true);
      templates.clear();
      setTemplatesList(null);
    }

    List<Template> newTemplates = [];

    LoggerUtil.log(
        "Templates Bloc", "fetch templates", "run : " + runCount.toString());

    if (runCount == 0) {
      newTemplates = await repository.getInitialMemeTemplatesByCategory(
          categoryID, docLimit);
    } else {
      newTemplates =
          await repository.getMoreMemeTemplatesByCategory(categoryID, docLimit);
    }

    if (newTemplates.length < docLimit) {
      LoggerUtil.log("Templates Bloc", "fetch templates", "has more is false");
      hasMore = false;
      setHasMoreTemplates(false);
    }

    runCount++;
    LoggerUtil.log("Templates Bloc", "fetch templates",
        "new added : " + newTemplates.length.toString());

    for (Template template in newTemplates) templates.add(template);

    setTemplatesList(templates);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.templates_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  contributeTemplate(StorageReference storageReference) async {
    String url = await storageReference.getDownloadURL();
    User user = await repository.getLoggedInUser();
    MemecistUser memecistUser = await repository.getMemecistUser(user.uid);

    await repository.uploadTemplate(Template(
        categoryName, templateName, user.uid, memecistUser.userName, url));

    setTemplateUploadStatus(TemplateUploadStatus.Complete);
  }

  uploadTemplate(File imageFile, String categoryName, String subCategoryName,
      String templateName) async {
    this.templateName = templateName;
    this.categoryName = categoryName;
    this.subCategoryName = subCategoryName;

    User user = await repository.getLoggedInUser();

    setImageUploadTask(repository.uploadTemplateImage(imageFile, user.uid));
  }

  TemplatesBloc() {
    _imageUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        LoggerUtil.log(
            "Templates Bloc", "initializer", "image upload task started");

        imageUploadTask.events.listen((eventReceived) {
          if (imageUploadTask.isComplete) {
            LoggerUtil.log(
                "Templates Bloc", "initializer", "image upload task complete");

            contributeTemplate(eventReceived.snapshot.ref);
          } else {
            setTemplateUploadStatus(TemplateUploadStatus.Uploading);

            LoggerUtil.log("Templates Bloc", "initializer",
                "image upload task event received");

            var event = eventReceived.snapshot;

            double progressPercent = event != null
                ? event.bytesTransferred / event.totalByteCount
                : 0;

            setImageUploadProgress(progressPercent);
          }
        });
      }
    });
  }

  void dispose() async {
    await _categories.drain();
    _categories.close();

    await _templatesList.drain();
    _templatesList.close();

    await _isLoading.drain();
    _isLoading.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isSearching.drain();
    _isSearching.close();

    await _imageFile.drain();
    _imageFile.close();

    await _imageUploadTask.drain();
    _imageUploadTask.close();

    await _templateUploadStatus.drain();
    _templateUploadStatus.close();

    await _imageUploadProgress.drain();
    _imageUploadProgress.close();
  }
}
