import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/notifications/notifications_bloc.dart';

class NotificationsBlocProvider extends InheritedWidget {
  final NotificationsBloc notificationsBloc;

  NotificationsBlocProvider({Key key, Widget child})
      : notificationsBloc = NotificationsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static NotificationsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<NotificationsBlocProvider>()
        .notificationsBloc;
  }
}
