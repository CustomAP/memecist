import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/notification_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class NotificationsBloc {
  final _notificationsList = PublishSubject<List<MemecistNotification>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();

  List<MemecistNotification> notifications = [];

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreNotifications => _hasMore.stream;

  Function(bool) get setHasMoreNotifications => _hasMore.sink.add;

  Stream<List<MemecistNotification>> get notificationsList =>
      _notificationsList.stream;

  Function(List<MemecistNotification>) get setNotificationsList =>
      _notificationsList.sink.add;

  void initializeNotificationsBloc() async {
    User user = await repository.getLoggedInUser();
    repository.updateLastOpenedNotificationTime(user.uid);
  }

  Future<void> fetchNotifications() async {
    setLoading(true);
    List<MemecistNotification> newNotification = [];

    User user = await repository.getLoggedInUser();

    if (runCount == 0) {
      LoggerUtil.log("Notifications Bloc", "fetch notifications", "first run");
      newNotification =
          await repository.getInitialNotifications(user.uid, docLimit);
    } else {
      LoggerUtil.log("Notifications Bloc", "fetch notifications",
          "run : " + runCount.toString());
      newNotification =
          await repository.getMoreNotifications(user.uid, docLimit);
    }
    if (newNotification.length < docLimit) {
      LoggerUtil.log(
          "Notifications Bloc", "fetch notifications", "has more is false");
      hasMore = false;
      setHasMoreNotifications(false);
    }
    runCount++;
    LoggerUtil.log("Notifications Bloc", "fetch notifications",
        "new added : " + newNotification.length.toString());

    for (MemecistNotification notification in newNotification)
      notifications.add(notification);

    setNotificationsList(notifications);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.notifications_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  void dispose() async {
    await _notificationsList.drain();
    _notificationsList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();
  }
}
