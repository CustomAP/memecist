import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class SearchBloc {
  final _usersList = PublishSubject<List<MemecistUser>>();
  final _memesList = PublishSubject<List<MemePost>>();
  final _isLoadingMemes = BehaviorSubject<bool>();
  final _hasMoreMemes = BehaviorSubject<bool>();
  final _groupsList = PublishSubject<List<Group>>();

  int docLimit = 10;
  int runCount = 0;

  List<MemePost> memePosts = [];

  Stream<List<MemecistUser>> get usersList => _usersList.stream;

  Function(List<MemecistUser>) get setUsersList => _usersList.sink.add;

  Stream<List<MemePost>> get memesList => _memesList.stream;

  Function(List<MemePost>) get setMemesList => _memesList.sink.add;

  Stream<bool> get isLoadingMemes => _isLoadingMemes.stream;

  Function(bool) get setIsLoadingMemes => _isLoadingMemes.sink.add;

  Stream<bool> get hasMoreMemes => _hasMoreMemes.stream;

  Function(bool) get setHasMoreMemes => _hasMoreMemes.sink.add;

  Stream<List<Group>> get groupsList => _groupsList.stream;

  Function(List<Group>) get setGroupsList => _groupsList.sink.add;

  searchUsers(String query) async {
    if (query.length < 3) {
      setUsersList(null);
      return;
    }

    LoggerUtil.log("Search Bloc", "search users", "query: " + query);

    List<MemecistUser> users = await repository.searchUsers(query);

    repository.firebaseAnalytics
        .logSearch(searchTerm: query, origin: FirebaseAnalyticsConstants.user);

    setUsersList(users);
  }

  searchMemes(String query, bool refresh) async {
    if (refresh) {
      memePosts.clear();
      runCount = 0;
    }

    if(query.length < 3){
      return;
    }

    setIsLoadingMemes(true);
    LoggerUtil.log("Search Bloc", "search memes", "query: " + query);
    List<MemePost> newMemes = [];

    User user = await repository.getLoggedInUser();

    if (runCount == 0) {
      newMemes = await repository.getInitialMemePostsForSearch(
          docLimit, user.uid, query);
    } else {
      newMemes =
          await repository.getMoreMemePostsForSearch(docLimit, user.uid, query);
    }

    if (newMemes.length < docLimit) {
      LoggerUtil.log("Search Bloc", "search memes", "has more is false");
      setHasMoreMemes(false);
    }
    runCount++;
    LoggerUtil.log("Search Bloc", "search memes",
        "new added : " + newMemes.length.toString());

    for (MemePost memePost in newMemes) memePosts.add(memePost);

    setMemesList(memePosts);
    setIsLoadingMemes(false);

    repository.firebaseAnalytics.logSearch(
        searchTerm: query, origin: FirebaseAnalyticsConstants.meme_post);
  }

  searchGroups(String query) async {
    if (query.length < 3) {
      setGroupsList(null);
      return;
    }

    LoggerUtil.log("Search Bloc", "search groups", "query: " + query);

    List<Group> groups = await repository.searchGroups(query);

    repository.firebaseAnalytics
        .logSearch(searchTerm: query, origin: FirebaseAnalyticsConstants.group);

    setGroupsList(groups);
  }

  deletePost(MemePost memePost) async {
    repository.deletePost(memePost.postID);
    for (MemePost post in memePosts) {
      if (post.postID == memePost.postID) {
        memePosts.remove(post);
        break;
      }
    }
    setMemesList(memePosts);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post);
  }

  void dispose() async {
    await _usersList.drain();
    _usersList.close();

    await _memesList.drain();
    _memesList.close();

    await _isLoadingMemes.drain();
    _isLoadingMemes.close();

    await _hasMoreMemes.drain();
    _hasMoreMemes.close();

    await _groupsList.drain();
    _groupsList.close();
  }
}
