import 'package:memecist/core/blocs/explore/hashtag_posts_bloc.dart';
import 'package:flutter/material.dart';

class HashtagPostsBlocProvider extends InheritedWidget {
  final HashtagPostsBloc hashtagPostsBloc;

  HashtagPostsBlocProvider({Key key, Widget child})
      : hashtagPostsBloc = HashtagPostsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static HashtagPostsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<HashtagPostsBlocProvider>()
        .hashtagPostsBloc;
  }
}
