import 'package:memecist/core/blocs/explore/explore_bloc.dart';
import 'package:flutter/material.dart';

class ExploreBlocProvider extends InheritedWidget {
  final exploreBloc;

  ExploreBlocProvider({Key key, Widget child})
      : exploreBloc = ExploreBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ExploreBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ExploreBlocProvider>()
        .exploreBloc;
  }
}
