import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/explore/search_bloc.dart';

class SearchBlocProvider extends InheritedWidget {
  final SearchBloc searchBloc;

  SearchBlocProvider({Key key, Widget child})
      : searchBloc = SearchBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static SearchBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<SearchBlocProvider>()
        .searchBloc;
  }
}
