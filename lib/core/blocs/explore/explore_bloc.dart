import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/utils/logger.dart';

class ExploreBloc {
  List<BehaviorSubject<List<MemePost>>> _memesList = [];
  List<BehaviorSubject<bool>> _hasMore = [];
  List<BehaviorSubject<bool>> _isLoadingMemes = [];

  int docLimit = 20;
  List<int> runCount = [];

  List<String> _categories = [];

  List<List<MemePost>> memePostsLists = [];

  List<Stream<bool>> hasMorePosts = [];

  List<Function(bool)> setHasMorePosts = [];

  List<Stream<List<MemePost>>> memeLists = [];
  List<Function(List<MemePost>)> setMemeList = [];

  List<Stream<bool>> isLoadingMemes = [];

  List<Function(bool)> setIsLoadingMemes = [];

  void initializeExploreBloc(List<String> categories) {
    _categories = categories;

    categories.forEach((category) {
      BehaviorSubject<List<MemePost>> memeCategoryBehaviorSubject =
          BehaviorSubject<List<MemePost>>();
      BehaviorSubject<bool> memeCategoryHasMorePosts = BehaviorSubject<bool>();
      BehaviorSubject<bool> memeCategoryIsLoading = BehaviorSubject<bool>();
      _memesList.add(memeCategoryBehaviorSubject);
      memeLists.add(memeCategoryBehaviorSubject.stream);
      setMemeList.add(memeCategoryBehaviorSubject.sink.add);

      _hasMore.add(memeCategoryHasMorePosts);
      hasMorePosts.add(memeCategoryHasMorePosts.stream);
      setHasMorePosts.add(memeCategoryHasMorePosts.sink.add);

      _isLoadingMemes.add(memeCategoryIsLoading);
      isLoadingMemes.add(memeCategoryIsLoading.stream);
      setIsLoadingMemes.add(memeCategoryIsLoading.sink.add);

      memePostsLists.add([]);

      runCount.add(0);
    });
  }

  Future<void> getFeed(int index) async {
    setIsLoadingMemes[index](true);
    runCount[index]++;

    LoggerUtil.log("Explore Bloc", "get feed", "run: " + runCount.toString());

    String country = await repository.getUserCountry();

    User user = await repository.getLoggedInUser();
    List<MemePost> memePosts = await repository.getExploreFeed(
        runCount[index], user.uid, _categories[index], country);

    memePosts.forEach((memePost) {
      memePostsLists[index].add(memePost);
    });

    LoggerUtil.log(
        "Explore Bloc", "get feed", "received: " + memePosts.length.toString());

    if (memePosts.length == 0) {
      LoggerUtil.log("Explore Bloc", "get feed", "has more is false");
      setHasMorePosts[index](false);
    }

    setMemeList[index](memePostsLists[index]);
    setIsLoadingMemes[index](false);

    String category = _categories[index];

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.explore_feed_fetch,
        parameters: {
          FirebaseAnalyticsConstants.run_count: runCount[index],
          FirebaseAnalyticsConstants.category: category
        });
  }

  void refreshScreen(int index) {
    runCount[index] = 0;
    memePostsLists[index].clear();
    setMemeList[index](memePostsLists[index]);
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.refresh_screen,
        parameters: {
          FirebaseAnalyticsConstants.category: _categories[index],
        });
  }

  deletePost(MemePost memePost, int tabIndex) async {
    repository.deletePost(memePost.postID);
    for (MemePost post in memePostsLists[tabIndex]) {
      if (post.postID == memePost.postID) {
        memePostsLists[tabIndex].remove(post);
        break;
      }
    }
    setMemeList[tabIndex](memePostsLists[tabIndex]);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post);
  }

  void dispose() async {
    _memesList.forEach((memeList) async {
      await memeList.drain();
      memeList.close();
    });

    _hasMore.forEach((hasMore) async {
      await hasMore.drain();
      hasMore.close();
    });

    _isLoadingMemes.forEach((isLoadingMemes) async {
      await isLoadingMemes.drain();
      isLoadingMemes.close();
    });
  }
}
