import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class HashtagPostsBloc {
  final _memesList = PublishSubject<List<MemePost>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();

  List<MemePost> memePosts = [];

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreMemes => _hasMore.stream;

  Function(bool) get setHasMoreMemes => _hasMore.sink.add;

  Stream<List<MemePost>> get memesList => _memesList.stream;

  Function(List<MemePost>) get setMemesList => _memesList.sink.add;

  Future<void> fetchMemes(String hashtag) async {
    hashtag = hashtag.substring(1);
    setLoading(true);
    List<MemePost> newMemePosts = [];

    User user = await repository.getLoggedInUser();

    if (runCount == 0) {
      LoggerUtil.log("Hashtag PostsBloc", "fetch memes", "first run");
      newMemePosts = await repository.getInitialMemePostsByHashtag(
          docLimit, hashtag, user.uid);
    } else {
      LoggerUtil.log(
          "Hashtag PostsBloc", "fetch memes", "run : " + runCount.toString());
      newMemePosts = await repository.getMoreMemePostsByHashtag(
          docLimit, hashtag, user.uid);
    }
    if (newMemePosts.length < docLimit) {
      LoggerUtil.log("Hashtag PostsBloc", "fetch memes", "has more is false");
      hasMore = false;
      setHasMoreMemes(false);
    }
    runCount++;
    LoggerUtil.log("Hashtag PostsBloc", "fetch memes",
        "new added : " + newMemePosts.length.toString());

    for (MemePost memePost in newMemePosts) memePosts.add(memePost);

    setMemesList(memePosts);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.fetch_hashtag_memes,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  deletePost(MemePost memePost) async {
    repository.deletePost(memePost.postID);
    for (MemePost post in memePosts) {
      if (post.postID == memePost.postID) {
        memePosts.remove(post);
        break;
      }
    }
    setMemesList(memePosts);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post, parameters: {
      FirebaseAnalyticsConstants.source:
      FirebaseAnalyticsConstants.hashtag_posts_screen,
    });
  }

  void dispose() async {
    await _memesList.drain();
    _memesList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();
  }
}
