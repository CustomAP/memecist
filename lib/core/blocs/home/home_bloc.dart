import 'dart:io';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_appavailability/flutter_appavailability.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/memecist_remote_config.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'dart:ui' as ui;

enum Cards { shareUserName, None }

class HomeBloc {
  final _initialRun = PublishSubject<bool>();
  final _followSuggestions = PublishSubject<List<MemecistUser>>();
  final _groupSuggestions = PublishSubject<List<Group>>();
  final _user = BehaviorSubject<MemecistUser>();
  final _memePosts = BehaviorSubject<List<MemePost>>();
  final _competitions = BehaviorSubject<List<Competition>>();
  final _groups = BehaviorSubject<List<Group>>();
  final _hasMoreMemes = BehaviorSubject<bool>();
  final _isLoadingMemes = BehaviorSubject<bool>();
  final _hasMoreGroupsSuggestions = BehaviorSubject<bool>();
  final _isLoadingGroupsSuggestions = BehaviorSubject<bool>();
  final _hasMoreFollowSuggestions = BehaviorSubject<bool>();
  final _isLoadingFollowSuggestions = BehaviorSubject<bool>();
  final _hasNewNotifications = BehaviorSubject<bool>();
  final _appRunCount = PublishSubject<int>();
  final _isRateAppDialogShown = BehaviorSubject<bool>();
  final _isSuggestionsDialogShown = BehaviorSubject<bool>();

  bool _isInitialRunDone = false;

  int runCountForMemes = 0;
  int docLimitForMemes = 20;

  int runCountForGroupsSuggestions = 0;
  int runCountForFollowSuggestions = 0;

  List<MemePost> memePostsList = [];
  List<Group> groupsList = [];
  List<MemecistUser> usersList = [];

  Stream<bool> get isInitialRunDone => _initialRun.stream;

  Function(bool) get setIsInitialRunDone => _initialRun.sink.add;

  Stream<List<MemecistUser>> get followSuggestions => _followSuggestions.stream;

  Function(List<MemecistUser>) get setFollowSuggestions =>
      _followSuggestions.sink.add;

  Stream<List<Group>> get groupSuggestions => _groupSuggestions.stream;

  Function(List<Group>) get setGroupSuggestions => _groupSuggestions.sink.add;

  Stream<MemecistUser> get user => _user.stream;

  Function(MemecistUser) get setUser => _user.sink.add;

  Stream<List<MemePost>> get memePosts => _memePosts.stream;

  Function(List<MemePost>) get setMemePosts => _memePosts.sink.add;

  Stream<List<Competition>> get competitions => _competitions.stream;

  Function(List<Competition>) get setCompetitions => _competitions.sink.add;

  Stream<List<Group>> get groups => _groups.stream;

  Function(List<Group>) get setGroups => _groups.sink.add;

  Stream<bool> get isLoadingMemes => _isLoadingMemes.stream;

  Function(bool) get setIsLoadingMemes => _isLoadingMemes.sink.add;

  Stream<bool> get hasMoreMemes => _hasMoreMemes.stream;

  Function(bool) get setHasMoreMemes => _hasMoreMemes.sink.add;

  Stream<bool> get isLoadingGroupsSuggestions =>
      _isLoadingGroupsSuggestions.stream;

  Function(bool) get setIsLoadingGroupsSuggestions =>
      _isLoadingGroupsSuggestions.sink.add;

  Stream<bool> get hasMoreGroupsSuggestions => _hasMoreGroupsSuggestions.stream;

  Function(bool) get setHasMoreGroupsSuggestions =>
      _hasMoreGroupsSuggestions.sink.add;

  Stream<bool> get isLoadingFollowSuggestions =>
      _isLoadingFollowSuggestions.stream;

  Function(bool) get setIsLoadingFollowSuggestions =>
      _isLoadingFollowSuggestions.sink.add;

  Stream<bool> get hasMoreFollowSuggestions => _hasMoreFollowSuggestions.stream;

  Function(bool) get setHasMoreFollowSuggestions =>
      _hasMoreFollowSuggestions.sink.add;

  Stream<bool> get hasNewNotifications => _hasNewNotifications.stream;

  Function(bool) get setHasNewNotifications => _hasNewNotifications.sink.add;

  Stream<int> get appRunCount => _appRunCount.stream;

  Function(int) get setAppRunCount => _appRunCount.sink.add;

  Stream<bool> get isRateAppDialogShown => _isRateAppDialogShown.stream;

  Function(bool) get setIsRateAppDialogShown => _isRateAppDialogShown.sink.add;

  Stream<bool> get isSuggestionsDialogShown => _isSuggestionsDialogShown.stream;

  Function(bool) get setIsSuggestionsDialogShown =>
      _isSuggestionsDialogShown.sink.add;

  initializeHomeBloc() async {
    User currentUser = await repository.getLoggedInUser();

    if (await repository.getIsInitialRunDone()) {
      setIsInitialRunDone(true);
      if (!await repository.getCanDownloadTemplateWithoutWatermark()) {
        getUserPrivateInfo();
      }
    } else {
      getUserPrivateInfo();
    }

    repository.getUserStream(currentUser.uid).listen((user) {
      setUser(user);
    });

    repository
        .getHasNewNotificationsStream(currentUser.uid)
        .listen((hasNewNotif) {
      setHasNewNotifications(hasNewNotif);
    });

    getCompetitions();
    LoggerUtil.log("Home Bloc", "Initialize", "userID: " + currentUser.uid);
  }

  getIsRateAppDialogShown() async {
    setIsRateAppDialogShown(await repository.getIsAppRateDialogShown());
  }

  getIsSuggestionsDialogShown() async {
    setIsSuggestionsDialogShown(await repository.getIsSuggestionsDialogShown());
  }

  updateIsRateAppDialogShown() async {
    repository.setIsAppRateDialogShown(true);
  }

  updateIsSuggestionsDialogShown() async {
    repository.setIsSuggestionsDialogShown(true);
  }

  getUserPrivateInfo() async {
    User user = await repository.getLoggedInUser();
    Map privateInfo = await repository.getAccountPrivateInfo(user.uid);
    setIsInitialRunDone(privateInfo[FirestoreConstants.is_first_run_completed]);
    await repository.setIsInitialRunDone(
        privateInfo[FirestoreConstants.is_first_run_completed]);

    await repository.setCanDownloadTemplateWithoutWatermark(privateInfo[
        FirestoreConstants.can_download_template_without_watermark]);

    LoggerUtil.log(
        "Home Bloc", "Get Initial Run", _isInitialRunDone.toString());
  }

  setInitialRunDone() async {
    if (!_isInitialRunDone) {
      User user = await repository.getLoggedInUser();
      await repository.setInitialRunDone(user.uid);
      LoggerUtil.log("Home Bloc", "Set Initial Run", "done");

      repository.firebaseAnalytics
          .logEvent(name: FirebaseAnalyticsConstants.initial_run_complete);
      _isInitialRunDone = true;
    }
  }

  getFollowSuggestions() async {
    setIsLoadingFollowSuggestions(true);
    runCountForFollowSuggestions++;

    String country = await repository.getUserCountry();

    List<MemecistUser> users = await repository.getFollowSuggestions(
        runCountForFollowSuggestions, country);

    if (users.length == 0) setHasMoreFollowSuggestions(false);

    users.forEach((user) {
      usersList.add(user);
    });

    LoggerUtil.log("Home Bloc", "get follow suggestions",
        "total: " + users.length.toString());

    setFollowSuggestions(usersList);
    setIsLoadingFollowSuggestions(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.follow_suggestions_fetch,
        parameters: {
          FirebaseAnalyticsConstants.run_count: runCountForFollowSuggestions
        });
  }

  getGroupSuggestions() async {
    setIsLoadingGroupsSuggestions(true);
    runCountForGroupsSuggestions++;

    String country = await repository.getUserCountry();

    List<Group> groups = await repository.getGroupSuggestions(
        runCountForGroupsSuggestions, country);

    if (groups.length == 0) setHasMoreGroupsSuggestions(false);

    groups.forEach((group) {
      groupsList.add(group);
    });

    LoggerUtil.log("Home Bloc", "get group suggestions",
        "total:" + groups.length.toString());

    setGroupSuggestions(groupsList);
    setIsLoadingGroupsSuggestions(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.group_suggestions_fetch,
        parameters: {
          FirebaseAnalyticsConstants.run_count: runCountForGroupsSuggestions
        });
  }

  getFeed() async {
    setIsLoadingMemes(true);
    runCountForMemes++;

    LoggerUtil.log(
        "Home Bloc", "get feed", "run: " + runCountForMemes.toString());

    User user = await repository.getLoggedInUser();
    List<MemePost> memePosts =
        await repository.getFeed(runCountForMemes, user.uid);

    memePosts.forEach((memePost) {
      memePostsList.add(memePost);
    });

    LoggerUtil.log(
        "Home Bloc", "get feed", "received: " + memePosts.length.toString());

    if (memePosts.length == 0) {
      LoggerUtil.log("Home Bloc", "get feed", "has more is false");
      setHasMoreMemes(false);
    }

    memePostsList.sort((a, b) => b.createDateTime.compareTo(a.createDateTime));

    setMemePosts(memePostsList);
    setIsLoadingMemes(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.feed_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCountForMemes});
  }

  shareUserNameCard(GlobalKey globalKey) async {
    User user = await repository.getLoggedInUser();
    try {
      return new Future.delayed(const Duration(milliseconds: 20), () async {
        RenderRepaintBoundary boundary =
            globalKey.currentContext.findRenderObject();
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);
        ByteData byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);

        String link =
            (await repository.createUserDynamicLink(user.uid)).toString();
        LoggerUtil.log("Home Bloc", "Share image", "link : $link");

        repository.firebaseAnalytics.logShare(
            contentType: FirebaseAnalyticsConstants.user_name_card,
            itemId: user.uid,
            method: FirebaseAnalyticsConstants.user_name_share_card);

        await Share.file(
            'Share', 'post.png', byteData.buffer.asUint8List(), 'image/png',
            text: 'We are now on Memecist!\nFollow us: $link');
      });
    } catch (e) {
      print(e);
    }
  }

  getCompetitions() async {
    String country = await repository.getUserCountry();
    List<Competition> competitions =
        await repository.getActiveCompetitions(country);
    setCompetitions(competitions);
    LoggerUtil.log("Home Bloc", "get competitions",
        "total : " + competitions.length.toString());

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.competitions_fetch);
  }

  getWars() async {
    User user = await repository.getLoggedInUser();
    List<Group> groups = await repository.getGroupsWithActiveWars(user.uid);
    setGroups(groups);
    LoggerUtil.log(
        "Home Bloc", "get wars", "total : " + groups.length.toString());

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.groups_w_active_wars_fetch,
        parameters: {
          FirebaseAnalyticsConstants.total: groups.length,
        });
  }

  refreshScreen(int appRunCount) async {
    runCountForGroupsSuggestions = 0;
    runCountForFollowSuggestions = 0;
    runCountForMemes = 0;

    memePostsList.clear();
    groupsList.clear();
    usersList.clear();

    _isInitialRunDone = false;
    setFollowSuggestions(null);
    setGroupSuggestions(null);

    setAppRunCount(appRunCount);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.refresh_screen);
  }

  updateRunCount(MemecistRemoteConfig memecistRemoteConfig) async {
    try {
      int runCount = await repository.getApplicationRunCount();
      setAppRunCount(runCount);
      if (memecistRemoteConfig.userFollowersCountUpdateAtRun == 0) {
        User user = await repository.getLoggedInUser();
        await repository.updateFollowersCount(user.uid);
      } else if (runCount %
              memecistRemoteConfig.userFollowersCountUpdateAtRun ==
          0) {
        User user = await repository.getLoggedInUser();

        repository.firebaseAnalytics
            .logEvent(name: FirebaseAnalyticsConstants.update_followers_count);
        await repository.updateFollowersCount(user.uid);
      }

      if (runCount % memecistRemoteConfig.appAvailabilityUpdateAtRun == 0) {
        if (Platform.isAndroid) {
          List<Map<String, String>> installedApps =
              await AppAvailability.getInstalledApps();

          User user = await repository.getLoggedInUser();

          repository.firebaseAnalytics.logEvent(
              name: FirebaseAnalyticsConstants.update_app_availability);
          await repository.setAppsList(user.uid, installedApps);
        }
      }

      runCount++;
      repository.setApplicationRunCount(runCount);

      await repository.setToUpdateGroupMembersCountAtRun(
          memecistRemoteConfig.groupMembersCountUpdateAtRun);
    } catch (e) {
      print(e);
    }
  }

  deletePost(MemePost memePost) async {
    repository.deletePost(memePost.postID);
    for (MemePost post in memePostsList) {
      if (post.postID == memePost.postID) {
        memePostsList.remove(post);
        break;
      }
    }
    setMemePosts(memePostsList);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post);
  }

  void dispose() async {
    await _initialRun.drain();
    _initialRun.close();

    await _followSuggestions.drain();
    _followSuggestions.close();

    await _groupSuggestions.drain();
    _groupSuggestions.close();

    await _user.drain();
    _user.close();

    await _memePosts.drain();
    _memePosts.close();

    await _competitions.drain();
    _competitions.close();

    await _groups.drain();
    _groups.close();

    await _hasMoreMemes.drain();
    _hasMoreMemes.close();

    await _isLoadingMemes.drain();
    _isLoadingMemes.close();

    await _hasMoreGroupsSuggestions.drain();
    _hasMoreGroupsSuggestions.close();

    await _isLoadingGroupsSuggestions.drain();
    _isLoadingGroupsSuggestions.close();

    await _hasMoreFollowSuggestions.drain();
    _hasMoreFollowSuggestions.close();

    await _isLoadingFollowSuggestions.drain();
    _isLoadingFollowSuggestions.close();

    await _hasNewNotifications.drain();
    _hasNewNotifications.close();

    await _appRunCount.drain();
    _appRunCount.close();

    await _isRateAppDialogShown.drain();
    _isRateAppDialogShown.close();

    await _isSuggestionsDialogShown.drain();
    _isSuggestionsDialogShown.close();
  }
}
