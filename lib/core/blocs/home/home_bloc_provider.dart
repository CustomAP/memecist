import 'package:memecist/core/blocs/home/home_bloc.dart';
import 'package:flutter/material.dart';

class HomeBlocProvider extends InheritedWidget {
  final homeBloc;

  HomeBlocProvider({Key key, Widget child})
      : homeBloc = HomeBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static HomeBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<HomeBlocProvider>()
        .homeBloc;
  }
}
