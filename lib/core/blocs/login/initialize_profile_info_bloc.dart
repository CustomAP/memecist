import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/keywords_generator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/utils/logger.dart';

enum InitializeProfileInfoStates {
  RequestSent,
  RequestSuccess,
  RequestFailed,
  UserNameTaken
}

class InitializeProfileInfoBloc {
  final _birthDate = BehaviorSubject<String>();
  final _initializeProfileInfoStates =
      BehaviorSubject<InitializeProfileInfoStates>();

  int userNameTakenRetries = 0;

  Stream<InitializeProfileInfoStates> get initializeProfileInfoState =>
      _initializeProfileInfoStates.stream;

  Function(InitializeProfileInfoStates) get setInitializeProfileInfoState =>
      _initializeProfileInfoStates.sink.add;

  Stream<String> get birthDate => _birthDate.stream;

  Function(String date) get setBirthDate => _birthDate.sink.add;

  Future<bool> isUserNameTaken(String userName) async {
    return repository.isUserNameTaken(userName);
  }

  submitProfileInfo(MemecistUser user) async {
    LoggerUtil.log("Initialize profile info bloc", "submit profile info",
        "data() submitted");
    setInitializeProfileInfoState(InitializeProfileInfoStates.RequestSent);
    User currentUser = await repository.getLoggedInUser();

    bool _isUserNameTaken = await isUserNameTaken(user.userName);

    if (_isUserNameTaken) {
      userNameTakenRetries++;
      LoggerUtil.log("Initialize profile info bloc", "submit profile info",
          "username taken");
      setInitializeProfileInfoState(InitializeProfileInfoStates.UserNameTaken);
    } else {
      LoggerUtil.log("Initialize profile info bloc", "submit profile info",
          "username available");

      List<String> keywords = KeywordsGenerator.generateKeywordsForNames(
          user.firstName, user.lastName, user.userName);

      repository
          .updateProfileInfo(currentUser.uid, user, keywords)
          .then((value) {
        LoggerUtil.log(
            "Intialize profile info bloc", "submit profile info", "success");
        setInitializeProfileInfoState(
            InitializeProfileInfoStates.RequestSuccess);

        repository.firebaseAnalytics.logEvent(
            name: FirebaseAnalyticsConstants.initialize_profile_info,
            parameters: {
              FirebaseAnalyticsConstants.user_name_taken_retries:
                  userNameTakenRetries,
              FirebaseAnalyticsConstants.keyword_length: keywords.length
            });
      }).catchError((e) {
        setInitializeProfileInfoState(
            InitializeProfileInfoStates.RequestFailed);
        LoggerUtil.log("Initialize Profile info bloc", "submit profile info",
            "error: " + e.toString());
        throw e;
      });
    }
  }

  void dispose() async {
    await _birthDate.drain();
    _birthDate.close();

    await _initializeProfileInfoStates.drain();
    _initializeProfileInfoStates.close();
  }
}
