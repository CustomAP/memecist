import 'dart:io';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/android_device_info_model.dart';
import 'package:memecist/core/models/ios_device_info_model.dart';
import 'package:memecist/core/models/ip_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/models/country_model.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/utils/logger.dart';

enum PhoneAuthState {
  Started,
  CodeSent,
  CodeResent,
  Verified,
  Failed,
  Error,
  AutoRetrievalTimeout
}

class PhoneSignUpBloc {
  static String _actualCode, _phoneNumber;

  final _countryCodesList = BehaviorSubject<Future<List<Country>>>();
  final _selectedCountryIndex = PublishSubject<int>();
  final _countrySearchList = BehaviorSubject<List<Country>>();
  static final _phoneAuthState = PublishSubject<PhoneAuthState>();

  static Function(PhoneAuthState) setPhoneAuthState = _phoneAuthState.sink.add;
  Stream<PhoneAuthState> getPhoneAuthState = _phoneAuthState.stream;

  Function(int) get selectCountryIndex => _selectedCountryIndex.sink.add;

  Stream<int> get getSelectedCountryIndex => _selectedCountryIndex.stream;

  Stream<Future<List<Country>>> get countryCodesList =>
      _countryCodesList.stream;

  Stream<List<Country>> get getCountries => _countrySearchList.stream;

  Function(List<Country> countries) get addCountries =>
      _countrySearchList.sink.add;

  void fetchCountryCodesList(BuildContext context) async {
    _countryCodesList.sink.add(repository.loadCountriesJson(context));
  }

  startPhoneAuth(String phoneNumber) {
    assert(phoneNumber != null);
    _phoneNumber = phoneNumber;
    try {
      repository
          .startPhoneAuth(_phoneNumber, verificationCompleted,
              verificationFailed, codeSent, codeAutoRetrievalTimeout)
          .then((value) {
        setPhoneAuthState(PhoneAuthState.Started);
        LoggerUtil.log("Phone SignUp Bloc", "startPhoneAuth",
            "PhoneNumber : " + _phoneNumber);
      });
    } catch (exception) {
      LoggerUtil.log(
          "Phone SignUp Bloc", "startPhoneAuth", "exception : " + exception);
    }
  }

  PhoneCodeSent codeSent = (String verificationId, [int forceResendingToken]) {
    _actualCode = verificationId;
    LoggerUtil.log(
        "Phone Signup Bloc", "code sent", "verificationid: " + verificationId);
    setPhoneAuthState(PhoneAuthState.CodeSent);
  };

  PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
      (String verificationId) {
    _actualCode = verificationId;
    LoggerUtil.log("Phone Signup Bloc", "Code Auto Retrieval Timeout",
        "verificationid: " + verificationId);
    setPhoneAuthState(PhoneAuthState.AutoRetrievalTimeout);
  };

  PhoneVerificationFailed verificationFailed = (FirebaseAuthException authException) {
    setPhoneAuthState(PhoneAuthState.Error);
    LoggerUtil.log("Phone SignUp Bloc", "Phone verification failed",
        "error: " + authException.message);
  };

  PhoneVerificationCompleted verificationCompleted =
      (AuthCredential authCredential) {
    LoggerUtil.log("Phone SignUp Bloc", "Phone verification completed", "");
    repository.signInWithCredential(authCredential).then((UserCredential value) {
      if (value.user != null) {
        onAuthenticationSuccessful();
      } else {}
    });
  };

  void signInWithPhoneNumber(String smsCode) {
    LoggerUtil.log("Phone signup bloc", "sign in with phone number",
        "sms code : " + smsCode);
    repository
        .signInWithPhoneNumber(_actualCode, smsCode)
        .then((UserCredential value) {
      if (value.user != null) {
        onAuthenticationSuccessful();
      } else {
        _phoneAuthState.add(PhoneAuthState.Error);
      }
    }).catchError((error) {
      LoggerUtil.log("Phone signup bloc", "signin with phone number",
          "error: " + error.toString());
      _phoneAuthState.add(PhoneAuthState.Error);
    });
  }

  static onAuthenticationSuccessful() async {
    IP ipDetails = await repository.fetchIPDetails();

    await repository.getLoggedInUser().then((User firebaseUser) =>
        repository.initializeUser(firebaseUser, ipDetails).then((value) async {
          await repository.setUserCountry(ipDetails.countryCodeIso3);

          await repository.subscribeToAllGroups(firebaseUser);
          await repository.subscribeToAllWarRooms(firebaseUser);

          if (Platform.isAndroid) {
            MemecistAndroidDeviceInfo memecistAndroidDeviceInfo =
                await repository.getAndroidDeviceInfo();
            await repository.setAndroidDeviceInfo(
                firebaseUser.uid, memecistAndroidDeviceInfo);
          } else if (Platform.isIOS) {
            MemecistIOSDeviceInfo memecistIOSDeviceInfo =
                await repository.getIOSDeviceInfo();
            await repository.setIOSDeviceInfo(
                firebaseUser.uid, memecistIOSDeviceInfo);
          }

          Crashlytics.instance.setUserIdentifier(firebaseUser.uid);

          repository.firebaseAnalytics.logSignUp(
              signUpMethod: FirebaseAnalyticsConstants.phone_sign_in);
          repository.firebaseAnalytics.setUserId(firebaseUser.uid);
          repository.firebaseAnalytics.setUserProperty(
              name: FirebaseAnalyticsConstants.country,
              value: ipDetails.countryCodeIso3);

          setPhoneAuthState(PhoneAuthState.Verified);
        }));
  }

  Future<void> dispose() async {
    LoggerUtil.log("Phone signup bloc", "dispose", "disposing all streams");
    await _countryCodesList.drain();
    _countryCodesList.close();

    await _selectedCountryIndex.drain();
    _selectedCountryIndex.close();

    await _countrySearchList.drain();
    _countryCodesList.close();

    await _phoneAuthState.drain();
    _phoneAuthState.close();
  }
}
