import 'package:memecist/core/blocs/login/initialize_profile_info_bloc.dart';
import 'package:flutter/material.dart';

class InitializeProfileInfoBlocProvider extends InheritedWidget {

  final InitializeProfileInfoBloc initializeProfileInfoBloc;

  InitializeProfileInfoBlocProvider({Key key, Widget child})
      :
        initializeProfileInfoBloc = InitializeProfileInfoBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static InitializeProfileInfoBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<InitializeProfileInfoBlocProvider>()
        .initializeProfileInfoBloc;
  }

}