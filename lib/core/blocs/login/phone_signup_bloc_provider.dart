import 'package:memecist/core/blocs/login/phone_signup_bloc.dart';
import 'package:flutter/material.dart';

class PhoneSignUpBlocProvider extends InheritedWidget{

  final PhoneSignUpBloc phoneSignUpBloc;

  PhoneSignUpBlocProvider({Key key, Widget child}) : phoneSignUpBloc = PhoneSignUpBloc(),
  super(key:key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static PhoneSignUpBloc of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<PhoneSignUpBlocProvider>().phoneSignUpBloc;
  }

}