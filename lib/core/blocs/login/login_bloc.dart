import 'dart:io';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/android_device_info_model.dart';
import 'package:memecist/core/models/ios_device_info_model.dart';
import 'package:memecist/core/models/ip_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/utils/logger.dart';

enum LoginStates {
  GoogleSignInRequestSent,
  GoogleSignInRequestSuccess,
  GoogleSignInRequestFail
}

class LoginBloc {
  final _loginStates = BehaviorSubject<LoginStates>();

  Stream<LoginStates> get loginState => _loginStates.stream;

  Function(LoginStates) get setLoginState => _loginStates.sink.add;

  Future<void> googleSignIn() async {
    LoggerUtil.log("Login Bloc", "Google sign in", "signing in");
    setLoginState(LoginStates.GoogleSignInRequestSent);
    try {
      User firebaseUser = await repository.signIn();
      if (firebaseUser != null) {
        IP ipDetails = await repository.fetchIPDetails();

        await repository.getLoggedInUser().then((User firebaseUser) =>
            repository
                .initializeUser(firebaseUser, ipDetails)
                .then((value) async {
              await repository.setUserCountry(ipDetails.countryCodeIso3);

              await repository.subscribeToAllGroups(firebaseUser);
              await repository.subscribeToAllWarRooms(firebaseUser);

              if (Platform.isAndroid) {
                MemecistAndroidDeviceInfo memecistAndroidDeviceInfo =
                    await repository.getAndroidDeviceInfo();
                await repository.setAndroidDeviceInfo(
                    firebaseUser.uid, memecistAndroidDeviceInfo);
              } else if (Platform.isIOS) {
                MemecistIOSDeviceInfo memecistIOSDeviceInfo =
                    await repository.getIOSDeviceInfo();
                await repository.setIOSDeviceInfo(
                    firebaseUser.uid, memecistIOSDeviceInfo);
              }

              Crashlytics.instance.setUserIdentifier(firebaseUser.uid);

              repository.firebaseAnalytics.logSignUp(
                  signUpMethod: FirebaseAnalyticsConstants.google_sign_in);
              repository.firebaseAnalytics.setUserId(firebaseUser.uid);
              repository.firebaseAnalytics.setUserProperty(
                  name: FirebaseAnalyticsConstants.country,
                  value: ipDetails.countryCodeIso3);

              LoggerUtil.log("Login Bloc", "Google sign in", "Sign In success");
              setLoginState(LoginStates.GoogleSignInRequestSuccess);
            }));
      }
    } catch (e) {
      LoggerUtil.log("Login Bloc", "Google sign in", "error: " + e.toString());
      setLoginState(LoginStates.GoogleSignInRequestFail);
    }
  }

  Future<void> dispose() async {
    await _loginStates.drain();
    _loginStates.close();
  }
}
