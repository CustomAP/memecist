import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_tab_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/groups_tab_bloc.dart';

class CompetitionsTabBlocProvider extends InheritedWidget{
  final CompetitionsTabBloc competitionsTabBloc;

  CompetitionsTabBlocProvider({Key key, Widget child})
      : competitionsTabBloc = CompetitionsTabBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static CompetitionsTabBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<CompetitionsTabBlocProvider>()
        .competitionsTabBloc;
  }
}