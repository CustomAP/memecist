import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/competition_winner_group.dart';
import 'package:memecist/core/models/competition_winner_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:ui' as ui;

class CompetitionDetailsBloc {
  final _winnersList = BehaviorSubject<List<CompetitionWinnerGroup>>();
  final _memePost = PublishSubject<MemePost>();
  final _user = BehaviorSubject<MemecistUser>();

  Competition _currentCompetition;

  List<CompetitionWinnerGroup> winners = [];

  Stream<List<CompetitionWinnerGroup>> get winnersList => _winnersList.stream;

  Function(List<CompetitionWinnerGroup>) get setWinnersList =>
      _winnersList.sink.add;

  Stream<MemePost> get memePost => _memePost.stream;

  Function(MemePost) get setMemePost => _memePost.sink.add;

  Stream<MemecistUser> get user => _user.stream;

  Function(MemecistUser) get setUser => _user.sink.add;

  initializeCompetitionDetailsBloc(Competition competition) {
    _currentCompetition = competition;
  }

  getWinners(String competitionID) async {
    winners = await repository.getCompetitionWinners(competitionID);

    LoggerUtil.log("Competition Details Bloc", "get winners",
        "winners : " + winners.length.toString());

    setWinnersList(winners);

    User currentUser = await repository.getLoggedInUser();

    winners.forEach((competitionWinnerGroup) {
      competitionWinnerGroup.competitionWinners.forEach((winner) async {
        if (winner.user.userID == currentUser.uid) {
          setUser(await repository.getMemecistUser(currentUser.uid));
        }
      });
    });
  }

  shareWarWinnerCard(GlobalKey globalKey) async {
    try {
      return new Future.delayed(const Duration(milliseconds: 20), () async {
        RenderRepaintBoundary boundary =
            globalKey.currentContext.findRenderObject();
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);
        ByteData byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);

        repository.firebaseAnalytics.logShare(
            contentType: FirebaseAnalyticsConstants.competition_winner_card,
            itemId: _currentCompetition.competitionID,
            method: FirebaseAnalyticsConstants.competition_winner_share_card);

        await Share.file(
            'Share', 'winner.png', byteData.buffer.asUint8List(), 'image/png');
      });
    } catch (e) {
      print(e);
    }
  }

  void dispose() async {
    await _winnersList.drain();
    _winnersList.close();

    await _memePost.drain();
    _memePost.close();

    await _user.drain();
    _user.close();
  }
}
