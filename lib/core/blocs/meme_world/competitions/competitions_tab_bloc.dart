import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class CompetitionsTabBloc {
  final _competitionsList = PublishSubject<List<Competition>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();

  List<Competition> competitions = [];

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreGroups => _hasMore.stream;

  Function(bool) get setHasMoreGroups => _hasMore.sink.add;

  Stream<List<Competition>> get competitionsList => _competitionsList.stream;

  Function(List<Competition>) get setCompetitionsList =>
      _competitionsList.sink.add;

  Future<void> fetchCompetitions() async {
    setLoading(true);
    List<Competition> newCompetitions = [];

    String country = await repository.getUserCountry();

    newCompetitions = await repository.getActiveCompetitions(country);
    hasMore = false;
    setHasMoreGroups(false);

//    if (runCount == 0) {
//      newGroups = await repository.getGroupsWithActiveWars(_currentUser.uid);
//
//      LoggerUtil.log("Competitions Tab Bloc", "fetch groups", "first run");
//    } else {
//      LoggerUtil.log(
//          "Competitions Tab Bloc", "fetch groups", "run : " + runCount.toString());
//      newGroups = await repository.getGroupsWithActiveWars(_currentUser.uid);
//    }
//    if (newGroups.length < docLimit) {
//      LoggerUtil.log("Competitions Tab Bloc", "fetch groups", "has more is false");
//      hasMore = false;
//      setHasMoreGroups(false);
//    }
    runCount++;
    LoggerUtil.log("Competitions Tab Bloc", "fetch competitions",
        "new added : " + newCompetitions.length.toString());

    for (Competition competition in newCompetitions)
      competitions.add(competition);

    setCompetitionsList(competitions);
    setLoading(false);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.competitions_fetch);
  }

  void refreshScreen() {
    hasMore = true;
    competitions.clear();
    setCompetitionsList(null);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.refresh_screen);
  }

  void dispose() async {
    await _competitionsList.drain();
    _competitionsList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();
  }
}
