import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competition_details_bloc.dart';

class CompetitionDetailsBlocProvider extends InheritedWidget {
  final CompetitionDetailsBloc competitionDetailsBloc;

  CompetitionDetailsBlocProvider({Key key, Widget child})
      : competitionDetailsBloc = CompetitionDetailsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static CompetitionDetailsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<CompetitionDetailsBlocProvider>()
        .competitionDetailsBloc;
  }
}
