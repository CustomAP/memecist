import 'package:memecist/core/blocs/meme_world/war_room/war_room_chat_bloc.dart';
import 'package:flutter/material.dart';

class WarRoomChatBlocProvider extends InheritedWidget{

  final WarRoomChatBloc warRoomChatBloc;

  WarRoomChatBlocProvider({Key key, Widget child}) : warRoomChatBloc = WarRoomChatBloc(),
        super(key:key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static WarRoomChatBloc of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<WarRoomChatBlocProvider>().warRoomChatBloc;
  }

}