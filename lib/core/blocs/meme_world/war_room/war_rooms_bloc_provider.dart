import 'package:memecist/core/blocs/meme_world/war_room/war_rooms_bloc.dart';
import 'package:flutter/material.dart';

class WarRoomsBlocProvider extends InheritedWidget{

  final WarRoomsBloc warRoomsBloc;

  WarRoomsBlocProvider({Key key, Widget child}) : warRoomsBloc = WarRoomsBloc(),
        super(key:key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static WarRoomsBloc of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<WarRoomsBlocProvider>().warRoomsBloc;
  }

}