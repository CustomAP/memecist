import 'dart:async';

import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_room_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_auth/firebase_auth.dart';

class WarRoomsBloc {
  final _warRoomsList = PublishSubject<List<WarRoom>>();
  final _isLoading = BehaviorSubject<bool>();

  List<WarRoom> warRooms = [];

  StreamSubscription warRoomsStreamSubscription;

  int docLimit = 10;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<List<WarRoom>> get warRoomsList => _warRoomsList.stream;

  Function(List<WarRoom>) get setWarRoomsList => _warRoomsList.sink.add;

  Future<void> fetchWarRooms() async {
    setLoading(true);
    User user = await repository.getLoggedInUser();

    warRoomsStreamSubscription =
        repository.getWarRoomsStream(user.uid).listen((warRoomsList) async {
      print(warRoomsList.length.toString());
      LoggerUtil.log("War Rooms Bloc", "fetch war rooms",
          "new added : " + warRoomsList.length.toString());

      for (WarRoom warRoom in warRoomsList) {
        warRoom.isUnRead =
            await repository.getLastOpenedWarRoomTime(warRoom.warRoomID) <
                warRoom.lastMessageDateTime.millisecondsSinceEpoch;
      }

      repository.firebaseAnalytics.logEvent(
          name: FirebaseAnalyticsConstants.war_rooms_fetch,
          parameters: {
            FirebaseAnalyticsConstants.total: warRoomsList.length.toString()
          });

      warRooms = warRoomsList;
      setWarRoomsList(warRoomsList);
      setLoading(false);
    });
  }

  void dispose() async {
    warRoomsStreamSubscription.cancel();

    await _warRoomsList.drain();
    _warRoomsList.close();

    await _isLoading.drain();
    _isLoading.close();
  }
}
