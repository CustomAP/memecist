import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/message_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/models/war_room_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class WarRoomChatBloc {
  final _messages = BehaviorSubject<List<Message>>();
  final _user = BehaviorSubject<User>();
  final _imageFile = BehaviorSubject<File>();
  final _imageUploadTask = PublishSubject<StorageUploadTask>();
  final _isLoadingForImageUpload = BehaviorSubject<bool>();
  final _isLoadingMoreMessages = BehaviorSubject<bool>();
  final _hasMoreMessages = BehaviorSubject<bool>();
  final _toAllowTeamRename = BehaviorSubject<bool>();
  final _warRoom = BehaviorSubject<WarRoom>();
  final _teamPicFile = BehaviorSubject<File>();
  final _teamPicUploadTask = BehaviorSubject<StorageUploadTask>();
  final _isLoadingForTeamPicUpload = BehaviorSubject<bool>();

  List<Message> messages = [];

  StreamSubscription messagesSubscription;

  bool hasMore = true;
  int docLimit = 10;

  Stream<List<Message>> get messageList => _messages.stream;

  Function(List<Message>) get setMessageList => _messages.sink.add;

  Stream<User> get currentUser => _user.stream;

  Function(User) get setCurrentUser => _user.sink.add;

  Stream<StorageUploadTask> get imageUploadTask => _imageUploadTask.stream;

  Function(StorageUploadTask) get setImageUploadTask =>
      _imageUploadTask.sink.add;

  Stream<File> get imageFile => _imageFile.stream;

  Function(File) get setImageFile => _imageFile.sink.add;

  Stream<bool> get isLoadingForImageUpload => _isLoadingForImageUpload.stream;

  Function(bool) get setIsLoadingForImageUpload =>
      _isLoadingForImageUpload.sink.add;

  Stream<bool> get isLoadingMoreMessages => _isLoadingMoreMessages.stream;

  Function(bool) get setIsLoadingMoreMessages =>
      _isLoadingMoreMessages.sink.add;

  Stream<bool> get hasMoreMessages => _hasMoreMessages.stream;

  Function(bool) get setHasMoreMessages => _hasMoreMessages.sink.add;

  Stream<bool> get toAllowTeamRename => _toAllowTeamRename.stream;

  Function(bool) get setToAllowTeamRename => _toAllowTeamRename.sink.add;

  Stream<WarRoom> get warRoom => _warRoom.stream;

  Function(WarRoom) get setWarRoom => _warRoom.sink.add;

  Stream<StorageUploadTask> get teamPicUploadTask => _teamPicUploadTask.stream;

  Function(StorageUploadTask) get setTeamPicUploadTask =>
      _teamPicUploadTask.sink.add;

  Stream<bool> get isLoadingForTeamPicUpload =>
      _isLoadingForTeamPicUpload.stream;

  Function(bool) get setIsLoadingForTeamPicUpload =>
      _isLoadingForTeamPicUpload.sink.add;

  WarRoom _currentWarRoom;

  initializeWarRoomChatBloc(WarRoom warRoom) async {
    this._currentWarRoom = warRoom;
    User user = await repository.getLoggedInUser();
    setCurrentUser(user);
    messagesSubscription = repository
        .getRecentMessages(_currentWarRoom.warRoomID, true)
        .listen((messageList) {
      LoggerUtil.log("War Room Chat Bloc", "initialize war room chat bloc",
          "new message recieved and total is:" + messageList.length.toString());

      messageList.forEach((message) {
        bool found = false;
        for (int i = 0; i < messages.length; i++) {
          if (messages[i].messageID == message.messageID) {
            found = true;
            break;
          }
        }
        if (!found) messages.add(message);
      });

      messages.sort((Message a, Message b) => b
          .createDateTime.millisecondsSinceEpoch
          .compareTo(a.createDateTime.millisecondsSinceEpoch));

      setMessageList(messages);
    });
  }

  loadMoreMessages() async {
    setIsLoadingMoreMessages(true);
    List<Message> newMessages =
        await repository.getMoreMessages(_currentWarRoom.warRoomID, docLimit);

    newMessages.forEach((message) {
      bool found = false;
      for (int i = 0; i < messages.length; i++) {
        if (messages[i].messageID == message.messageID) {
          found = true;
          break;
        }
      }
      if (!found) messages.add(message);
    });

    messages.sort((Message a, Message b) => b
        .createDateTime.millisecondsSinceEpoch
        .compareTo(a.createDateTime.millisecondsSinceEpoch));

    if (newMessages.length < docLimit) setHasMoreMessages(false);

    setMessageList(messages);
    setIsLoadingMoreMessages(false);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.load_more_messages);
  }

  sendMessage(String messageContent, String type) async {
    User currentUser = await repository.getLoggedInUser();
    MemecistUser user = await repository.getMemecistUser(currentUser.uid);
    LoggerUtil.log(
        "War Room Chat Bloc", "Send message", "message : " + messageContent);
    repository.sendMessage(
        Message(
            content: messageContent,
            type: type,
            sender: MemecistUser(
                userID: user.userID,
                userName: user.userName,
                firstName: user.firstName,
                lastName: user.lastName,
                profilePic: user.profilePic,
                isVerified: user.isVerified)),
        _currentWarRoom.warRoomID);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.message_send, parameters: {
      FirebaseAnalyticsConstants.message_type:
          type == FirestoreConstants.image_message
              ? FirebaseAnalyticsConstants.image
              : FirebaseAnalyticsConstants.text
    });
  }

  uploadImage(File image) async {
    setIsLoadingForImageUpload(true);
    LoggerUtil.log(
        "War Room Chat Bloc", "Upload image", "image : " + image.path);

    User currentUser = await repository.getLoggedInUser();

    var result =
        await FlutterImageCompress.compressWithList(image.readAsBytesSync());

    setImageUploadTask(repository.uploadMessageImage(
        Uint8List.fromList(result), currentUser.uid));
  }

  uploadTeamPic(File image) async {
    setIsLoadingForTeamPicUpload(true);
    LoggerUtil.log(
        "War Room Chat Bloc", "Change team pic", "image : " + image.path);

    var result =
        await FlutterImageCompress.compressWithList(image.readAsBytesSync());

    setTeamPicUploadTask(repository.uploadTeamImage(
        Uint8List.fromList(result), _currentWarRoom.teamID));
  }

  changeTeamPic(String imageUrl, String teamID, String warID) async {
    repository.changeTeamPic(imageUrl, teamID, warID);
    _currentWarRoom.teamPic = imageUrl;
    setWarRoom(_currentWarRoom);
  }

  WarRoomChatBloc() {
    _imageUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        LoggerUtil.log(
            "War Room Chat Bloc", "initializer", "image upload task started");

        imageUploadTask.events.listen((eventReceived) async {
          if (imageUploadTask.isComplete) {
            LoggerUtil.log("War Room chat Bloc", "initializer",
                "image upload task complete");

            sendMessage(await eventReceived.snapshot.ref.getDownloadURL(),
                FirestoreConstants.image_message);
            setIsLoadingForImageUpload(false);
            setImageUploadTask(null);
          } else if (imageUploadTask.isCanceled) {
            LoggerUtil.log("War Room chat Bloc", "initializer",
                "image upload task cancelled");

            setIsLoadingForImageUpload(false);
            setImageUploadTask(null);
          }
        });
      }
    });

    _teamPicUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        LoggerUtil.log("War Room Chat Bloc", "initializer",
            "team pic upload task started");

        imageUploadTask.events.listen((eventReceived) async {
          if (imageUploadTask.isComplete) {
            LoggerUtil.log("War Room chat Bloc", "initializer",
                "team pic upload task complete");

            changeTeamPic(await eventReceived.snapshot.ref.getDownloadURL(),
                _currentWarRoom.teamID, _currentWarRoom.warID);
            setIsLoadingForTeamPicUpload(false);
            setTeamPicUploadTask(null);
          } else if (imageUploadTask.isCanceled) {
            LoggerUtil.log("War Room chat Bloc", "initializer",
                "team pic upload task cancelled");

            setIsLoadingForTeamPicUpload(false);
            setTeamPicUploadTask(null);
          }
        });
      }
    });
  }

  checkToAllowTeamRename(String warID, String teamID) async {
    bool isTeamAlreadyRenamed =
        await repository.isTeamAlreadyRenamed(warID, teamID);

    War war = await repository.getWar(warID);

    setToAllowTeamRename(!isTeamAlreadyRenamed &&
        (war.status == FirestoreConstants.team_leaders_selection ||
            war.status == FirestoreConstants.team_members_selection));
  }

  renameTeam(String warID, String teamID, String teamName) async {
    repository.renameTeam(warID, teamID, teamName);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.rename_team);

    _currentWarRoom.teamName = teamName;

    setWarRoom(_currentWarRoom);
  }

  void dispose() async {
    await repository.setLastOpenedWarRoomTime(
        _currentWarRoom.warRoomID, DateTime.now().millisecondsSinceEpoch);

    messagesSubscription.cancel();

    await _messages.drain();
    _messages.close();

    await _user.drain();
    _user.close();

    await _isLoadingForImageUpload.drain();
    _isLoadingForImageUpload.close();

    await _imageFile.drain();
    _imageFile.close();

    await _imageUploadTask.drain();
    _imageUploadTask.close();

    await _isLoadingMoreMessages.drain();
    _isLoadingMoreMessages.close();

    await _hasMoreMessages.drain();
    _hasMoreMessages.close();

    await _toAllowTeamRename.drain();
    _toAllowTeamRename.close();

    await _warRoom.drain();
    _warRoom.close();

    await _teamPicFile.drain();
    _teamPicFile.close();

    await _isLoadingForTeamPicUpload.drain();
    _isLoadingForTeamPicUpload.close();
  }
}
