import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/create_group_bloc.dart';

class CreateGroupBlocProvider extends InheritedWidget{

  final CreateGroupBloc createGroupBloc;

  CreateGroupBlocProvider({Key key, Widget child}) : createGroupBloc = CreateGroupBloc(),
        super(key:key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static CreateGroupBloc of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<CreateGroupBlocProvider>().createGroupBloc;
  }

}