import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_messaging_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/keywords_generator.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class CreateGroupBloc {
  final _imageFile = BehaviorSubject<File>();
  final _imageUploadTask = PublishSubject<StorageUploadTask>();
  final _showLoading = BehaviorSubject<bool>();
  final _isGroupClosed = BehaviorSubject<bool>();
  final _groupID = BehaviorSubject<String>();

  Group _group;

  Stream<StorageUploadTask> get imageUploadTask => _imageUploadTask.stream;

  Function(StorageUploadTask) get setImageUploadTask =>
      _imageUploadTask.sink.add;

  Stream<File> get imageFile => _imageFile.stream;

  Function(File) get setImageFile => _imageFile.sink.add;

  Stream<bool> get showLoading => _showLoading.stream;

  Function(bool) get setShowLoading => _showLoading.sink.add;

  Stream<bool> get isGroupClosed => _isGroupClosed.stream;

  Function(bool) get setIsGroupClosed => _isGroupClosed.sink.add;

  Stream<String> get groupID => _groupID.stream;

  Function(String) get setGroupID => _groupID.sink.add;

  createGroup(File image, Group group) async {
    setShowLoading(true);
    User currentUser = await repository.getLoggedInUser();
    _group = group;

    if (image != null) {
      LoggerUtil.log(
          "Create Group Bloc", "Create Group", "image : " + image.path);

      var result =
          await FlutterImageCompress.compressWithList(image.readAsBytesSync());

      setImageUploadTask(repository.uploadGroupImage(
          Uint8List.fromList(result), currentUser.uid));
    } else {
      uploadToDb(FirebaseStorageConstants.defaultGroupPicUrl);
    }
  }

  uploadToDb(String imageUrl) async {
    User user = await repository.getLoggedInUser();
    MemecistUser currentUser = await repository.getMemecistUser(user.uid);

    List<String> groupSearchKeywords =
        KeywordsGenerator.generateKeywordsForGroups(_group.groupName);

    List<String> adminSearchKeywords =
        KeywordsGenerator.generateKeywordsForNames(
            currentUser.firstName, currentUser.lastName, currentUser.userName);

    currentUser.searchKeywords = adminSearchKeywords;
    _group.createdByID = currentUser.userID;
    _group.groupProfilePic = imageUrl;
    _group.searchKeywords = groupSearchKeywords;

    String groupID = await repository.createGroup(currentUser, _group);
    repository.subscribeTopic(FirebaseMessagingConstants.group + "-" + groupID);

    LoggerUtil.log(
        "Create Group Bloc", "create group", "Group Created: " + groupID);
    setGroupID(groupID);
    setShowLoading(false);
    setImageUploadTask(null);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.group_create);
  }

  uploadToDbWithStorageRef(StorageReference storageReference) async {
    uploadToDb(await storageReference.getDownloadURL());
  }

  CreateGroupBloc() {
    _imageUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        LoggerUtil.log(
            "Create Group Bloc", "initializer", "image upload task started");

        imageUploadTask.events.listen((eventReceived) {
          if (imageUploadTask.isComplete) {
            LoggerUtil.log("Create Group Bloc", "initializer",
                "image upload task complete");

            uploadToDbWithStorageRef(eventReceived.snapshot.ref);
          }
        });
      }
    });
  }

  void dispose() async {
    await _imageUploadTask.drain();
    _imageUploadTask.close();

    await _imageFile.drain();
    _imageFile.close();

    await _showLoading.drain();
    _showLoading.close();

    await _isGroupClosed.drain();
    _isGroupClosed.close();

    await _groupID.drain();
    _groupID.close();
  }
}
