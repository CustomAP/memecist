import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_members_bloc.dart';

class ViewMembersBlocProvider extends InheritedWidget {
  final ViewMembersBloc viewMembersBloc;

  ViewMembersBlocProvider({Key key, Widget child})
      : viewMembersBloc = ViewMembersBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ViewMembersBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ViewMembersBlocProvider>()
        .viewMembersBloc;
  }
}
