import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ViewUnapprovedMemesBloc {
  final _memePosts = PublishSubject<List<MemePost>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _approve = BehaviorSubject<MemePost>();
  final _reject = BehaviorSubject<MemePost>();

  List<MemePost> memePosts = [];

  String _groupID;

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreMemes => _hasMore.stream;

  Function(bool) get setHasMoreMemes => _hasMore.sink.add;

  Stream<List<MemePost>> get memePostList => _memePosts.stream;

  Function(List<MemePost>) get setMemePostList => _memePosts.sink.add;

  Stream<MemePost> get approve => _approve.stream;

  Function(MemePost) get setApprove => _approve.sink.add;

  Stream<MemePost> get reject => _reject.stream;

  Function(MemePost) get setReject => _reject.sink.add;

  initializeViewUnapprovedMemesBloc(String groupID) {
    _groupID = groupID;
    LoggerUtil.log(
        "View Unapproved memes Bloc", "Initialize", "groupID : " + groupID);
  }

  Future<void> fetchUnapprovedMemes() async {
    setLoading(true);
    List<MemePost> newPosts = [];

    if (runCount == 0) {
      LoggerUtil.log("View Unapproved memes Bloc", "fetch reacts", "first run");
      newPosts = await repository.getInitialUnapprovedMemePostsByGroup(
          docLimit, _groupID);
    } else {
      LoggerUtil.log("View Unapproved memes Bloc", "fetch memes",
          "run : " + runCount.toString());
      newPosts = await repository.getMoreUnapprovedMemePostsByGroup(
          docLimit, _groupID);
    }
    if (newPosts.length < docLimit) {
      LoggerUtil.log(
          "View Unapproved memes Bloc", "fetch memes", "has more is false");
      hasMore = false;
      setHasMoreMemes(false);
    }
    runCount++;
    LoggerUtil.log("View Unapproved memes Bloc", "fetch memes",
        "new added : " + newPosts.length.toString());

    for (MemePost post in newPosts) memePosts.add(post);

    setMemePostList(memePosts);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.unapproved_memes_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  approveMeme(MemePost memePost) async {
    LoggerUtil.log("View Unapproved memes Bloc", "approve meme",
        "approve meme : " + memePost.postID);
    setApprove(memePost);
    repository.approveMeme(memePost.postID, _groupID);
    memePosts.remove(memePost);
    setMemePostList(memePosts);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.approve_meme);
  }

  rejectMeme(MemePost memePost) async {
    LoggerUtil.log("View Unapproved memes Bloc", "reject meme",
        "reject meme : " + memePost.postID);
    setReject(memePost);
    User currentUser = await repository.getLoggedInUser();
    repository.rejectMeme(memePost.postID, currentUser.uid, _groupID);
    memePosts.remove(memePost);
    setMemePostList(memePosts);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.reject_meme);
  }

  void dispose() async {
    await _memePosts.drain();
    _memePosts.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();

    await _reject.drain();
    _reject.close();
  }
}
