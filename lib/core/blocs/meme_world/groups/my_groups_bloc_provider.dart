import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/my_groups_bloc.dart';

class MyGroupsBlocProvider extends InheritedWidget {
  final MyGroupsBloc myGroupsBloc;

  MyGroupsBlocProvider({Key key, Widget child})
      : myGroupsBloc = MyGroupsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static MyGroupsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<MyGroupsBlocProvider>()
        .myGroupsBloc;
  }
}
