import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/groups_tab_bloc.dart';

class GroupsTabBlocProvider extends InheritedWidget{
  final GroupsTabBloc groupsTabBloc;

  GroupsTabBlocProvider({Key key, Widget child})
      : groupsTabBloc = GroupsTabBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static GroupsTabBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<GroupsTabBlocProvider>()
        .groupsTabBloc;
  }
}