import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_member_model.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class MyGroupsBloc {
  final _groupsList = PublishSubject<List<Group>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();

  List<Group> groups = [];

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreGroups => _hasMore.stream;

  Function(bool) get setHasMoreGroups => _hasMore.sink.add;

  Stream<List<Group>> get groupsList => _groupsList.stream;

  Function(List<Group>) get setGroupsList => _groupsList.sink.add;

  Future<void> fetchGroups() async {
    setLoading(true);
    List<Group> newGroups = [];

    User currentUser = await repository.getLoggedInUser();

    newGroups = await repository.getGroupsForUser(currentUser.uid);
    hasMore = false;
    setHasMoreGroups(false);

    //TODO implement this
//    if (runCount == 0) {
//      newGroups = await repository.getGroupsForUser(_currentUser.uid);
//
//      LoggerUtil.log("My Groups Bloc", "fetch groups", "first run");
//    } else {
//      LoggerUtil.log(
//          "My Groups Bloc", "fetch groups", "run : " + runCount.toString());
//      newGroups = await repository.getGroupsForUser(_currentUser.uid);
//    }
//    if (newGroups.length < docLimit) {
//      LoggerUtil.log("My Groups Bloc", "fetch groups", "has more is false");
//      hasMore = false;
//      setHasMoreGroups(false);
//    }
    runCount++;
    LoggerUtil.log("My Groups Bloc", "fetch groups",
        "new added : " + newGroups.length.toString());

    for (Group group in newGroups) groups.add(group);

    setGroupsList(groups);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.groups_fetch,
        parameters: {FirebaseAnalyticsConstants.total: groups.length});
  }

  void dispose() async {
    await _groupsList.drain();
    _groupsList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();
  }
}
