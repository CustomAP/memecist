import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_pending_approvals_bloc.dart';

class ViewPendingApprovalsBlocProvider extends InheritedWidget {
  final ViewPendingApprovalsBloc viewPendingApprovalsBloc;

  ViewPendingApprovalsBlocProvider({Key key, Widget child})
      : viewPendingApprovalsBloc = ViewPendingApprovalsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ViewPendingApprovalsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ViewPendingApprovalsBlocProvider>()
        .viewPendingApprovalsBloc;
  }
}
