import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:ui' as ui;

class WarEndedBloc {
  final _war = BehaviorSubject<War>();
  final _memesList = PublishSubject<List<MemePost>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _user = BehaviorSubject<MemecistUser>();
  final _memeOfTheWar = BehaviorSubject<MemePost>();

  List<MemePost> memePosts = [];

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  War _currentWar;

  Stream<War> get war => _war.stream;

  Function(War) get setWar => _war.sink.add;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreMemes => _hasMore.stream;

  Function(bool) get setHasMoreMemes => _hasMore.sink.add;

  Stream<List<MemePost>> get memesList => _memesList.stream;

  Function(List<MemePost>) get setMemesList => _memesList.sink.add;

  Stream<MemecistUser> get user => _user.stream;

  Function(MemecistUser) get setUser => _user.sink.add;

  Stream<MemePost> get memeOfTheWar => _memeOfTheWar.stream;

  Function(MemePost) get setMemeOfTheWar => _memeOfTheWar.sink.add;

  void initializeDDayBloc(War war) async {
    LoggerUtil.log("War Ended Bloc", "Initialize", "war: " + war.warID);
    _currentWar = war;

    User currentUser = await repository.getLoggedInUser();
    if (_currentWar.winnerTeam.members.contains(currentUser.uid)) {
      MemecistUser user = await repository.getMemecistUser(currentUser.uid);
      setUser(user);
    }
  }

  shareWarWinnerCard(GlobalKey globalKey) async {
    try {
      return new Future.delayed(const Duration(milliseconds: 20), () async {
        RenderRepaintBoundary boundary =
            globalKey.currentContext.findRenderObject();
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);
        ByteData byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);

        repository.firebaseAnalytics.logShare(
            contentType: FirebaseAnalyticsConstants.war_winner_card,
            itemId: _currentWar.warID,
            method: FirebaseAnalyticsConstants.war_winner_share_card);

        await Share.file(
            'Share', 'winner.png', byteData.buffer.asUint8List(), 'image/png');
      });
    } catch (e) {
      print(e);
    }
  }

  getWar() async {
    LoggerUtil.log("War Ended Bloc", "get war", "war: " + _currentWar.warID);
    War war = await repository.getWar(_currentWar.warID);
    setWar(war);
  }

  Future<void> fetchWarMemes() async {
    setLoading(true);

    User currentUser = await repository.getLoggedInUser();

    List<MemePost> newMemePosts = [];

    if (runCount == 0) {
      LoggerUtil.log("War Ended Bloc", "fetch meme posts", "first run");
      newMemePosts = await repository.getInitialMemePostsByWar(
          docLimit, _currentWar.warID, currentUser.uid);
    } else {
      LoggerUtil.log(
          "War Ended Bloc", "fetch meme posts", "run : " + runCount.toString());
      newMemePosts = await repository.getMoreMemePostsByWar(
          docLimit, _currentWar.warID, currentUser.uid);
    }
    if (newMemePosts.length < docLimit) {
      LoggerUtil.log("War Ended Bloc", "fetch meme posts", "has more is false");
      hasMore = false;
      setHasMoreMemes(false);
    }
    runCount++;
    LoggerUtil.log("War Ended Bloc", "fetch meme posts",
        "new added : " + newMemePosts.length.toString());

    for (MemePost memePost in newMemePosts) memePosts.add(memePost);

    setMemesList(memePosts);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.war_memes_fetch,
        parameters: {
          FirebaseAnalyticsConstants.run_count: runCount,
          FirebaseAnalyticsConstants.is_war_participant:
              _currentWar.participants.contains(currentUser.uid)
        });
  }

  Future<void> getMemeOfTheWar() async {
    User user = await repository.getLoggedInUser();
    MemePost memePost =
        await repository.getMemePost(_currentWar.memeOfTheWar, user.uid);
    LoggerUtil.log(
        "War Ended Bloc", "meme of the war", "post id: " + memePost.postID);
    setMemeOfTheWar(memePost);
  }

  void dispose() async {
    await _war.drain();
    _war.close();

    await _memesList.drain();
    _memesList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();

    await _user.drain();
    _user.close();

    await _memeOfTheWar.drain();
    _memeOfTheWar.close();
  }
}
