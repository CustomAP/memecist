import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class ViewAllTeamsBloc {
  final _warTeams = BehaviorSubject<List<WarTeam>>();

  Stream<List<WarTeam>> get warTeams => _warTeams.stream;

  Function(List<WarTeam>) get setWarTeams => _warTeams.sink.add;

  getAllWarTeams(String warID) async {
    List<WarTeam> warTeams = await repository.getAllWarTeams(warID);
    LoggerUtil.log("View All Teams Bloc", "get all teams", "warID: " + warID);
    setWarTeams(warTeams);
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.view_all_teams);
  }

  void dispose() async {
    await _warTeams.drain();
    _warTeams.close();
  }
}
