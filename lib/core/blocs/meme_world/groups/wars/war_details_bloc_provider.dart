import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc.dart';

class WarDetailsBlocProvider extends InheritedWidget {
  final WarDetailsBloc warDetailsBloc;

  WarDetailsBlocProvider({Key key, Widget child})
      : warDetailsBloc = WarDetailsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static WarDetailsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<WarDetailsBlocProvider>()
        .warDetailsBloc;
  }
}
