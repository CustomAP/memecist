import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class DDayBloc {
  final _war = BehaviorSubject<War>();
  final _memesList = PublishSubject<List<MemePost>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _user = BehaviorSubject<User>();
  final _team = BehaviorSubject<WarTeam>();

  List<MemePost> memePosts = [];

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  War _currentWar;

  StreamSubscription warTeamStreamSubscription;

  Stream<War> get war => _war.stream;

  Function(War) get setWar => _war.sink.add;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreMemes => _hasMore.stream;

  Function(bool) get setHasMoreMemes => _hasMore.sink.add;

  Stream<List<MemePost>> get memesList => _memesList.stream;

  Function(List<MemePost>) get setMemesList => _memesList.sink.add;

  Stream<User> get user => _user.stream;

  Function(User) get setUser => _user.sink.add;

  Stream<WarTeam> get warTeam => _team.stream;

  Function(WarTeam) get setWarTeam => _team.sink.add;

  void initializeDDayBloc(War war) async {
    LoggerUtil.log("D Day Bloc", "Initialize", "war: " + war.warID);
    _currentWar = war;

    User currentUser = await repository.getLoggedInUser();
    setUser(currentUser);
  }

  getWar() async {
    LoggerUtil.log("D Day Bloc", "get war", "war: " + _currentWar.warID);
    War war = await repository.getWar(_currentWar.warID);
    setWar(war);
  }

  Future<void> fetchWarMemes() async {
    setLoading(true);

    User currentUser = await repository.getLoggedInUser();

    List<MemePost> newMemePosts = [];

    if (runCount == 0) {
      LoggerUtil.log("D Day Bloc", "fetch meme posts", "first run");
      newMemePosts = await repository.getInitialMemePostsByWar(
          docLimit, _currentWar.warID, currentUser.uid);
    } else {
      LoggerUtil.log(
          "D Day Bloc", "fetch meme posts", "run : " + runCount.toString());
      newMemePosts = await repository.getMoreMemePostsByWar(
          docLimit, _currentWar.warID, currentUser.uid);
    }
    if (newMemePosts.length < docLimit) {
      LoggerUtil.log("D Day Bloc", "fetch meme posts", "has more is false");
      hasMore = false;
      setHasMoreMemes(false);
    }
    runCount++;
    LoggerUtil.log("D Day Bloc", "fetch meme posts",
        "new added : " + newMemePosts.length.toString());

    for (MemePost memePost in newMemePosts) memePosts.add(memePost);

    setMemesList(memePosts);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.war_memes_fetch,
        parameters: {
          FirebaseAnalyticsConstants.run_count: runCount,
          FirebaseAnalyticsConstants.is_war_participant:
              _currentWar.participants.contains(currentUser.uid)
        });
  }

  Future<void> getWarTeam() async {
    User currentUser = await repository.getLoggedInUser();

    warTeamStreamSubscription = repository
        .streamWarTeam(currentUser.uid, _currentWar.warID)
        .listen((warTeam) {
      LoggerUtil.log(
          "D Day Bloc", "get war team", "war team: " + warTeam.teamID);

      setWarTeam(warTeam);
    });
  }

  deletePost(MemePost memePost) async {
    repository.deletePost(memePost.postID);
    for (MemePost post in memePosts) {
      if (post.postID == memePost.postID) {
        memePosts.remove(post);
        break;
      }
    }
    setMemesList(memePosts);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post);
  }

  void dispose() async {
    warTeamStreamSubscription.cancel();

    await _war.drain();
    _war.close();

    await _memesList.drain();
    _memesList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();

    await _user.drain();
    _user.close();

    await _team.drain();
    _team.close();
  }
}
