import 'package:flutter/material.dart';
import 'd_day_bloc.dart';

class DDayBlocProvider extends InheritedWidget {
  final DDayBloc dDayBloc;

  DDayBlocProvider({Key key, Widget child})
      : dDayBloc = DDayBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static DDayBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<DDayBlocProvider>()
        .dDayBloc;
  }
}
