import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class WarDetailsBloc {
  final _war = BehaviorSubject<War>();

  Stream<War> get war => _war.stream;

  Function(War) get setWar => _war.sink.add;

  getWar(String warID) async {
    War war = await repository.getWar(warID);
    LoggerUtil.log("War Details Bloc", "get war", "warID: " + warID);
    setWar(war);
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.war_details_fetch);
  }

  void dispose() async {
    await _war.drain();
    _war.close();
  }
}
