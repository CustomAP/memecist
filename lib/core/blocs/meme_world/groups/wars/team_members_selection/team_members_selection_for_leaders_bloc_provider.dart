import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_members_selection/team_members_selection_for_leaders_bloc.dart';

class TeamMembersSelectionForLeadersBlocProvider extends InheritedWidget {
  final TeamMembersSelectionForLeadersBloc teamMembersSelectionBloc;

  TeamMembersSelectionForLeadersBlocProvider({Key key, Widget child})
      : teamMembersSelectionBloc = TeamMembersSelectionForLeadersBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static TeamMembersSelectionForLeadersBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<TeamMembersSelectionForLeadersBlocProvider>()
        .teamMembersSelectionBloc;
  }
}
