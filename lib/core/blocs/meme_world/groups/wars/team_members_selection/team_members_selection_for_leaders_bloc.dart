import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_application_model.dart';
import 'package:memecist/core/models/war_member_application_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class TeamMembersSelectionForLeadersBloc {
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _warApplications = BehaviorSubject<List<WarMemberApplication>>();
  final _selectedTeamMembers = BehaviorSubject<List<WarMemberApplication>>();
  final _war = BehaviorSubject<War>();
  final _warTeam = BehaviorSubject<WarTeam>();

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;
  WarTeam _currentWarTeam;

  List<WarMemberApplication> warApplications = [];
  List<WarMemberApplication> selectedTeamMembers = [];

  War _currentWar;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreApplications => _hasMore.stream;

  Function(bool) get setHasMoreApplications => _hasMore.sink.add;

  Stream<List<WarMemberApplication>> get warApplicationsList =>
      _warApplications.stream;

  Function(List<WarMemberApplication>) get setWarApplicationsList =>
      _warApplications.sink.add;

  Stream<List<WarMemberApplication>> get selectedTeamMembersList =>
      _selectedTeamMembers.stream;

  Function(List<WarMemberApplication>) get setSelectedTeamMembersList =>
      _selectedTeamMembers.sink.add;

  Stream<War> get war => _war.stream;

  Function(War) get setWar => _war.sink.add;

  Stream<WarTeam> get warTeam => _warTeam.stream;

  Function(WarTeam) get setWarTeam => _warTeam.sink.add;

  initializeTeamMembersSelectionForLeadersBloc(War war) async {
    this._currentWar = war;
    LoggerUtil.log(
        "Team members selection for members bloc",
        "initialize team members selection for members",
        "warID: " + _currentWar.warID);

    User currentUser = await repository.getLoggedInUser();

    _currentWarTeam =
        await repository.getWarTeam(_currentWar.warID, currentUser.uid);
    setWarTeam(_currentWarTeam);
  }

  Future<void> fetchTeamMembersApplications() async {
    setLoading(true);
    List<WarMemberApplication> newWarApplications = [];

    if (runCount == 0) {
      LoggerUtil.log("Team members selection for members bloc",
          "fetch team members applications", "first run");
      newWarApplications =
          await repository.getInitialTeamMemberApplications(_currentWar.warID);
    } else {
      LoggerUtil.log("Team members selection for members bloc",
          "fetch team members applications", "run : " + runCount.toString());
      newWarApplications =
          await repository.getMoreTeamMemberApplications(_currentWar.warID);
    }
    if (newWarApplications.length < docLimit) {
      LoggerUtil.log("Team members selection for members bloc",
          "fetch team members applications", "has more is false");
      hasMore = false;
      setHasMoreApplications(false);
    }
    runCount++;
    LoggerUtil.log(
        "Team members selection for members bloc",
        "fetch team members applications",
        "new added : " + newWarApplications.length.toString());

    for (WarMemberApplication warApplication in newWarApplications)
      warApplications.add(warApplication);

    setWarApplicationsList(warApplications);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.team_members_applications_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  selectApplication(WarMemberApplication warApplication) {
    int i;
    for (i = 0; i < warApplications.length; i++) {
      if (warApplications[i] == warApplication) break;
    }
    LoggerUtil.log("Team members selection for members bloc",
        "select application", "user id" + warApplication.user.userID);
    warApplication.isSelected = true;
    warApplications[i] = warApplication;
    setWarApplicationsList(warApplications);

    selectedTeamMembers.add(warApplication);
    setSelectedTeamMembersList(selectedTeamMembers);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.select_team_member_application);
  }

  unSelectApplication(WarMemberApplication warApplication) {
    int i;
    for (i = 0; i < warApplications.length; i++) {
      if (warApplications[i] == warApplication) break;
    }
    LoggerUtil.log("Team members selection for members bloc",
        "Unselect application", "user id" + warApplication.user.userID);
    warApplication.isSelected = false;
    warApplications[i] = warApplication;
    setWarApplicationsList(warApplications);

    selectedTeamMembers.remove(warApplication);
    setSelectedTeamMembersList(selectedTeamMembers);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.unselect_team_member_application);
  }

  approveTeamMembers() async {
    LoggerUtil.log("Team members selection for members bloc",
        "Approve team members", "approving..");

    if (_currentWarTeam == null) {
      User currentUser = await repository.getLoggedInUser();
      _currentWarTeam =
          await repository.getWarTeam(_currentWar.warID, currentUser.uid);
      setWarTeam(_currentWarTeam);
    }

    repository.approveTeamMembers(
        selectedTeamMembers, _currentWar.warID, _currentWarTeam.teamID);

    _currentWarTeam.membersCount += warApplications.length;

    selectedTeamMembers.forEach((selectedTeamMember) {
      _currentWarTeam.teamMembers.add(selectedTeamMember.user);
      int i = 0;
      for (i = 0; i < warApplications.length; i++) {
        if (warApplications[i].user.userID == selectedTeamMember.user.userID) {
          warApplications[i].isAccepted = true;
          warApplications[i].isSelected = false;
          break;
        }
      }
    });

    selectedTeamMembers.clear();

    setSelectedTeamMembersList(selectedTeamMembers);
    setWarTeam(_currentWarTeam);
    setWarApplicationsList(warApplications);

    double avgSecondsSinceApplied = 0;
    for (WarMemberApplication warMemberApplication in selectedTeamMembers) {
      avgSecondsSinceApplied += DateTime.now()
          .difference(warMemberApplication.createDateTime)
          .inSeconds;
    }

    avgSecondsSinceApplied = avgSecondsSinceApplied / warApplications.length;

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.approve_team_members,
        parameters: {
          FirebaseAnalyticsConstants.total: selectedTeamMembers.length,
          FirebaseAnalyticsConstants.mins_since_member_app_start: DateTime.now()
              .difference(_currentWar.teamLeadersSelectionEndTime)
              .inSeconds,
          FirebaseAnalyticsConstants.avg_mins_since_member_applied:
              avgSecondsSinceApplied
        });
  }

  void dispose() async {
    await _isLoading.drain();
    _isLoading.drain();

    await _hasMore.drain();
    _hasMore.close();

    await _warApplications.drain();
    _warApplications.close();

    await _selectedTeamMembers.drain();
    _selectedTeamMembers.close();

    await _war.drain();
    _war.close();

    await _warTeam.drain();
    _warTeam.close();
  }
}
