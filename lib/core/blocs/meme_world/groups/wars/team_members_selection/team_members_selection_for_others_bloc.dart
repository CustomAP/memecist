import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_member_application_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class TeamMembersSelectionForOthersBloc {
  final _isApplied = BehaviorSubject<bool>();
  final _isAccepted = BehaviorSubject<bool>();

  War _war;

  Stream<bool> get isApplied => _isApplied.stream;

  Function(bool) get setIsApplied => _isApplied.sink.add;

  Stream<bool> get isAccepted => _isAccepted.stream;

  Function(bool) get setIsAccepted => _isAccepted.sink.add;

  initializeTeamMembersSelectionForOthersBloc(War war) {
    this._war = war;
    LoggerUtil.log(
        "Team Members Selection For Others Bloc",
        "initialize Team Members Selection For Others Bloc",
        "warID: " + _war.warID);
  }

  getTeamMemberApplication() async {
    User currentUser = await repository.getLoggedInUser();

    WarMemberApplication warMemberApplication =
        await repository.getTeamMemberApplication(_war.warID, currentUser.uid);

    if (warMemberApplication != null) {
      setIsApplied(true);
      setIsAccepted(warMemberApplication.isAccepted);

      LoggerUtil.log("Team Members Selection For Others Bloc",
          "get team member application", warMemberApplication?.user?.userID);
    } else {
      setIsApplied(false);
      setIsAccepted(false);
    }
  }

  applyAsTeamMember() async {
    User user = await repository.getLoggedInUser();

    MemecistUser currentUser = await repository.getMemecistUser(user.uid);

    LoggerUtil.log("Team Members Selection For Others Bloc",
        "Apply As Team member", "user id" + user.uid);
    repository.applyForTeamMember(currentUser, _war.warID);
    setIsApplied(true);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.team_member_apply,
        parameters: {
          FirebaseAnalyticsConstants.mins_since_member_app_start:
              DateTime.now()
                  .difference(_war.teamLeadersSelectionEndTime)
                  .inSeconds
        });
  }

  void dispose() async {
    await _isApplied.drain();
    _isApplied.close();

    await _isAccepted.drain();
    _isAccepted.close();
  }
}
