import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_members_selection/team_members_selection_for_others_bloc.dart';

class TeamMembersSelectionForOthersBlocProvider extends InheritedWidget {
  final TeamMembersSelectionForOthersBloc teamMembersSelectionForOthersBloc;

  TeamMembersSelectionForOthersBlocProvider({Key key, Widget child})
      : teamMembersSelectionForOthersBloc = TeamMembersSelectionForOthersBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static TeamMembersSelectionForOthersBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<
            TeamMembersSelectionForOthersBlocProvider>()
        .teamMembersSelectionForOthersBloc;
  }
}
