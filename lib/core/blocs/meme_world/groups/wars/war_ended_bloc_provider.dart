import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_ended_bloc.dart';

class WarEndedBlocProvider extends InheritedWidget {
  final WarEndedBloc warEndedBloc;

  WarEndedBlocProvider({Key key, Widget child})
      : warEndedBloc = WarEndedBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static WarEndedBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<WarEndedBlocProvider>()
        .warEndedBloc;
  }
}
