import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_leader_application_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class TeamLeadersSelectionForOthersBloc {
  final _isApplied = BehaviorSubject<bool>();
  final _isAccepted = BehaviorSubject<bool>();

  War _war;

  Stream<bool> get isApplied => _isApplied.stream;

  Function(bool) get setIsApplied => _isApplied.sink.add;

  Stream<bool> get isAccepted => _isAccepted.stream;

  Function(bool) get setIsAccepted => _isAccepted.sink.add;

  initializeTeamLeadersSelectionForOthersBloc(War war) {
    this._war = war;
    LoggerUtil.log(
        "Team Leaders Selection For Others Bloc",
        "initialize team leaders selection for others bloc",
        "warID: " + _war.warID);
  }

  getSelfWarLeaderApplication() async {
    User currentUser = await repository.getLoggedInUser();

    WarLeaderApplication warLeaderApplication =
        await repository.getTeamLeaderApplication(_war.warID, currentUser.uid);

    LoggerUtil.log(
        "Team Leaders Selection For Others Bloc",
        "self war leader application",
        warLeaderApplication?.user?.userID ?? "");

    if (warLeaderApplication != null) {
      setIsApplied(true);
      setIsAccepted(warLeaderApplication.isAccepted);
    } else {
      setIsApplied(false);
      setIsAccepted(false);
    }
  }

  applyAsTeamLeader() async {
    User user = await repository.getLoggedInUser();

    MemecistUser currentUser = await repository.getMemecistUser(user.uid);

    LoggerUtil.log("Team Leaders Selection For Others Bloc",
        "Apply As Team Leader", "user id" + user.uid);
    repository.applyForTeamLeader(currentUser, _war.warID);
    setIsApplied(true);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.team_leader_apply,
        parameters: {
          FirebaseAnalyticsConstants.mins_since_leader_app_start:
              DateTime.now().difference(_war.startDateTime).inSeconds
        });
  }

  void dispose() async {
    await _isApplied.drain();
    _isApplied.close();

    await _isAccepted.drain();
    _isAccepted.close();
  }
}
