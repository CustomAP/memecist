import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_others_bloc.dart';

class TeamLeadersSelectionForOthersBlocProvider extends InheritedWidget {
  final TeamLeadersSelectionForOthersBloc teamLeadersSelectionForOthersBloc;

  TeamLeadersSelectionForOthersBlocProvider({Key key, Widget child})
      : teamLeadersSelectionForOthersBloc = TeamLeadersSelectionForOthersBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static TeamLeadersSelectionForOthersBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<TeamLeadersSelectionForOthersBlocProvider>()
        .teamLeadersSelectionForOthersBloc;
  }
}
