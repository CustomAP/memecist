import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_application_model.dart';
import 'package:memecist/core/models/war_leader_application_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class TeamLeadersSelectionForWarCreatorBloc {
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _warApplications = BehaviorSubject<List<WarLeaderApplication>>();
  final _selectedTeamLeaders = BehaviorSubject<List<WarLeaderApplication>>();
  final _war = BehaviorSubject<War>();

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  List<WarLeaderApplication> warApplications = [];
  List<WarLeaderApplication> selectedTeamLeaders = [];

  War _currentWar;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreApplications => _hasMore.stream;

  Function(bool) get setHasMoreApplications => _hasMore.sink.add;

  Stream<List<WarLeaderApplication>> get warApplicationsList =>
      _warApplications.stream;

  Function(List<WarLeaderApplication>) get setWarApplicationsList =>
      _warApplications.sink.add;

  Stream<List<WarLeaderApplication>> get selectedTeamLeadersList =>
      _selectedTeamLeaders.stream;

  Function(List<WarLeaderApplication>) get setSelectedTeamLeadersList =>
      _selectedTeamLeaders.sink.add;

  Stream<War> get war => _war.stream;

  Function(War) get setWar => _war.sink.add;

  initializeTeamLeadersSelectionForWarCreatorBloc(War war) {
    this._currentWar = war;
    LoggerUtil.log(
        "Team leaders selection for war creator bloc",
        "initialize team leaders selection for war creator",
        "warID: " + _currentWar.warID);
  }

  Future<void> fetchTeamLeadersApplications() async {
    setLoading(true);
    List<WarLeaderApplication> newWarApplications = [];

    if (runCount == 0) {
      LoggerUtil.log("Team leaders selection for war creator bloc",
          "fetch team leaders applications", "first run");
      newWarApplications =
          await repository.getInitialTeamLeaderApplications(_currentWar.warID);
    } else {
      LoggerUtil.log("Team leaders selection for war creator bloc",
          "fetch team leaders applications", "run : " + runCount.toString());
      newWarApplications =
          await repository.getMoreTeamLeaderApplications(_currentWar.warID);
    }
    if (newWarApplications.length < docLimit) {
      LoggerUtil.log("Team leaders selection for war creator bloc",
          "fetch team leaders applications", "has more is false");
      hasMore = false;
      setHasMoreApplications(false);
    }
    runCount++;
    LoggerUtil.log(
        "Team leaders selection for war creator bloc",
        "fetch team leaders applications",
        "new added : " + newWarApplications.length.toString());

    for (WarLeaderApplication warApplication in newWarApplications)
      warApplications.add(warApplication);

    setWarApplicationsList(warApplications);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.team_leaders_applications_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  selectApplication(WarLeaderApplication warApplication) {
    int i;
    for (i = 0; i < warApplications.length; i++) {
      if (warApplications[i] == warApplication) break;
    }
    LoggerUtil.log("Team leaders selection for war creator bloc",
        "select application", "user id" + warApplication.user.userID);
    warApplication.isSelected = true;
    warApplications[i] = warApplication;
    setWarApplicationsList(warApplications);

    selectedTeamLeaders.add(warApplication);
    setSelectedTeamLeadersList(selectedTeamLeaders);

    repository.firebaseAnalytics.logEvent(
      name: FirebaseAnalyticsConstants.select_team_leader_application,
    );
  }

  unSelectApplication(WarLeaderApplication warApplication) {
    int i;
    for (i = 0; i < warApplications.length; i++) {
      if (warApplications[i] == warApplication) break;
    }
    LoggerUtil.log("Team leaders selection for war creator bloc",
        "Unselect application", "user id" + warApplication.user.userID);
    warApplication.isSelected = false;
    warApplications[i] = warApplication;
    setWarApplicationsList(warApplications);

    selectedTeamLeaders.remove(warApplication);
    setSelectedTeamLeadersList(selectedTeamLeaders);

    repository.firebaseAnalytics.logEvent(
      name: FirebaseAnalyticsConstants.unselect_team_leader_application,
    );
  }

  approveTeamLeaders() async {
    LoggerUtil.log("Team leaders selection for war creator bloc",
        "Approve team leaders", "approving..");

    repository.approveTeamLeaders(
        selectedTeamLeaders, _currentWar.warID, _currentWar.warName);

    _currentWar.areTeamLeadersSelected = true;

    selectedTeamLeaders.forEach((selectedTeamLeader) {
      for (int i = 0; i < warApplications.length; i++) {
        if (warApplications[i].user.userID == selectedTeamLeader.user.userID) {
          warApplications[i].isAccepted = true;
          break;
        }
      }
    });

    setWarApplicationsList(warApplications);
    setWar(_currentWar);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.approve_team_leaders,
        parameters: {
          FirebaseAnalyticsConstants.total: warApplications.length,
          FirebaseAnalyticsConstants.mins_since_leader_app_start:
              DateTime.now().difference(_currentWar.startDateTime).inSeconds
        });
  }

  void fetchSelectedTeamLeaders() async {
    setLoading(true);
    selectedTeamLeaders =
        await repository.getSelectedTeamLeaders(_currentWar.warID);

    LoggerUtil.log(
        "Team leaders selection for war creator bloc",
        "fetch selected team leader",
        "new added:" + selectedTeamLeaders.length.toString());

    setSelectedTeamLeadersList(selectedTeamLeaders);
    setLoading(false);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.selected_team_leaders_fetch);
  }

  void dispose() async {
    await _isLoading.drain();
    _isLoading.drain();

    await _hasMore.drain();
    _hasMore.close();

    await _warApplications.drain();
    _warApplications.close();

    await _selectedTeamLeaders.drain();
    _selectedTeamLeaders.close();

    await _war.drain();
    _war.close();
  }
}
