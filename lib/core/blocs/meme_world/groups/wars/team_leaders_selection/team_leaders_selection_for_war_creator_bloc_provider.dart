import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_war_creator_bloc.dart';

class TeamLeadersSelectionForWarCreatorBlocProvider extends InheritedWidget {
  final TeamLeadersSelectionForWarCreatorBloc teamLeadersSelectionBloc;

  TeamLeadersSelectionForWarCreatorBlocProvider({Key key, Widget child})
      : teamLeadersSelectionBloc = TeamLeadersSelectionForWarCreatorBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static TeamLeadersSelectionForWarCreatorBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<TeamLeadersSelectionForWarCreatorBlocProvider>()
        .teamLeadersSelectionBloc;
  }
}
