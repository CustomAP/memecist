import 'dart:io';
import 'dart:typed_data';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:rxdart/rxdart.dart';

class StartWarBloc {
  final _imageFile = BehaviorSubject<File>();
  final _imageUploadTask = PublishSubject<StorageUploadTask>();
  final _showLoading = BehaviorSubject<bool>();
  final _totalTeams = BehaviorSubject<int>();
  final _warID = BehaviorSubject<String>();
  final _cardColor = BehaviorSubject<String>();

  String _groupID;
  String _color = MemecistColors.card_colors[7];
  War _war;

  Stream<StorageUploadTask> get imageUploadTask => _imageUploadTask.stream;

  Function(StorageUploadTask) get setImageUploadTask =>
      _imageUploadTask.sink.add;

  Stream<File> get imageFile => _imageFile.stream;

  Function(File) get setImageFile => _imageFile.sink.add;

  Stream<bool> get showLoading => _showLoading.stream;

  Function(bool) get setShowLoading => _showLoading.sink.add;

  Stream<int> get totalTeams => _totalTeams.stream;

  Function(int) get setTotalTeams => _totalTeams.sink.add;

  Stream<String> get warID => _warID.stream;

  Function(String) get setWarID => _warID.sink.add;

  Stream<String> get cardColor => _cardColor.stream;

  Function(String) get setCardColor => _cardColor.sink.add;

  void initializeStartWarBloc(String groupID) {
    LoggerUtil.log(
        "Start War Bloc", "Initialize Start War Bloc", "group : " + groupID);

    _groupID = groupID;
  }

  startWar(File image, War war) async {
    setShowLoading(true);
    _war = war;

    //getting group again though we have group in start war screen to ensure latest snapshot
    Group group = await repository.getGroup(_groupID);
    _war.groupID = group.groupID;
    _war.groupProfilePic = group.groupProfilePic;
    _war.groupName = group.groupName;
    _war.cardColor = _color;

    if (image != null) {
      var result =
          await FlutterImageCompress.compressWithList(image.readAsBytesSync());
      LoggerUtil.log("Start War Bloc", "Start War", "image : " + image.path);
      setImageUploadTask(
          repository.uploadWarImage(Uint8List.fromList(result), _groupID));
    } else {
      uploadToDb(FirebaseStorageConstants.warIdentifierUrl);
    }
  }

  uploadToDb(String url) async {
    User user = await repository.getLoggedInUser();
    MemecistUser currentUser = await repository.getMemecistUser(user.uid);

    Map<String, dynamic> createdBy = Map();

    createdBy[FirestoreConstants.user_id] = currentUser.userID;
    createdBy[FirestoreConstants.user_name] = currentUser.userName;
    createdBy[FirestoreConstants.first_name] = currentUser.userName;
    createdBy[FirestoreConstants.last_name] = currentUser.lastName;
    createdBy[FirestoreConstants.profile_pic] = currentUser.profilePic;

    _war.createdBy = createdBy;
    _war.warPic = url;

    String warID = await repository.startWar(_war);

    LoggerUtil.log("Start War Bloc", "upload to db", "War Started : " + warID);

    setWarID(warID);
    setShowLoading(false);
    setImageUploadTask(null);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.start_war, parameters: {
      FirebaseAnalyticsConstants.war_teams_count: _war.maxTeamsCount,
      FirebaseAnalyticsConstants.card_color: _war.cardColor
    });
  }

  uploadToDbWithStorageRef(StorageReference storageReference) async {
    uploadToDb(await storageReference.getDownloadURL());
  }

  StartWarBloc() {
    _imageUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        LoggerUtil.log(
            "Start War Bloc", "initializer", "image upload task started");

        imageUploadTask.events.listen((eventReceived) {
          if (imageUploadTask.isComplete) {
            LoggerUtil.log(
                "Start War Bloc", "initializer", "image upload task complete");

            uploadToDbWithStorageRef(eventReceived.snapshot.ref);
          }
        });
      }
    });
  }

  setColor(String color) {
    setCardColor(color);
    _color = color;
  }

  void dispose() async {
    await _imageUploadTask.drain();
    _imageUploadTask.close();

    await _imageFile.drain();
    _imageFile.close();

    await _showLoading.drain();
    _showLoading.close();

    await _totalTeams.drain();
    _totalTeams.close();

    await _warID.drain();
    _warID.close();

    await _cardColor.drain();
    _cardColor.close();
  }
}
