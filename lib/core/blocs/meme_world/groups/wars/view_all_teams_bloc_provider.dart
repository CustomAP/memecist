import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/view_all_teams_bloc.dart';

class ViewAllTeamsBlocProvider extends InheritedWidget {
  final ViewAllTeamsBloc viewAllTeamsBloc;

  ViewAllTeamsBlocProvider({Key key, Widget child})
      : viewAllTeamsBloc = ViewAllTeamsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ViewAllTeamsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ViewAllTeamsBlocProvider>()
        .viewAllTeamsBloc;
  }
}
