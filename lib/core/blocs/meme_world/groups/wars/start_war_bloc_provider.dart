import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/start_war_bloc.dart';

class StartWarBlocProvider extends InheritedWidget{

  final StartWarBloc startWarBloc;

  StartWarBlocProvider({Key key, Widget child}) : startWarBloc = StartWarBloc(),
        super(key:key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static StartWarBloc of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<StartWarBlocProvider>().startWarBloc;
  }

}