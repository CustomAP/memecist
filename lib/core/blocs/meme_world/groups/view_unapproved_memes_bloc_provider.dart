import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_unapproved_memes_bloc.dart';

class ViewUnapprovedMemesBlocProvider extends InheritedWidget {
  final ViewUnapprovedMemesBloc viewUnapprovedMemesBloc;

  ViewUnapprovedMemesBlocProvider({Key key, Widget child})
      : viewUnapprovedMemesBloc = ViewUnapprovedMemesBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ViewUnapprovedMemesBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ViewUnapprovedMemesBlocProvider>()
        .viewUnapprovedMemesBloc;
  }
}
