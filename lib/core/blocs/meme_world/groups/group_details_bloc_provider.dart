import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc.dart';

class GroupDetailsBlocProvider extends InheritedWidget {
  final GroupDetailsBloc groupDetailsBloc;

  GroupDetailsBlocProvider({Key key, Widget child})
      : groupDetailsBloc = GroupDetailsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static GroupDetailsBloc of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<GroupDetailsBlocProvider>().groupDetailsBloc;
  }
}
