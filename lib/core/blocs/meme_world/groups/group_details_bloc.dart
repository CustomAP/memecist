import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_messaging_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/keywords_generator.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class GroupDetailsBloc {
  final _group = BehaviorSubject<Group>();
  final _memesList = BehaviorSubject<List<MemePost>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _member = BehaviorSubject<bool>();
  final _admin = BehaviorSubject<bool>();

  List<MemePost> memePosts = [];

  Group currentGroup;

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  String _groupID;

  Stream<Group> get group => _group.stream;

  Function(Group) get setGroup => _group.sink.add;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreMemes => _hasMore.stream;

  Function(bool) get setHasMoreMemes => _hasMore.sink.add;

  Stream<List<MemePost>> get memesList => _memesList.stream;

  Function(List<MemePost>) get setMemesList => _memesList.sink.add;

  Stream<bool> get member => _member.stream;

  Function(bool) get setMember => _member.sink.add;

  Stream<bool> get admin => _admin.stream;

  Function(bool) get setAdmin => _admin.sink.add;

  void initializeGroupDetailsBloc(String groupID) {
    LoggerUtil.log("Group Details Bloc", "Initialize", "group: " + groupID);
    _groupID = groupID;
  }

  getGroup() async {
    LoggerUtil.log("Group Details Bloc", "get group", "group: " + _groupID);
    Group group = await repository.getGroup(_groupID);
    setGroup(group);
    currentGroup = group;
  }

  Future<void> fetchGroupMemes() async {
    setLoading(true);

    User currentUser = await repository.getLoggedInUser();

    List<MemePost> newMemePosts = [];

    if (runCount == 0) {
      LoggerUtil.log("Group Details Bloc", "fetch meme posts", "first run");
      newMemePosts = await repository.getInitialMemePostsByGroup(
          docLimit, _groupID, currentUser.uid);
    } else {
      LoggerUtil.log("Group Details Bloc", "fetch meme posts",
          "run : " + runCount.toString());
      newMemePosts = await repository.getMoreMemePostsByGroup(
          docLimit, _groupID, currentUser.uid);
    }
    if (newMemePosts.length < docLimit) {
      LoggerUtil.log(
          "Group Details Bloc", "fetch meme posts", "has more is false");
      hasMore = false;
      setHasMoreMemes(false);
    }
    runCount++;
    LoggerUtil.log("Group Details Bloc", "fetch meme posts",
        "new added : " + newMemePosts.length.toString());

    for (MemePost memePost in newMemePosts) memePosts.add(memePost);

    setMemesList(memePosts);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.group_memes_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  Future<bool> isGroupAdmin() async {
    User currentUser = await repository.getLoggedInUser();
    if (await repository.isAdmin(_groupID, currentUser.uid)) {
      LoggerUtil.log("Group Details Bloc", "Is Admin", "yes");
      setAdmin(true);
      setMember(true);
      return true;
    }

    LoggerUtil.log("Group Details Bloc", "Is admin", "no");
    setAdmin(false);
    return false;
  }

  Future<void> isGroupMember() async {
    User currentUser = await repository.getLoggedInUser();
    if (await isGroupAdmin()) return;

    if (await repository.isMember(_groupID, currentUser.uid)) {
      LoggerUtil.log("Group Details Bloc", "Is Member", "yes");
      setMember(true);
      return;
    }

    LoggerUtil.log("Group Details Bloc", "Is Member", "no");
    setMember(false);
  }

  Future<void> joinGroup() async {
    User user = await repository.getLoggedInUser();
    setMember(true);

    MemecistUser currentUser = await repository.getMemecistUser(user.uid);

    currentGroup.membersCount += 1;
    setGroup(currentGroup);

    LoggerUtil.log("Group Details Bloc", "Join Group", "member now");

    repository.joinGroup(_groupID, currentUser);
    repository
        .subscribeTopic(FirebaseMessagingConstants.group + "-" + _groupID);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.join_group);
  }

  Future<void> leaveGroup(bool isAdmin) async {
    setMember(false);
    if (isAdmin) setAdmin(false);

    User currentUser = await repository.getLoggedInUser();

    LoggerUtil.log("Group Details Bloc", "Leave Group", "leaving now");
    repository.leaveGroup(currentUser.uid, _groupID, isAdmin);
    repository
        .unSubscribeTopic(FirebaseMessagingConstants.group + "-" + _groupID);

    currentGroup.membersCount -= 1;
    setGroup(currentGroup);

    if (isAdmin)
      repository.unSubscribeTopic(FirebaseMessagingConstants.group +
          "-" +
          _groupID +
          "-" +
          FirebaseMessagingConstants.admin);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.leave_group);
  }

  Future<void> getInviteLink(String groupName) async {
    String link =
        (await repository.createGroupDynamicLink(_groupID)).toString();

    LoggerUtil.log("Group Details Bloc", "Get Invite Link", "link : $link");

    Share.text('Share', "Join $groupName now:\n $link", "text/plain");

    repository.firebaseAnalytics.logShare(
        contentType: FirebaseAnalyticsConstants.group,
        itemId: _groupID,
        method: FirebaseAnalyticsConstants.copy_group_link_button);
  }

  Future<void> updateGroupMembersCount(String groupID) async {
    try {
      int runCount = await repository.getGroupOpenCount(groupID);
      int updateAtRunCount =
          await repository.getToUpdateGroupMembersCountAtRun();

      if (updateAtRunCount == 0) {
        await repository.updateGroupMembersCount(groupID);
      } else if (runCount % updateAtRunCount == 0) {
        await repository.updateGroupMembersCount(groupID);
      }

      runCount++;
      await repository.setGroupOpenCount(groupID, runCount);
    } catch (e) {
      print(e);
    }

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.update_group_members_count);
  }

  deletePost(MemePost memePost) async {
    repository.deletePost(memePost.postID);
    for (MemePost post in memePosts) {
      if (post.postID == memePost.postID) {
        memePosts.remove(post);
        break;
      }
    }
    setMemesList(memePosts);

    repository.firebaseAnalytics.logEvent(name: FirebaseAnalyticsConstants.delete_post);
  }

  void dispose() async {
    await _group.drain();
    _group.close();

    await _memesList.drain();
    _memesList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();

    await _member.drain();
    _member.close();

    await _admin.drain();
    _admin.close();
  }
}
