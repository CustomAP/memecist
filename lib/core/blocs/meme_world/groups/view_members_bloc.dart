import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_member_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/keywords_generator.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class ViewMembersBloc {
  final _membersList = PublishSubject<List<GroupMember>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _isAdmin = BehaviorSubject<bool>();

  List<GroupMember> members = [];

  String _groupID;

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreMembers => _hasMore.stream;

  Function(bool) get setHasMoreMembers => _hasMore.sink.add;

  Stream<List<GroupMember>> get membersList => _membersList.stream;

  Function(List<GroupMember>) get setMembersList => _membersList.sink.add;

  Stream<bool> get isAdmin => _isAdmin.stream;

  Function(bool) get setIsAdmin => _isAdmin.sink.add;

  initializeViewMembersBloc(String groupID) {
    _groupID = groupID;
    LoggerUtil.log("View Members Bloc", "Initialize", "group: " + _groupID);
  }

  Future<void> remove(GroupMember member) async {
    LoggerUtil.log(
        "View Members Bloc", "Remove member", "member: " + member.user.userID);

    User currentUser = await repository.getLoggedInUser();

    repository.removeMember(currentUser.uid, member, _groupID);
    members.remove(member);
    setMembersList(members);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.remove_member);
  }

  Future<void> fetchMembers() async {
    setLoading(true);
    List<GroupMember> newMembers = [];
    User currentUser = await repository.getLoggedInUser();

    if (runCount == 0) {
      List<GroupMember> newlyReceivedAdmins = [];

      newlyReceivedAdmins = await repository.getAdmins(_groupID).then((value) {
        return value.map((admin) => GroupMember(admin, true)).toList();
      });

      List<GroupMember> newlyReceivedMembers = [];

      LoggerUtil.log("View Members Bloc", "fetch members", "first run");
      newlyReceivedMembers =
          await repository.getInitialMembers(_groupID, docLimit).then((value) {
        return value.map((member) => GroupMember(member, false)).toList();
      });

      newlyReceivedAdmins.forEach((admin) {
        newMembers.add(admin);
        if (admin.user.userID == currentUser.uid) {
          LoggerUtil.log("View Members Bloc", "fetch members", "MemecistUser is admin");
          setIsAdmin(true);
        }
      });

      newlyReceivedMembers.forEach((member) {
        newMembers.add(member);
      });
    } else {
      LoggerUtil.log(
          "View Members Bloc", "fetch members", "run : " + runCount.toString());
      newMembers =
          await repository.getMoreMembers(_groupID, docLimit).then((value) {
        return value.map((member) => GroupMember(member, false)).toList();
      });
    }
    if (newMembers.length < docLimit) {
      LoggerUtil.log("View Members Bloc", "fetch members", "has more is false");
      hasMore = false;
      setHasMoreMembers(false);
    }
    runCount++;
    LoggerUtil.log("View Members Bloc", "fetch members",
        "new added : " + newMembers.length.toString());

    for (GroupMember member in newMembers) members.add(member);

    setMembersList(members);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.group_members_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  Future<void> makeAdmin(GroupMember member) async {
    LoggerUtil.log(
        "View Members Bloc", "Make admin", "userID : " + member.user.userID);

    repository.makeAdmin(_groupID, member.user);

    members[members.indexOf(member)].isAdmin = true;

    setMembersList(members);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.make_admin);
  }

  searchMembers(String query) async {
    LoggerUtil.log("View Members Bloc", "search members", "query: " + query);

    List<GroupMember> members = await repository
        .searchMembers(_groupID, query)
        .then(
            (users) => users.map((user) => GroupMember(user, false)).toList());

    setMembersList(members);

    repository.firebaseAnalytics
        .logSearch(searchTerm: query, origin: FirebaseAnalyticsConstants.group);
  }

  void dispose() async {
    await _membersList.drain();
    _membersList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();

    await _isAdmin.drain();
    _isAdmin.close();
  }
}
