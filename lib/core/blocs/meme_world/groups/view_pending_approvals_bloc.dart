import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class ViewPendingApprovalsBloc {
  final _memePosts = PublishSubject<List<MemePost>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();

  List<MemePost> memePosts = [];

  String _groupID;

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreMemes => _hasMore.stream;

  Function(bool) get setHasMoreMemes => _hasMore.sink.add;

  Stream<List<MemePost>> get memePostList => _memePosts.stream;

  Function(List<MemePost>) get setMemePostList => _memePosts.sink.add;

  initializeViewPendingApprovalMemes(String groupID) {
    _groupID = groupID;
    LoggerUtil.log(
        "View Pending Approvals Bloc", "Initialize", "groupID : " + groupID);
  }

  Future<void> fetchUnapprovedMemes() async {
    setLoading(true);
    List<MemePost> newPosts = [];

    User currentUser = await repository.getLoggedInUser();

    if (runCount == 0) {
      LoggerUtil.log(
          "View Pending Approvals Bloc", "fetch reacts", "first run");
      newPosts = await repository.getInitialPendingApprovalMemes(
          docLimit, _groupID, currentUser.uid);
    } else {
      LoggerUtil.log("View Pending Approvals Bloc", "fetch memes",
          "run : " + runCount.toString());
      newPosts = await repository.getMorePendingApprovalMemes(
          docLimit, _groupID, currentUser.uid);
    }
    if (newPosts.length < docLimit) {
      LoggerUtil.log(
          "View Pending Approvals Bloc", "fetch memes", "has more is false");
      hasMore = false;
      setHasMoreMemes(false);
    }
    runCount++;
    LoggerUtil.log("View Pending Approvals Bloc", "fetch memes",
        "new added : " + newPosts.length.toString());

    for (MemePost post in newPosts) memePosts.add(post);

    setMemePostList(memePosts);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.pending_unapproved_memes_fetch);
  }

  void dispose() async {
    await _memePosts.drain();
    _memePosts.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();
  }
}
