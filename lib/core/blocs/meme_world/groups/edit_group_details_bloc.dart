import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_messaging_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/keywords_generator.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class EditGroupDetailsBloc {
  final _imageFile = BehaviorSubject<File>();
  final _imageUploadTask = PublishSubject<StorageUploadTask>();
  final _showLoading = BehaviorSubject<bool>();
  final _isGroupEdited = BehaviorSubject<bool>();
  final _canEditGroupName = BehaviorSubject<bool>();

  String groupID;
  String newGroupName, newGroupDescription;
  Group group;

  Stream<StorageUploadTask> get imageUploadTask => _imageUploadTask.stream;

  Function(StorageUploadTask) get setImageUploadTask =>
      _imageUploadTask.sink.add;

  Stream<File> get imageFile => _imageFile.stream;

  Function(File) get setImageFile => _imageFile.sink.add;

  Stream<bool> get showLoading => _showLoading.stream;

  Function(bool) get setShowLoading => _showLoading.sink.add;

  Stream<bool> get isGroupEdited => _isGroupEdited.stream;

  Function(bool) get setIsGroupEdited => _isGroupEdited.sink.add;

  Stream<bool> get canEditGroupName => _canEditGroupName.stream;

  Function(bool) get setCanEditGroupName => _canEditGroupName.sink.add;

  editGroupDetails(File image, String groupName, String groupDescription,
      String groupID) async {
    setShowLoading(true);
    User currentUser = await repository.getLoggedInUser();

    this.groupID = groupID;

    if(groupName != null) {
      if (await repository.getCanChangeGroupName(groupID)) {
        newGroupName = groupName;
        newGroupDescription = groupDescription;

        if (image != null) {
          LoggerUtil.log(
              "Edit Group Details Bloc", "edit Group details bloc",
              "image : " + image.path);

          var result = await FlutterImageCompress.compressWithList(
              image.readAsBytesSync());

          setImageUploadTask(repository.uploadGroupImage(
              Uint8List.fromList(result), currentUser.uid));
        } else {
          uploadToDb(null);
        }
      } else {
        setCanEditGroupName(false);
        setShowLoading(false);
      }
    } else {
      newGroupDescription = groupDescription;

      if (image != null) {
        LoggerUtil.log(
            "Edit Group Details Bloc", "edit Group details bloc",
            "image : " + image.path);

        var result = await FlutterImageCompress.compressWithList(
            image.readAsBytesSync());

        setImageUploadTask(repository.uploadGroupImage(
            Uint8List.fromList(result), currentUser.uid));
      } else {
        uploadToDb(null);
      }
    }
  }

  uploadToDb(String imageUrl) async {
    List<String> groupSearchKeywords;

    if (newGroupName != null)
      groupSearchKeywords =
          KeywordsGenerator.generateKeywordsForGroups(newGroupName);

    bool result = await repository.editGroupDetails(groupID, newGroupName,
        newGroupDescription, imageUrl, groupSearchKeywords);

    LoggerUtil.log("Edit Group Details Bloc", "edit group details",
        "group details edited");
    setIsGroupEdited(result);
    setShowLoading(false);
    setImageUploadTask(null);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.group_details_edit);
  }

  uploadToDbWithStorageRef(StorageReference storageReference) async {
    uploadToDb(await storageReference.getDownloadURL());
  }

  EditGroupDetailsBloc() {
    _imageUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        LoggerUtil.log("Edit Group Details Bloc", "initializer",
            "image upload task started");

        imageUploadTask.events.listen((eventReceived) {
          if (imageUploadTask.isComplete) {
            LoggerUtil.log("Edit Group Details Bloc", "initializer",
                "image upload task complete");

            uploadToDbWithStorageRef(eventReceived.snapshot.ref);
          }
        });
      }
    });
  }

  void dispose() async {
    await _imageUploadTask.drain();
    _imageUploadTask.close();

    await _imageFile.drain();
    _imageFile.close();

    await _showLoading.drain();
    _showLoading.close();

    await _isGroupEdited.drain();
    _isGroupEdited.close();

    await _canEditGroupName.drain();
    _canEditGroupName.close();
  }
}
