import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/edit_group_details_bloc.dart';

class EditGroupDetailsBlocProvider extends InheritedWidget {
  final EditGroupDetailsBloc editGroupDetailsBloc;

  EditGroupDetailsBlocProvider({Key key, Widget child})
      : editGroupDetailsBloc = EditGroupDetailsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static EditGroupDetailsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<EditGroupDetailsBlocProvider>()
        .editGroupDetailsBloc;
  }
}
