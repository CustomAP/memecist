import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:memecist/core/models/comments_model.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MemePostDetailBloc {
  final _commentList = BehaviorSubject<List<Comment>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _memePost = BehaviorSubject<MemePost>();

  List<Comment> comments = [];

  MemePost _currentMemePost;

  bool hasMore = true;
  int docLimit = 25;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreComments => _hasMore.stream;

  Function(bool) get setHasMoreComments => _hasMore.sink.add;

  Stream<List<Comment>> get commentList => _commentList.stream;

  Function(List<Comment>) get setCommentList => _commentList.sink.add;

  Stream<MemePost> get memePost => _memePost.stream;

  Function(MemePost) get setMemePost => _memePost.sink.add;

  initializeMemePostDetailBloc(MemePost memePost) {
    _currentMemePost = memePost;
    LoggerUtil.log("Meme Post Detail Bloc", "Initialize",
        " memePost: " + _currentMemePost.postID);
    setMemePost(memePost);
  }

  Future<void> fetchMemePost(String postID) async {
    try {
      User user = await repository.getLoggedInUser();
      MemePost memePost = await repository.getMemePost(postID, user.uid);
      initializeMemePostDetailBloc(memePost);

      repository.firebaseAnalytics
          .logEvent(name: FirebaseAnalyticsConstants.fetch_meme_post);
    } catch (e) {
      Fluttertoast.showToast(msg: "Some error occured");
      print(e);
    }
  }

  Future<void> fetchComments() async {
    setLoading(true);
    List<Comment> newComments = [];

    if (runCount == 0) {
      LoggerUtil.log("Meme Post Detail Bloc", "fetch comments", "first run");
      newComments = await repository.getInitialCommentsForPost(
          _currentMemePost.postID, docLimit);
    } else {
      LoggerUtil.log("Meme Post Detail Bloc", "fetch comments",
          "run : " + runCount.toString());
      newComments = await repository.getMoreCommentsForPost(
          _currentMemePost.postID, docLimit);
    }
    if (newComments.length < docLimit) {
      LoggerUtil.log(
          "Meme Post Detail Bloc", "fetch comments", "has more is false");
      hasMore = false;
      setHasMoreComments(false);
    }
    runCount++;
    LoggerUtil.log("Meme Post Detail Bloc", "fetch comments",
        "new added : " + newComments.length.toString());

    for (Comment comment in newComments) comments.add(comment);

    setCommentList(comments);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.comments_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  Future<void> comment(String comment) async {
    LoggerUtil.log("Meme Post Detail Bloc", "comment", "comment: " + comment);

    User currentUser = await repository.getLoggedInUser();

    MemecistUser user = await repository.getMemecistUser(currentUser.uid);
    String commentID =
        await repository.comment(user, _currentMemePost.postID, comment);
    comments.add(Comment(comment, DateTime.now(), user, commentID));

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.comment,
        parameters: {
          FirebaseAnalyticsConstants.comment_length: comment.length.toString()
        });

    setCommentList(comments);
  }

  Future<void> deleteComment(Comment comment) async {
    await repository.deleteComment(_currentMemePost.postID, comment.commentID);
    comments.remove(comment);
    setCommentList(comments);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_comment);
  }

  void dispose() async {
    await _commentList.drain();
    _commentList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();

    await _memePost.drain();
    _memePost.close();
  }
}
