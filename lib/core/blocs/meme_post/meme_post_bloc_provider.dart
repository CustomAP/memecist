import 'package:memecist/core/blocs/meme_post/meme_post_bloc.dart';
import 'package:flutter/material.dart';

class MemePostBlocProvider extends InheritedWidget {
  final MemePostBloc memePostBloc;

  MemePostBlocProvider({Key key, Widget child})
      : memePostBloc = MemePostBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static MemePostBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<MemePostBlocProvider>()
        .memePostBloc;
  }
}
