import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/image_resizer_for_instagram.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:ui' as ui;

enum ExternalLinksUpdateStatus { Updating, Complete }

class AddLinksForCompetitionBloc {
  final _externalLinksUpdateStatus =
  PublishSubject<ExternalLinksUpdateStatus>();
  final _memePost = BehaviorSubject<MemePost>();
  final _padding = BehaviorSubject<double>();
  final _sizedBoxHeight = BehaviorSubject<double>();

  String _competitionID;
  String _postID;

  Stream<ExternalLinksUpdateStatus> get externalLinksUpdateStatus =>
      _externalLinksUpdateStatus.stream;

  Function(ExternalLinksUpdateStatus) get setExternalLinksUpdateStatus =>
      _externalLinksUpdateStatus.sink.add;

  Stream<MemePost> get memePost => _memePost.stream;

  Function(MemePost) get setMemePost => _memePost.sink.add;

  Stream<double> get padding => _padding.stream;

  Function(double) get setPadding => _padding.sink.add;

  Stream<double> get sizedBoxHeight => _sizedBoxHeight.stream;

  Function(double) get setSizedBoxHeight => _sizedBoxHeight.sink.add;

  void initializeAddLinksForCompetitionBloc(String competitionID,
      String postID) {
    _competitionID = competitionID;
    _postID = postID;
    LoggerUtil.log("Add Links For Competition Bloc", "Initialize",
        "competitionID : " + _competitionID);
  }

  getMemePost(String postID) async {
    User user = await repository.getLoggedInUser();
    MemePost memePost = await repository.getMemePost(postID, user.uid);
    setMemePost(memePost);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.fetch_meme_post);
  }

  submitLinks(String link1, String link2, String link3) async {
    setExternalLinksUpdateStatus(ExternalLinksUpdateStatus.Updating);
    await repository.updateLinksForCompetitionMemePost(
        _postID, link1, link2, link3);
    setExternalLinksUpdateStatus(ExternalLinksUpdateStatus.Complete);

    int linksCount = 0;
    if (link1.trim() != "") {
      linksCount++;
    }
    if (link2.trim() != "") {
      linksCount++;
    }
    if (link3.trim() != "") {
      linksCount++;
    }

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.links_submit_for_competition,
        parameters: {
          FirebaseAnalyticsConstants.links_count: linksCount,
          FirebaseAnalyticsConstants.is_memecist_social : false
        });
  }

  sharePost(GlobalKey key, MemePost memePost) async {
    try {
      return new Future.delayed(const Duration(milliseconds: 20), () async {
        RenderRepaintBoundary boundary = key.currentContext.findRenderObject();
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);
        ByteData byteData =
        await image.toByteData(format: ui.ImageByteFormat.png);

        String link = (await repository.createPostDynamicLink(memePost.postID))
            .toString();
        LoggerUtil.log("Create Meme Post Bloc", "Share image", "link : $link");

        repository.firebaseAnalytics.logShare(
            contentType: FirebaseAnalyticsConstants.meme_post_item,
            itemId: memePost.postID,
            method: FirebaseAnalyticsConstants.add_links_for_comp_share_button);

        repository.sharePost(memePost.postID, link);

        await Share.file('Share', 'post.png',
            byteData.buffer.asUint8List(), 'image/png',
            text:
            'Meme by ${memePost.user.firstName} ${memePost.user
                .lastName}:\n $link');
      });
    } catch (e) {
      print(e);
    }
  }

  resizeImage(GlobalKey key) {
    List<double> sizes = ImageReSizerForInstagram.sizesForInstagram(key);

    setPadding(sizes[0]);
    setSizedBoxHeight(sizes[1]);

    repository.firebaseAnalytics.logEvent(name: FirebaseAnalyticsConstants.resize_image, parameters: {
      FirebaseAnalyticsConstants.padding : sizes[0],
      FirebaseAnalyticsConstants.sized_box_height : sizes[1]
    });
  }

  void dispose() async {
    await _externalLinksUpdateStatus.drain();
    _externalLinksUpdateStatus.close();

    await _memePost.drain();
    _memePost.close();

    await _padding.drain();
    _padding.close();

    await _sizedBoxHeight.drain();
    _sizedBoxHeight.close();
  }
}
