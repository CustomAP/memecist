import 'package:memecist/core/blocs/meme_post/create_meme_post_bloc.dart';
import 'package:flutter/material.dart';

class CreateMemePostBlocProvider extends InheritedWidget {
  final CreateMemePostBloc createMemePostBloc;

  CreateMemePostBlocProvider({Key key, Widget child})
      : createMemePostBloc = CreateMemePostBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static CreateMemePostBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<CreateMemePostBlocProvider>()
        .createMemePostBloc;
  }
}
