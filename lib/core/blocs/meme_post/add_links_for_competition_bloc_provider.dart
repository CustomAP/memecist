import 'package:memecist/core/blocs/meme_post/add_links_for_competition_bloc.dart';
import 'package:flutter/material.dart';

class AddLinksForCompetitionBlocProvider extends InheritedWidget {
  final AddLinksForCompetitionBloc addLinksForCompetitionBloc;

  AddLinksForCompetitionBlocProvider({Key key, Widget child})
      : addLinksForCompetitionBloc = AddLinksForCompetitionBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static AddLinksForCompetitionBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<
            AddLinksForCompetitionBlocProvider>()
        .addLinksForCompetitionBloc;
  }
}
