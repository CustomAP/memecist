import 'package:memecist/core/blocs/meme_post/view_reacts_bloc.dart';
import 'package:flutter/material.dart';

class ViewReactsBlocProvider extends InheritedWidget {
  final ViewReactsBloc viewReactsBloc;

  ViewReactsBlocProvider({Key key, Widget child})
      : viewReactsBloc = ViewReactsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ViewReactsBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ViewReactsBlocProvider>()
        .viewReactsBloc;
  }
}
