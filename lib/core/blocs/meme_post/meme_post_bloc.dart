import 'dart:typed_data';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/utils/image_resizer_for_instagram.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/resources/repository.dart';
import 'dart:ui' as ui;

class MemePostBloc {
  MemePost _memePost;

  final _react = PublishSubject<bool>();
  final _showMemecist = BehaviorSubject<bool>();
  final _competition = BehaviorSubject<Competition>();
  final _loading = BehaviorSubject<bool>();
  final _showMentionDetailsDialog = BehaviorSubject<bool>();
  final _meme = BehaviorSubject<MemePost>();
  final _sizedBoxHeight = BehaviorSubject<double>();
  final _padding = BehaviorSubject<double>();

  Stream<bool> get react => _react.stream;

  Function(bool) get setReact => _react.sink.add;

  Stream<bool> get showMemecist => _showMemecist.stream;

  Function(bool) get setShowMemecist => _showMemecist.sink.add;

  Stream<Competition> get competition => _competition.stream;

  Function(Competition) get setCompetition => _competition.sink.add;

  Stream<bool> get loading => _loading.stream;

  Function(bool) get setLoading => _loading.sink.add;

  Stream<bool> get showMentionDetailsDialog => _showMentionDetailsDialog.stream;

  Function(bool) get setShowMentionDetailsDialog =>
      _showMentionDetailsDialog.sink.add;

  Stream<MemePost> get memePost => _meme.stream;

  Function(MemePost) get setMemePost => _meme.sink.add;

  Stream<double> get sizedBoxHeight => _sizedBoxHeight.stream;

  Function(double) get setSizedBoxHeight => _sizedBoxHeight.sink.add;

  Stream<double> get padding => _padding.stream;

  Function(double) get setPadding => _padding.sink.add;

  initializeBloc(MemePost memePost) {
    _memePost = memePost;
  }

  reactUnreactToPost(bool toReact) async {
    if (toReact) {
      reactToPost();
    } else {
      unReactToPost();
    }
  }

  unReactToPost() async {
    _memePost.reactCount -= 1;
    setMemePost(_memePost);
    setReact(false);
    User user = await repository.getLoggedInUser();

    MemecistUser currentUser = await repository.getMemecistUser(user.uid);
    bool isReacted = await repository.isReacted(user.uid, _memePost.postID);

    if (!isReacted) {
      LoggerUtil.log("Meme Post Bloc", "unReact to post",
          _memePost.postID + " already unReacted");
      return;
    }

    LoggerUtil.log(
        "Meme Post Bloc", "unReact to post", _memePost.postID + " unreacting");
    repository.toUnReact(currentUser, _memePost.postID);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.unreact_post);
  }

  reactToPost() async {
    _memePost.reactCount += 1;
    setMemePost(_memePost);
    setReact(true);
    User user = await repository.getLoggedInUser();

    MemecistUser currentUser = await repository.getMemecistUser(user.uid);
    bool isReacted = await repository.isReacted(user.uid, _memePost.postID);

    if (isReacted) {
      LoggerUtil.log("Meme Post Bloc", "React to post",
          _memePost.postID + " already reacted");
      return;
    }

    LoggerUtil.log(
        "Meme Post Bloc", "React to post", _memePost.postID + " reacting");
    repository.toReact(currentUser, _memePost.postID);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.react_post);
  }

  shareImage(GlobalKey globalKey) async {
    setShowMemecist(true);
    try {
      return new Future.delayed(const Duration(milliseconds: 20), () async {
        RenderRepaintBoundary boundary =
            globalKey.currentContext.findRenderObject();
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);
        ByteData byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);
        setShowMemecist(false);

        String link = (await repository.createPostDynamicLink(_memePost.postID))
            .toString();
        LoggerUtil.log("Meme Post Bloc", "Share image", "link : $link");

        repository.firebaseAnalytics.logShare(
            contentType: FirebaseAnalyticsConstants.meme_post_item,
            itemId: _memePost.postID,
            method: FirebaseAnalyticsConstants.meme_post_share_button);

        repository.sharePost(_memePost.postID, link);

        await Share.file('Share', 'post.png',
            byteData.buffer.asUint8List(), 'image/png',
            text:
                'Meme by ${_memePost.user.firstName} ${_memePost.user.lastName}:\n $link');
      });
    } catch (e) {
      setShowMemecist(false);
      print(e);
    }
  }

  getCompetition(String competitionID) async {
    setLoading(true);
    Competition competition = await repository.getCompetition(competitionID);

    LoggerUtil.log(
        "Meme Post Bloc", "get competition", "competitionID : $competitionID");

    setCompetition(competition);
    setLoading(false);
  }

  mention(String postID) async {
    bool isLoggedInViaPhoneNum = await repository.isLoggedInViaPhone();

    if (!isLoggedInViaPhoneNum) {
      LoggerUtil.log("Meme Post Bloc", "mention", "not logged in via phone");
      setShowMentionDetailsDialog(true);
    } else {
      LoggerUtil.log("Meme Post Bloc", "mention", "logged in via phone");
    }
  }

  reportMeme(String reason) async {
    User user = await repository.getLoggedInUser();
    repository.reportMeme(user.uid, _memePost.postID, reason);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.report_meme);
  }

  resizeImage(GlobalKey key) {
    List<double> sizes = ImageReSizerForInstagram.sizesForInstagram(key);

    setPadding(sizes[0]);
    setSizedBoxHeight(sizes[1]);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.resize_image, parameters: {
      FirebaseAnalyticsConstants.padding: sizes[0],
      FirebaseAnalyticsConstants.sized_box_height: sizes[1]
    });
  }

  void dispose() async {
    await _react.drain();
    _react.close();

    await _showMemecist.drain();
    _showMemecist.close();

    await _competition.drain();
    _competition.close();

    await _loading.drain();
    _loading.close();

    await _showMentionDetailsDialog.drain();
    _showMentionDetailsDialog.close();

    await _meme.drain();
    _meme.close();

    await _sizedBoxHeight.drain();
    _sizedBoxHeight.close();

    await _padding.drain();
    _padding.close();
  }
}
