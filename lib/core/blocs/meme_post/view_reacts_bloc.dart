import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class ViewReactsBloc {
  final _usersList = PublishSubject<List<MemecistUser>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();

  List<MemecistUser> users = [];

  MemePost _memePost;

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreReacts => _hasMore.stream;

  Function(bool) get setHasMoreReacts => _hasMore.sink.add;

  Stream<List<MemecistUser>> get usersList => _usersList.stream;

  Function(List<MemecistUser>) get setUserList => _usersList.sink.add;

  initializeViewReactsBloc(MemePost memePost) {
    _memePost = memePost;
    LoggerUtil.log(
        "View Reacts Bloc", "Initialize", " memePost: " + _memePost.postID);
  }

  Future<void> fetchReacts() async {
    setLoading(true);
    List<MemecistUser> newUsers = [];

    if (runCount == 0) {
      LoggerUtil.log("View Reacts Bloc", "fetch reacts", "first run");
      newUsers = await repository.getInitialReacts(_memePost.postID, docLimit);
    } else {
      LoggerUtil.log(
          "View Reacts Bloc", "fetch reacts", "run : " + runCount.toString());
      newUsers = await repository.getMoreReacts(_memePost.postID, docLimit);
    }
    if (newUsers.length < docLimit) {
      LoggerUtil.log("View Reacts Bloc", "fetch reacts", "has more is false");
      hasMore = false;
      setHasMoreReacts(false);
    }
    runCount++;
    LoggerUtil.log("View Reacts Bloc", "fetch reacts",
        "new added : " + newUsers.length.toString());

    for (MemecistUser user in newUsers) users.add(user);

    setUserList(users);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.reacts_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  void dispose() async {
    await _usersList.drain();
    _usersList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();
  }
}
