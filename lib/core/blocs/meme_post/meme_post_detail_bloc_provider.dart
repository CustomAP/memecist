import 'package:memecist/core/blocs/meme_post/meme_post_detail_bloc.dart';
import 'package:flutter/material.dart';

class MemePostDetailBlocProvider extends InheritedWidget {
  final MemePostDetailBloc memePostDetailBloc;

  MemePostDetailBlocProvider({Key key, Widget child})
      : memePostDetailBloc = MemePostDetailBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static MemePostDetailBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<MemePostDetailBlocProvider>()
        .memePostDetailBloc;
  }
}
