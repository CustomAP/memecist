import 'dart:io';
import 'dart:typed_data';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/ip_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/promo_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/image_resizer_for_instagram.dart';
import 'package:memecist/core/utils/keywords_generator.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:ui' as ui;
import 'package:image/image.dart';

enum MemePostStatus { Uploading, Complete }

class CreateMemePostBloc {
  final _imageFile = BehaviorSubject<File>();
  final _imageUploadTask = PublishSubject<StorageUploadTask>();
  final _memePostStatus = PublishSubject<MemePostStatus>();
  final _imageUploadProgress = PublishSubject<double>();
  final _showPromoBar = BehaviorSubject<bool>();
  final _promo = BehaviorSubject<Promo>();
  final _post = BehaviorSubject<String>();
  final _categoryOption = BehaviorSubject<String>();
  final _memePost = BehaviorSubject<MemePost>();
  final _padding = BehaviorSubject<double>();
  final _sizedBoxHeight = BehaviorSubject<double>();
  final _memecistSocialCompetition = BehaviorSubject<Competition>();

  String _caption, _category;
  String _groupID, _competitionID, _warID, _teamID;
  bool _isForWar = false, _isForGroup = false, _isForCompetition = false;
  String _groupName, _warName, _competitionName, _teamName;
  bool _isGroupAdmin;
  String _postID;

  List<String> _hashtags;

  String _imageText = "";
  bool _isImageTextExtracted = true;

  MemecistUser _currentUser;
  Promo promoBar;

  Stream<StorageUploadTask> get imageUploadTask => _imageUploadTask.stream;

  Function(StorageUploadTask) get setImageUploadTask =>
      _imageUploadTask.sink.add;

  Stream<File> get imageFile => _imageFile.stream;

  Function(File) get setImageFile => _imageFile.sink.add;

  Stream<MemePostStatus> get memePostStatus => _memePostStatus.stream;

  Function(MemePostStatus) get setMemePostStatus => _memePostStatus.sink.add;

  Stream<double> get imageUploadProgress => _imageUploadProgress.stream;

  Function(double) get setImageUploadProgress => _imageUploadProgress.sink.add;

  Stream<String> get postID => _post.stream;

  Function(String) get setPostID => _post.sink.add;

  Stream<bool> get showPromoBar => _showPromoBar.stream;

  Function(bool) get setShowPromoBar => _showPromoBar.sink.add;

  Stream<Promo> get promo => _promo.stream;

  Function(Promo) get setPromo => _promo.sink.add;

  Stream<String> get category => _categoryOption.stream;

  Function(String) get setCategory => _categoryOption.sink.add;

  Stream<MemePost> get memePost => _memePost.stream;

  Function(MemePost) get setMemePost => _memePost.sink.add;

  Stream<double> get padding => _padding.stream;

  Function(double) get setPadding => _padding.sink.add;

  Stream<double> get sizedBoxHeight => _sizedBoxHeight.stream;

  Function(double) get setSizedBoxHeight => _sizedBoxHeight.sink.add;

  Stream<Competition> get memecistSocialCompetition =>
      _memecistSocialCompetition.stream;

  Function(Competition) get setMemecistSocialCompetition =>
      _memecistSocialCompetition.sink.add;

  void initializeCreateMemePostBloc(
      {String groupID,
      String competitionID,
      String warID,
      String groupName,
      String warName,
      String competitionName,
      bool isGroupAdmin,
      String teamName,
      String teamID}) {
    if (groupID != null) {
      _groupID = groupID;
      _groupName = groupName;
      _isGroupAdmin = isGroupAdmin;
      _isForGroup = true;
      LoggerUtil.log(
          "Create Meme Post Bloc",
          "Initialize Create Meme Post Bloc",
          "groupID : " + _groupID + " isAdmin: " + _isGroupAdmin.toString());
    }

    if (competitionID != null) {
      _competitionID = competitionID;
      _isForCompetition = true;
      _competitionName = competitionName;
      LoggerUtil.log(
          "Create Meme Post Bloc",
          "Initialize Create Meme Post Bloc",
          "competitionID : " + _competitionID);
    }

    if (warID != null) {
      _warID = warID;
      _isForWar = true;
      _warName = warName;
      _teamName = teamName;
      _teamID = teamID;
      LoggerUtil.log("Create Meme Post Bloc",
          "Initialize Create Meme Post Bloc", "warID : " + _warID);
    }
  }

  getPromos() async {
    User user = await repository.getLoggedInUser();
    promoBar = await repository.getPromos(user.uid);
    LoggerUtil.log(
        "Create Meme Post Bloc", "get promos", "userID : " + user.uid);
    setPromo(promoBar);
    if (promoBar.isSet) setShowPromoBar(true);
  }

  savePromos(Promo promo) async {
    User user = await repository.getLoggedInUser();
    MemecistUser currentUser = await repository.getMemecistUser(user.uid);
    promo.memecistID = currentUser.userName;
    repository.savePromos(user.uid, promo);
    LoggerUtil.log(
        "Create Meme Post Bloc", "set promos", "userID : " + user.uid);
    setPromo(promo);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.promo_update);
  }

  uploadPost(
      GlobalKey key, String caption, String category, File imageFile) async {
    setMemePostStatus(MemePostStatus.Uploading);

    _imageText = "";
    _isImageTextExtracted = true;

    try {
      FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(imageFile);
      TextRecognizer textRecognizer = FirebaseVision.instance.textRecognizer();
      VisionText visionText = await textRecognizer.processImage(visionImage);
      _imageText = visionText.text;
    } catch (e) {
      _isImageTextExtracted = false;
      print(e);
    }

    _caption = caption ?? "";

    _hashtags = extractHashtags(_caption);

    _category = category;

    Future.delayed(const Duration(milliseconds: 20), () async {
      RenderRepaintBoundary boundary = key.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);

      Uint8List data = byteData.buffer.asUint8List();

      var result = await FlutterImageCompress.compressWithList(
        data.toList(),
        quality: 50,
      );

      LoggerUtil.log("Create Meme Post Bloc", "Upload post", "image");

      User currentUser = await repository.getLoggedInUser();

      setImageUploadTask(repository.uploadMemeImage(
          Uint8List.fromList(result), currentUser.uid));
    });
  }

  submitLinks(String link1, String link2) async {
    await repository.updateLinksForMemecistSocial(_postID, link1, link2);

    int linksCount = 0;
    if (link1.trim() != "") {
      linksCount++;
    }
    if (link2.trim() != "") {
      linksCount++;
    }

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.links_submit_for_competition,
        parameters: {
          FirebaseAnalyticsConstants.links_count: linksCount,
          FirebaseAnalyticsConstants.is_memecist_social: true
        });
  }

  postMeme(StorageReference imageReference) async {
    String url = await imageReference.getDownloadURL();

    User user = await repository.getLoggedInUser();
    _currentUser = await repository.getMemecistUser(user.uid);

    List<String> searchKeywords =
        KeywordsGenerator.generateKeywordsForPosts(_caption, _imageText);

    MemePost memePost = MemePost(
        mediaUrl: url,
        user: MemecistUser(
          userName: _currentUser.userName,
          firstName: _currentUser.firstName,
          lastName: _currentUser.lastName,
          userID: _currentUser.userID,
          profilePic: _currentUser.profilePic,
          isVerified: _currentUser.isVerified,
        ),
        reactCount: 0,
        commentsCount: 0,
        postType: FirestoreConstants.image,
        caption: _caption,
        category: _category,
        isForGroup: _isForGroup,
        isForCompetition: _isForCompetition,
        isForWar: _isForWar,
        imageText: _imageText,
        isImageTextExtracted: _isImageTextExtracted,
        searchKeywords: searchKeywords,
        hashtags: _hashtags);

    Map<String, dynamic> firebaseAnalyticsParams = Map();

    firebaseAnalyticsParams[FirebaseAnalyticsConstants.post_for] =
        FirebaseAnalyticsConstants.regular_post;
    firebaseAnalyticsParams[FirebaseAnalyticsConstants.post_type] =
        memePost.postType;

    firebaseAnalyticsParams[FirebaseAnalyticsConstants
        .is_image_text_extracted] = memePost.isImageTextExtracted;

    firebaseAnalyticsParams[FirebaseAnalyticsConstants.caption_length] =
        memePost.caption.length;

    firebaseAnalyticsParams[FirebaseAnalyticsConstants.hashtags_count] =
        _hashtags.length;

    if (_isForGroup) {
      memePost.groupID = _groupID;
      memePost.groupName = _groupName;

      firebaseAnalyticsParams[FirebaseAnalyticsConstants.post_for] =
          FirebaseAnalyticsConstants.group_post;
    }

    if (_isForWar) {
      memePost.warName = _warName;
      memePost.warID = _warID;
      memePost.teamID = _teamID;
      memePost.teamName = _teamName;

      firebaseAnalyticsParams[FirebaseAnalyticsConstants.post_for] =
          FirebaseAnalyticsConstants.war_post;
    }

    if (_isForCompetition) {
      memePost.competitionName = _competitionName;
      memePost.competitionID = _competitionID;

      firebaseAnalyticsParams[FirebaseAnalyticsConstants.post_for] =
          FirebaseAnalyticsConstants.competition_post;
    }

    firebaseAnalyticsParams[FirebaseAnalyticsConstants.category] = _category;

    _postID =
        await repository.uploadMemePost(memePost, isGroupAdmin: _isGroupAdmin);

    LoggerUtil.log(
        "Create Meme Post Bloc", "Post Meme", "Meme Posted : " + _postID);

    setMemePostStatus(MemePostStatus.Complete);
    setImageUploadTask(null);
    setCategory(null);
    setPostID(_postID);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.meme_post,
        parameters: firebaseAnalyticsParams);
  }

  CreateMemePostBloc() {
    _imageUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        LoggerUtil.log("Create Meme Post Bloc", "initializer",
            "image upload task started");

        imageUploadTask.events.listen((eventReceived) {
          if (imageUploadTask.isComplete) {
            LoggerUtil.log("Create Meme Post Bloc", "initializer",
                "image upload task complete");

            postMeme(eventReceived.snapshot.ref);
          } else {
            setMemePostStatus(MemePostStatus.Uploading);

            LoggerUtil.log("Create Meme Post Bloc", "initializer",
                "image upload task event received");

            var event = eventReceived.snapshot;

            double progressPercent = event != null
                ? event.bytesTransferred / event.totalByteCount
                : 0;

            setImageUploadProgress(progressPercent);
          }
        });
      }
    });
  }

  fetchMeme(String postID) async {
    User user = await repository.getLoggedInUser();
    MemePost memePost = await repository.getMemePost(postID, user.uid);
    setMemePost(memePost);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.fetch_meme_post);
  }

  sharePost(GlobalKey key, MemePost memePost) async {
    try {
      return new Future.delayed(const Duration(milliseconds: 20), () async {
        RenderRepaintBoundary boundary = key.currentContext.findRenderObject();
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);
        ByteData byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);

        String link = (await repository.createPostDynamicLink(memePost.postID))
            .toString();
        LoggerUtil.log("Create Meme Post Bloc", "Share image", "link : $link");

        repository.firebaseAnalytics.logShare(
            contentType: FirebaseAnalyticsConstants.meme_post_item,
            itemId: memePost.postID,
            method: FirebaseAnalyticsConstants.create_meme_post_share_button);

        repository.sharePost(memePost.postID, link);

        await Share.file(
            'Share', 'post.png', byteData.buffer.asUint8List(), 'image/png',
            text:
                'Meme by ${memePost.user.firstName} ${memePost.user.lastName}:\n $link');
      });
    } catch (e) {
      print(e);
    }
  }

  resizeImage(GlobalKey key) {
    List<double> sizes = ImageReSizerForInstagram.sizesForInstagram(key);

    setPadding(sizes[0]);
    setSizedBoxHeight(sizes[1]);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.resize_image, parameters: {
      FirebaseAnalyticsConstants.padding: sizes[0],
      FirebaseAnalyticsConstants.sized_box_height: sizes[1]
    });
  }

  List<String> extractHashtags(String caption) {
    String uw = StringConstants.hashtag_regex;
    RegExp rx = new RegExp("($uw)?#+($uw+)");
    return rx
        .allMatches(caption)
        .where((i) => i.group(1) == null)
        .map((m) => m.group(2))
        .toList();
  }

  fetchMemecistSocialCompetition() async {
    String country = await repository.getUserCountry();

    if(country != null && country != "") {
      Competition memecistSocialCompetition =
      await repository.getMemecistSocialCompetition(country);

      setMemecistSocialCompetition(memecistSocialCompetition);

      repository.firebaseAnalytics
          .logEvent(
          name: FirebaseAnalyticsConstants.learn_more_memecist_social);
    } else {
      try{
        IP ip  = await repository.fetchIPDetails();
        await repository.setUserCountry(ip.countryCodeIso3);

        Competition memecistSocialCompetition =
        await repository.getMemecistSocialCompetition(ip.countryCodeIso3);

        setMemecistSocialCompetition(memecistSocialCompetition);

        repository.firebaseAnalytics
            .logEvent(
            name: FirebaseAnalyticsConstants.learn_more_memecist_social);
      } catch(e){
        setMemecistSocialCompetition(null);
        Crashlytics.instance.log(e.toString());
      }
    }
  }

  void dispose() async {
    await _imageFile.drain();
    _imageFile.close();

    await _imageUploadTask.drain();
    _imageUploadTask.close();

    await _memePostStatus.drain();
    _memePostStatus.close();

    await _imageUploadProgress.drain();
    _imageUploadProgress.close();

    await _post.drain();
    _post.close();

    await _showPromoBar.drain();
    _showPromoBar.close();

    await _promo.drain();
    _promo.close();

    await _categoryOption.drain();
    _categoryOption.close();

    await _memePost.drain();
    _memePost.close();

    await _padding.drain();
    _padding.close();

    await _sizedBoxHeight.drain();
    _sizedBoxHeight.close();

    await _memecistSocialCompetition.drain();
    _memecistSocialCompetition.close();
  }
}
