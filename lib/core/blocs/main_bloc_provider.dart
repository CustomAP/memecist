import 'main_bloc.dart';
import 'package:flutter/material.dart';

class MainBlocProvider extends InheritedWidget {
  final MainBloc mainBloc;

  MainBlocProvider({Key key, Widget child})
      : mainBloc = MainBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static MainBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<MainBlocProvider>()
        .mainBloc;
  }
}
