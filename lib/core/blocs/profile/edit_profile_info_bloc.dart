import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_messaging_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/keywords_generator.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

class EditProfileInfoBloc {
  final _imageFile = BehaviorSubject<File>();
  final _imageUploadTask = PublishSubject<StorageUploadTask>();
  final _showLoading = BehaviorSubject<bool>();
  final _isProfileInfoEdited = BehaviorSubject<bool>();
  final _isUserNameTaken = BehaviorSubject<bool>();
  final _canEditProfile = BehaviorSubject<bool>();

  String newFirstName, newLastName, newUserName;
  int userNameTakenRetries = 0;
  MemecistUser _currentUser;

  Stream<StorageUploadTask> get imageUploadTask => _imageUploadTask.stream;

  Function(StorageUploadTask) get setImageUploadTask =>
      _imageUploadTask.sink.add;

  Stream<File> get imageFile => _imageFile.stream;

  Function(File) get setImageFile => _imageFile.sink.add;

  Stream<bool> get showLoading => _showLoading.stream;

  Function(bool) get setShowLoading => _showLoading.sink.add;

  Stream<bool> get isProfileInfoEdited => _isProfileInfoEdited.stream;

  Function(bool) get setIsProfileInfoEdited => _isProfileInfoEdited.sink.add;

  Stream<bool> get isUserNameTaken => _isUserNameTaken.stream;

  Function(bool) get setIsUserNameTaken => _isUserNameTaken.sink.add;

  Stream<bool> get canEditProfile => _canEditProfile.stream;

  Function(bool) get setCanEditProfile => _canEditProfile.sink.add;

  initializeEditProfileInfoBloc(MemecistUser user) {
    this._currentUser = user;
  }

  editProfileDetails(
      File image, String firstName, String lastName, String userName) async {
    setShowLoading(true);
    User currentUser = await repository.getLoggedInUser();

    if (await repository.getCanEditProfileInfo(currentUser.uid)) {
      newFirstName = firstName;
      newLastName = lastName;
      newUserName = userName;

      if (image != null) {
        var result = await FlutterImageCompress.compressWithList(
            image.readAsBytesSync());

        setImageUploadTask(repository.uploadProfileImage(
            Uint8List.fromList(result), currentUser.uid));
      } else {
        uploadToDb(_currentUser.profilePic);
      }
    } else {
      setShowLoading(false);
      setCanEditProfile(false);
    }
  }

  uploadToDb(String imageUrl) async {
    bool _isUserNameTaken = false;
    User currentUser = await repository.getLoggedInUser();

    if (_currentUser.userName != newUserName) {
      _isUserNameTaken = await isUserNameTakenChecker(newUserName);
    }

    if (_isUserNameTaken) {
      userNameTakenRetries++;
      setIsUserNameTaken(true);
    } else {
      setIsUserNameTaken(false);
      List<String> keywords = KeywordsGenerator.generateKeywordsForNames(
          newFirstName, newLastName, newUserName);

      bool result = await repository.editProfileInfo(currentUser.uid,
          newFirstName, newLastName, newUserName, imageUrl, keywords);

      setIsProfileInfoEdited(result);
      setShowLoading(false);
      setImageUploadTask(null);

      repository.firebaseAnalytics.logEvent(
          name: FirebaseAnalyticsConstants.profile_info_edit,
          parameters: {
            FirebaseAnalyticsConstants.user_name_taken_retries:
                userNameTakenRetries,
            FirebaseAnalyticsConstants.keyword_length: keywords.length,
          });
    }
  }

  Future<bool> isUserNameTakenChecker(String userName) async {
    return repository.isUserNameTaken(userName);
  }

  uploadToDbWithStorageRef(StorageReference storageReference) async {
    uploadToDb(await storageReference.getDownloadURL());
  }

  EditProfileInfoBloc() {
    _imageUploadTask.listen((StorageUploadTask imageUploadTask) {
      if (imageUploadTask != null) {
        imageUploadTask.events.listen((eventReceived) {
          if (imageUploadTask.isComplete) {
            uploadToDbWithStorageRef(eventReceived.snapshot.ref);
          }
        });
      }
    });
  }

  void dispose() async {
    await _imageUploadTask.drain();
    _imageUploadTask.close();

    await _imageFile.drain();
    _imageFile.close();

    await _showLoading.drain();
    _showLoading.close();

    await _isProfileInfoEdited.drain();
    _isProfileInfoEdited.close();

    await _isUserNameTaken.drain();
    _isUserNameTaken.close();

    await _canEditProfile.drain();
    _canEditProfile.close();
  }
}
