import 'package:memecist/core/blocs/profile/profile_bloc.dart';
import 'package:flutter/material.dart';

class ProfileBlocProvider extends InheritedWidget{

  final ProfileBloc profileScreenBloc;

  ProfileBlocProvider({Key key, Widget child}) : profileScreenBloc = ProfileBloc(),
        super(key:key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ProfileBloc of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<ProfileBlocProvider>().profileScreenBloc;
  }

}