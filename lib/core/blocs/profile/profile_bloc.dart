import 'dart:io';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/badge_category.dart';
import 'package:memecist/core/models/badge_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/trouble_shoot_info_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';

class ProfileBloc {
  final _user = BehaviorSubject<MemecistUser>();
  final _memesList = BehaviorSubject<List<MemePost>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();
  final _follow = BehaviorSubject<bool>();
  final _isOwnProfile = BehaviorSubject<bool>();
  final _badgeBoardList = BehaviorSubject<List<BadgeCategory>>();
  final _badgesList = BehaviorSubject<List<Badge>>();
  final _showTroubleShoot = BehaviorSubject<bool>();
  final _model = BehaviorSubject<String>();
  final _troubleShootInfo = BehaviorSubject<TroubleShootInfo>();

  List<MemePost> memePosts = [];

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  MemecistUser userForProfile;

  String _userIDForProfile;

  Stream<MemecistUser> get user => _user.stream;

  Function(MemecistUser) get setUser => _user.sink.add;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreMemes => _hasMore.stream;

  Function(bool) get setHasMoreMemes => _hasMore.sink.add;

  Stream<List<MemePost>> get memesList => _memesList.stream;

  Function(List<MemePost>) get setMemesList => _memesList.sink.add;

  Stream<bool> get follow => _follow.stream;

  Function(bool) get setFollow => _follow.sink.add;

  Stream<bool> get isOwnProfile => _isOwnProfile.stream;

  Function(bool) get setIsOwnProfile => _isOwnProfile.sink.add;

  Stream<List<BadgeCategory>> get badgeBoardList => _badgeBoardList.stream;

  Function(List<BadgeCategory>) get setBadgeBoardList =>
      _badgeBoardList.sink.add;

  Stream<List<Badge>> get badgesList => _badgesList.stream;

  Function(List<Badge>) get setBadgesList => _badgesList.sink.add;

  Stream<bool> get showTroubleShoot => _showTroubleShoot.stream;

  Function(bool) get setShowTroubleShoot => _showTroubleShoot.sink.add;

  Stream<String> get model => _model.stream;

  Function(String) get setModel => _model.sink.add;

  Stream<TroubleShootInfo> get troubleShootInfo => _troubleShootInfo.stream;

  Function(TroubleShootInfo) get setTroubleShootInfo =>
      _troubleShootInfo.sink.add;

  Future<void> initializeProfileScreenBloc(String user) async {
    _userIDForProfile = user;

    if (Platform.isAndroid) {
      String brand =
          (await repository.getAndroidDeviceInfo()).brand.toLowerCase();

      if (StringConstants.troubleShootBrands.contains(brand)) {
        setShowTroubleShoot(true);
        setModel(brand);
      }
    } else {
      setShowTroubleShoot(false);
      setModel("");
    }
  }

  getTroubleShootInfo(String model) async {
    setTroubleShootInfo(await repository.getTroubleShootInfo(model));
  }

  getUser() async {
    LoggerUtil.log(
        "Profile Screen Bloc", "get user", "user: " + _userIDForProfile);
    userForProfile = await repository.getMemecistUser(_userIDForProfile);
    setUser(userForProfile);
  }

  determineIfOwnProfile() async {
    User currentUser = await repository.getLoggedInUser();

    if (currentUser.uid == _userIDForProfile) {
      setIsOwnProfile(true);
      repository.firebaseAnalytics
          .logEvent(name: FirebaseAnalyticsConstants.view_own_profile);
    } else {
      setIsOwnProfile(false);
      repository.firebaseAnalytics
          .logEvent(name: FirebaseAnalyticsConstants.view_others_profile);
    }
  }

  Future<void> fetchBadgesList() async {
    List<BadgeCategory> badgeCategories = await repository.getBadgesList();
    setBadgeBoardList(badgeCategories);

    List<Badge> badges = [];

    badgeCategories.forEach((category) {
      category.badges.forEach((badge) {
        if (userForProfile.badges.contains(badge.image)) {
          badges.add(badge);
        }
      });
    });

    setBadgesList(badges);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.badges_list_fetch);
  }

  Future<void> updateBio(String bio) async {
    User user = await repository.getLoggedInUser();
    repository.updateBio(user.uid, bio, userForProfile.bio);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.update_bio,
        parameters: {FirebaseAnalyticsConstants.bio_length: bio.length});
  }

  Future<void> fetchUserMemes() async {
    setLoading(true);
    List<MemePost> newMemePosts = [];

    User user = await repository.getLoggedInUser();

    if (runCount == 0) {
      LoggerUtil.log("Profile Screen Bloc", "fetch meme posts", "first run");
      newMemePosts = await repository.getInitialMemePostsByUser(
          docLimit, _userIDForProfile, user.uid);
    } else {
      LoggerUtil.log("Profile Screen Bloc", "fetch meme posts",
          "run : " + runCount.toString());
      newMemePosts = await repository.getMoreMemePostsByUser(
          docLimit, _userIDForProfile, user.uid);
    }
    if (newMemePosts.length < docLimit) {
      LoggerUtil.log(
          "Profile Screen Bloc", "fetch meme posts", "has more is false");
      hasMore = false;
      setHasMoreMemes(false);
    }
    runCount++;
    LoggerUtil.log("Profile Screen Bloc", "fetch meme posts",
        "new added : " + newMemePosts.length.toString());

    for (MemePost memePost in newMemePosts) memePosts.add(memePost);

    setMemesList(memePosts);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.fetch_user_memes,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  Future<void> isFollowing() async {
    User user = await repository.getLoggedInUser();

    if (await repository.isFollowingUser(_userIDForProfile, user.uid)) {
      LoggerUtil.log("Profile Screen Bloc", "Is following", "yes");
      setFollow(true);
      return;
    }

    LoggerUtil.log("Profile Screen Bloc", "Is following", "no");
    setFollow(false);
  }

  Future<void> followUser() async {
    setFollow(true);
    User user = await repository.getLoggedInUser();
    MemecistUser currentUser = await repository.getMemecistUser(user.uid);
    repository.followUser(_userIDForProfile, currentUser);
    LoggerUtil.log("Profile Screen Bloc", "Follow user", "following now");
    userForProfile.followersCount += 1;
    setUser(userForProfile);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.follow_user);
  }

  Future<void> unFollowUser() async {
    setFollow(false);
    User user = await repository.getLoggedInUser();

    LoggerUtil.log("Profile Screen Bloc", "uFollow user", "unfollowing now");
    repository.unFollowUser(_userIDForProfile, user.uid);
    userForProfile.followersCount -= 1;
    setUser(userForProfile);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.unfollow_user);
  }

  Future<void> logout() async {
    await repository.deleteFirebaseMessagingToken();
    return repository.signOut();
  }

  deletePost(MemePost memePost) async {
    repository.deletePost(memePost.postID);
    for (MemePost post in memePosts) {
      if (post.postID == memePost.postID) {
        memePosts.remove(post);
        break;
      }
    }
    setMemesList(memePosts);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post);
  }

  copyLink() async {
    String link =
        (await repository.createUserDynamicLink(_userIDForProfile)).toString();

    LoggerUtil.log("Profile link", "copy link", "link : $link");

    Share.text('Share', "Follow ${userForProfile.firstName} ${userForProfile.lastName}:\n $link", "text/plain");

    repository.firebaseAnalytics.logShare(
        contentType: FirebaseAnalyticsConstants.user,
        itemId: _userIDForProfile,
        method: FirebaseAnalyticsConstants.copy_profile_link_button);
  }

  void dispose() async {
    await _user.drain();
    _user.close();

    await _memesList.drain();
    _memesList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();

    await _follow.drain();
    _follow.close();

    await _isOwnProfile.drain();
    _isOwnProfile.close();

    await _badgeBoardList.drain();
    _badgeBoardList.close();

    await _badgesList.drain();
    _badgeBoardList.close();

    await _showTroubleShoot.drain();
    _showTroubleShoot.close();

    await _model.drain();
    _model.close();

    await _troubleShootInfo.drain();
    _troubleShootInfo.close();
  }
}
