import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:ui' as ui;

class ProfileVerificationBloc {
  final _user = BehaviorSubject<MemecistUser>();

  Stream<MemecistUser> get user => _user.stream;

  Function(MemecistUser) get setUser => _user.sink.add;

  getUser() async {
    User currentUser = await repository.getLoggedInUser();
    setUser(await repository.getMemecistUser(currentUser.uid));
  }

  shareUserNameCard(GlobalKey globalKey) async {
    User user = await repository.getLoggedInUser();
    try {
      return new Future.delayed(const Duration(milliseconds: 20), () async {
        RenderRepaintBoundary boundary =
            globalKey.currentContext.findRenderObject();
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);
        ByteData byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);

        String link =
            (await repository.createUserDynamicLink(user.uid)).toString();
        LoggerUtil.log("Home Bloc", "Share image", "link : $link");

        repository.firebaseAnalytics.logShare(
            contentType: FirebaseAnalyticsConstants.user_name_card,
            itemId: user.uid,
            method: FirebaseAnalyticsConstants.profile_verif_user_name_card);

        await Share.file(
            'Share', 'post.png', byteData.buffer.asUint8List(), 'image/png',
            text: 'We are now on Memecist!\nFollow us: $link');
      });
    } catch (e) {
      print(e);
    }
  }

  submitLinks(String link1, String link2, String method) async {
    User currentUser = await repository.getLoggedInUser();
    repository.submitLinksForProfileVerification(
        link1, link2, method, currentUser.uid);

    int linksCount = 0;

    if (link1 != null) linksCount++;

    if (link2 != null) linksCount++;

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.submit_links_for_prof_verif,
        parameters: {
          FirebaseAnalyticsConstants.links_count: linksCount,
          FirebaseAnalyticsConstants.method: method
        });
  }

  Future<void> dispose() async {
    await _user.drain();
    _user.close();
  }
}
