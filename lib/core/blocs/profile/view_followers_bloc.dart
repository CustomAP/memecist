import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ViewFollowersBloc {
  final _usersList = PublishSubject<List<MemecistUser>>();
  final _hasMore = BehaviorSubject<bool>();
  final _isLoading = BehaviorSubject<bool>();

  List<MemecistUser> users = [];

  String _userID;

  bool hasMore = true;
  int docLimit = 10;
  int runCount = 0;

  Stream<bool> get isLoading => _isLoading.stream;

  Function(bool) get setLoading => _isLoading.sink.add;

  Stream<bool> get hasMoreFollowers => _hasMore.stream;

  Function(bool) get setHasMoreFollowers => _hasMore.sink.add;

  Stream<List<MemecistUser>> get usersList => _usersList.stream;

  Function(List<MemecistUser>) get setUserList => _usersList.sink.add;

  initializeViewFollowersBloc(String userID) {
    _userID = userID;
    LoggerUtil.log("View Followers Bloc", "Initialize", " user: " + _userID);
  }

  Future<void> fetchFollowers() async {
    setLoading(true);
    List<MemecistUser> newUser = [];

    if (runCount == 0) {
      LoggerUtil.log("View Followers Bloc", "fetch followers", "first run");
      newUser = await repository.getInitialFollowers(_userID, docLimit);
    } else {
      LoggerUtil.log("View Followers Bloc", "fetch followers",
          "run : " + runCount.toString());
      newUser = await repository.getMoreFollowers(_userID, docLimit);
    }
    if (newUser.length < docLimit) {
      LoggerUtil.log(
          "View Followers Bloc", "fetch followers", "has more is false");
      hasMore = false;
      setHasMoreFollowers(false);
    }
    runCount++;
    LoggerUtil.log("View Followers Bloc", "fetch followers",
        "new added : " + newUser.length.toString());

    for (MemecistUser user in newUser) users.add(user);

    setUserList(users);
    setLoading(false);

    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.followers_fetch,
        parameters: {FirebaseAnalyticsConstants.run_count: runCount});
  }

  void dispose() async {
    await _usersList.drain();
    _usersList.close();

    await _hasMore.drain();
    _hasMore.close();

    await _isLoading.drain();
    _isLoading.close();
  }
}
