import 'package:memecist/core/blocs/profile/edit_profile_info_bloc.dart';
import 'package:flutter/material.dart';

class EditProfileInfoBlocProvider extends InheritedWidget {
  final EditProfileInfoBloc editProfileInfoBloc;

  EditProfileInfoBlocProvider({Key key, Widget child})
      : editProfileInfoBloc = EditProfileInfoBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static EditProfileInfoBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<EditProfileInfoBlocProvider>()
        .editProfileInfoBloc;
  }
}
