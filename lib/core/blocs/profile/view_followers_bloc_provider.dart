import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/profile/view_followers_bloc.dart';

class ViewFollowersBlocProvider extends InheritedWidget {
  final ViewFollowersBloc viewFollowersBloc;

  ViewFollowersBlocProvider({Key key, Widget child})
      : viewFollowersBloc = ViewFollowersBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ViewFollowersBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ViewFollowersBlocProvider>()
        .viewFollowersBloc;
  }
}
