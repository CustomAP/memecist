import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/profile/profile_verification_bloc.dart';

class ProfileVerificationBlocProvider extends InheritedWidget {
  final ProfileVerificationBloc profileVerificationBloc;

  ProfileVerificationBlocProvider({Key key, Widget child})
      : profileVerificationBloc = ProfileVerificationBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static ProfileVerificationBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProfileVerificationBlocProvider>()
        .profileVerificationBloc;
  }
}
