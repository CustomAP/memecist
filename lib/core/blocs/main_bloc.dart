import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/ip_model.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MainBloc {
  final _currentIndex = BehaviorSubject<int>();

  Stream<int> get currentIndex => _currentIndex.stream;

  Function(int) get setCurrentIndex => _currentIndex.sink.add;

  getUserIpDetails() async {
    try {
      LoggerUtil.log("Main Bloc", "get user session details", "started");

      IP ip = await repository.fetchIPDetails();

      User currentUser = await repository.getLoggedInUser();

      repository.addSessionDetails(currentUser.uid, ip);
    } catch (e) {
      repository.firebaseAnalytics
          .logEvent(name: FirebaseAnalyticsConstants.ip_fetch_fail);
      print(e);
    }
  }

  void dispose() async {
    await _currentIndex.drain();
    _currentIndex.close();
  }
}
