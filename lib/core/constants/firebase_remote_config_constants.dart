class FirebaseRemoteConfigConstants {
  static const String force_update_android_version =
      'force_update_android_version';
  static const String force_update_ios_version = 'force_update_ios_version';
  static const String should_show_rating_dialog = 'should_show_rating_dialog';
  static const String should_show_suggestions_dialog =
      'should_show_suggestions_dialog';
  static const String user_followers_count_update_at_run =
      "user_followers_count_update_at_run";
  static const String group_members_count_update_at_run =
      "group_members_count_update_at_run";
  static const String app_availability_update_at_run =
      "app_availability_update_at_run";
}
