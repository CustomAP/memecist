class SharedPrefsConstants {
  static const String country = "country";
  static const String app_run_count = "app_run_count";
  static const String group = "group";
  static const String group_members_count_update_at_run =
      "group_members_count_update_at_run";
  static const String is_rate_app_dialog_shown = "is_rate_app_dialog_shown";
  static const String is_suggestions_dialog_shown =
      "is_suggestions_dialog_shown";
  static const String can_download_template_without_watermark =
      "can_download_template_without_watermark";
  static const String war_room = "war_room";
  static const String is_initial_run_done = "is_initial_run_done";
}
