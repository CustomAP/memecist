class FirebaseStorageConstants {
  //links
  static final String defaultProfilePicUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fprofile_pic_avatar.png?alt=media";
  static final String warIdentifierUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fidentifiers%2Fwar.png?alt=media";
  static final String competitionIdentifierUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fidentifiers%2Fcompetition.png?alt=media";
  static final String groupIdentifierUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fidentifiers%2Fgroup.png?alt=media";
  static final String defaultGroupPicUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fgroup_default_pic.png?alt=media";
  static final String stampIconUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fstamp.png?alt=media";
  static final String warRoomIconUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fwar_room.png?alt=media";
  static final String teamLeaderIconUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fteam_leader.png?alt=media";
  static final String warWinnerMedalUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fwar_winner_medal.png?alt=media";
  static final String memeOfTheWarUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fmeme_of_the_war.png?alt=media";
  static final String competitionWinnerTrophy =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fcompetition_winner_trophy.png?alt=media";
  static final String facebookLogo =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fsocial_media_handles%2Ffacebook.png?alt=media";
  static final String instagramLogo =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fsocial_media_handles%2Finstagram.png?alt=media";
  static final String twitterLogo =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fsocial_media_handles%2Ftwitter.png?alt=media";
  static final String youtubeLogo =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fsocial_media_handles%2Fyoutube.png?alt=media";
  static final String memecistLogo =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fsocial_media_handles%2Fmc.png?alt=media";
  static final String whatsappLogo =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fsocial_media_handles%2Fwhatsapp.png?alt=media";
  static final String defaultTeamIconUrl =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fteam.png?alt=media";
  static final String newBieBadge =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fnewbie.png?alt=media";
  static final String memecistStoryShare =
      "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fmemecist_story_share.png?alt=media";

  //constants
  static const String profile_pic = "profile_pic";
  static const String war_pic = "war_pic";
  static const String group_pic = "group_pic";
  static const String post_pic = "post_pic";
  static const String message_pic = "message_pic";
  static const String team_pic = "team_pic";
  static const String mc_story_ss = "mc_story_ss";
  static const String template_pic = "template_pic";
}
