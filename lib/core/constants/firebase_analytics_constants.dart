class FirebaseAnalyticsConstants {
  //screens
  static const home_screen = "Home";
  static const search_users_screen = "Search users";
  static const search_screen = "Search screen";
  static const explore_screen = "Explore";
  static const initialize_profile_info_screen = "Initialize profile info";
  static const login_screen = "Login";
  static const otp_screen = "OTP";
  static const phone_sign_up_screen = "Phone sign up";
  static const add_links_for_competition_screen = "Add links for Competition";
  static const create_meme_post_screen = "Create meme post";
  static const meme_post_detail_screen = "Meme post detail";
  static const view_reacts_screen = "View reacts";
  static const competition_details_screen = "Competition details";
  static const competition_tab_screen = "Competition tab";
  static const team_leaders_selection_for_others_screen =
      "Team leaders selection - Others";
  static const team_leaders_selection_for_war_creator_screen =
      "Team leaders selection - War Creator";
  static const team_members_selection_for_others_screen =
      "Team members selection - Others";
  static const team_members_selection_for_leaders_screen =
      "Team members selection - Leaders";
  static const d_day_screen = "D-Day";
  static const start_war_screen = "Start war";
  static const view_all_teams_screen = "View all teams";
  static const war_ended_screen = "War ended";
  static const war_dismissed_screen = "War dismissed";
  static const create_group_screen = "Create group";
  static const group_details_screen = "Group details";
  static const groups_tab_screen = "Groups tab";
  static const my_groups_screen = "My groups";
  static const view_members_screen = "View members";
  static const view_pending_approvals_screen = "View pending approvals";
  static const view_unapproved_memes_screen = "View unapproved memes";
  static const full_photo_screen = "Full photo";
  static const war_room_chat_screen = "War room chat";
  static const war_rooms_screen = "War rooms";
  static const profile_screen = "Profile";
  static const view_followers_screen = "View followers";
  static const templates_screen = "Templates";
  static const notifications_screen = "Notifications";
  static const hashtag_posts_screen = "Hashtag Posts";
  static const profile_verification_screen = "Profile verification";
  static const edit_group_details_screen = "Edit group details";
  static const edit_profile_info_screen = "Edit profile info";
  static const help_memecist_grow_screen = "Help memecist grow";

  //events
  static const initialize_profile_info = "initialize_profile_info";
  static const explore_feed_fetch = "explore_feed_fetch";
  static const links_submit_for_competition = "links_submit_for_competition";
  static const promo_update = "promo_update";
  static const meme_post = "meme_post";
  static const react_post = "react_post";
  static const unreact_post = "unreact_post";
  static const comments_fetch = "comments_fetch";
  static const comment = "comment";
  static const reacts_fetch = "reacts_fetch";
  static const competitions_fetch = "competitions_fetch";
  static const team_leader_apply = "team_leader_apply";
  static const team_leaders_applications_fetch =
      "team_leaders_applications_fetch";
  static const select_team_leader_application =
      "select_team_leader_application";
  static const unselect_team_leader_application =
      "unselect_team_leader_application";
  static const approve_team_leaders = "approve_team_leaders";
  static const selected_team_leaders_fetch = "selected_team_leaders_fetch";
  static const team_members_applications_fetch =
      "team_members_applications_fetch";
  static const select_team_member_application =
      "select_team_member_application";
  static const unselect_team_member_application =
      "unselect_team_member_application";
  static const approve_team_members = "approve_team_members";
  static const selected_team_members_fetch = "selected_team_members_fetch";
  static const team_member_apply = "team_member_apply";
  static const war_memes_fetch = "war_memes_fetch";
  static const start_war = "start_war";
  static const view_all_teams = "view_all_teams";
  static const war_details_fetch = "war_details_fetch";
  static const group_create = "group_create";
  static const group_details_edit = "group_details_edit";
  static const profile_info_edit = "profile_info_edit";
  static const group_memes_fetch = "group_memes_fetch";
  static const join_group = "join_group";
  static const leave_group = "leave_group";
  static const groups_fetch = "groups_fetch";
  static const remove_member = "remove_member";
  static const group_members_fetch = "groups_members_fetch";
  static const make_admin = "make_admin";
  static const unapproved_memes_fetch = "unapproved_memes_fetch";
  static const pending_unapproved_memes_fetch =
      "pending_unapproved_memes_fetch";
  static const approve_meme = "approve_meme";
  static const reject_meme = "reject_meme";
  static const load_more_messages = "load_more_messages";
  static const message_send = "message_send";
  static const view_own_profile = "view_own_profile";
  static const view_others_profile = "view_others_profile";
  static const fetch_user_memes = "fetch_user_memes";
  static const follow_user = "follow_user";
  static const unfollow_user = "unfollow_user";
  static const followers_fetch = "followers_fetch";
  static const templates_fetch = "templates_fetch";
  static const template_download = "template_download";
  static const memecist_story_share = "memecist_story_share";
  static const initial_run_complete = "initial_run_complete";
  static const follow_suggestions_fetch = "follow_suggestions_fetch";
  static const group_suggestions_fetch = "group_suggestions_fetch";
  static const feed_fetch = "feed_fetch";
  static const notifications_fetch = "notifications_fetch";
  static const suggestion_send = "suggestion_send";
  static const delete_comment = "delete_comment";
  static const rate_app = "rate_app";
  static const rename_team = "rename_team";
  static const delete_post = "delete_post";
  static const dismiss_rate_app_prompt = "dismiss_rate_app_prompt";
  static const dismiss_suggestion_prompt = "dismiss_suggestion_prompt";
  static const dismiss_update_bio_prompt = "dismiss_update_bio_prompt";
  static const dismiss_delete_comment_prompt = "dismiss_delete_comment_prompt";
  static const dismiss_delete_post_prompt = "dismiss_delete_post_prompt";
  static const dismiss_edit_team_info_prompt = "dismiss_edit_team_info_prompt";
  static const dismiss_team_renamed_prompt = "dismiss_team_renamed_prompt";
  static const dismiss_feature_req_prompt = "dismiss_feature_req_prompt";
  static const feature_request = "feature_request";
  static const fetch_hashtag_memes = "fetch_hashtag_memes";
  static const update_bio = "update_bio";
  static const ip_fetch_fail = "ip_fetch_fail";
  static const refresh_screen = "refresh_screen";
  static const groups_w_active_wars_fetch = "groups_w_active_war_fetch";
  static const fetch_meme_post = "fetch_meme_post";
  static const resize_image = "resize_image";
  static const learn_more_memecist_social = "learn_more_memecist_social";
  static const fetch_competition = "fetch_competition";
  static const report_meme = "report_meme";
  static const update_group_members_count = "update_group_members_count";
  static const update_followers_count = "update_followers_count";
  static const update_app_availability = "update_app_availability";
  static const war_rooms_fetch = "war_rooms_fetch";
  static const badges_list_fetch = "badges_list_fetch";
  static const submit_links_for_prof_verif =
      "submit_links_for_prof_verif";
  static const method = "method";
  static const mc_story_ss_upload = "mc_story_ss_upload";

  //parameters
  static const user_id = "user_id";
  static const user_name_taken_retries = "user_name_taken_retries";
  static const posts_fetch_count = "posts_fetch_count";
  static const users_fetch_count = "users_fetch_count";
  static const search_query = "search_query";
  static const links_count = "links_count";
  static const run_count = "run_count";
  static const post_for = "post_for";
  static const post_type = "post_type";
  static const is_war_participant = "is_war_participant";
  static const message_type = "message_type";
  static const category = "category";
  static const source = "source";
  static const is_without_watermark = "is_without_watermark";
  static const total = "total";
  static const keyword_length = "keyword_length";
  static const comment_length = "comment_length";
  static const caption_length = "caption_lenght";
  static const padding = "padding";
  static const sized_box_height = "sized_box_height";
  static const is_memecist_social = "is_memecist_social";
  static const is_image_text_extracted = "is_image_text_extracted";
  static const hashtags_count = "hashtags_count";
  static const mins_since_leader_app_start = "mins_since_leader_app_start";
  static const mins_since_member_app_start = "mins_since_member_app_start";
  static const avg_mins_since_member_applied = "avg_mins_since_member_applied";
  static const war_teams_count = "war_teams_count";
  static const card_color = "card_color";
  static const bio_length = "bio_length";
  static const country = "country";

  //values
  static const google_sign_in = "Google";
  static const phone_sign_in = "Phone number";
  static const meme_post_item = "Meme post";
  static const meme_post_share_button = "Meme post share button";
  static const regular_post = "Regular post";
  static const group_post = "Group post";
  static const competition_post = "Competition post";
  static const war_post = "War post";
  static const group = "Group";
  static const copy_group_link_button = "Copy group link button";
  static const copy_profile_link_button = "Copy profile link button";
  static const user = "MemecistUser";
  static const text = "Text";
  static const image = "Image";
  static const template_category = "Template Category";
  static const template = "Template";
  static const user_name_card = "MemecistUser name card";
  static const user_name_share_card = "MemecistUser name share card";
  static const verify_user_name_share_card = "Verify user name share card";
  static const profile_verif_user_name_card = "Profile verif user name card";
  static const follow_suggestion_card = "Follow suggestion card";
  static const group_suggestion_card = "Group suggestion card";
  static const create_meme_post_share_button = "Create meme post share button";
  static const add_links_for_comp_share_button =
      "Add links for comp share button";
  static const war_winner_card = "War winner card";
  static const war_winner_share_card = "War winner share card";
  static const competition_winner_card = "Competition winner card";
  static const competition_winner_share_card = "Competition winner share card";
}
