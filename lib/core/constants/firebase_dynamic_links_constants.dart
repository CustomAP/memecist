class FirebaseDynamicLinksConstants{
  static final String uriPrefix = "https://memecist.com/share";
  static final String groupLink = "https://www.memecist.com/groups";
  static final String postLink = "https://www.memecist.com/posts";
  static final String userLink = "https://www.memecist.com/users";

  static final String androidPackageName = "com.memecist.memecist";
  static final int minimumAndroidVersion = 1;
}