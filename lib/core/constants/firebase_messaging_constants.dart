class FirebaseMessagingConstants {
  static const String group = "group";
  static const String admin = "admin";
  static const String notification = "notification";
  static const String body = "body";
  static const String title = "title";
  static const String data = "data";
  static const String id = "id";
  static const String type = "type";
  static const String group_approval_post = "group_approval_post";
  static const String post = "post";
  static const String war = "war";
  static const String user = "user";
  static const String war_room = "war_room";
  static const String competition = "competition";
  static const String url = "url";
}
