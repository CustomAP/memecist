import 'dart:io';
import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';

class FirebaseStorageProvider {
  FirebaseStorage _firebaseStorage = FirebaseStorage.instance;

  StorageUploadTask uploadMemeImage(Uint8List data, String userID) {
    return _firebaseStorage
        .ref()
        .child(FirebaseStorageConstants.post_pic)
        .child(userID + DateTime.now().toString() + '.jpg')
        .putData(data);
  }

  StorageUploadTask uploadWarImage(Uint8List data, String warID) {
    return _firebaseStorage
        .ref()
        .child(FirebaseStorageConstants.war_pic)
        .child(warID + DateTime.now().toString() + '.jpg')
        .putData(data);
  }

  StorageUploadTask uploadGroupImage(Uint8List data, String groupID) {
    return _firebaseStorage
        .ref()
        .child(FirebaseStorageConstants.group_pic)
        .child(groupID + DateTime.now().toString() + '.jpg')
        .putData(data);
  }

  StorageUploadTask uploadMessageImage(Uint8List data, String userID) {
    return _firebaseStorage
        .ref()
        .child(FirebaseStorageConstants.message_pic)
        .child(userID + DateTime.now().toString() + '.jpg')
        .putData(data);
  }

  StorageUploadTask uploadTeamImage(Uint8List data, String teamID) {
    return _firebaseStorage
        .ref()
        .child(FirebaseStorageConstants.team_pic)
        .child(teamID + DateTime.now().toString() + '.jpg')
        .putData(data);
  }

  StorageUploadTask uploadProfileImage(Uint8List data, String groupID) {
    return _firebaseStorage
        .ref()
        .child(FirebaseStorageConstants.profile_pic)
        .child(groupID + DateTime.now().toString() + '.jpg')
        .putData(data);
  }

  StorageUploadTask uploadMemecistStoryScreenshot(
      Uint8List data, String userID) {
    return _firebaseStorage
        .ref()
        .child(FirebaseStorageConstants.mc_story_ss)
        .child(userID + DateTime.now().toString() + '.jpg')
        .putData(data);
  }

  StorageUploadTask uploadTemplateImage(File data, String userID) {
    return _firebaseStorage
        .ref()
        .child(FirebaseStorageConstants.template_pic)
        .child(userID + DateTime.now().toString() + '.jpg')
        .putFile(data);
  }
}
