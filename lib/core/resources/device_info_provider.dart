import 'package:device_info/device_info.dart';
import 'package:memecist/core/models/android_device_info_model.dart';
import 'package:memecist/core/models/ios_device_info_model.dart';

class DeviceInfoProvider {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  Future<MemecistAndroidDeviceInfo> getAndroidDeviceInfo() async {
    AndroidDeviceInfo build = await deviceInfoPlugin.androidInfo;
    return MemecistAndroidDeviceInfo(
      build.version.securityPatch,
      build.version.sdkInt,
      build.version.release,
      build.version.previewSdkInt,
      build.version.incremental,
      build.version.codename,
      build.version.baseOS,
      build.board,
      build.bootloader,
      build.brand,
      build.device,
      build.display,
      build.fingerprint,
      build.hardware,
      build.host,
      build.id,
      build.manufacturer,
      build.model,
      build.product,
      build.supported32BitAbis,
      build.supported64BitAbis,
      build.supportedAbis,
      build.tags,
      build.type,
      build.isPhysicalDevice,
      build.androidId,
      build.systemFeatures,
    );
  }

  Future<MemecistIOSDeviceInfo> getIOSDeviceInfo() async {
    IosDeviceInfo data = await deviceInfoPlugin.iosInfo;
    return MemecistIOSDeviceInfo(
      data.name,
      data.systemName,
      data.systemVersion,
      data.model,
      data.localizedModel,
      data.identifierForVendor,
      data.isPhysicalDevice,
      data.utsname.sysname,
      data.utsname.nodename,
      data.utsname.release,
      data.utsname.version,
      data.utsname.machine,
    );
  }
}
