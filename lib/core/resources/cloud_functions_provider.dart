import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';

class CloudFunctionsProvider {
  CloudFunctions _cloudFunctions = CloudFunctions.instance;

  String _lastFetchedGroupSuggestionsDocumentID;
  int _lastFetchedGroupSuggestionsMemberCount;

  String _lastFetchedFollowSuggestionsDocumentID;
  int _lastFetchedFollowSuggestionsFollowersCount;

  String _lastFetchedExploreFeedDocumentID;

  Future<List<MemecistUser>> getFollowSuggestions(
      int runCount, String country) async {
    List<MemecistUser> users = [];

    final HttpsCallable callable = _cloudFunctions.getHttpsCallable(
        functionName: 'followSuggestions')
      ..timeout = const Duration(seconds: 30);

    if (runCount == 1) {
      return callable.call({
        FirestoreConstants.run_count: runCount,
        FirestoreConstants.country: country
      }).then((value) {
        List<dynamic> snapshots = value.data;

        snapshots.forEach((doc) {
          users.add(MemecistUser.fromMap(doc));
        });

        if (users.length != 0) {
          _lastFetchedFollowSuggestionsDocumentID =
              users[users.length - 1].userID;
          _lastFetchedFollowSuggestionsFollowersCount =
              users[users.length - 1].followersCount;
        }

        return users;
      });
    } else {
      return callable.call({
        FirestoreConstants.run_count: runCount,
        FirestoreConstants.followers_count:
            _lastFetchedFollowSuggestionsFollowersCount,
        FirestoreConstants.user_id: _lastFetchedFollowSuggestionsDocumentID,
        FirestoreConstants.country: country
      }).then((value) {
        List<dynamic> snapshots = value.data;

        snapshots.forEach((doc) {
          users.add(MemecistUser.fromMap(doc));
        });

        if (users.length != 0) {
          _lastFetchedFollowSuggestionsDocumentID =
              users[users.length - 1].userID;
          _lastFetchedFollowSuggestionsFollowersCount =
              users[users.length - 1].followersCount;
        }

        return users;
      });
    }
  }

  Future<List<Group>> getGroupSuggestions(int runCount, String country) async {
    List<Group> groups = [];

    final HttpsCallable callable = _cloudFunctions.getHttpsCallable(
        functionName: 'groupSuggestions')
      ..timeout = const Duration(seconds: 30);

    if (runCount == 1) {
      return callable.call({
        FirestoreConstants.run_count: runCount,
        FirestoreConstants.country: country
      }).then((value) {
        List<dynamic> snapshots = value.data;

        snapshots.forEach((doc) {
          groups.add(Group.fromMap(doc));
        });

        if (groups.length != 0) {
          _lastFetchedGroupSuggestionsDocumentID =
              groups[groups.length - 1].groupID;
          _lastFetchedGroupSuggestionsMemberCount =
              groups[groups.length - 1].membersCount;
        }

        return groups;
      });
    } else {
      return callable.call({
        FirestoreConstants.run_count: runCount,
        FirestoreConstants.group_id: _lastFetchedGroupSuggestionsDocumentID,
        FirestoreConstants.members_count:
            _lastFetchedGroupSuggestionsMemberCount,
        FirestoreConstants.country: country
      }).then((value) {
        List<dynamic> snapshots = value.data;

        snapshots.forEach((doc) {
          groups.add(Group.fromMap(doc));
        });

        if (groups.length != 0) {
          _lastFetchedGroupSuggestionsDocumentID =
              groups[groups.length - 1].groupID;
          _lastFetchedGroupSuggestionsMemberCount =
              groups[groups.length - 1].membersCount;
        }

        return groups;
      });
    }
  }

  Future<void> approveMeme(String postID, String groupID) async {
    final HttpsCallable callable = _cloudFunctions.getHttpsCallable(
        functionName: 'approveMeme')
      ..timeout = const Duration(seconds: 30);
    return callable.call(<String, dynamic>{
      FirestoreConstants.post_id: postID,
      FirestoreConstants.group_id: groupID
    });
  }

  Future<List<MemePost>> getFeed(int runCount, String userID) async {
    List<MemePost> memePosts = [];
    List<Future> futures = [];

    final HttpsCallable callable = _cloudFunctions.getHttpsCallable(
        functionName: 'feed')
      ..timeout = const Duration(seconds: 30);

    await callable
        .call(<String, dynamic>{FirestoreConstants.run_count: runCount}).then(
            (value) {
      List<dynamic> snapshots = value.data;
      if (snapshots == null) return null;
      snapshots.forEach((doc) {
        futures.add(repository
            .getMemePost(doc[FirestoreConstants.post_id], userID)
            .then((memePost) => memePosts.add(memePost))
            .catchError((error) => print(error.toString())));
      });
    });

    await Future.wait(futures);

    return memePosts;
  }

  Future<List<MemePost>> getExploreFeed(
      int runCount, String userID, String category, String country) async {
    List<MemePost> memePosts = [];

    final HttpsCallable callable = _cloudFunctions.getHttpsCallable(
        functionName: 'exploreFeed')
      ..timeout = const Duration(seconds: 30);

    if (runCount == 1) {
      return callable.call({
        FirestoreConstants.run_count: runCount,
        FirestoreConstants.category: category,
        FirestoreConstants.country: country
      }).then((value) {
        List<dynamic> snapshots = value.data;

        snapshots.forEach((doc) {
          memePosts.add(MemePost.fromMap(doc));
        });

        if (memePosts.length != 0) {
          _lastFetchedExploreFeedDocumentID =
              memePosts[memePosts.length - 1].postID;
        }

        return memePosts;
      });
    } else {
      return callable.call({
        FirestoreConstants.run_count: runCount,
        FirestoreConstants.post_id: _lastFetchedExploreFeedDocumentID,
        FirestoreConstants.category: category
      }).then((value) {
        List<dynamic> snapshots = value.data;

        snapshots.forEach((doc) {
          memePosts.add(MemePost.fromMap(doc));
        });

        if (memePosts.length != 0) {
          _lastFetchedExploreFeedDocumentID =
              memePosts[memePosts.length - 1].postID;
        }

        return memePosts;
      });
    }
  }

  Future<void> sharePost(String postID, String link) async {
    final HttpsCallable callable = _cloudFunctions.getHttpsCallable(
        functionName: 'sharePost')
      ..timeout = const Duration(seconds: 30);

    return callable.call(<String, dynamic>{
      FirestoreConstants.link: link,
      FirestoreConstants.post_id: postID
    });
  }

  Future<void> updateFollowersCount(String userID) async {
    return _cloudFunctions
        .getHttpsCallable(functionName: 'updateFollowersCount')
        .call(<String, dynamic>{FirestoreConstants.user_id: userID})
          ..timeout(Duration(seconds: 30));
  }

  Future<void> updateGroupMembersCount(String groupID) async {
    return _cloudFunctions
        .getHttpsCallable(functionName: 'updateGroupMembersCount')
        .call(<String, dynamic>{FirestoreConstants.group_id: groupID})
          ..timeout(Duration(seconds: 30));
  }

  Future<void> deletePost(String postID) async {
    return _cloudFunctions
        .getHttpsCallable(functionName: 'deletePost')
        .call(<String, dynamic>{FirestoreConstants.post_id: postID})
          ..timeout(Duration(seconds: 30));
  }

  Future<bool> editGroupDetails(
      String groupID,
      String groupName,
      String groupDescription,
      String imageUrl,
      List<String> groupSearchKeywords) async {
    Map<String, dynamic> params = {FirestoreConstants.group_id: groupID};

    if (groupName != null) params[FirestoreConstants.group_name] = groupName;

    if (groupDescription != null)
      params[FirestoreConstants.group_description] = groupDescription;

    if (imageUrl != null)
      params[FirestoreConstants.group_profile_pic] = imageUrl;

    if (groupSearchKeywords != null)
      params[FirestoreConstants.search_keywords] = groupSearchKeywords;

    return _cloudFunctions
        .getHttpsCallable(functionName: 'editGroupDetails')
        .call(params)
        .then((value) => value.data[FirestoreConstants.result])
          ..timeout(Duration(seconds: 30));
  }

  Future<bool> editProfileInfo(String userID, String firstName, String lastName,
      String userName, String imageUrl, List<String> searchKeywords) async {
    return _cloudFunctions
        .getHttpsCallable(functionName: 'editProfileInfo')
        .call({
      FirestoreConstants.user_id: userID,
      FirestoreConstants.first_name: firstName,
      FirestoreConstants.last_name: lastName,
      FirestoreConstants.user_name: userName,
      FirestoreConstants.profile_pic: imageUrl,
      FirestoreConstants.search_keywords: searchKeywords
    }).then((value) => value.data[FirestoreConstants.result])
          ..timeout(Duration(seconds: 30));
  }
}
