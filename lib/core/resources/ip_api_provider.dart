import 'package:http/http.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/ip_model.dart';
import 'dart:convert';

class IpApiProvider {
  Client httpClient = Client();

  Future<IP> fetchIPDetails() async {
    final response = await httpClient.get('https://ipapi.co/json');

    if (response.statusCode == 200) {
      Map<String, dynamic> object = json.decode(response.body);
      return IP(
          object[FirestoreConstants.ip],
          object[FirestoreConstants.city],
          object[FirestoreConstants.region],
          object[FirestoreConstants.country_code_iso3],
          object[FirestoreConstants.country_name],
          object[FirestoreConstants.postal],
          object[FirestoreConstants.timezone],
          object[FirestoreConstants.utc_offset],
          object[FirestoreConstants.country_calling_code],
          object[FirestoreConstants.asn],
          object[FirestoreConstants.org],
          object[FirestoreConstants.latitude],
          object[FirestoreConstants.longitude]);
    } else {
      print("Failed to get Ip details");
      return null;
    }
  }
}