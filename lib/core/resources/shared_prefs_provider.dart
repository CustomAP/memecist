import 'package:memecist/core/constants/shared_prefs_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefsProvider {
  Future<String> getUserCountry() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString(SharedPrefsConstants.country) ?? "";
  }

  Future<void> setUserCountry(String country) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setString(SharedPrefsConstants.country, country);
  }

  Future<int> getApplicationRunCount() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getInt(SharedPrefsConstants.app_run_count) ?? 0;
  }

  Future<void> setApplicationRunCount(int count) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setInt(SharedPrefsConstants.app_run_count, count);
  }

  Future<int> getGroupOpenCount(String groupID) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getInt(SharedPrefsConstants.group + "-" + groupID) ?? 0;
  }

  Future<void> setGroupOpenCount(String groupID, int count) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setInt(
        SharedPrefsConstants.group + "-" + groupID, count);
  }

  Future<int> getToUpdateGroupMembersCountAtRun() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences
            .getInt(SharedPrefsConstants.group_members_count_update_at_run) ??
        0;
  }

  Future<void> setToUpdateGroupMembersCountAtRun(int count) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setInt(
        SharedPrefsConstants.group_members_count_update_at_run, count);
  }

  Future<bool> getIsAppRateDialogShown() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool(SharedPrefsConstants.is_rate_app_dialog_shown) ??
        false;
  }

  Future<void> setIsAppRateDialogShown(bool isShown) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setBool(
        SharedPrefsConstants.is_rate_app_dialog_shown, isShown);
  }

  Future<bool> getIsSuggestionsDialogShown() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences
            .getBool(SharedPrefsConstants.is_suggestions_dialog_shown) ??
        false;
  }

  Future<void> setIsSuggestionsDialogShown(bool isShown) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setBool(
        SharedPrefsConstants.is_suggestions_dialog_shown, isShown);
  }

  Future<bool> getCanDownloadTemplateWithoutWatermark() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool(
            SharedPrefsConstants.can_download_template_without_watermark) ??
        false;
  }

  Future<void> setCanDownloadTemplateWithoutWatermark(bool canDownload) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setBool(
        SharedPrefsConstants.can_download_template_without_watermark,
        canDownload);
  }

  Future<int> getLastOpenedWarRoomTime(String warRoomID) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences
            .getInt(SharedPrefsConstants.war_room + "-" + warRoomID) ??
        0;
  }

  Future<void> setLastOpenedWarRoomTime(String warRoomID, int time) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setInt(
        SharedPrefsConstants.war_room + "-" + warRoomID, time);
  }

  Future<bool> getIsInitialRunDone() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences
        .getBool(SharedPrefsConstants.is_initial_run_done) ??
        false;
  }

  Future<void> setIsInitialRunDone(bool isInitialRunDone) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.setBool(
        SharedPrefsConstants.is_initial_run_done, isInitialRunDone);
  }
}
