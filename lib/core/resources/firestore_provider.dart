import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/models/android_device_info_model.dart';
import 'package:memecist/core/models/badge_category.dart';
import 'package:memecist/core/models/badge_model.dart';
import 'package:memecist/core/models/comments_model.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/competition_winner_group.dart';
import 'package:memecist/core/models/competition_winner_model.dart';
import 'package:memecist/core/models/group_member_model.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/ios_device_info_model.dart';
import 'package:memecist/core/models/ip_model.dart';
import 'package:memecist/core/models/message_model.dart';
import 'package:memecist/core/models/notification_model.dart';
import 'package:memecist/core/models/promo_model.dart';
import 'package:memecist/core/models/template_category_model.dart';
import 'package:memecist/core/models/template_model.dart';
import 'package:memecist/core/models/trouble_shoot_info_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/war_application_model.dart';
import 'package:memecist/core/models/war_leader_application_model.dart';
import 'package:memecist/core/models/war_member_application_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/models/war_room_model.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/core/utils/mapper.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';

class FirestoreProvider {
  FirebaseFirestore _firestore = FirebaseFirestore.instance;

  DocumentSnapshot _lastFetchedCommentDocument;
  DocumentSnapshot _lastFetchedReactDocument;
  DocumentSnapshot _lastFetchedFollowersDocument;
  DocumentSnapshot _lastFetchedMembersDocument;
  DocumentSnapshot _lastFetchedPostsByUserDocument;
  DocumentSnapshot _lastFetchedPostsByGroupDocument;
  DocumentSnapshot _lastFetchedUnapprovedMemePostByGroupDocument;
  DocumentSnapshot _lastFetchedPendingApprovalMemePostsDocument;
  DocumentSnapshot _lastFetchedTeamLeadersApplicationDocument;
  DocumentSnapshot _lastFetchedTeamMembersApplicationDocument;
  DocumentSnapshot _lastFetchedWarRoomDocument;
  DocumentSnapshot _lastFetchedWarRoomMessageDocument;
  DocumentSnapshot _lastFetchedPostsByWarDocument;
  DocumentSnapshot _lastFetchedMemeTemplateDocument;
  DocumentSnapshot _lastFetchedNotificationDocument;
  DocumentSnapshot _lastFetchedSearchedMemeTemplateDocument;
  DocumentSnapshot _lastFetchedPostsForSearchDocument;
  DocumentSnapshot _lastFetchedPostsByHashtagDocument;

  Future<void> initializeUser(User firebaseUser, IP ipDetails) async {
    final snapshot = await _firestore
        .collection(FirestoreConstants.users)
        .doc(firebaseUser.uid)
        .get();

    if (!snapshot.exists) {
      WriteBatch initializeBatch = _firestore.batch();

      initializeBatch.set(
          _firestore.collection(FirestoreConstants.users).doc(firebaseUser.uid),
          {
            FirestoreConstants.followers_count: 0,
            FirestoreConstants.following_count: 0,
            FirestoreConstants.memes_count: 0,
            FirestoreConstants.first_name: "Memes",
            FirestoreConstants.last_name: "Lover",
            FirestoreConstants.bio: "I'm here for Memes❤",
            FirestoreConstants.user_id: firebaseUser.uid,
            FirestoreConstants.is_first_run_completed: false,
            FirestoreConstants.profile_pic:
                FirebaseStorageConstants.defaultProfilePicUrl,
            FirestoreConstants.is_verified: false,
            FirestoreConstants.badges: [FirebaseStorageConstants.newBieBadge],
            FirestoreConstants.rep: 1.0,
            FirestoreConstants.user_name: firebaseUser.uid,
            FirestoreConstants.country: ipDetails.countryCodeIso3
          });

      initializeBatch.set(
          _firestore
              .collection(FirestoreConstants.users)
              .doc(firebaseUser.uid)
              .collection(FirestoreConstants.private_data)
              .doc(FirestoreConstants.account_info),
          {
            FirestoreConstants.account_create_date_time:
                FieldValue.serverTimestamp(),
            FirestoreConstants.is_first_run_completed: false
          });

      initializeBatch.set(
          _firestore
              .collection(FirestoreConstants.users)
              .doc(firebaseUser.uid)
              .collection(FirestoreConstants.private_data)
              .doc(FirestoreConstants.notification),
          {
            FirestoreConstants.last_notification_time:
                FieldValue.serverTimestamp(),
            FirestoreConstants.last_opened_time: FieldValue.serverTimestamp()
          });

      initializeBatch.set(
          _firestore
              .collection(FirestoreConstants.users)
              .doc(firebaseUser.uid)
              .collection(FirestoreConstants.private_data)
              .doc(FirestoreConstants.user_details),
          {
            FirestoreConstants.last_profile_edit_date_time:
                DateTime(1970, 1, 1, 0, 0, 0, 0, 0)
          });

      return initializeBatch.commit();
    } else {
      return _firestore
          .collection(FirestoreConstants.users)
          .doc(firebaseUser.uid)
          .update({FirestoreConstants.country: ipDetails.countryCodeIso3});
    }
  }

  Future<bool> isUserNameTaken(String userName) async {
    QuerySnapshot querySnapshot = await _firestore
        .collection(FirestoreConstants.users)
        .where(FirestoreConstants.user_name, isEqualTo: userName)
        .get();

    if (querySnapshot.docs.length == 0) {
      return false;
    }

    return true;
  }

  Future<void> updateProfileInfo(
      String userID, MemecistUser user, List<String> keywords) {
    Map profileInfoMap = Mapper.getNonEmptyMap({
      FirestoreConstants.first_name: user.firstName,
      FirestoreConstants.last_name: user.lastName,
      FirestoreConstants.user_name: user.userName,
      FirestoreConstants.profile_pic: user.profilePic,
      FirestoreConstants.birth_date: user.userPrivateData.birthDate
    });

    profileInfoMap[FirestoreConstants.search_keywords] = keywords;

    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .update(profileInfoMap);
  }

  Future<MemecistUser> getMemecistUser(String userID) async {
    MemecistUser user = MemecistUser();

    await _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .get()
        .then((value) {
      user = MemecistUser.fromDocumentSnapshot(value);
    });

    await _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.follower_shards)
        .get()
        .then((querySnapshot) {
      int followersCount = 0;

      for (DocumentSnapshot doc in querySnapshot.docs) {
        followersCount += doc.data()[FirestoreConstants.count];
      }

      user.followersCount = followersCount;
    });

    return user;
  }

  Stream<MemecistUser> getUserStream(String userID) {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .snapshots()
        .map((value) => MemecistUser.fromDocumentSnapshot(value));
  }

  Future<bool> isReacted(String userID, String postID) async {
    QuerySnapshot querySnapshot = await _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.reacts)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get();

    if (querySnapshot.docs.length == 0) return false;

    return true;
  }

  Future<void> toReact(MemecistUser currentUser, String postID) async {
    _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.reacts)
        .doc(currentUser.userID)
        .set({
      FirestoreConstants.user_id: currentUser.userID,
      FirestoreConstants.user_name: currentUser.userName,
      FirestoreConstants.first_name: currentUser.firstName,
      FirestoreConstants.last_name: currentUser.lastName,
      FirestoreConstants.profile_pic: currentUser.profilePic,
      FirestoreConstants.is_verified: currentUser.isVerified,
      FirestoreConstants.search_keywords: currentUser.searchKeywords,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp()
    });
  }

  Future<void> toUnReact(MemecistUser currentUser, String postID) async {
    _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.reacts)
        .doc(currentUser.userID)
        .delete();
  }

  Future<List<Comment>> getInitialCommentsForPost(
      String postID, int limit) async {
    //Not handling long comment list rn. Ideally if very long list of comments,
    //display user comments first and the rest only upto a limit. similar to insta

    return _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.comments)
        .orderBy(FirestoreConstants.create_date_time)
        .limit(limit)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedCommentDocument = querySnapshot.docs.last;

      return querySnapshot.docs.map((documentSnapshot) {
        return Comment.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    });
  }

  Future<List<Comment>> getMoreCommentsForPost(String postID, int limit) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.comments)
        .orderBy(FirestoreConstants.create_date_time)
        .limit(limit)
        .startAfterDocument(_lastFetchedCommentDocument)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedCommentDocument = querySnapshot.docs.last;
      return querySnapshot.docs.map((documentSnapshot) {
        return Comment.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    });
  }

  Future<String> comment(
      MemecistUser currentUser, String postID, String comment) async {
    DocumentReference commentRef = _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.comments)
        .doc();

    commentRef.set({
      FirestoreConstants.user_id: currentUser.userID,
      FirestoreConstants.user_name: currentUser.userName,
      FirestoreConstants.first_name: currentUser.firstName,
      FirestoreConstants.last_name: currentUser.lastName,
      FirestoreConstants.is_verified: currentUser.isVerified,
      FirestoreConstants.profile_pic: currentUser.profilePic,
      FirestoreConstants.comment: comment,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp()
    });

    return commentRef.id;
  }

  Future<List<MemecistUser>> getInitialReacts(String postID, int limit) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.reacts)
        .orderBy(FirestoreConstants.create_date_time)
        .limit(limit)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedReactDocument = querySnapshot.docs.last;

      return querySnapshot.docs.map((doc) {
        return MemecistUser.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<MemecistUser>> getMoreReacts(String postID, int limit) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.reacts)
        .orderBy(FirestoreConstants.create_date_time)
        .startAfterDocument(_lastFetchedReactDocument)
        .limit(limit)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedReactDocument = querySnapshot.docs.last;

      return querySnapshot.docs.map((doc) {
        return MemecistUser.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<MemePost>> getInitialMemePostsByUser(
      int limit, String userID, String currentUserID) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .where(FirestoreConstants.is_for_group, isEqualTo: false)
        .where(FirestoreConstants.is_for_war, isEqualTo: false)
        .limit(limit)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPostsByUserDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: currentUserID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<List<MemePost>> getMoreMemePostsByUser(
      int limit, String userID, String currentUserID) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .where(FirestoreConstants.is_for_group, isEqualTo: false)
        .where(FirestoreConstants.is_for_war, isEqualTo: false)
        .limit(limit)
        .startAfterDocument(_lastFetchedPostsByUserDocument)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPostsByUserDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: currentUserID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<void> followUser(String userID, MemecistUser currentUser) async {
    WriteBatch followBatch = _firestore.batch();

    followBatch.set(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(currentUser.userID)
            .collection(FirestoreConstants.following)
            .doc(userID),
        {
          FirestoreConstants.following_user_id: userID,
          FirestoreConstants.user_id: currentUser.userID,
          FirestoreConstants.user_name: currentUser.userName,
          FirestoreConstants.first_name: currentUser.firstName,
          FirestoreConstants.last_name: currentUser.lastName,
          FirestoreConstants.profile_pic: currentUser.profilePic,
          FirestoreConstants.is_verified: currentUser.isVerified,
          FirestoreConstants.search_keywords: currentUser.searchKeywords,
          FirestoreConstants.create_date_time: FieldValue.serverTimestamp()
        });

    followBatch.set(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(currentUser.userID)
            .collection(FirestoreConstants.private_data)
            .doc(FirestoreConstants.following),
        {
          FirestoreConstants.user_ids: FieldValue.arrayUnion([userID])
        },
        SetOptions(merge: true));

    followBatch.commit();
  }

  Future<void> unFollowUser(String userID, String currentUserID) async {
    WriteBatch unFollowBatch = _firestore.batch();

    unFollowBatch.delete(_firestore
        .collection(FirestoreConstants.users)
        .doc(currentUserID)
        .collection(FirestoreConstants.following)
        .doc(userID));

    unFollowBatch.update(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(currentUserID)
            .collection(FirestoreConstants.private_data)
            .doc(FirestoreConstants.following),
        {
          FirestoreConstants.user_ids: FieldValue.arrayRemove([userID])
        });

    unFollowBatch.commit();
  }

  Future<bool> isFollowingUser(String userID, String currentUserID) async {
    QuerySnapshot querySnapshot = await _firestore
        .collection(FirestoreConstants.users)
        .doc(currentUserID)
        .collection(FirestoreConstants.following)
        .where(FirestoreConstants.following_user_id, isEqualTo: userID)
        .get();

    if (querySnapshot.docs.length == 0) return false;

    return true;
  }

  Future<List<MemecistUser>> getInitialFollowers(
      String userID, int limit) async {
    return _firestore
        .collectionGroup(FirestoreConstants.following)
        .where(FirestoreConstants.following_user_id, isEqualTo: userID)
        .orderBy(FirestoreConstants.create_date_time)
        .limit(limit)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedFollowersDocument = querySnapshot.docs.last;

      return querySnapshot.docs.map((doc) {
        return MemecistUser.fromMap(doc.data());
      }).toList();
    });
  }

  Future<List<MemecistUser>> getMoreFollowers(String userID, int limit) async {
    return _firestore
        .collectionGroup(FirestoreConstants.following)
        .where(FirestoreConstants.following_user_id, isEqualTo: userID)
        .orderBy(FirestoreConstants.create_date_time)
        .limit(limit)
        .startAfterDocument(_lastFetchedFollowersDocument)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedFollowersDocument = querySnapshot.docs.last;

      return querySnapshot.docs.map((doc) {
        return MemecistUser.fromMap(doc.data());
      }).toList();
    });
  }

  Future<String> uploadMemePost(MemePost memePost, {bool isGroupAdmin}) async {
    WriteBatch memePostBatch = _firestore.batch();

    Map<String, dynamic> memePostMap = {
      FirestoreConstants.caption: memePost.caption,
      FirestoreConstants.comments_count: memePost.commentsCount,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.first_name: memePost.user.firstName,
      FirestoreConstants.react_count: 0,
      FirestoreConstants.is_verified: memePost.user.isVerified,
      FirestoreConstants.last_name: memePost.user.lastName,
      FirestoreConstants.media_url: memePost.mediaUrl,
      FirestoreConstants.post_type: memePost.postType,
      FirestoreConstants.profile_pic: memePost.user.profilePic,
      FirestoreConstants.user_id: memePost.user.userID,
      FirestoreConstants.user_name: memePost.user.userName,
      FirestoreConstants.is_for_war: memePost.isForWar,
      FirestoreConstants.is_for_competition: memePost.isForCompetition,
      FirestoreConstants.is_for_group: memePost.isForGroup,
      FirestoreConstants.category: memePost.category,
      FirestoreConstants.image_text: memePost.imageText,
      FirestoreConstants.is_image_text_extracted: memePost.isImageTextExtracted,
      FirestoreConstants.search_keywords: memePost.searchKeywords,
      FirestoreConstants.hashtags: memePost.hashtags
    };

    // Normal post : isForGroup - False; isForWar - false; isForComp - false;
    // Group post : isForGroup - True; isForWar - false; isForComp - false;
    // War Post : isForGroup - True; isForWar - True; isForComp - false;
    // Comp Post: isForGroup - false; isForWar - false; isForComp - True;
    // Home screen : show all;
    // Explore screen : show all;
    // Group screen : show all with isForGroups is true;
    // War screen : show all with isForWar is true;
    // Comp screen : show all with isForComp is true;
    // Updating all counters except team posts counter via cloud function. Since constraint on total memes by a team is there, atomicity needs to be assured
    // Competition post : update user posts and competition posts counter
    // Group post : update group posts counter
    // War post : update group and war posts counter
    // Normal post : update user posts counter

    if (memePost.isForGroup)
      memePostMap.addAll({
        FirestoreConstants.group_id: memePost.groupID,
        FirestoreConstants.group_name: memePost.groupName,
        FirestoreConstants.is_approved: isGroupAdmin ? true : false,
        FirestoreConstants.is_rejected: false
      });

    if (memePost.isForCompetition)
      memePostMap.addAll({
        FirestoreConstants.competition_id: memePost.competitionID,
        FirestoreConstants.competition_name: memePost.competitionName
      });

    if (memePost.isForWar) {
      memePostMap.addAll({
        FirestoreConstants.war_id: memePost.warID,
        FirestoreConstants.war_name: memePost.warName,
        FirestoreConstants.is_approved: true,
        FirestoreConstants.is_rejected: false,
        FirestoreConstants.team_id: memePost.teamID,
        FirestoreConstants.team_name: memePost.teamName
      });

      memePostBatch.update(
          _firestore
              .collection(FirestoreConstants.wars)
              .doc(memePost.warID)
              .collection(FirestoreConstants.teams)
              .doc(memePost.teamID),
          {FirestoreConstants.memes_count: FieldValue.increment(1)});
    }

    DocumentReference memePostRef =
        _firestore.collection(FirestoreConstants.posts).doc();

    memePostMap.addAll({FirestoreConstants.post_id: memePostRef.id});

    memePostBatch.set(memePostRef, memePostMap);

    await memePostBatch.commit();

    return memePostRef.id;
  }

  Future<String> createGroup(MemecistUser currentUser, Group group) async {
    WriteBatch createGroupBatch = _firestore.batch();

    DocumentReference newGroupDocRef =
        _firestore.collection(FirestoreConstants.groups).doc();

    createGroupBatch.set(newGroupDocRef, {
      FirestoreConstants.group_name: group.groupName,
      FirestoreConstants.group_description: group.groupDescription,
      FirestoreConstants.is_closed: group.isClosed,
      FirestoreConstants.created_by: currentUser.userID,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.group_profile_pic: group.groupProfilePic ??
          FirebaseStorageConstants.defaultProfilePicUrl,
      FirestoreConstants.memes_count: 0,
      FirestoreConstants.wars_count: 0,
      FirestoreConstants.members_count: 1,
      FirestoreConstants.unapproved_memes_count: 0,
      FirestoreConstants.wars_left_this_month: 2,
      FirestoreConstants.is_verified: false,
      FirestoreConstants.is_war_active: false,
      FirestoreConstants.group_id: newGroupDocRef.id,
      FirestoreConstants.search_keywords: group.searchKeywords
    });

//    Map<String, dynamic> map = {
//      FirestoreConstants.create_date_time: DateTime.now(),
//      FirestoreConstants.group_name: group.groupName,
//      FirestoreConstants.group_description: group.groupDescription,
//      FirestoreConstants.group_profile_pic: group.groupProfilePic
//    };
//
//    createGroupBatch.set(
//        _firestore
//            .collection(FirestoreConstants.groups)
//            .doc(newGroupDocRef.id)
//            .collection(FirestoreConstants.group_data)
//            .doc(FirestoreConstants.group_details),
//        {
//          FirestoreConstants.last_name_change_date_time:
//              DateTime(1970, 1, 1, 0, 0, 0, 0),
//          FirestoreConstants.changes_history: FieldValue.arrayUnion([map])
//        });

    createGroupBatch.set(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(currentUser.userID)
            .collection(FirestoreConstants.private_data)
            .doc(FirestoreConstants.groups),
        {
          FirestoreConstants.group_ids:
              FieldValue.arrayUnion([newGroupDocRef.id])
        },
        SetOptions(merge: true));

    await createGroupBatch.commit();

    /*Keeping this here instead of onGroupCreated(cloud) for instant UI change*/
    await newGroupDocRef
        .collection(FirestoreConstants.group_admins)
        .doc(currentUser.userID)
        .set({
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.join_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.user_id: currentUser.userID,
      FirestoreConstants.first_name: currentUser.firstName,
      FirestoreConstants.last_name: currentUser.lastName,
      FirestoreConstants.profile_pic: currentUser.profilePic,
      FirestoreConstants.user_name: currentUser.userName,
      FirestoreConstants.is_verified: currentUser.isVerified,
      FirestoreConstants.search_keywords: currentUser.searchKeywords
    });

    return newGroupDocRef.id;
  }

  Future<Group> getGroup(String groupID) async {
    Group group = Group();

    await _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .get()
        .then((value) {
      group = Group.fromDocumentSnapshot(value);
    });

    await _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.member_shards)
        .get()
        .then((querySnapshot) {
      int membersCount = 0;

      for (DocumentSnapshot doc in querySnapshot.docs) {
        membersCount += doc.data()[FirestoreConstants.count];
      }

      group.membersCount = membersCount;
    });

    return group;
  }

  Future<bool> isAdmin(String groupID, String userID) async {
    QuerySnapshot adminQuery = await _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.group_admins)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get();

    if (adminQuery.docs.length != 0) return true;

    return false;
  }

  Future<bool> isMember(String groupID, String userID) async {
    QuerySnapshot membersQuery = await _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.group_members)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get();

    if (membersQuery.docs.length != 0) return true;

    return false;
  }

  Future<void> joinGroup(String groupID, MemecistUser user) async {
    WriteBatch groupBatch = _firestore.batch();

    groupBatch.set(
        _firestore
            .collection(FirestoreConstants.groups)
            .doc(groupID)
            .collection(FirestoreConstants.group_members)
            .doc(user.userID),
        {
          FirestoreConstants.join_date_time: FieldValue.serverTimestamp(),
          FirestoreConstants.user_id: user.userID,
          FirestoreConstants.user_name: user.userName,
          FirestoreConstants.first_name: user.firstName,
          FirestoreConstants.last_name: user.lastName,
          FirestoreConstants.is_verified: user.isVerified,
          FirestoreConstants.profile_pic: user.profilePic,
          FirestoreConstants.search_keywords: user.searchKeywords
        });

    groupBatch.set(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(user.userID)
            .collection(FirestoreConstants.private_data)
            .doc(FirestoreConstants.groups),
        {
          FirestoreConstants.group_ids: FieldValue.arrayUnion([groupID])
        },
        SetOptions(merge: true));

    groupBatch.commit();
  }

  Future<List<MemecistUser>> getAdmins(String groupID) async {
    return _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.group_admins)
        .get()
        .then((querySnapshot) {
      return querySnapshot.docs.map((doc) {
        return MemecistUser.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<MemecistUser>> getInitialMembers(
      String groupID, int limit) async {
    return await _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.group_members)
        .orderBy(FirestoreConstants.join_date_time)
        .limit(limit)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedMembersDocument = querySnapshot.docs.last;

      return querySnapshot.docs.map((doc) {
        return MemecistUser.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<MemecistUser>> getMoreMembers(String groupID, int limit) async {
    return await _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.group_members)
        .orderBy(FirestoreConstants.join_date_time)
        .limit(limit)
        .startAfterDocument(_lastFetchedMembersDocument)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedMembersDocument = querySnapshot.docs.last;

      return querySnapshot.docs.map((doc) {
        return MemecistUser.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<MemePost>> getInitialMemePostsByGroup(
      int limit, String groupID, String userID) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.group_id, isEqualTo: groupID)
        .where(FirestoreConstants.is_for_group, isEqualTo: true)
        .where(FirestoreConstants.is_approved, isEqualTo: true)
        .limit(limit)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPostsByGroupDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: userID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<List<MemePost>> getMoreMemePostsByGroup(
      int limit, String groupID, String userID) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.group_id, isEqualTo: groupID)
        .where(FirestoreConstants.is_for_group, isEqualTo: true)
        .where(FirestoreConstants.is_approved, isEqualTo: true)
        .limit(limit)
        .startAfterDocument(_lastFetchedPostsByGroupDocument)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPostsByGroupDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: userID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<List<Group>> getGroupsForUser(String userID) async {
    List<Future> futures = [];
    List<Future> futures1 = [];

    List<Group> groups = [];

    futures.add(_firestore
        .collectionGroup(FirestoreConstants.group_members)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> members = querySnapshot.docs;

      List<String> groupIDs =
          members.map((member) => member.reference.parent.parent.id).toList();

      for (String groupID in groupIDs) {
        futures1.add(_firestore
            .collection(FirestoreConstants.groups)
            .doc(groupID)
            .get()
            .then((value) {
          groups.add(Group.fromDocumentSnapshot(value));
        }));
      }
    }));

    futures.add(_firestore
        .collectionGroup(FirestoreConstants.group_admins)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> members = querySnapshot.docs;

      List<String> groupIDs =
          members.map((member) => member.reference.parent.parent.id).toList();

      for (String groupID in groupIDs) {
        futures1.add(_firestore
            .collection(FirestoreConstants.groups)
            .doc(groupID)
            .get()
            .then((value) {
          groups.add(Group.fromDocumentSnapshot(value));
        }));
      }
    }));

    await Future.wait(futures);
    await Future.wait(futures1);

    return groups;
  }

  Future<String> startWar(War war) async {
    DocumentReference reference =
        _firestore.collection(FirestoreConstants.wars).doc();

    await reference.set({
      FirestoreConstants.start_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.group_id: war.groupID,
      FirestoreConstants.group_name: war.groupName,
      FirestoreConstants.group_profile_pic: war.groupProfilePic,
      FirestoreConstants.war_name: war.warName,
      FirestoreConstants.war_pic: war.warPic,
      FirestoreConstants.war_status: FirestoreConstants.team_leaders_selection,
      FirestoreConstants.war_description: war.warDescription,
      FirestoreConstants.war_rules: war.warRules,
      FirestoreConstants.created_by: war.createdBy,
      FirestoreConstants.is_war_active: true,
      FirestoreConstants.max_teams_count: war.maxTeamsCount,
      FirestoreConstants.posts_count: 0,
      FirestoreConstants.per_team_limit: 4,
      FirestoreConstants.are_team_leaders_selected: false,
      FirestoreConstants.team_leaders_application_count: 0,
      FirestoreConstants.team_members_application_count: 0,
      FirestoreConstants.card_color: war.cardColor
    });

    return reference.id;
  }

  Future<List<Group>> getGroupsWithActiveWars(String userID) async {
    List<Future> futures = [];
    List<Future> futures1 = [];

    List<Group> groups = [];

    futures.add(_firestore
        .collectionGroup(FirestoreConstants.group_members)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> members = querySnapshot.docs;

      List<String> groupIDs =
          members.map((member) => member.reference.parent.parent.id).toList();

      for (String groupID in groupIDs) {
        futures1.add(_firestore
            .collection(FirestoreConstants.groups)
            .where(FieldPath.documentId, isEqualTo: groupID)
            .where(FirestoreConstants.is_war_active, isEqualTo: true)
            .get()
            .then((value) {
          List<DocumentSnapshot> documentSnapshots = value.docs;

          for (DocumentSnapshot documentSnapshot in documentSnapshots) {
            groups.add(Group.fromDocumentSnapshot(documentSnapshot));
          }
        }));
      }
    }));

    futures.add(_firestore
        .collectionGroup(FirestoreConstants.group_admins)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> members = querySnapshot.docs;

      List<String> groupIDs =
          members.map((member) => member.reference.parent.parent.id).toList();

      for (String groupID in groupIDs) {
        futures1.add(_firestore
            .collection(FirestoreConstants.groups)
            .where(FieldPath.documentId, isEqualTo: groupID)
            .where(FirestoreConstants.is_war_active, isEqualTo: true)
            .get()
            .then((value) {
          List<DocumentSnapshot> documentSnapshots = value.docs;

          for (DocumentSnapshot documentSnapshot in documentSnapshots) {
            groups.add(Group.fromDocumentSnapshot(documentSnapshot));
          }
        }));
      }
    }));

    await Future.wait(futures);
    await Future.wait(futures1);

    return groups;
  }

  Future<MemePost> getMemePost(String postID, String userID) async {
    MemePost memePost = await _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .get()
        .then((documentSnapshot) =>
            MemePost.fromDocumentSnapshot(documentSnapshot));

    List<Future> futures = [];

    futures.add(_firestore
        .collection(FirestoreConstants.posts)
        .doc(memePost.postID)
        .collection(FirestoreConstants.reacts)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length != 0) memePost.isReacted = true;
    }));

    futures.add(_firestore
        .collection(FirestoreConstants.posts)
        .doc(memePost.postID)
        .collection(FirestoreConstants.react_shards)
        .get()
        .then((querySnapshot) {
      int hahaCount = 0;

      for (DocumentSnapshot doc in querySnapshot.docs) {
        hahaCount += doc.data()[FirestoreConstants.count];
      }

      memePost.reactCount = hahaCount;
    }));

    futures.add(_firestore
        .collection(FirestoreConstants.posts)
        .doc(memePost.postID)
        .collection(FirestoreConstants.comment_shards)
        .get()
        .then((querySnapshot) {
      int commentsCount = 0;

      for (DocumentSnapshot doc in querySnapshot.docs) {
        commentsCount += doc.data()[FirestoreConstants.count];
      }

      memePost.commentsCount = commentsCount;
    }));

    await Future.wait(futures);

    return memePost;
  }

  Future<void> removeMember(
      String userID, GroupMember member, String groupID) async {
    if (member.isAdmin) {
      _firestore
          .collection(FirestoreConstants.groups)
          .doc(groupID)
          .collection(FirestoreConstants.group_admins)
          .doc(member.user.userID)
          .delete();
    } else {
      _firestore
          .collection(FirestoreConstants.groups)
          .doc(groupID)
          .collection(FirestoreConstants.group_members)
          .doc(member.user.userID)
          .delete();
    }
  }

  Future<List<MemePost>> getInitialUnapprovedMemePostsByGroup(
      int limit, String groupID) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.group_id, isEqualTo: groupID)
        .where(FirestoreConstants.is_for_group, isEqualTo: true)
        .where(FirestoreConstants.is_approved, isEqualTo: false)
        .where(FirestoreConstants.is_rejected, isEqualTo: false)
        .limit(limit)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedUnapprovedMemePostByGroupDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    });
  }

  Future<List<MemePost>> getMoreUnapprovedMemePostsByGroup(
      int limit, String groupID) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.group_id, isEqualTo: groupID)
        .where(FirestoreConstants.is_for_group, isEqualTo: true)
        .where(FirestoreConstants.is_approved, isEqualTo: false)
        .where(FirestoreConstants.is_rejected, isEqualTo: false)
        .limit(limit)
        .startAfterDocument(_lastFetchedUnapprovedMemePostByGroupDocument)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedUnapprovedMemePostByGroupDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    });
  }

  Future<void> rejectMeme(String postID, String userID, String groupID) async {
    WriteBatch rejectBatch = _firestore.batch();

    rejectBatch
        .update(_firestore.collection(FirestoreConstants.posts).doc(postID), {
      FirestoreConstants.is_rejected: true,
      FirestoreConstants.rejected_by: userID,
      FirestoreConstants.reject_date_time: FieldValue.serverTimestamp()
    });

    rejectBatch
        .update(_firestore.collection(FirestoreConstants.groups).doc(groupID), {
      FirestoreConstants.memes_count: FieldValue.increment(1),
      FirestoreConstants.unapproved_memes_count: FieldValue.increment(-1)
    });

    rejectBatch.commit();
  }

  Future<void> leaveGroup(String userID, String groupID, bool isAdmin) async {
    WriteBatch leaveGroupBatch = _firestore.batch();

    if (isAdmin) {
      leaveGroupBatch.delete(_firestore
          .collection(FirestoreConstants.groups)
          .doc(groupID)
          .collection(FirestoreConstants.group_admins)
          .doc(userID));
    } else {
      leaveGroupBatch.delete(_firestore
          .collection(FirestoreConstants.groups)
          .doc(groupID)
          .collection(FirestoreConstants.group_members)
          .doc(userID));
    }

    leaveGroupBatch.update(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(userID)
            .collection(FirestoreConstants.private_data)
            .doc(FirestoreConstants.groups),
        {
          FirestoreConstants.group_ids: FieldValue.arrayRemove([groupID])
        });

    leaveGroupBatch.commit();
  }

  Future<List<MemePost>> getInitialPendingApprovalMemes(
      int limit, String groupID, String userID) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.group_id, isEqualTo: groupID)
        .where(FirestoreConstants.is_for_group, isEqualTo: true)
        .where(FirestoreConstants.is_approved, isEqualTo: false)
        .where(FirestoreConstants.is_rejected, isEqualTo: false)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .limit(limit)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPendingApprovalMemePostsDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    });
  }

  Future<List<MemePost>> getMorePendingApprovalMemes(
      int limit, String groupID, String userID) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.group_id, isEqualTo: groupID)
        .where(FirestoreConstants.is_for_group, isEqualTo: true)
        .where(FirestoreConstants.is_approved, isEqualTo: false)
        .where(FirestoreConstants.is_rejected, isEqualTo: false)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .limit(limit)
        .startAfterDocument(_lastFetchedPendingApprovalMemePostsDocument)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPendingApprovalMemePostsDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    });
  }

  Future<List<MemecistUser>> searchUsers(String query) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .where(FirestoreConstants.search_keywords,
            arrayContains: query.toLowerCase())
        .orderBy(FirestoreConstants.followers_count, descending: true)
        .limit(10)
        .get()
        .then((snapshot) {
      List<DocumentSnapshot> docs = snapshot.docs;

      return docs.map((doc) {
        return MemecistUser.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<void> makeAdmin(String groupID, MemecistUser user) async {
    WriteBatch adminBatch = _firestore.batch();

    adminBatch.delete(_firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.group_members)
        .doc(user.userID));

    adminBatch.set(
        _firestore
            .collection(FirestoreConstants.groups)
            .doc(groupID)
            .collection(FirestoreConstants.group_admins)
            .doc(user.userID),
        {
          FirestoreConstants.join_date_time: FieldValue.serverTimestamp(),
          FirestoreConstants.user_id: user.userID,
          FirestoreConstants.first_name: user.firstName,
          FirestoreConstants.last_name: user.lastName,
          FirestoreConstants.profile_pic: user.profilePic,
          FirestoreConstants.user_name: user.userName,
          FirestoreConstants.is_verified: user.isVerified,
          FirestoreConstants.search_keywords: user.searchKeywords
        });

    adminBatch.commit();
  }

  Future<List<MemecistUser>> searchMembers(String groupID, String query) async {
    return await _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.group_members)
        .where(FirestoreConstants.search_keywords,
            arrayContains: query.toLowerCase())
        .limit(5)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedMembersDocument = querySnapshot.docs.last;

      return querySnapshot.docs.map((doc) {
        return MemecistUser.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<void> addSessionDetails(String userID, IP ip) async {
    WriteBatch sessionBatch = _firestore.batch();

    sessionBatch.set(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(userID)
            .collection(FirestoreConstants.sessions)
            .doc(),
        {
          FirestoreConstants.ip: ip.ip,
          FirestoreConstants.city: ip.city,
          FirestoreConstants.region: ip.region,
          FirestoreConstants.country_name: ip.countryName,
          FirestoreConstants.country_code_iso3: ip.countryCodeIso3,
          FirestoreConstants.country_calling_code: ip.countryCallingCode,
          FirestoreConstants.postal: ip.postal,
          FirestoreConstants.latitude: ip.latitude,
          FirestoreConstants.longitude: ip.longitude,
          FirestoreConstants.timezone: ip.timezone,
          FirestoreConstants.utc_offset: ip.utcOffset,
          FirestoreConstants.asn: ip.asn,
          FirestoreConstants.org: ip.org,
          FirestoreConstants.create_date_time: FieldValue.serverTimestamp()
        });

    sessionBatch.set(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(userID)
            .collection(FirestoreConstants.private_data)
            .doc(FirestoreConstants.session_data),
        {FirestoreConstants.total_sessions: FieldValue.increment(1)},
        SetOptions(merge: true));

    return sessionBatch.commit();
  }

  Future<War> getWar(String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .get()
        .then((snapshot) {
      return War.fromDocumentSnapshot(snapshot);
    });
  }

  Future<WarLeaderApplication> getTeamLeaderApplication(
      String warID, String userID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_leaders_applications)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length == 0)
        return null;
      else
        return WarLeaderApplication.fromDocumentSnapshot(querySnapshot.docs[0]);
    });
  }

  Future<List<WarLeaderApplication>> getInitialTeamLeaderApplications(
      String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_leaders_applications)
        .orderBy(FirestoreConstants.rep, descending: true)
        .limit(10)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> docs = querySnapshot.docs;

      if (docs.length > 0)
        _lastFetchedTeamLeadersApplicationDocument = docs.last;

      return docs.map((doc) {
        return WarLeaderApplication.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<WarLeaderApplication>> getMoreTeamLeaderApplications(
      String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_leaders_applications)
        .orderBy(FirestoreConstants.rep, descending: true)
        .startAfterDocument(_lastFetchedTeamLeadersApplicationDocument)
        .limit(10)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> docs = querySnapshot.docs;

      if (docs.length > 0)
        _lastFetchedTeamLeadersApplicationDocument = docs.last;

      return docs.map((doc) {
        return WarLeaderApplication.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<void> approveTeamLeaders(List<WarLeaderApplication> warApplications,
      String warID, String warName) async {
    WriteBatch approveBatch = _firestore.batch();

    List<String> teamLeaders = [];

    for (int i = 0; i < warApplications.length; i++) {
      Map<String, dynamic> user = Map();
      user[FirestoreConstants.user_id] = warApplications[i].user.userID;
      user[FirestoreConstants.user_name] = warApplications[i].user.userName;
      user[FirestoreConstants.first_name] = warApplications[i].user.firstName;
      user[FirestoreConstants.last_name] = warApplications[i].user.lastName;
      user[FirestoreConstants.profile_pic] = warApplications[i].user.profilePic;
      user[FirestoreConstants.rep] =
          double.parse(warApplications[i].user.rep.toStringAsPrecision(3));
      user[FirestoreConstants.is_verified] = warApplications[i].user.isVerified;

      DocumentReference teamRef = _firestore
          .collection(FirestoreConstants.wars)
          .doc(warID)
          .collection(FirestoreConstants.teams)
          .doc();

      List<String> members = [];
      members.add(warApplications[i].user.userID);

      approveBatch.set(teamRef, {
        FirestoreConstants.team_name: "TEAM ${i + 1}",
        FirestoreConstants.team_pic:
            FirebaseStorageConstants.defaultTeamIconUrl,
        FirestoreConstants.team_rep_score: warApplications[i].user.rep,
        FirestoreConstants.members_count: 1,
        FirestoreConstants.memes_count: 0,
        FirestoreConstants.team_leader: user,
        FirestoreConstants.members: members,
        FirestoreConstants.is_team_renamed: false,
        FirestoreConstants.create_date_time: FieldValue.serverTimestamp()
      });

      approveBatch.update(
          _firestore
              .collection(FirestoreConstants.wars)
              .doc(warID)
              .collection(FirestoreConstants.team_leaders_applications)
              .doc(warApplications[i].user.userID),
          {FirestoreConstants.is_accepted: true});

      DocumentReference warRoomRef =
          _firestore.collection(FirestoreConstants.war_rooms).doc(teamRef.id);

      approveBatch.set(warRoomRef, {
        FirestoreConstants.team_id: teamRef.id,
        FirestoreConstants.team_name: "TEAM ${i + 1}",
        FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
        FirestoreConstants.messages_count: 0,
        FirestoreConstants.war_name: warName,
        FirestoreConstants.war_id: warID,
        FirestoreConstants.chat_participants:
            FieldValue.arrayUnion([warApplications[i].user.userID]),
        FirestoreConstants.team_pic:
            FirebaseStorageConstants.defaultTeamIconUrl,
        FirestoreConstants.last_message: "War room created!",
        FirestoreConstants.last_message_time: FieldValue.serverTimestamp(),
        FirestoreConstants.participants_count: 1
      });

      approveBatch.set(
          warRoomRef
              .collection(FirestoreConstants.chat_participants)
              .doc(warApplications[i].user.userID),
          {
            FirestoreConstants.user_id: warApplications[i].user.userID,
            FirestoreConstants.user_name: warApplications[i].user.userName,
            FirestoreConstants.first_name: warApplications[i].user.firstName,
            FirestoreConstants.last_name: warApplications[i].user.lastName,
            FirestoreConstants.profile_pic: warApplications[i].user.profilePic,
            FirestoreConstants.is_verified: warApplications[i].user.isVerified,
            FirestoreConstants.create_date_time: FieldValue.serverTimestamp()
          });

      teamLeaders.add(warApplications[i].user.userID);
    }

    approveBatch
        .update(_firestore.collection(FirestoreConstants.wars).doc(warID), {
      FirestoreConstants.are_team_leaders_selected: true,
      FirestoreConstants.team_leaders: teamLeaders,
      FirestoreConstants.war_participants: teamLeaders
    });

    approveBatch.commit();
  }

  Future<void> applyForTeamLeader(MemecistUser user, String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_leaders_applications)
        .doc(user.userID)
        .set({
      FirestoreConstants.user_id: user.userID,
      FirestoreConstants.user_name: user.userName,
      FirestoreConstants.first_name: user.firstName,
      FirestoreConstants.last_name: user.lastName,
      FirestoreConstants.profile_pic: user.profilePic,
      FirestoreConstants.is_verified: user.isVerified,
      FirestoreConstants.search_keywords: user.searchKeywords,
      FirestoreConstants.rep: double.parse(user.rep.toString()),
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.is_accepted: false
    });
  }

  Future<List<WarLeaderApplication>> getSelectedTeamLeaders(
      String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_leaders_applications)
        .where(FirestoreConstants.is_accepted, isEqualTo: true)
        .orderBy(FirestoreConstants.rep, descending: true)
        .get()
        .then((querySnapshot) {
      return querySnapshot.docs
          .map((doc) => WarLeaderApplication.fromDocumentSnapshot(doc))
          .toList();
    });
  }

  Stream<List<WarRoom>> getWarRoomsStream(String userID) {
    return _firestore
        .collection(FirestoreConstants.war_rooms)
        .where(FirestoreConstants.chat_participants, arrayContains: userID)
        .orderBy(FirestoreConstants.last_message_time, descending: true)
        .limit(20)
        .snapshots()
        .map((querySnapshot) {
      return querySnapshot.docs
          .map((doc) => WarRoom.fromDocumentSnapshot(doc))
          .toList();
    });
  }

  Future<List<WarRoom>> getAllWarRooms(String userID) async {
    return _firestore
        .collection(FirestoreConstants.war_rooms)
        .where(FirestoreConstants.chat_participants, arrayContains: userID)
        .orderBy(FirestoreConstants.last_message_time, descending: true)
        .get()
        .then((querySnapshot) => querySnapshot.docs
            .map((doc) => WarRoom.fromDocumentSnapshot(doc))
            .toList());
  }

  Future<WarRoom> getWarRoom(String warRoomID) async {
    return _firestore
        .collection(FirestoreConstants.war_rooms)
        .doc(warRoomID)
        .get()
        .then((value) => WarRoom.fromDocumentSnapshot(value));
  }

  Stream<List<Message>> getRecentMessages(String warRoomID, bool isNewRun) {
    if (isNewRun) _lastFetchedWarRoomMessageDocument = null;

    return _firestore
        .collection(FirestoreConstants.war_rooms)
        .doc(warRoomID)
        .collection(FirestoreConstants.war_room_messages)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .limit(20)
        .snapshots()
        .map((querySnapshot) {
      List<DocumentSnapshot> docs = querySnapshot.docs;
      if (_lastFetchedWarRoomMessageDocument == null && docs.length > 0) {
        _lastFetchedWarRoomMessageDocument = docs.last;
      }
      return docs.map((doc) {
        return Message.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<void> sendMessage(Message message, String warRoomID) async {
    return _firestore.runTransaction((transaction) async {
      await transaction.set(
          _firestore
              .collection(FirestoreConstants.war_rooms)
              .doc(warRoomID)
              .collection(FirestoreConstants.war_room_messages)
              .doc(),
          {
            FirestoreConstants.content: message.content,
            FirestoreConstants.type: message.type,
            FirestoreConstants.user_id: message.sender.userID,
            FirestoreConstants.user_name: message.sender.userName,
            FirestoreConstants.first_name: message.sender.firstName,
            FirestoreConstants.last_name: message.sender.lastName,
            FirestoreConstants.profile_pic: message.sender.profilePic,
            FirestoreConstants.is_verified: message.sender.isVerified,
            FirestoreConstants.create_date_time: FieldValue.serverTimestamp()
          });

      await transaction.update(
          _firestore.collection(FirestoreConstants.war_rooms).doc(warRoomID), {
        FirestoreConstants.last_message:
            message.type == FirestoreConstants.text_message
                ? message.content
                : FirestoreConstants.Photo,
        FirestoreConstants.members_count: FieldValue.increment(1),
        FirestoreConstants.last_message_time: FieldValue.serverTimestamp(),
      });
    });
  }

  Future<List<Message>> getMoreMessages(String warRoomID, int docLimit) async {
    return _firestore
        .collection(FirestoreConstants.war_rooms)
        .doc(warRoomID)
        .collection(FirestoreConstants.war_room_messages)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .startAfterDocument(_lastFetchedWarRoomMessageDocument)
        .limit(docLimit)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> docs = querySnapshot.docs;

      if (docs.length > 0) _lastFetchedWarRoomMessageDocument = docs.last;

      return docs.map((doc) {
        return Message.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<WarMemberApplication> getTeamMemberApplication(
      String warID, String userID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_members_applications)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length == 0)
        return null;
      else
        return WarMemberApplication.fromDocumentSnapshot(querySnapshot.docs[0]);
    });
  }

  Future<void> applyForTeamMember(MemecistUser user, String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_members_applications)
        .doc(user.userID)
        .set({
      FirestoreConstants.user_id: user.userID,
      FirestoreConstants.user_name: user.userName,
      FirestoreConstants.first_name: user.firstName,
      FirestoreConstants.last_name: user.lastName,
      FirestoreConstants.profile_pic: user.profilePic,
      FirestoreConstants.is_verified: user.isVerified,
      FirestoreConstants.search_keywords: user.searchKeywords,
      FirestoreConstants.rep: double.parse(user.rep.toString()),
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.is_accepted: false
    });
  }

  Future<List<WarMemberApplication>> getInitialTeamMemberApplications(
      String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_members_applications)
        .orderBy(FirestoreConstants.rep, descending: true)
        .limit(10)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> docs = querySnapshot.docs;

      if (docs.length > 0)
        _lastFetchedTeamMembersApplicationDocument = docs.last;

      return docs.map((doc) {
        return WarMemberApplication.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<WarMemberApplication>> getMoreTeamMemberApplications(
      String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.team_members_applications)
        .orderBy(FirestoreConstants.rep, descending: true)
        .startAfterDocument(_lastFetchedTeamMembersApplicationDocument)
        .limit(10)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> docs = querySnapshot.docs;

      if (docs.length > 0)
        _lastFetchedTeamMembersApplicationDocument = docs.last;

      return docs.map((doc) {
        return WarMemberApplication.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<WarTeam> getWarTeam(String warID, String memberID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.teams)
        .where(FirestoreConstants.members, arrayContains: memberID)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> docs = querySnapshot.docs;

      if (docs.length > 0) {
        return WarTeam.fromDocumentSnapshot(docs[0]);
      }

      return null;
    });
  }

  Future<void> approveTeamMembers(List<WarMemberApplication> warApplications,
      String warID, String teamID) async {
    WriteBatch teamMembersBatch = _firestore.batch();
    List<String> teamMembers = [];
    List<Map<String, dynamic>> members = [];
    double rep = 0;

    for (WarApplication application in warApplications) {
      rep += application.user.rep;
      teamMembers.add(application.user.userID);

      teamMembersBatch.update(
          _firestore
              .collection(FirestoreConstants.wars)
              .doc(warID)
              .collection(FirestoreConstants.team_members_applications)
              .doc(application.user.userID),
          {FirestoreConstants.is_accepted: true});

      teamMembersBatch.set(
          _firestore
              .collection(FirestoreConstants.war_rooms)
              .doc(teamID)
              .collection(FirestoreConstants.chat_participants)
              .doc(application.user.userID),
          {
            FirestoreConstants.user_id: application.user.userID,
            FirestoreConstants.user_name: application.user.userName,
            FirestoreConstants.first_name: application.user.firstName,
            FirestoreConstants.last_name: application.user.lastName,
            FirestoreConstants.profile_pic: application.user.profilePic,
            FirestoreConstants.is_verified: application.user.isVerified,
            FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
            FirestoreConstants.rep: application.user.rep
          });

      members.add({
        FirestoreConstants.user_id: application.user.userID,
        FirestoreConstants.user_name: application.user.userName,
        FirestoreConstants.first_name: application.user.firstName,
        FirestoreConstants.last_name: application.user.lastName,
        FirestoreConstants.profile_pic: application.user.profilePic,
        FirestoreConstants.is_verified: application.user.isVerified,
        FirestoreConstants.rep: application.user.rep
      });
    }

    teamMembersBatch.update(
        _firestore
            .collection(FirestoreConstants.wars)
            .doc(warID)
            .collection(FirestoreConstants.teams)
            .doc(teamID),
        {
          FirestoreConstants.team_members: FieldValue.arrayUnion(members),
          FirestoreConstants.members: FieldValue.arrayUnion(teamMembers),
          FirestoreConstants.members_count:
              FieldValue.increment(warApplications.length),
          FirestoreConstants.team_rep_score: FieldValue.increment(rep)
        });

    List<dynamic> chatParticipants = [];

    warApplications.forEach((application) {
      chatParticipants.add(application.user.userID);
    });

    teamMembersBatch.update(
        _firestore.collection(FirestoreConstants.war_rooms).doc(teamID), {
      FirestoreConstants.participants_count:
          FieldValue.increment(warApplications.length),
      FirestoreConstants.chat_participants:
          FieldValue.arrayUnion(chatParticipants)
    });

    teamMembersBatch.commit();
  }

  Future<List<MemePost>> getInitialMemePostsByWar(
      int limit, String warID, String userID) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.war_id, isEqualTo: warID)
        .limit(limit)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPostsByWarDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: userID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<List<MemePost>> getMoreMemePostsByWar(
      int limit, String warID, String userID) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.war_id, isEqualTo: warID)
        .limit(limit)
        .startAfterDocument(_lastFetchedPostsByWarDocument)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPostsByWarDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: userID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<List<WarTeam>> getAllWarTeams(String warID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.teams)
        .get()
        .then((querySnapshot) => querySnapshot.docs
            .map((team) => WarTeam.fromMap(team.data()))
            .toList());
  }

  Future<List<Competition>> getActiveCompetitions(String country) async {
    return _firestore
        .collection(FirestoreConstants.competitions)
        .where(FirestoreConstants.is_competition_active, isEqualTo: true)
        .where(FirestoreConstants.countries, arrayContains: country)
        .orderBy(FirestoreConstants.display_priority, descending: true)
        .get()
        .then((querySnapshot) => querySnapshot.docs
            .map((documentSnapshot) =>
                Competition.fromDocumentSnapshot(documentSnapshot))
            .toList());
  }

  Future<Competition> getCompetition(String competitionID) async {
    return _firestore
        .collection(FirestoreConstants.competitions)
        .doc(competitionID)
        .get()
        .then((documentSnapshot) =>
            Competition.fromDocumentSnapshot(documentSnapshot));
  }

  //winner_new in winners of 9JfNGuUSplpyTShPCDsT is the sample
  Future<List<CompetitionWinnerGroup>> getCompetitionWinners(
      String competitionID) async {
    return _firestore
        .collection(FirestoreConstants.competitions)
        .doc(competitionID)
        .collection(FirestoreConstants.competition_details)
        .doc(FirestoreConstants.winners)
        .get()
        .then((documentSnapshot) {
      List<CompetitionWinnerGroup> winners = [];
      documentSnapshot
          .data()[FirestoreConstants.winners]
          .forEach((doc) => winners.add(CompetitionWinnerGroup.fromMap(doc)));

      return winners;
    });
  }

  Future<void> updateLinksForCompetitionMemePost(
      String postID, link1, link2, link3) async {
    return _firestore.collection(FirestoreConstants.posts).doc(postID).update({
      FirestoreConstants.link1: link1,
      FirestoreConstants.link2: link2,
      FirestoreConstants.link3: link3
    });
  }

  Future<void> updateLinksForMemecistSocial(String postID, link1, link2) async {
    return _firestore.collection(FirestoreConstants.posts).doc(postID).update({
      FirestoreConstants.link1: link1,
      FirestoreConstants.link2: link2,
      FirestoreConstants.is_for_memecist_social: true
      //Not setting is for competition to true - otherwise competition name will be shown in the post widget
      //is for memecist social will be used for querying posts in a time range and eligible for winner
    });
  }

  Future<Promo> getPromos(String userID) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.private_data)
        .doc(FirestoreConstants.social_media_ids)
        .get()
        .then((documentSnapshot) {
      if (documentSnapshot.data() != null)
        return Promo.fromDocumentSnapshot(documentSnapshot);
      else
        return Promo("", "", "", false);
    });
  }

  Future<void> savePromos(String userID, Promo promo) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.private_data)
        .doc(FirestoreConstants.social_media_ids)
        .set({
      FirestoreConstants.facebook: promo.facebookID,
      FirestoreConstants.instagram: promo.instagramID,
      FirestoreConstants.memecist: promo.memecistID
    });
  }

  Future<List<TemplateCategory>> getTemplateCategories(String country) async {
    List<TemplateCategory> templateCategories = [];

    return _firestore
        .collection(FirestoreConstants.template_categories)
        .doc(country)
        .get()
        .then((value) {
      if (value.exists) {
        for (int i = 0;
            i < value.data()[FirestoreConstants.template_categories].length;
            i++) {
          templateCategories.add(TemplateCategory.fromMap(
              value.data()[FirestoreConstants.template_categories][i]));
        }
      }
      return templateCategories;
    });
  }

  Future<List<TemplateCategory>> getAllTemplateCategories() async {
    List<TemplateCategory> templateCategories = [];

    return _firestore
        .collection(FirestoreConstants.template_categories)
        .doc("ALL")
        .get()
        .then((value) {
      if (value.exists) {
        for (int i = 0;
            i < value.data()[FirestoreConstants.template_categories].length;
            i++) {
          templateCategories.add(TemplateCategory.fromMap(
              value.data()[FirestoreConstants.template_categories][i]));
        }
      }

      return templateCategories;
    });
  }

  Future<List<Template>> getInitialMemeTemplatesByCategory(
      String categoryID, int limit) async {
    return _firestore
        .collection(FirestoreConstants.meme_templates)
        .where(FirestoreConstants.template_category_id, isEqualTo: categoryID)
        .where(FirestoreConstants.is_approved, isEqualTo: true)
        .limit(limit)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedMemeTemplateDocument = querySnapshot.docs.last;

      return querySnapshot.docs
          .map((doc) => Template.fromDocumentSnapshot(doc))
          .toList();
    });
  }

  Future<List<Template>> getMoreMemeTemplatesByCategory(
      String categoryID, int limit) async {
    return _firestore
        .collection(FirestoreConstants.meme_templates)
        .where(FirestoreConstants.template_category_id, isEqualTo: categoryID)
        .where(FirestoreConstants.is_approved, isEqualTo: true)
        .startAfterDocument(_lastFetchedMemeTemplateDocument)
        .limit(limit)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedMemeTemplateDocument = querySnapshot.docs.last;

      return querySnapshot.docs
          .map((doc) => Template.fromDocumentSnapshot(doc))
          .toList();
    });
  }

  Future<List<Template>> getInitialSearchTemplates(String query) async {
    return _firestore
        .collection(FirestoreConstants.meme_templates)
        .where(FirestoreConstants.search_keywords,
            arrayContains: query.toLowerCase())
        .where(FirestoreConstants.is_approved, isEqualTo: true)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .limit(10)
        .get()
        .then((snapshot) {
      if (snapshot.docs.length > 0)
        _lastFetchedSearchedMemeTemplateDocument = snapshot.docs.last;
      List<DocumentSnapshot> docs = snapshot.docs;

      return docs.map((doc) {
        return Template.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<Template>> getMoreSearchTemplates(String query) async {
    return _firestore
        .collection(FirestoreConstants.meme_templates)
        .where(FirestoreConstants.search_keywords,
            arrayContains: query.toLowerCase())
        .where(FirestoreConstants.is_approved, isEqualTo: true)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .startAfterDocument(_lastFetchedSearchedMemeTemplateDocument)
        .limit(10)
        .get()
        .then((snapshot) {
      if (snapshot.docs.length > 0)
        _lastFetchedSearchedMemeTemplateDocument = snapshot.docs.last;
      List<DocumentSnapshot> docs = snapshot.docs;

      return docs.map((doc) {
        return Template.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<void> setInitialRunDone(String userID) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.private_data)
        .doc(FirestoreConstants.account_info)
        .update({FirestoreConstants.is_first_run_completed: true});
  }

  Future<Map> getAccountPrivateInfo(String userID) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.private_data)
        .doc(FirestoreConstants.account_info)
        .get()
        .then((value) {
      return {
        FirestoreConstants.is_first_run_completed:
            value.data()[FirestoreConstants.is_first_run_completed],
        FirestoreConstants.can_download_template_without_watermark: value
                    .data()[
                FirestoreConstants.can_download_template_without_watermark] ??
            false
      };
    });
  }

  Future<void> setDeviceToken(String userID, String token) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.device_tokens)
        .doc(token)
        .set({
      FirestoreConstants.token: token,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.platform: Platform.operatingSystem
    });
  }

  Future<List<Group>> getGroupsWithUserAsMember(String userID) async {
    List<Group> groups = [];
    List<Future> futures = [];

    await _firestore
        .collectionGroup(FirestoreConstants.group_members)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> members = querySnapshot.docs;

      List<String> groupIDs =
          members.map((member) => member.reference.parent.parent.id).toList();

      for (String groupID in groupIDs) {
        futures.add(_firestore
            .collection(FirestoreConstants.groups)
            .doc(groupID)
            .get()
            .then((value) {
          groups.add(Group.fromDocumentSnapshot(value));
        }));
      }
    });

    await Future.wait(futures);

    return groups;
  }

  Future<List<Group>> getGroupsWithUserAsAdmin(String userID) async {
    List<Future> futures = [];
    List<Group> groups = [];

    await _firestore
        .collectionGroup(FirestoreConstants.group_admins)
        .where(FirestoreConstants.user_id, isEqualTo: userID)
        .get()
        .then((querySnapshot) {
      List<DocumentSnapshot> members = querySnapshot.docs;

      List<String> groupIDs =
          members.map((member) => member.reference.parent.parent.id).toList();

      for (String groupID in groupIDs) {
        futures.add(_firestore
            .collection(FirestoreConstants.groups)
            .doc(groupID)
            .get()
            .then((value) {
          groups.add(Group.fromDocumentSnapshot(value));
        }));
      }
    });

    await Future.wait(futures);

    return groups;
  }

  Future<List<MemecistNotification>> getInitialNotifications(
      String userID, int limit) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.notifications)
        .limit(limit)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedNotificationDocument = querySnapshot.docs.last;

      return querySnapshot.docs
          .map((doc) => MemecistNotification.fromDocumentSnapshot(doc))
          .toList();
    });
  }

  Future<List<MemecistNotification>> getMoreNotifications(
      String userID, int limit) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.notifications)
        .startAfterDocument(_lastFetchedNotificationDocument)
        .limit(limit)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .get()
        .then((querySnapshot) {
      if (querySnapshot.docs.length > 0)
        _lastFetchedNotificationDocument = querySnapshot.docs.last;

      return querySnapshot.docs
          .map((doc) => MemecistNotification.fromDocumentSnapshot(doc))
          .toList();
    });
  }

  Stream<bool> getHasNewNotificationsStream(String userID) {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.private_data)
        .doc(FirestoreConstants.notification)
        .snapshots()
        .map((value) {
      if (value.data()[FirestoreConstants.last_opened_time] == null)
        return false;
      return DateTime.parse(value
              .data()[FirestoreConstants.last_opened_time]
              .toDate()
              .toString())
          .isBefore(DateTime.parse(value
              .data()[FirestoreConstants.last_notification_time]
              .toDate()
              .toString()));
    });
  }

  Future<void> updateLastOpenedNotificationTime(String userID) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.private_data)
        .doc(FirestoreConstants.notification)
        .set(
            {FirestoreConstants.last_opened_time: FieldValue.serverTimestamp()},
            SetOptions(merge: true));
  }

  Future<List<BadgeCategory>> getBadgesList() async {
    List<BadgeCategory> badgeCategories = [];
    return _firestore
        .collection(FirestoreConstants.mc_data)
        .doc(FirestoreConstants.badge_info)
        .get()
        .then((value) {
      List<Badge> memeBadgeList = [];
      List<dynamic> memeBadges = value.data()[FirestoreConstants.Meme];
      memeBadges.forEach((badge) {
        memeBadgeList.add(Badge.fromMap(badge));
      });
      badgeCategories
          .add(BadgeCategory(FirestoreConstants.Meme, memeBadgeList));

      List<Badge> competitionBadgeList = [];
      List<dynamic> competitionBadges =
          value.data()[FirestoreConstants.Competition];
      competitionBadges.forEach((badge) {
        competitionBadgeList.add(Badge.fromMap(badge));
      });
      badgeCategories.add(
          BadgeCategory(FirestoreConstants.Competition, competitionBadgeList));

      List<Badge> warBadgeList = [];
      List<dynamic> warBadges = value.data()[FirestoreConstants.War];
      warBadges.forEach((badge) {
        warBadgeList.add(Badge.fromMap(badge));
      });
      badgeCategories.add(BadgeCategory(FirestoreConstants.War, warBadgeList));

      List<Badge> warriorBadgeList = [];
      List<dynamic> warriorBadges = value.data()[FirestoreConstants.Warrior];
      warriorBadges.forEach((badge) {
        warriorBadgeList.add(Badge.fromMap(badge));
      });
      badgeCategories
          .add(BadgeCategory(FirestoreConstants.Warrior, warriorBadgeList));

      List<Badge> groupAdminBadgeList = [];
      List<dynamic> groupAdminBadges = value.data()[FirestoreConstants.Group];
      groupAdminBadges.forEach((badge) {
        groupAdminBadgeList.add(Badge.fromMap(badge));
      });
      badgeCategories
          .add(BadgeCategory(FirestoreConstants.Group, groupAdminBadgeList));

      List<Badge> shareBadgeList = [];
      List<dynamic> shareBadges = value.data()[FirestoreConstants.Share];
      shareBadges.forEach((badge) {
        shareBadgeList.add(Badge.fromMap(badge));
      });
      badgeCategories
          .add(BadgeCategory(FirestoreConstants.Share, shareBadgeList));

      List<Badge> commentBadgeList = [];
      List<dynamic> commentBadges = value.data()[FirestoreConstants.Comment];
      commentBadges.forEach((badge) {
        commentBadgeList.add(Badge.fromMap(badge));
      });
      badgeCategories
          .add(BadgeCategory(FirestoreConstants.Comment, commentBadgeList));

      List<Badge> reactBadgeList = [];
      List<dynamic> reactBadges = value.data()[FirestoreConstants.React];
      reactBadges.forEach((badge) {
        reactBadgeList.add(Badge.fromMap(badge));
      });
      badgeCategories
          .add(BadgeCategory(FirestoreConstants.React, reactBadgeList));

      List<Badge> otherBadgesList = [];
      List<dynamic> otherBadges = value.data()[FirestoreConstants.Other];
      otherBadges.forEach((badge) {
        otherBadgesList.add(Badge.fromMap(badge));
      });
      badgeCategories
          .add(BadgeCategory(FirestoreConstants.Other, otherBadgesList));

      return badgeCategories;
    });
  }

  Future<void> sendSuggestion(String userID, String suggestion) async {
    return _firestore.collection(FirestoreConstants.suggestions).doc().set({
      FirestoreConstants.user_id: userID,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.suggestion: suggestion
    });
  }

  Future<void> featureRequest(String userID, String feature) async {
    return _firestore
        .collection(FirestoreConstants.feature_requests)
        .doc()
        .set({
      FirestoreConstants.user_id: userID,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.feature_description: feature
    });
  }

  Future<void> reportMeme(String userID, String postID, String reason) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.reports)
        .doc()
        .set({
      FirestoreConstants.user_id: userID,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.reason: reason
    });
  }

  Future<List<MemePost>> getInitialMemePostsForSearch(
      int limit, String userID, String query) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .where(FirestoreConstants.search_keywords,
            arrayContains: query.toLowerCase())
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .limit(limit)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length > 0)
        _lastFetchedPostsForSearchDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: userID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<List<MemePost>> getMoreMemePostsForSearch(
      int limit, String userID, String query) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .where(FirestoreConstants.search_keywords,
            arrayContains: query.toLowerCase())
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .limit(limit)
        .startAfterDocument(_lastFetchedPostsForSearchDocument)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length > 0)
        _lastFetchedPostsForSearchDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: userID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<List<Group>> searchGroups(String query) async {
    return _firestore
        .collection(FirestoreConstants.groups)
        .where(FirestoreConstants.search_keywords,
            arrayContains: query.toLowerCase())
        .orderBy(FirestoreConstants.members_count, descending: true)
        .limit(10)
        .get()
        .then((snapshot) {
      List<DocumentSnapshot> docs = snapshot.docs;

      return docs.map((doc) {
        return Group.fromDocumentSnapshot(doc);
      }).toList();
    });
  }

  Future<List<MemePost>> getInitialMemePostsByHashtag(
      int limit, String hashtag, String currentUserID) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.hashtags, arrayContains: hashtag)
        .limit(limit)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPostsByHashtagDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: currentUserID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<List<MemePost>> getMoreMemePostsByHashtag(
      int limit, String hashtag, String currentUserID) async {
    List<Future> futures = [];

    return _firestore
        .collection(FirestoreConstants.posts)
        .orderBy(FirestoreConstants.create_date_time, descending: true)
        .where(FirestoreConstants.hashtags, arrayContains: hashtag)
        .limit(limit)
        .startAfterDocument(_lastFetchedPostsByHashtagDocument)
        .get()
        .then((value) {
      List<DocumentSnapshot> documentSnapshots = value.docs;

      if (documentSnapshots.length != 0)
        _lastFetchedPostsByHashtagDocument = documentSnapshots.last;

      return documentSnapshots.map((documentSnapshot) {
        return MemePost.fromDocumentSnapshot(documentSnapshot);
      }).toList();
    }).then((memePosts) async {
      for (MemePost memePost in memePosts) {
        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.reacts)
            .where(FirestoreConstants.user_id, isEqualTo: currentUserID)
            .get()
            .then((querySnapshot) {
          if (querySnapshot.docs.length != 0) memePost.isReacted = true;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.react_shards)
            .get()
            .then((querySnapshot) {
          int hahaCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            hahaCount += doc.data()[FirestoreConstants.count];
          }

          memePost.reactCount = hahaCount;
        }));

        futures.add(_firestore
            .collection(FirestoreConstants.posts)
            .doc(memePost.postID)
            .collection(FirestoreConstants.comment_shards)
            .get()
            .then((querySnapshot) {
          int commentsCount = 0;

          for (DocumentSnapshot doc in querySnapshot.docs) {
            commentsCount += doc.data()[FirestoreConstants.count];
          }

          memePost.commentsCount = commentsCount;
        }));
      }

      await Future.wait(futures);

      return memePosts;
    });
  }

  Future<void> deleteFirebaseMessagingTokenFromDb(
      String userID, String token) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.device_tokens)
        .doc(token)
        .delete();
  }

  Future<bool> isTeamAlreadyRenamed(String warID, String teamID) async {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.teams)
        .doc(teamID)
        .get()
        .then((value) {
      return value.data()[FirestoreConstants.is_team_renamed];
    });
  }

  Future<void> renameTeam(String warID, String teamID, String teamName) async {
    WriteBatch renameBatch = _firestore.batch();
    renameBatch.update(
        _firestore
            .collection(FirestoreConstants.wars)
            .doc(warID)
            .collection(FirestoreConstants.teams)
            .doc(teamID),
        {
          FirestoreConstants.team_name: teamName,
          FirestoreConstants.is_team_renamed: true
        });

    renameBatch.update(
        _firestore.collection(FirestoreConstants.war_rooms).doc(teamID),
        {FirestoreConstants.team_name: teamName});

    renameBatch.commit();
  }

  Future<void> changeTeamPic(String url, String teamID, String warID) async {
    WriteBatch changeTeamPicBatch = _firestore.batch();
    changeTeamPicBatch.update(
        _firestore
            .collection(FirestoreConstants.wars)
            .doc(warID)
            .collection(FirestoreConstants.teams)
            .doc(teamID),
        {FirestoreConstants.team_pic: url});

    changeTeamPicBatch.update(
        _firestore.collection(FirestoreConstants.war_rooms).doc(teamID),
        {FirestoreConstants.team_pic: url});

    changeTeamPicBatch.commit();
  }

  Stream<WarTeam> streamWarTeam(String memberID, String warID) {
    return _firestore
        .collection(FirestoreConstants.wars)
        .doc(warID)
        .collection(FirestoreConstants.teams)
        .where(FirestoreConstants.members, arrayContains: memberID)
        .snapshots()
        .map((querySnapshot) {
      List<DocumentSnapshot> docs = querySnapshot.docs;

      if (docs.length > 0) {
        return WarTeam.fromDocumentSnapshot(docs[0]);
      }

      return null;
    });
  }

  //TODO always maintain this state in db
  Future<Competition> getMemecistSocialCompetition(String country) {
    return _firestore
        .collection(FirestoreConstants.competitions)
        .where(FirestoreConstants.countries, arrayContains: country)
        .where(FirestoreConstants.is_memecist_social, isEqualTo: true)
        .get()
        .then((querySnapshot) {
      return Competition.fromDocumentSnapshot(querySnapshot.docs[0]);
    });
  }

  Future<void> submitLinksForProfileVerification(
      String link1, String link2, String method, String userID) async {
    return _firestore.collection(FirestoreConstants.verifications).doc().set({
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.user_id: userID,
      FirestoreConstants.method: method,
      FirestoreConstants.link1: link1,
      FirestoreConstants.link2: link2
    });
  }

  Future<void> deleteComment(String postID, String commentID) async {
    return _firestore
        .collection(FirestoreConstants.posts)
        .doc(postID)
        .collection(FirestoreConstants.comments)
        .doc(commentID)
        .delete();
  }

  Future<bool> getCanChangeGroupName(String groupID) async {
    return _firestore
        .collection(FirestoreConstants.groups)
        .doc(groupID)
        .collection(FirestoreConstants.group_data)
        .doc(FirestoreConstants.group_details)
        .get()
        .then((documentSnapshot) {
      return DateTime.now()
              .difference(DateTime.parse(documentSnapshot
                  .data()[FirestoreConstants.last_name_change_date_time]
                  .toDate()
                  .toString()))
              .inDays >
          28;
    });
  }

  Future<bool> getCanEditProfileInfo(String userID) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.private_data)
        .doc(FirestoreConstants.user_details)
        .get()
        .then((documentSnapshot) {
      return DateTime.now()
              .difference(DateTime.parse(documentSnapshot
                  .data()[FirestoreConstants.last_profile_edit_date_time]
                  .toDate()
                  .toString()))
              .inDays >
          28;
    });
  }

  Future<void> updateBio(String userID, String newBio, String oldBio) async {
    WriteBatch bioBatch = _firestore.batch();

    bioBatch.update(_firestore.collection(FirestoreConstants.users).doc(userID),
        {FirestoreConstants.bio: newBio});

    bioBatch.set(
        _firestore
            .collection(FirestoreConstants.users)
            .doc(userID)
            .collection(FirestoreConstants.private_data)
            .doc(FirestoreConstants.user_details),
        {
          FirestoreConstants.bio_updates: FieldValue.arrayUnion([
            {
              FirestoreConstants.bio: oldBio,
              FirestoreConstants.update_date_time: DateTime.now()
            }
          ])
        },
        SetOptions(merge: true));

    return bioBatch.commit();
  }

  Future<void> uploadMcStorySSDetails(String userID, String imageUrl) async {
    return _firestore.collection(FirestoreConstants.mc_story_ss).doc().set({
      FirestoreConstants.media_url: imageUrl,
      FirestoreConstants.user_id: userID,
      FirestoreConstants.is_verified: false,
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp()
    });
  }

  Future<void> setAndroidDeviceInfo(String userID,
      MemecistAndroidDeviceInfo memecistAndroidDeviceInfo) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.devices)
        .doc()
        .set({
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.platform: "Android",
      FirestoreConstants.type: memecistAndroidDeviceInfo.type,
      FirestoreConstants.id: memecistAndroidDeviceInfo.id,
      FirestoreConstants.versionSecurityPatch:
          memecistAndroidDeviceInfo.versionSecurityPatch,
      FirestoreConstants.versionRelease:
          memecistAndroidDeviceInfo.versionRelease,
      FirestoreConstants.versionIncremental:
          memecistAndroidDeviceInfo.versionIncremental,
      FirestoreConstants.versionCodename:
          memecistAndroidDeviceInfo.versionCodename,
      FirestoreConstants.versionBaseOS: memecistAndroidDeviceInfo.versionBaseOS,
      FirestoreConstants.board: memecistAndroidDeviceInfo.board,
      FirestoreConstants.bootloader: memecistAndroidDeviceInfo.bootloader,
      FirestoreConstants.brand: memecistAndroidDeviceInfo.brand,
      FirestoreConstants.device: memecistAndroidDeviceInfo.device,
      FirestoreConstants.display: memecistAndroidDeviceInfo.display,
      FirestoreConstants.fingerprint: memecistAndroidDeviceInfo.fingerprint,
      FirestoreConstants.hardware: memecistAndroidDeviceInfo.hardware,
      FirestoreConstants.host: memecistAndroidDeviceInfo.host,
      FirestoreConstants.manufacturer: memecistAndroidDeviceInfo.manufacturer,
      FirestoreConstants.model: memecistAndroidDeviceInfo.model,
      FirestoreConstants.product: memecistAndroidDeviceInfo.product,
      FirestoreConstants.tags: memecistAndroidDeviceInfo.tags,
      FirestoreConstants.androidId: memecistAndroidDeviceInfo.androidId,
      FirestoreConstants.versionSdkInt: memecistAndroidDeviceInfo.versionSdkInt,
      FirestoreConstants.versionPreviewSdkInt:
          memecistAndroidDeviceInfo.versionPreviewSdkInt,
      FirestoreConstants.supported32BitAbis:
          memecistAndroidDeviceInfo.supported32BitAbis,
      FirestoreConstants.supported64BitAbis:
          memecistAndroidDeviceInfo.supported64BitAbis,
      FirestoreConstants.supportedAbis: memecistAndroidDeviceInfo.supportedAbis,
      FirestoreConstants.systemFeatures:
          memecistAndroidDeviceInfo.systemFeatures,
      FirestoreConstants.isPhysicalDevice:
          memecistAndroidDeviceInfo.isPhysicalDevice
    });
  }

  Future<void> setIOSDeviceInfo(
      String userID, MemecistIOSDeviceInfo memecistIOSDeviceInfo) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.devices)
        .doc()
        .set({
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.platform: "Android",
      FirestoreConstants.name: memecistIOSDeviceInfo.name,
      FirestoreConstants.systemName: memecistIOSDeviceInfo.systemName,
      FirestoreConstants.systemVersion: memecistIOSDeviceInfo.systemVersion,
      FirestoreConstants.model: memecistIOSDeviceInfo.model,
      FirestoreConstants.localizedModel: memecistIOSDeviceInfo.localizedModel,
      FirestoreConstants.identifierForVendor:
          memecistIOSDeviceInfo.identifierForVendor,
      FirestoreConstants.utsnameSysname: memecistIOSDeviceInfo.utsnameSysname,
      FirestoreConstants.utsnameNodename: memecistIOSDeviceInfo.utsnameNodename,
      FirestoreConstants.utsnameRelease: memecistIOSDeviceInfo.utsnameRelease,
      FirestoreConstants.utsnameVersion: memecistIOSDeviceInfo.utsnameVersion,
      FirestoreConstants.utsnameMachine: memecistIOSDeviceInfo.utsnameMachine,
      FirestoreConstants.isPhysicalDevice:
          memecistIOSDeviceInfo.isPhysicalDevice
    });
  }

  Future<void> setAppsList(
      String userID, List<Map<String, String>> appsList) async {
    return _firestore
        .collection(FirestoreConstants.users)
        .doc(userID)
        .collection(FirestoreConstants.device_apps)
        .doc()
        .set({
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.device_apps: appsList
    });
  }

  Future<TroubleShootInfo> getTroubleShootInfo(String model) async {
    return _firestore
        .collection(FirestoreConstants.mc_data)
        .doc(FirestoreConstants.trouble_shoot_data)
        .collection(FirestoreConstants.brands)
        .doc(model)
        .get()
        .then((value) => TroubleShootInfo.fromDocumentSnapshot(value));
  }

  Future<void> uploadTemplate(Template template) async {
    return _firestore.collection(FirestoreConstants.meme_templates).doc().set({
      FirestoreConstants.create_date_time: FieldValue.serverTimestamp(),
      FirestoreConstants.template_category_name: template.categoryName,
      FirestoreConstants.template_pic: template.templatePic,
      FirestoreConstants.template_name: template.templateName,
      FirestoreConstants.user_id: template.createdBy.userID,
      FirestoreConstants.user_name: template.createdBy.userName,
      FirestoreConstants.is_approved: false
    });
  }
}
