import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseAuthProvider {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  GoogleSignIn googleSignIn = GoogleSignIn();

  Future<void> startPhoneAuth(String phoneNumber, verificationCompleted,
      verificationFailed, codeSent, codeAutoRetrievalTimeout) {
    assert(phoneNumber != null);

    return _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: Duration(seconds: 60),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<UserCredential> signInWithCredential(AuthCredential authCredential) async {
    return _firebaseAuth.signInWithCredential(authCredential);
  }

  Future<UserCredential> signInWithPhoneNumber(
      String actualCode, String smsCode) async {
    AuthCredential _authCredential = PhoneAuthProvider.credential(
        verificationId: actualCode, smsCode: smsCode);

    return _firebaseAuth.signInWithCredential(_authCredential);
  }

  Future<User> signIn() async {
    GoogleSignInAccount googleUser = await googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);

    return (await _firebaseAuth.signInWithCredential(credential)).user;
  }

  Stream<User> onLoggedInUserAuthChange() {
    return _firebaseAuth.onAuthStateChanged;
  }

  Future<User> getLoggedInUser() async {
    return _firebaseAuth.currentUser;
  }

  Future<bool> isLoggedInViaPhone() async {
    return _firebaseAuth.currentUser.phoneNumber != "";
  }

  Future<void> signOut() async {
    _firebaseAuth.signOut();
    googleSignIn.disconnect();
    googleSignIn.signOut();
  }
}
