import 'dart:io';
import 'dart:typed_data';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memecist/core/models/android_device_info_model.dart';
import 'package:memecist/core/models/badge_category.dart';
import 'package:memecist/core/models/comments_model.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/competition_winner_group.dart';
import 'package:memecist/core/models/competition_winner_model.dart';
import 'package:memecist/core/models/group_member_model.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/ios_device_info_model.dart';
import 'package:memecist/core/models/ip_model.dart';
import 'package:memecist/core/models/memecist_remote_config.dart';
import 'package:memecist/core/models/message_model.dart';
import 'package:memecist/core/models/notification_model.dart';
import 'package:memecist/core/models/promo_model.dart';
import 'package:memecist/core/models/template_category_model.dart';
import 'package:memecist/core/models/template_model.dart';
import 'package:memecist/core/models/trouble_shoot_info_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_application_model.dart';
import 'package:memecist/core/models/war_leader_application_model.dart';
import 'package:memecist/core/models/war_member_application_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/models/war_room_model.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/core/resources/cloud_functions_provider.dart';
import 'package:memecist/core/resources/device_info_provider.dart';
import 'package:memecist/core/resources/firebase_dynamic_links_provider.dart';
import 'package:memecist/core/resources/firebase_messaging_provider.dart';
import 'package:memecist/core/resources/firebase_remote_config_provider.dart';
import 'package:memecist/core/resources/firebase_storage_provider.dart';
import 'package:memecist/core/resources/ip_api_provider.dart';
import 'package:memecist/core/resources/shared_prefs_provider.dart';
import 'country_code_provider.dart';
import 'package:memecist/core/models/country_model.dart';
import 'package:flutter/material.dart';
import 'firebase_auth_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'firestore_provider.dart';
import 'package:memecist/core/models/meme_post_model.dart';

class Repository {
  final FirebaseAuthProvider _firebaseAuthProvider = FirebaseAuthProvider();
  final CountryCodeProvider _countryCodeProvider = CountryCodeProvider();
  final FirestoreProvider _firestoreProvider = FirestoreProvider();
  final FirebaseDynamicLinksProvider _firebaseDynamicLinksProvider =
      FirebaseDynamicLinksProvider();
  final FirebaseStorageProvider _firebaseStorageProvider =
      FirebaseStorageProvider();
  final IpApiProvider _ipApiProvider = IpApiProvider();
  final CloudFunctionsProvider _cloudFunctionsProvider =
      CloudFunctionsProvider();
  final FirebaseMessagingProvider _firebaseMessagingProvider =
      FirebaseMessagingProvider();
  final FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics();
  final SharedPrefsProvider _sharedPrefsProvider = SharedPrefsProvider();
  final FirebaseRemoteConfigProvider _firebaseRemoteConfigProvider =
      FirebaseRemoteConfigProvider();
  final DeviceInfoProvider _deviceInfoProvider = DeviceInfoProvider();

  //Country Code Provider functions

  Future<List<Country>> loadCountriesJson(BuildContext context) =>
      _countryCodeProvider.loadCountriesJson(context);

  //Firebase Auth functions

  Future<void> startPhoneAuth(String phoneNumber, verificationCompleted,
          verificationFailed, codeSent, codeAutoRetrievalTimeout) =>
      _firebaseAuthProvider.startPhoneAuth(phoneNumber, verificationCompleted,
          verificationFailed, codeSent, codeAutoRetrievalTimeout);

  Future<UserCredential> signInWithPhoneNumber(
          String actualCode, String smsCode) =>
      _firebaseAuthProvider.signInWithPhoneNumber(actualCode, smsCode);

  Future<UserCredential> signInWithCredential(AuthCredential authCredential) =>
      _firebaseAuthProvider.signInWithCredential(authCredential);

  Future<User> signIn() => _firebaseAuthProvider.signIn();

  Future<void> signOut() => _firebaseAuthProvider.signOut();

  Stream<User> onLoggedInUserAuthChange() =>
      _firebaseAuthProvider.onLoggedInUserAuthChange();

  Future<User> getLoggedInUser() => _firebaseAuthProvider.getLoggedInUser();

  Future<bool> isLoggedInViaPhone() =>
      _firebaseAuthProvider.isLoggedInViaPhone();

  //Firestore functions

  Future<void> initializeUser(User firebaseUser, IP ipDetails) =>
      _firestoreProvider.initializeUser(firebaseUser, ipDetails);

  Future<void> updateProfileInfo(
          String userID, MemecistUser user, List<String> keywords) =>
      _firestoreProvider.updateProfileInfo(userID, user, keywords);

  Future<bool> isUserNameTaken(String userName) =>
      _firestoreProvider.isUserNameTaken(userName);

  Future<MemecistUser> getMemecistUser(String userID) =>
      _firestoreProvider.getMemecistUser(userID);

  Stream<MemecistUser> getUserStream(String userID) =>
      _firestoreProvider.getUserStream(userID);

  Future<bool> isReacted(String userID, String postID) =>
      _firestoreProvider.isReacted(userID, postID);

  Future<void> toReact(MemecistUser currentUser, String postID) =>
      _firestoreProvider.toReact(currentUser, postID);

  Future<void> toUnReact(MemecistUser currentUser, String postID) =>
      _firestoreProvider.toUnReact(currentUser, postID);

  Future<List<Comment>> getInitialCommentsForPost(String postID, int limit) =>
      _firestoreProvider.getInitialCommentsForPost(postID, limit);

  Future<List<Comment>> getMoreCommentsForPost(String postID, int limit) =>
      _firestoreProvider.getMoreCommentsForPost(postID, limit);

  Future<String> comment(
          MemecistUser currentUser, String postID, String comment) =>
      _firestoreProvider.comment(currentUser, postID, comment);

  Future<List<MemecistUser>> getInitialReacts(String postID, int limit) =>
      _firestoreProvider.getInitialReacts(postID, limit);

  Future<List<MemecistUser>> getMoreReacts(String postID, int limit) =>
      _firestoreProvider.getMoreReacts(postID, limit);

  Future<List<MemePost>> getMoreMemePostsByUser(
          int limit, String userID, String currentUserID) =>
      _firestoreProvider.getMoreMemePostsByUser(limit, userID, currentUserID);

  Future<List<MemePost>> getInitialMemePostsByUser(
          int limit, String userID, String currentUserID) =>
      _firestoreProvider.getInitialMemePostsByUser(
          limit, userID, currentUserID);

  Future<void> followUser(String userID, MemecistUser currentUser) =>
      _firestoreProvider.followUser(userID, currentUser);

  Future<void> unFollowUser(String userID, String currentUser) =>
      _firestoreProvider.unFollowUser(userID, currentUser);

  Future<bool> isFollowingUser(String userID, String currentUserID) =>
      _firestoreProvider.isFollowingUser(userID, currentUserID);

  Future<List<MemecistUser>> getInitialFollowers(String userID, int limit) =>
      _firestoreProvider.getInitialFollowers(userID, limit);

  Future<List<MemecistUser>> getMoreFollowers(String userID, int limit) =>
      _firestoreProvider.getMoreFollowers(userID, limit);

  Future<String> uploadMemePost(MemePost memePost, {bool isGroupAdmin}) =>
      _firestoreProvider.uploadMemePost(memePost, isGroupAdmin: isGroupAdmin);

  Future<String> createGroup(MemecistUser currentUser, Group group) =>
      _firestoreProvider.createGroup(currentUser, group);

  Future<Group> getGroup(String groupID) =>
      _firestoreProvider.getGroup(groupID);

  Future<bool> isAdmin(String groupID, String userID) =>
      _firestoreProvider.isAdmin(groupID, userID);

  Future<bool> isMember(String groupID, String userID) =>
      _firestoreProvider.isMember(groupID, userID);

  Future<void> joinGroup(String groupID, MemecistUser user) =>
      _firestoreProvider.joinGroup(groupID, user);

  Future<List<MemecistUser>> getAdmins(String groupID) =>
      _firestoreProvider.getAdmins(groupID);

  Future<List<MemecistUser>> getInitialMembers(String groupID, int limit) =>
      _firestoreProvider.getInitialMembers(groupID, limit);

  Future<List<MemecistUser>> getMoreMembers(String groupID, int limit) =>
      _firestoreProvider.getMoreMembers(groupID, limit);

  Future<List<MemePost>> getInitialMemePostsByGroup(
          int limit, String groupID, String userID) =>
      _firestoreProvider.getInitialMemePostsByGroup(limit, groupID, userID);

  Future<List<MemePost>> getMoreMemePostsByGroup(
          int limit, String groupID, String userID) =>
      _firestoreProvider.getMoreMemePostsByGroup(limit, groupID, userID);

  Future<List<Group>> getGroupsForUser(String userID) =>
      _firestoreProvider.getGroupsForUser(userID);

  Future<String> startWar(War war) => _firestoreProvider.startWar(war);

  Future<List<Group>> getGroupsWithActiveWars(String userID) =>
      _firestoreProvider.getGroupsWithActiveWars(userID);

  Future<MemePost> getMemePost(String postID, String userID) =>
      _firestoreProvider.getMemePost(postID, userID);

  Future<void> removeMember(
          String userID, GroupMember member, String groupID) =>
      _firestoreProvider.removeMember(userID, member, groupID);

  Future<List<MemePost>> getInitialUnapprovedMemePostsByGroup(
          int limit, String groupID) =>
      _firestoreProvider.getInitialUnapprovedMemePostsByGroup(limit, groupID);

  Future<List<MemePost>> getMoreUnapprovedMemePostsByGroup(
          int limit, String groupID) =>
      _firestoreProvider.getMoreUnapprovedMemePostsByGroup(limit, groupID);

  Stream<bool> getHasNewNotificationsStream(String userID) =>
      _firestoreProvider.getHasNewNotificationsStream(userID);

  Future<void> updateLastOpenedNotificationTime(String userID) =>
      _firestoreProvider.updateLastOpenedNotificationTime(userID);

//  Future<void> approveMeme(String postID, String userID, String groupID) =>
//      _firestoreProvider.approveMeme(postID, userID, groupID);

  Future<void> rejectMeme(String postID, String userID, String groupID) =>
      _firestoreProvider.rejectMeme(postID, userID, groupID);

  Future<void> leaveGroup(String userID, String groupID, bool isAdmin) =>
      _firestoreProvider.leaveGroup(userID, groupID, isAdmin);

  Future<List<MemePost>> getInitialPendingApprovalMemes(
          int limit, String groupID, String userID) =>
      _firestoreProvider.getInitialPendingApprovalMemes(limit, groupID, userID);

  Future<List<MemePost>> getMorePendingApprovalMemes(
          int limit, String groupID, String userID) =>
      _firestoreProvider.getMorePendingApprovalMemes(limit, groupID, userID);

  Future<List<MemecistUser>> searchUsers(String query) =>
      _firestoreProvider.searchUsers(query);

  Future<void> makeAdmin(String groupID, MemecistUser user) =>
      _firestoreProvider.makeAdmin(groupID, user);

  Future<List<MemecistUser>> searchMembers(String groupID, String query) =>
      _firestoreProvider.searchMembers(groupID, query);

  Future<void> addSessionDetails(String userID, IP ip) =>
      _firestoreProvider.addSessionDetails(userID, ip);

  Future<War> getWar(String warID) => _firestoreProvider.getWar(warID);

  Future<WarLeaderApplication> getTeamLeaderApplication(
          String warID, String userID) =>
      _firestoreProvider.getTeamLeaderApplication(warID, userID);

  Future<List<WarLeaderApplication>> getInitialTeamLeaderApplications(
          String warID) =>
      _firestoreProvider.getInitialTeamLeaderApplications(warID);

  Future<List<WarLeaderApplication>> getMoreTeamLeaderApplications(
          String warID) =>
      _firestoreProvider.getMoreTeamLeaderApplications(warID);

  Future<void> applyForTeamLeader(MemecistUser user, String warID) =>
      _firestoreProvider.applyForTeamLeader(user, warID);

  Future<void> approveTeamLeaders(List<WarLeaderApplication> warApplications,
          String warID, String warName) =>
      _firestoreProvider.approveTeamLeaders(warApplications, warID, warName);

  Future<List<WarLeaderApplication>> getSelectedTeamLeaders(String warID) =>
      _firestoreProvider.getSelectedTeamLeaders(warID);

  Stream<List<WarRoom>> getWarRoomsStream(String userID) =>
      _firestoreProvider.getWarRoomsStream(userID);

  Future<List<WarRoom>> getAllWarRooms(String userID) =>
      _firestoreProvider.getAllWarRooms(userID);

  Future<WarRoom> getWarRoom(String warRoomID) =>
      _firestoreProvider.getWarRoom(warRoomID);

  Stream<List<Message>> getRecentMessages(String warRoomID, bool isNewRun) =>
      _firestoreProvider.getRecentMessages(warRoomID, isNewRun);

  Future<void> sendMessage(Message message, String warRoomID) =>
      _firestoreProvider.sendMessage(message, warRoomID);

  Future<List<Message>> getMoreMessages(String warRoomID, int docLimit) =>
      _firestoreProvider.getMoreMessages(warRoomID, docLimit);

  Future<WarMemberApplication> getTeamMemberApplication(
          String warID, String userID) =>
      _firestoreProvider.getTeamMemberApplication(warID, userID);

  Future<void> applyForTeamMember(MemecistUser user, String warID) =>
      _firestoreProvider.applyForTeamMember(user, warID);

  Future<List<WarMemberApplication>> getInitialTeamMemberApplications(
          String warID) =>
      _firestoreProvider.getInitialTeamMemberApplications(warID);

  Future<List<WarMemberApplication>> getMoreTeamMemberApplications(
          String warID) =>
      _firestoreProvider.getMoreTeamMemberApplications(warID);

  Future<WarTeam> getWarTeam(String warID, String memberID) =>
      _firestoreProvider.getWarTeam(warID, memberID);

  Future<void> approveTeamMembers(
          List<WarApplication> warApplications, String warID, String teamID) =>
      _firestoreProvider.approveTeamMembers(warApplications, warID, teamID);

  Future<List<MemePost>> getInitialMemePostsByWar(
          int limit, String warID, String userID) =>
      _firestoreProvider.getInitialMemePostsByWar(limit, warID, userID);

  Future<List<MemePost>> getMoreMemePostsByWar(
          int limit, String warID, String userID) =>
      _firestoreProvider.getMoreMemePostsByWar(limit, warID, userID);

  Future<List<WarTeam>> getAllWarTeams(String warID) =>
      _firestoreProvider.getAllWarTeams(warID);

  Future<List<Competition>> getActiveCompetitions(String country) =>
      _firestoreProvider.getActiveCompetitions(country);

  Future<Competition> getCompetition(String competitionID) =>
      _firestoreProvider.getCompetition(competitionID);

  Future<List<CompetitionWinnerGroup>> getCompetitionWinners(
          String competitionID) =>
      _firestoreProvider.getCompetitionWinners(competitionID);

  Future<void> updateLinksForCompetitionMemePost(
          String postID, link1, link2, link3) =>
      _firestoreProvider.updateLinksForCompetitionMemePost(
          postID, link1, link2, link3);

  Future<void> updateLinksForMemecistSocial(String postID, link1, link2) =>
      _firestoreProvider.updateLinksForMemecistSocial(postID, link1, link2);

  Future<void> savePromos(String userID, Promo promo) =>
      _firestoreProvider.savePromos(userID, promo);

  Future<Promo> getPromos(String userID) =>
      _firestoreProvider.getPromos(userID);

  Future<List<TemplateCategory>> getTemplateCategories(String country) =>
      _firestoreProvider.getTemplateCategories(country);

  Future<List<TemplateCategory>> getAllTemplateCategories() =>
      _firestoreProvider.getAllTemplateCategories();

  Future<List<Template>> getInitialMemeTemplatesByCategory(
          String categoryID, int limit) =>
      _firestoreProvider.getInitialMemeTemplatesByCategory(categoryID, limit);

  Future<List<Template>> getMoreMemeTemplatesByCategory(
          String categoryID, int limit) =>
      _firestoreProvider.getMoreMemeTemplatesByCategory(categoryID, limit);

  Future<List<Template>> getInitialSearchTemplates(String query) =>
      _firestoreProvider.getInitialSearchTemplates(query);

  Future<List<Template>> getMoreSearchTemplates(String query) =>
      _firestoreProvider.getMoreSearchTemplates(query);

  Future<void> setInitialRunDone(String userID) =>
      _firestoreProvider.setInitialRunDone(userID);

  Future<Map> getAccountPrivateInfo(String userID) =>
      _firestoreProvider.getAccountPrivateInfo(userID);

  Future<void> setDeviceToken(String userID, String token) =>
      _firestoreProvider.setDeviceToken(userID, token);

  Future<List<Group>> getGroupsWithUserAsMember(String userID) =>
      _firestoreProvider.getGroupsWithUserAsMember(userID);

  Future<List<Group>> getGroupsWithUserAsAdmin(String userID) =>
      _firestoreProvider.getGroupsWithUserAsAdmin(userID);

  Future<List<MemecistNotification>> getInitialNotifications(
          String userID, int limit) =>
      _firestoreProvider.getInitialNotifications(userID, limit);

  Future<List<MemecistNotification>> getMoreNotifications(
          String userID, int limit) =>
      _firestoreProvider.getMoreNotifications(userID, limit);

  Future<List<BadgeCategory>> getBadgesList() =>
      _firestoreProvider.getBadgesList();

  Future<void> sendSuggestion(String userID, String suggestion) =>
      _firestoreProvider.sendSuggestion(userID, suggestion);

  Future<void> featureRequest(String userID, String feature) =>
      _firestoreProvider.featureRequest(userID, feature);

  Future<void> reportMeme(String userID, String postID, String reason) =>
      _firestoreProvider.reportMeme(userID, postID, reason);

  Future<List<MemePost>> getInitialMemePostsForSearch(
          int limit, String userID, String query) =>
      _firestoreProvider.getInitialMemePostsForSearch(limit, userID, query);

  Future<List<MemePost>> getMoreMemePostsForSearch(
          int limit, String userID, String query) =>
      _firestoreProvider.getMoreMemePostsForSearch(limit, userID, query);

  Future<List<Group>> searchGroups(String query) =>
      _firestoreProvider.searchGroups(query);

  Future<List<MemePost>> getInitialMemePostsByHashtag(
          int limit, String hashtag, String currentUserID) =>
      _firestoreProvider.getInitialMemePostsByHashtag(
          limit, hashtag, currentUserID);

  Future<List<MemePost>> getMoreMemePostsByHashtag(
          int limit, String hashtag, String currentUserID) =>
      _firestoreProvider.getMoreMemePostsByHashtag(
          limit, hashtag, currentUserID);

  Future<void> deleteFirebaseMessagingTokenFromDb(
          String userID, String token) =>
      _firestoreProvider.deleteFirebaseMessagingTokenFromDb(userID, token);

  Future<bool> isTeamAlreadyRenamed(String warID, String teamID) =>
      _firestoreProvider.isTeamAlreadyRenamed(warID, teamID);

  Future<void> renameTeam(String warID, String teamID, String teamName) =>
      _firestoreProvider.renameTeam(warID, teamID, teamName);

  Future<void> changeTeamPic(String url, String teamID, String warID) =>
      _firestoreProvider.changeTeamPic(url, teamID, warID);

  Stream<WarTeam> streamWarTeam(String memberID, String warID) =>
      _firestoreProvider.streamWarTeam(memberID, warID);

  Future<Competition> getMemecistSocialCompetition(String country) =>
      _firestoreProvider.getMemecistSocialCompetition(country);

  Future<void> submitLinksForProfileVerification(
          String link1, String link2, String method, String userID) =>
      _firestoreProvider.submitLinksForProfileVerification(
          link1, link2, method, userID);

  Future<void> deleteComment(String postID, String commentID) =>
      _firestoreProvider.deleteComment(postID, commentID);

  Future<bool> getCanChangeGroupName(String groupID) =>
      _firestoreProvider.getCanChangeGroupName(groupID);

  Future<bool> getCanEditProfileInfo(String userID) =>
      _firestoreProvider.getCanEditProfileInfo(userID);

  Future<void> updateBio(String userID, String newBio, String oldBio) =>
      _firestoreProvider.updateBio(userID, newBio, oldBio);

  Future<void> uploadMcStorySSDetails(String userID, String imageUrl) =>
      _firestoreProvider.uploadMcStorySSDetails(userID, imageUrl);

  Future<void> setAndroidDeviceInfo(
          String userID, MemecistAndroidDeviceInfo memecistAndroidDeviceInfo) =>
      _firestoreProvider.setAndroidDeviceInfo(
          userID, memecistAndroidDeviceInfo);

  Future<void> setIOSDeviceInfo(
          String userID, MemecistIOSDeviceInfo memecistIOSDeviceInfo) =>
      _firestoreProvider.setIOSDeviceInfo(userID, memecistIOSDeviceInfo);

  Future<void> setAppsList(String userID, List<Map<String, String>> appsList) =>
      _firestoreProvider.setAppsList(userID, appsList);

  Future<TroubleShootInfo> getTroubleShootInfo(String model) =>
      _firestoreProvider.getTroubleShootInfo(model);

  Future<void> uploadTemplate(Template template) =>
      _firestoreProvider.uploadTemplate(template);

  //Firebase Storage functions

  StorageUploadTask uploadMemeImage(Uint8List data, String userID) =>
      _firebaseStorageProvider.uploadMemeImage(data, userID);

  StorageUploadTask uploadWarImage(Uint8List data, String warID) =>
      _firebaseStorageProvider.uploadWarImage(data, warID);

  StorageUploadTask uploadGroupImage(Uint8List data, String groupID) =>
      _firebaseStorageProvider.uploadGroupImage(data, groupID);

  StorageUploadTask uploadMessageImage(Uint8List data, String groupID) =>
      _firebaseStorageProvider.uploadMessageImage(data, groupID);

  StorageUploadTask uploadTeamImage(Uint8List data, String teamID) =>
      _firebaseStorageProvider.uploadTeamImage(data, teamID);

  StorageUploadTask uploadProfileImage(Uint8List data, String groupID) =>
      _firebaseStorageProvider.uploadProfileImage(data, groupID);

  StorageUploadTask uploadMemecistStoryScreenshot(
          Uint8List data, String userID) =>
      _firebaseStorageProvider.uploadMemecistStoryScreenshot(data, userID);

  StorageUploadTask uploadTemplateImage(File data, String userID) =>
      _firebaseStorageProvider.uploadTemplateImage(data, userID);

  //Firebase Dynamic Links functions

  Future<Uri> createGroupDynamicLink(String groupID) =>
      _firebaseDynamicLinksProvider.createGroupDynamicLink(groupID);

  Future<Uri> createPostDynamicLink(String postID) =>
      _firebaseDynamicLinksProvider.createPostDynamicLink(postID);

  Future<Uri> createUserDynamicLink(String userID) =>
      _firebaseDynamicLinksProvider.createUserDynamicLink(userID);

  //IpApi functions
  Future<IP> fetchIPDetails() => _ipApiProvider.fetchIPDetails();

  //Cloud functions

  Future<List<MemecistUser>> getFollowSuggestions(
          int runCount, String country) =>
      _cloudFunctionsProvider.getFollowSuggestions(runCount, country);

  Future<List<Group>> getGroupSuggestions(int runCount, String country) =>
      _cloudFunctionsProvider.getGroupSuggestions(runCount, country);

  Future<void> approveMeme(String postID, String groupID) =>
      _cloudFunctionsProvider.approveMeme(postID, groupID);

  Future<List<MemePost>> getFeed(int runCount, String userID) =>
      _cloudFunctionsProvider.getFeed(runCount, userID);

  Future<List<MemePost>> getExploreFeed(
          int runCount, String userID, String category, String country) =>
      _cloudFunctionsProvider.getExploreFeed(
          runCount, userID, category, country);

  Future<void> sharePost(String postID, String link) =>
      _cloudFunctionsProvider.sharePost(postID, link);

  Future<void> updateFollowersCount(String userID) =>
      _cloudFunctionsProvider.updateFollowersCount(userID);

  Future<void> updateGroupMembersCount(String groupID) =>
      _cloudFunctionsProvider.updateGroupMembersCount(groupID);

  Future<void> deletePost(String postID) =>
      _cloudFunctionsProvider.deletePost(postID);

  Future<bool> editGroupDetails(
          String groupID,
          String groupName,
          String groupDescription,
          String imageUrl,
          List<String> groupSearchKeywords) =>
      _cloudFunctionsProvider.editGroupDetails(
          groupID, groupName, groupDescription, imageUrl, groupSearchKeywords);

  Future<bool> editProfileInfo(String userID, String firstName, String lastName,
          String userName, String imageUrl, List<String> searchKeywords) =>
      _cloudFunctionsProvider.editProfileInfo(
          userID, firstName, lastName, userName, imageUrl, searchKeywords);

  //Firebase messaging functions
  Future<void> initializeFirebaseMessaging(BuildContext context) =>
      _firebaseMessagingProvider.initialise(context);

  Future<void> subscribeToAllGroups(User user) =>
      _firebaseMessagingProvider.subscribeToAllGroups(user);

  Future<void> subscribeToAllWarRooms(User user) =>
      _firebaseMessagingProvider.subscribeToAllWarRooms(user);

  Future<void> subscribeTopic(String topic) =>
      _firebaseMessagingProvider.subscribeTopic(topic);

  Future<void> unSubscribeTopic(String topic) =>
      _firebaseMessagingProvider.unSubscribeTopic(topic);

  Future<void> cancelIOSSubscription() =>
      _firebaseMessagingProvider.cancelIOSSubscription();

  Future<void> deleteFirebaseMessagingToken() =>
      _firebaseMessagingProvider.deleteFirebaseMessagingToken();

  //Shared Prefs functions
  Future<String> getUserCountry() => _sharedPrefsProvider.getUserCountry();

  Future<void> setUserCountry(String country) =>
      _sharedPrefsProvider.setUserCountry(country);

  Future<int> getApplicationRunCount() =>
      _sharedPrefsProvider.getApplicationRunCount();

  Future<void> setApplicationRunCount(int count) =>
      _sharedPrefsProvider.setApplicationRunCount(count);

  Future<int> getGroupOpenCount(String groupID) =>
      _sharedPrefsProvider.getGroupOpenCount(groupID);

  Future<void> setGroupOpenCount(String groupID, int count) =>
      _sharedPrefsProvider.setGroupOpenCount(groupID, count);

  Future<int> getToUpdateGroupMembersCountAtRun() =>
      _sharedPrefsProvider.getToUpdateGroupMembersCountAtRun();

  Future<void> setToUpdateGroupMembersCountAtRun(int count) =>
      _sharedPrefsProvider.setToUpdateGroupMembersCountAtRun(count);

  Future<bool> getIsAppRateDialogShown() =>
      _sharedPrefsProvider.getIsAppRateDialogShown();

  Future<void> setIsAppRateDialogShown(bool isShown) =>
      _sharedPrefsProvider.setIsAppRateDialogShown(isShown);

  Future<bool> getIsSuggestionsDialogShown() =>
      _sharedPrefsProvider.getIsSuggestionsDialogShown();

  Future<void> setIsSuggestionsDialogShown(bool isShown) =>
      _sharedPrefsProvider.setIsSuggestionsDialogShown(isShown);

  Future<bool> getCanDownloadTemplateWithoutWatermark() =>
      _sharedPrefsProvider.getCanDownloadTemplateWithoutWatermark();

  Future<void> setCanDownloadTemplateWithoutWatermark(bool canDownload) =>
      _sharedPrefsProvider.setCanDownloadTemplateWithoutWatermark(canDownload);

  Future<int> getLastOpenedWarRoomTime(String warRoomID) =>
      _sharedPrefsProvider.getLastOpenedWarRoomTime(warRoomID);

  Future<void> setLastOpenedWarRoomTime(String warRoomID, int time) =>
      _sharedPrefsProvider.setLastOpenedWarRoomTime(warRoomID, time);

  Future<bool> getIsInitialRunDone() =>
      _sharedPrefsProvider.getIsInitialRunDone();

  Future<void> setIsInitialRunDone(bool isInitialRunDone) =>
      _sharedPrefsProvider.setIsInitialRunDone(isInitialRunDone);

  //Remote config functions
  Future<MemecistRemoteConfig> fetchRemoteConfig() =>
      _firebaseRemoteConfigProvider.fetchRemoteConfig();

  //Device info functions
  Future<MemecistAndroidDeviceInfo> getAndroidDeviceInfo() =>
      _deviceInfoProvider.getAndroidDeviceInfo();

  Future<MemecistIOSDeviceInfo> getIOSDeviceInfo() =>
      _deviceInfoProvider.getIOSDeviceInfo();
}

final repository = Repository();
