import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/constants/firebase_remote_config_constants.dart';
import 'package:memecist/core/models/memecist_remote_config.dart';

class FirebaseRemoteConfigProvider {
  RemoteConfig _remoteConfig;

  Future<MemecistRemoteConfig> fetchRemoteConfig() async {
    _remoteConfig = await RemoteConfig.instance;

    //TODO change this to default in production
//    await _remoteConfig
//        .setConfigSettings(RemoteConfigSettings(debugMode: true));
    await _remoteConfig.fetch(expiration: const Duration(seconds: 0));
    await _remoteConfig.activateFetched();

    String forceUpdateVersionForAndroidString = _remoteConfig
        .getString(FirebaseRemoteConfigConstants.force_update_android_version);
    String forceUpdateVersionForIosString = _remoteConfig
        .getString(FirebaseRemoteConfigConstants.force_update_ios_version);

    bool shouldShowRatingDialog = _remoteConfig
            .getBool(FirebaseRemoteConfigConstants.should_show_rating_dialog) ??
        false;
    bool shouldShowSuggestionsDialog = _remoteConfig.getBool(
            FirebaseRemoteConfigConstants.should_show_suggestions_dialog) ??
        false;

    int groupMembersCountUpdateAtRun = _remoteConfig.getInt(
            FirebaseRemoteConfigConstants.group_members_count_update_at_run) ??
        1;
    int userFollowersCountUpdateAtRun = _remoteConfig.getInt(
            FirebaseRemoteConfigConstants.user_followers_count_update_at_run) ??
        1;

    int appAvailabilityUpdateAtRun = _remoteConfig.getInt(
            FirebaseRemoteConfigConstants.app_availability_update_at_run) ??
        1;

    return MemecistRemoteConfig(
        forceUpdateVersionForIosString,
        forceUpdateVersionForAndroidString,
        shouldShowRatingDialog,
        shouldShowSuggestionsDialog,
        groupMembersCountUpdateAtRun,
        userFollowersCountUpdateAtRun,
        appAvailabilityUpdateAtRun);
  }
}
