import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:memecist/core/constants/firebase_dynamic_links_constants.dart';

class FirebaseDynamicLinksProvider {

  Future<Uri> createGroupDynamicLink(String groupID) async {
    DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: FirebaseDynamicLinksConstants.uriPrefix,
      link: Uri.parse(
          FirebaseDynamicLinksConstants.groupLink + "?groupID=$groupID"),
      androidParameters: AndroidParameters(
          packageName: FirebaseDynamicLinksConstants.androidPackageName,
          minimumVersion: FirebaseDynamicLinksConstants.minimumAndroidVersion),
    );

    final link = await parameters.buildUrl();
    final ShortDynamicLink shortDynamicLink = await DynamicLinkParameters.shortenUrl(
      link,
      DynamicLinkParametersOptions(shortDynamicLinkPathLength: ShortDynamicLinkPathLength.unguessable)
    );

    return shortDynamicLink.shortUrl;
  }

  Future<Uri> createPostDynamicLink(String postID) async {
    DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: FirebaseDynamicLinksConstants.uriPrefix,
      link: Uri.parse(
          FirebaseDynamicLinksConstants.postLink + "?postID=$postID"),
      androidParameters: AndroidParameters(
          packageName: FirebaseDynamicLinksConstants.androidPackageName,
          minimumVersion: FirebaseDynamicLinksConstants.minimumAndroidVersion),
    );

    final link = await parameters.buildUrl();
    final ShortDynamicLink shortDynamicLink = await DynamicLinkParameters.shortenUrl(
        link,
        DynamicLinkParametersOptions(shortDynamicLinkPathLength: ShortDynamicLinkPathLength.unguessable)
    );

    return shortDynamicLink.shortUrl;
  }

  Future<Uri> createUserDynamicLink(String userID) async {
    DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: FirebaseDynamicLinksConstants.uriPrefix,
      link: Uri.parse(
          FirebaseDynamicLinksConstants.userLink + "?userID=$userID"),
      androidParameters: AndroidParameters(
          packageName: FirebaseDynamicLinksConstants.androidPackageName,
          minimumVersion: FirebaseDynamicLinksConstants.minimumAndroidVersion),
    );

    final link = await parameters.buildUrl();
    final ShortDynamicLink shortDynamicLink = await DynamicLinkParameters.shortenUrl(
        link,
        DynamicLinkParametersOptions(shortDynamicLinkPathLength: ShortDynamicLinkPathLength.unguessable)
    );

    return shortDynamicLink.shortUrl;
  }
}
