import 'package:memecist/core/models/country_model.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class CountryCodeProvider{
  List<Country> countries = [];

  Future<List<Country>> loadCountriesJson(BuildContext context) async {
    countries.clear();

    var value = await DefaultAssetBundle.of(context)
        .loadString("data/country_phone_codes.json");
    var countriesJson = json.decode(value);
    for (var country in countriesJson) {
      countries.add(Country.fromJson(country));
    }

    return countries;
  }
}