import 'dart:async';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_detail_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_unapproved_memes_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/war_room/war_room_chat_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_messaging_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/war_room_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_detail_screen.dart';
import 'package:memecist/ui/screens/meme_world/competitions/competition_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/view_unapproved_memes_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/war_room/war_room_chat_screen.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FirebaseMessagingProvider {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  StreamSubscription iosSubscription;

  Future<void> initialise(BuildContext context) async {
    User user = await repository.getLoggedInUser();

    if (Platform.isIOS) {
      iosSubscription =
          _firebaseMessaging.onIosSettingsRegistered.listen((data) async {
        print(data);
        String token = await _firebaseMessaging.getToken();
        if (token != null) repository.setDeviceToken(user.uid, token);
      });

      _firebaseMessaging
          .requestNotificationPermissions(IosNotificationSettings());
    } else {
      String token = await _firebaseMessaging.getToken();
      if (token != null) repository.setDeviceToken(user.uid, token);
    }

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        String chatScreenID =
            ObserverProvider.chatScreenTrackerOf(context).chatScreenID ?? "";
        if (Platform.isAndroid) {
          if (message[FirebaseMessagingConstants.data] == null ||
              message[FirebaseMessagingConstants.data]
                      [FirebaseMessagingConstants.type] !=
                  FirebaseMessagingConstants.war_room ||
              (message[FirebaseMessagingConstants.data]
                          [FirebaseMessagingConstants.type] ==
                      FirebaseMessagingConstants.war_room &&
                  message[FirebaseMessagingConstants.data]
                          [FirebaseMessagingConstants.id] !=
                      chatScreenID)) showFlushBar(context, message);
        } else if (Platform.isIOS) {
          if (message[FirebaseMessagingConstants.type] == null ||
              message[FirebaseMessagingConstants.type] !=
                  FirebaseMessagingConstants.war_room ||
              (message[FirebaseMessagingConstants.type] ==
                      FirebaseMessagingConstants.war_room &&
                  message[FirebaseMessagingConstants.id] != chatScreenID))
            showFlushBar(context, message);
        }
      },
      onLaunch: (Map<String, dynamic> message) async {
        routeNotification(message, context);
      },
      onResume: (Map<String, dynamic> message) async {
        routeNotification(message, context);
      },
    );

    _firebaseMessaging.onTokenRefresh.listen((event) async {
      String token = await _firebaseMessaging.getToken();
      if (token != null) {
        User user = await repository.getLoggedInUser();
        if (user != null) repository.setDeviceToken(user.uid, token);
      }
      subscribeToAllGroups(user);
    });
  }

  subscribeToAllGroups(User user) async {
    try {
      List<Group> groupsWithUserAsMember =
          await repository.getGroupsWithUserAsMember(user.uid);
      groupsWithUserAsMember.forEach((group) {
        _firebaseMessaging.subscribeToTopic(
            FirebaseMessagingConstants.group + "-" + group.groupID);
      });

      List<Group> groupsWithUserAsAdmin =
          await repository.getGroupsWithUserAsAdmin(user.uid);
      groupsWithUserAsAdmin.forEach((group) {
        _firebaseMessaging.subscribeToTopic(
            FirebaseMessagingConstants.group + "-" + group.groupID);
        _firebaseMessaging.subscribeToTopic(FirebaseMessagingConstants.group +
            "-" +
            group.groupID +
            "-" +
            FirebaseMessagingConstants.admin);
      });
    } catch (e) {
      print(e);
    }
  }

  subscribeToAllWarRooms(User user) async {
    try {
      List<WarRoom> warRooms = await repository.getAllWarRooms(user.uid);

      warRooms.forEach((warRoom) {
        _firebaseMessaging.subscribeToTopic("war_room-" + warRoom.teamID);
      });
    } catch (e) {
      print(e);
    }
  }

  showFlushBar(BuildContext context, Map<String, dynamic> message) {
    Flushbar(
        titleText: Text(
          message[FirebaseMessagingConstants.notification]
                  [FirebaseMessagingConstants.title]
              .toString(),
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 16.0.sp),
        ),
        messageText: Text(
          message[FirebaseMessagingConstants.notification]
                  [FirebaseMessagingConstants.body]
              .toString(),
          style: TextStyle(color: Colors.white, fontSize: 14.0.sp),
        ),
        flushbarPosition: FlushbarPosition.TOP,
        flushbarStyle: FlushbarStyle.FLOATING,
        dismissDirection: FlushbarDismissDirection.HORIZONTAL,
        reverseAnimationCurve: Curves.easeOut,
        forwardAnimationCurve: Curves.easeIn,
        isDismissible: true,
        duration: Duration(seconds: 3),
        mainButton: FlatButton(
          onPressed: () => routeNotification(message, context),
          child: Text(
            "VIEW",
            style: TextStyle(
                color: MemecistColors.primaryColor, fontSize: 16.0.sp),
          ),
        )).show(context);
  }

  routeNotification(Map<String, dynamic> message, BuildContext context) async {
    String id = Platform.isAndroid
        ? message[FirebaseMessagingConstants.data]
            [FirebaseMessagingConstants.id]
        : message[FirebaseMessagingConstants.id];
    String type = Platform.isAndroid
        ? message[FirebaseMessagingConstants.data]
            [FirebaseMessagingConstants.type]
        : message[FirebaseMessagingConstants.type];

    switch (type) {
      case FirebaseMessagingConstants.group_approval_post:
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) {
              return ViewUnapprovedMemesBlocProvider(
                child: ViewUnapprovedMemesScreen(id),
              );
            },
            settings: RouteSettings(
                name:
                    FirebaseAnalyticsConstants.view_unapproved_memes_screen)));
        break;
      case FirebaseMessagingConstants.post:
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) {
              return MemePostDetailBlocProvider(
                  child: MemePostDetailScreen(
                true,
                postID: id,
              ));
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.meme_post_detail_screen)));
        break;
      case FirebaseMessagingConstants.user:
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) {
              return ProfileBlocProvider(
                child: ProfileScreen(id),
              );
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.profile_screen)));
        break;
      case FirebaseMessagingConstants.group:
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) {
              return GroupDetailsBlocProvider(
                child: GroupDetailsScreen(id),
              );
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.group_details_screen)));
        break;
      case FirebaseMessagingConstants.war:
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return WarDetailsBlocProvider(
            child: WarDetailsScreen(id),
          );
        }));
        break;
      case FirebaseMessagingConstants.war_room:
        WarRoom warRoom = await repository.getWarRoom(id);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) {
              return WarRoomChatBlocProvider(
                child: WarRoomChatScreen(warRoom),
              );
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.war_room_chat_screen)));
        break;
      case FirebaseMessagingConstants.competition:
        Competition competition = await repository.getCompetition(id);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) {
              return CompetitionDetailsBlocProvider(
                child: CompetitionDetailsScreen(competition),
              );
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.competition_details_screen)));
        break;
      case FirebaseMessagingConstants.url:
        if (await canLaunch(id)) launch(id);
        break;
    }
  }

  Future<void> subscribeTopic(String topic) async {
    _firebaseMessaging.subscribeToTopic(topic);
  }

  Future<void> unSubscribeTopic(String topic) async {
    _firebaseMessaging.unsubscribeFromTopic(topic);
  }

  Future<void> cancelIOSSubscription() async {
    if (iosSubscription != null) iosSubscription.cancel();
  }

  Future<void> deleteFirebaseMessagingToken() async {
    User user = await repository.getLoggedInUser();
    await repository.deleteFirebaseMessagingTokenFromDb(
        user.uid, await _firebaseMessaging.getToken());
    return _firebaseMessaging.deleteInstanceID();
  }
}
