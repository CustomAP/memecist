import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memecist/ui/screens/login/phone_signup_screen.dart';
import 'package:memecist/core/blocs/login/phone_signup_bloc_provider.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/blocs/login/initialize_profile_info_bloc_provider.dart';
import 'package:memecist/ui/screens/wrapper.dart';
import 'package:memecist/ui/screens/login/intialize_profile_info_screen.dart';

class Memecist extends StatelessWidget {
  final FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: FirebaseAnalytics());
  final ChatScreenTracker chatScreenTracker = ChatScreenTracker();

  @override
  Widget build(BuildContext context) {
    return ObserverProvider(
      observer: observer,
      chatScreenTracker: chatScreenTracker,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Wrapper(),
        navigatorObservers: <NavigatorObserver>[observer],
      ),
    );
  }
}

class ObserverProvider extends InheritedWidget {
  const ObserverProvider({Widget child, Key key, this.observer, this.chatScreenTracker})
      : super(key: key, child: child);
  final FirebaseAnalyticsObserver observer;
  final ChatScreenTracker chatScreenTracker;

  static FirebaseAnalyticsObserver of(BuildContext context) {
    return (context
        .dependOnInheritedWidgetOfExactType<ObserverProvider>()
        .observer);
  }

  static ChatScreenTracker chatScreenTrackerOf(BuildContext context) {
    return (context
        .dependOnInheritedWidgetOfExactType<ObserverProvider>()
        .chatScreenTracker);
  }

  @override
  bool updateShouldNotify(ObserverProvider e) => false;
}

class ChatScreenTracker {
  String chatScreenID = "";
}
