class RegexValidator{
  static bool isStringWithOnlyAlphabets(String input){
    return RegExp(r'^[a-zA-Z]+$').hasMatch(input);
  }

  static bool isStringValidForUserName(String input){
    return RegExp(r'^[a-z0-9._]+$').hasMatch(input);
  }
}