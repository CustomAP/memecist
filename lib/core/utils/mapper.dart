class Mapper{
  static getNonEmptyMap(Map params){
    Map<String, dynamic> mapToBeReturned = {};
    params.forEach((key, value) {
      if(value != null){
        mapToBeReturned[key] = value;
      }
    });

    return mapToBeReturned;
  }
}