class KeywordsGenerator {
  static List<String> createKeyWords(String name) {
    List<String> keywords = [];

    String currentString = '';

    name.split('').forEach((letter) {
      currentString += letter;
      keywords.add(currentString);
    });

    return keywords;
  }

  static List<String> generateKeywordsForNames(
      String firstName, String lastName, String userName) {
    List<String> keywordsWithFirstAndLastNames =
        createKeyWords('${firstName.toLowerCase()} ${lastName.toLowerCase()}');
    List<String> keywordsWithUserName = createKeyWords(userName.toLowerCase());

    List<String> keywords = [];

    keywordsWithFirstAndLastNames.forEach((keyword) {
      keywords.add(keyword);
    });
    keywordsWithUserName.forEach((keyword) {
      keywords.add(keyword);
    });

    return keywords;
  }

  static List<String> generateKeywordsForGroups(String groupName) {
    return createKeyWords(groupName.toLowerCase());
  }

  static List<String> generateUnigrams(String str) {
    List<String> unigrams = [];
    List<String> strings = str.split('\n');
    for (String string in strings) {
      string.split(new RegExp("[\\s@&.?\$+-]+")).forEach((element) {
        unigrams.add(element);
      });
    }

    return unigrams;
  }

  static List<String> generateBigrams(List<String> unigrams) {
    List<String> bigrams = [];

    if (unigrams.length - 1 > 0)
      for (int i = 0; i < unigrams.length - 1; i++) {
        bigrams.add('${unigrams[i]} ${unigrams[i + 1]}');
      }

    return bigrams;
  }

  static List<String> generateTrigrams(List<String> unigrams) {
    List<String> trigrams = [];

    if (unigrams.length - 2 > 0)
      for (int i = 0; i < unigrams.length - 2; i++) {
        trigrams.add('${unigrams[i]} ${unigrams[i + 1]} ${unigrams[i + 2]}');
      }

    return trigrams;
  }

  static List<String> generateQuadgrams(List<String> unigrams) {
    List<String> quadgrams = [];

    if (unigrams.length - 3 > 0)
      for (int i = 0; i < unigrams.length - 3; i++) {
        quadgrams.add(
            '${unigrams[i]} ${unigrams[i + 1]} ${unigrams[i + 2]} ${unigrams[i + 3]}');
      }

    return quadgrams;
  }

  static List<String> generateKeywordsForPosts(
      String caption, String imageText) {
    List<String> keywords = [];

    List<String> unigrams =
        generateUnigrams(caption.toLowerCase() + " " + imageText.toLowerCase());

    unigrams.forEach((unigram) {
      keywords.add(unigram);
    });

    generateBigrams(unigrams).forEach((bigram) {
      keywords.add(bigram);
    });

    generateTrigrams(unigrams).forEach((trigram) {
      keywords.add(trigram);
    });

    generateQuadgrams(unigrams).forEach((quadgram) {
      keywords.add(quadgram);
    });

    return keywords;
  }
}
