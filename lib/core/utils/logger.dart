class LoggerUtil {
  static void log(String module, String function, String message) {
    print(DateTime.now().toString() + " ******Logs from Dev =>"
        " Module: " +
        module.toUpperCase() +
        " : " +
        "Function: " +
        function +
        " - " +
        "Message: " +
        " => " +
        message);
  }
}
