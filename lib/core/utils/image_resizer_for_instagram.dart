import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ImageReSizerForInstagram {
  static List<double> sizesForInstagram(GlobalKey key) {
    //width, height
    List<double> sizes = [0, 10];

    double height = key.currentContext.size.height;
    double width = key.currentContext.size.width;

    double heightOriginal = height;
    double widthOriginal = width;

    double aspectRatio = width / height;
    double targetRatio = 0.0;

    if (aspectRatio > 1.91) {
      targetRatio = 1.91;
    } else if (aspectRatio > 1.0) {
      targetRatio = 1.0;
    } else if (aspectRatio > 0.8) {
      targetRatio = 0.8;
    } else {
      while (width / height - 0.8 >= 0.0001 || 0.8 - width / height >= 0.0001) {
        width += 0.03;
      }

      double requiredWidth = widthOriginal * widthOriginal / width;
      sizes[0] = ((widthOriginal -
              requiredWidth +
              20.w /* extra padding to make aspect ratio just above 0.8*/) /
          2);
      return sizes;
    }

    while (width / height - targetRatio >= 0.0001 ||
        targetRatio - width / height >= 0.0001) {
      height += 0.03;
    }

    height = (height -
            heightOriginal +
            20.h /* original height of the two sized boxes*/) /
        2;

    sizes[1] = height;
    return sizes;
  }
}
