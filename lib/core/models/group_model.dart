import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';

class Group {
  String groupID,
      groupName,
      groupDescription,
      createdByID,
      groupProfilePic,
      warID,
      warName,
      warStatus,
      warCardColor,
      warPic;

  bool isClosed, isVerified, isWarActive;

  int membersCount,
      memesCount,
      warsCount,
      warsLeftThisMonthCount,
      unapprovedMemesCount;

  List<String> searchKeywords;

  Group(
      {this.groupID,
      this.groupName,
      this.groupDescription,
      this.createdByID,
      this.groupProfilePic,
      this.isClosed,
      this.isVerified,
      this.membersCount,
      this.memesCount,
      this.warsCount,
      this.warsLeftThisMonthCount,
      this.isWarActive,
      this.warID,
      this.warName,
      this.warStatus,
      this.warPic,
      this.unapprovedMemesCount,
      this.searchKeywords});

  Group.fromMap(Map map) {
    groupID = map[FirestoreConstants.group_id];
    groupName = map[FirestoreConstants.group_name];
    groupDescription = map[FirestoreConstants.group_description];
    createdByID = map[FirestoreConstants.created_by];
    groupProfilePic = map[FirestoreConstants.group_profile_pic];
    isClosed = map[FirestoreConstants.is_closed];
    isVerified = map[FirestoreConstants.is_verified];
    memesCount = map[FirestoreConstants.memes_count];
    warsCount = map[FirestoreConstants.wars_count];
    warsLeftThisMonthCount = map[FirestoreConstants.wars_left_this_month];
    warName = map[FirestoreConstants.war_name];
    warID = map[FirestoreConstants.war_id];
    warPic = map[FirestoreConstants.war_pic];
    warStatus = map[FirestoreConstants.war_status];
    unapprovedMemesCount = map[FirestoreConstants.unapproved_memes_count];
    isWarActive = map[FirestoreConstants.is_war_active];
    membersCount = map[FirestoreConstants.members_count] ?? 0;
    warCardColor = map[FirestoreConstants.card_color];
  }

  Group.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    groupID = documentSnapshot.documentID;
    groupName = documentSnapshot.data()[FirestoreConstants.group_name];
    groupDescription =
        documentSnapshot.data()[FirestoreConstants.group_description];
    createdByID = documentSnapshot.data()[FirestoreConstants.created_by];
    groupProfilePic =
        documentSnapshot.data()[FirestoreConstants.group_profile_pic];
    isClosed = documentSnapshot.data()[FirestoreConstants.is_closed];
    isVerified = documentSnapshot.data()[FirestoreConstants.is_verified];
    memesCount = documentSnapshot.data()[FirestoreConstants.memes_count];
    warsCount = documentSnapshot.data()[FirestoreConstants.wars_count];
    warsLeftThisMonthCount =
        documentSnapshot.data()[FirestoreConstants.wars_left_this_month];
    warName = documentSnapshot.data()[FirestoreConstants.war_name];
    warID = documentSnapshot.data()[FirestoreConstants.war_id];
    warPic = documentSnapshot.data()[FirestoreConstants.war_pic];
    warStatus = documentSnapshot.data()[FirestoreConstants.war_status];
    unapprovedMemesCount =
        documentSnapshot.data()[FirestoreConstants.unapproved_memes_count];
    isWarActive = documentSnapshot.data()[FirestoreConstants.is_war_active];
    membersCount = documentSnapshot.data()[FirestoreConstants.members_count] ?? 0;
    warCardColor = documentSnapshot.data()[FirestoreConstants.card_color];
  }
}
