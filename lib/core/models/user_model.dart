import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'user_private_data_model.dart';

class MemecistUser {
  String firstName, lastName, userName, profilePic, userID, bio;

  UserPrivateData userPrivateData;

  int followersCount, followingCount, memesCount;

  List<dynamic> searchKeywords;

  List badges;
  bool isVerified;
  double rep;

  MemecistUser(
      {this.firstName,
      this.lastName,
      this.userName,
      this.bio,
      this.badges,
      this.userID,
      this.isVerified,
      this.profilePic,
      this.memesCount,
      this.followersCount,
      this.followingCount,
      this.userPrivateData,
      this.rep,
      this.searchKeywords});

  MemecistUser.fromMap(Map map) {
    userID = map[FirestoreConstants.user_id];
    firstName = map[FirestoreConstants.first_name];
    lastName = map[FirestoreConstants.last_name];
    userName = map[FirestoreConstants.user_name];
    badges = map[FirestoreConstants.badges];
    isVerified = map[FirestoreConstants.is_verified];
    profilePic = map[FirestoreConstants.profile_pic];
    followersCount = map[FirestoreConstants.followers_count] ?? 0;
    followingCount = map[FirestoreConstants.following_count];
    memesCount = map[FirestoreConstants.memes_count];
    searchKeywords = map[FirestoreConstants.search_keywords];
    bio = map[FirestoreConstants.bio];
    if (map[FirestoreConstants.rep] != null)
      rep = (map[FirestoreConstants.rep] as num).toDouble();
  }

  MemecistUser.fromDocumentSnapshot(DocumentSnapshot value) {
    userID = value.data()[FirestoreConstants.user_id];
    firstName = value.data()[FirestoreConstants.first_name];
    lastName = value.data()[FirestoreConstants.last_name];
    userName = value.data()[FirestoreConstants.user_name];
    badges = value.data()[FirestoreConstants.badges];
    isVerified = value.data()[FirestoreConstants.is_verified];
    profilePic = value.data()[FirestoreConstants.profile_pic];
    followingCount = value.data()[FirestoreConstants.following_count];
    followersCount = value.data()[FirestoreConstants.followers_count] ?? 0;
    memesCount = value.data()[FirestoreConstants.memes_count];
    searchKeywords = value.data()[FirestoreConstants.search_keywords];
    bio = value.data()[FirestoreConstants.bio];
    if (value.data()[FirestoreConstants.rep] != null)
      rep = (value.data()[FirestoreConstants.rep] as num).toDouble();
  }
}
