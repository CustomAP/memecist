import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';

class WarRoom {
  String warRoomID, warID, warName, teamID, teamName, teamPic, lastMessage;
  int participantsCount, messagesCount;
  DateTime lastMessageDateTime, warRoomCreatedDateTime;
  bool isUnRead;

  WarRoom(
      {this.warRoomID,
      this.warID,
      this.warName,
      this.teamID,
      this.teamName,
      this.teamPic,
      this.lastMessage,
      this.participantsCount,
      this.messagesCount,
      this.lastMessageDateTime,
      this.warRoomCreatedDateTime});

  WarRoom.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    warName = documentSnapshot.data()[FirestoreConstants.war_name];
    warID = documentSnapshot.data()[FirestoreConstants.war_id];
    warRoomCreatedDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
    warRoomID = documentSnapshot.documentID;
    lastMessage = documentSnapshot.data()[FirestoreConstants.last_message];
    lastMessageDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.last_message_time]
        .toDate()
        .toString());
    messagesCount = documentSnapshot.data()[FirestoreConstants.messages_count];
    participantsCount = documentSnapshot.data()[FirestoreConstants.participants_count];
    teamID = documentSnapshot.data()[FirestoreConstants.team_id];
    teamName = documentSnapshot.data()[FirestoreConstants.team_name];
    teamPic = documentSnapshot.data()[FirestoreConstants.team_pic];
  }
}
