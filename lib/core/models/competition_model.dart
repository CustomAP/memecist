import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/competition_winner_model.dart';
import 'package:memecist/core/models/prize_model.dart';
import 'package:memecist/core/models/user_model.dart';

class Competition {
  String competitionName,
      competitionID,
      info,
      prizesInShort,
      competitionPic,
      cardColor;
  DateTime createDateTime, endDateTime;
  int totalSubmissions, totalParticipants, displayPriority;
  bool isCompetitionActive,
      requirePost,
      areExternalLinksAllowed,
      areExternalLinksCompulsory;
  String competitionStatus;
  MemecistUser hostedBy;
  List<dynamic> prizes, rules;

  Competition.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    prizes = [];

    competitionName = documentSnapshot.data()[FirestoreConstants.competition_name];
    competitionPic = documentSnapshot.data()[FirestoreConstants.competition_pic];
    info = documentSnapshot.data()[FirestoreConstants.info];
    documentSnapshot.data()[FirestoreConstants.prizes]
        .forEach((map) => prizes.add(Prize.fromMap(map)));
    prizesInShort = documentSnapshot.data()[FirestoreConstants.prizes_in_short];
    rules = documentSnapshot.data()[FirestoreConstants.rules];
    createDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
    endDateTime = DateTime.parse(
        documentSnapshot.data()[FirestoreConstants.end_date_time].toDate().toString());
    totalParticipants = documentSnapshot.data()[FirestoreConstants.total_participants];
    totalSubmissions = documentSnapshot.data()[FirestoreConstants.total_submissions];
    displayPriority = documentSnapshot.data()[FirestoreConstants.display_priority];
    isCompetitionActive =
        documentSnapshot.data()[FirestoreConstants.is_competition_active];
    hostedBy = MemecistUser.fromMap(documentSnapshot.data()[FirestoreConstants.hosted_by]);
    cardColor = documentSnapshot.data()[FirestoreConstants.card_color];
    competitionID = documentSnapshot.documentID;
    requirePost = documentSnapshot.data()[FirestoreConstants.require_post];
    competitionStatus = documentSnapshot.data()[FirestoreConstants.competition_status];
    areExternalLinksAllowed =
        documentSnapshot.data()[FirestoreConstants.are_external_links_allowed];
    areExternalLinksCompulsory =
        documentSnapshot.data()[FirestoreConstants.are_external_links_compulsory];
  }
}
