import 'package:memecist/core/models/user_model.dart';

class GroupMember{
  MemecistUser user;

  bool isAdmin;

  GroupMember(this.user, this.isAdmin);
}