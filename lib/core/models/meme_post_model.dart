import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';

class MemePost {
  String mediaUrl,
      postType,
      caption,
      imageText,
      category,
      postID,
      warID,
      groupID,
      competitionID,
      groupName,
      warName,
      competitionName,
      teamID,
      teamName;

  MemecistUser user;

  int reactCount, commentsCount;
  DateTime createDateTime;
  bool isReacted, isForGroup, isForCompetition, isForWar, isImageTextExtracted;
  List<dynamic> hashtags;
  List<String> searchKeywords;

  MemePost(
      {this.user,
      this.mediaUrl,
      this.reactCount,
      this.commentsCount,
      this.createDateTime,
      this.postType,
      this.isReacted,
      this.caption,
      this.imageText,
      this.category,
      this.hashtags,
      this.groupID,
      this.warID,
      this.competitionID,
      this.isForGroup,
      this.isForCompetition,
      this.isForWar,
      this.groupName,
      this.warName,
      this.competitionName,
      this.postID,
      this.teamID,
      this.teamName,
      this.isImageTextExtracted,
      this.searchKeywords});

  MemePost.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    mediaUrl = documentSnapshot.data()[FirestoreConstants.media_url];
    user = MemecistUser.fromDocumentSnapshot(documentSnapshot);
    reactCount = 0;
    commentsCount = 0;
    createDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
    postType = documentSnapshot.data()[FirestoreConstants.post_type];
    isReacted = false;
    caption = documentSnapshot.data()[FirestoreConstants.caption];
    category = documentSnapshot.data()[FirestoreConstants.category];
    hashtags = documentSnapshot.data()[FirestoreConstants.hashtags];
    isForGroup = documentSnapshot.data()[FirestoreConstants.is_for_group];
    groupID = documentSnapshot.data()[FirestoreConstants.group_id];
    groupName = documentSnapshot.data()[FirestoreConstants.group_name];
    isForWar = documentSnapshot.data()[FirestoreConstants.is_for_war];
    warID = documentSnapshot.data()[FirestoreConstants.war_id];
    warName = documentSnapshot.data()[FirestoreConstants.war_name];
    isForCompetition =
        documentSnapshot.data()[FirestoreConstants.is_for_competition];
    competitionID = documentSnapshot.data()[FirestoreConstants.competition_id];
    competitionName =
        documentSnapshot.data()[FirestoreConstants.competition_name];
    postID = documentSnapshot.documentID;
    teamID = documentSnapshot.data()[FirestoreConstants.team_id];
    teamName = documentSnapshot.data()[FirestoreConstants.team_name];
  }

  MemePost.fromMap(Map map) {
    mediaUrl = map[FirestoreConstants.media_url];
    user = MemecistUser.fromMap(map);
    reactCount = map[FirestoreConstants.react_count];
    commentsCount = map[FirestoreConstants.comments_count];
    createDateTime = DateTime.fromMillisecondsSinceEpoch(
        map[FirestoreConstants.create_date_time]);
    postType = map[FirestoreConstants.post_type];
    isReacted = map[FirestoreConstants.is_reacted];
    caption = map[FirestoreConstants.caption];
    category = map[FirestoreConstants.category];
    hashtags = map[FirestoreConstants.hashtags];
    isForGroup = map[FirestoreConstants.is_for_group];
    groupID = map[FirestoreConstants.group_id];
    groupName = map[FirestoreConstants.group_name];
    isForWar = map[FirestoreConstants.is_for_war];
    warID = map[FirestoreConstants.war_id];
    warName = map[FirestoreConstants.war_name];
    isForCompetition = map[FirestoreConstants.is_for_competition];
    competitionID = map[FirestoreConstants.competition_id];
    competitionName = map[FirestoreConstants.competition_name];
    postID = map[FirestoreConstants.post_id];
    teamID = map[FirestoreConstants.team_id];
    teamName = map[FirestoreConstants.team_name];
  }
}
