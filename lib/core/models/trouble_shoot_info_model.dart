import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';

class TroubleShootInfo {
  String info, link;

  TroubleShootInfo.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    info = documentSnapshot.data()[FirestoreConstants.info];
    link = documentSnapshot.data()[FirestoreConstants.link];
  }
}
