class MemecistAndroidDeviceInfo {
  String versionSecurityPatch,
      versionRelease,
      versionIncremental,
      versionCodename,
      versionBaseOS,
      board,
      bootloader,
      brand,
      device,
      display,
      fingerprint,
      hardware,
      host,
      id,
      manufacturer,
      model,
      product,
      tags,
      type,
      androidId;

  int versionSdkInt, versionPreviewSdkInt;

  List<String> supported32BitAbis,
      supported64BitAbis,
      supportedAbis,
      systemFeatures;

  bool isPhysicalDevice;

  MemecistAndroidDeviceInfo(
      this.versionSecurityPatch,
      this.versionSdkInt,
      this.versionRelease,
      this.versionPreviewSdkInt,
      this.versionIncremental,
      this.versionCodename,
      this.versionBaseOS,
      this.board,
      this.bootloader,
      this.brand,
      this.device,
      this.display,
      this.fingerprint,
      this.hardware,
      this.host,
      this.id,
      this.manufacturer,
      this.model,
      this.product,
      this.supported32BitAbis,
      this.supported64BitAbis,
      this.supportedAbis,
      this.tags,
      this.type,
      this.isPhysicalDevice,
      this.androidId,
      this.systemFeatures);
}
