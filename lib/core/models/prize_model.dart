import 'package:memecist/core/constants/firestore_constants.dart';

class Prize{
  String position, prize, icon;

  Prize.fromMap(Map<String, dynamic> map){
    position = map[FirestoreConstants.position];
    prize = map[FirestoreConstants.prize];
    icon = map[FirestoreConstants.icon];
  }
}