import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';

class CompetitionWinner {
  String postID, prize;
  MemecistUser user;
  int rank;
  bool postApplicable;

  CompetitionWinner.fromMap(Map map){
    rank = map[FirestoreConstants.rank];
    postID = map[FirestoreConstants.post_id];
    user = MemecistUser.fromMap(map);
    prize = map[FirestoreConstants.prize];
    postApplicable = map[FirestoreConstants.post_applicable];
  }
}