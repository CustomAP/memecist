import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/war_team_model.dart';

class War {
  String warID,
      groupID,
      groupName,
      groupProfilePic,
      warName,
      warPic,
      warDescription,
      warRules,
      memeOfTheWar,
      status,
      cardColor;

  Map<String, dynamic> createdBy;

  WarTeam winnerTeam;

  bool isActive, areTeamLeadersSelected;

  int maxTeamsCount, postsCount, perTeamLimit;

  DateTime startDateTime,
      teamLeadersSelectionEndTime,
      teamMembersSelectionEndTime,
      dDayEndTime;

  List<dynamic> teamLeaders;

  List<dynamic> participants;

  War(
      {this.warID,
      this.groupID,
      this.groupName,
      this.groupProfilePic,
      this.warName,
      this.warPic,
      this.warDescription,
      this.warRules,
      this.status,
      this.createdBy,
      this.isActive,
      this.maxTeamsCount,
      this.postsCount,
      this.perTeamLimit,
      this.startDateTime,
      this.teamLeadersSelectionEndTime,
      this.teamMembersSelectionEndTime,
      this.dDayEndTime,
      this.winnerTeam,
      this.areTeamLeadersSelected,
      this.memeOfTheWar,
      this.participants,
      this.teamLeaders,
      this.cardColor});

  War.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    warID = documentSnapshot.documentID;
    groupID = documentSnapshot.data()[FirestoreConstants.group_id];
    groupName = documentSnapshot.data()[FirestoreConstants.group_name];
    groupProfilePic =
        documentSnapshot.data()[FirestoreConstants.group_profile_pic];
    warName = documentSnapshot.data()[FirestoreConstants.war_name];
    warPic = documentSnapshot.data()[FirestoreConstants.war_pic];
    warDescription = documentSnapshot.data()[FirestoreConstants.war_description];
    warRules = documentSnapshot.data()[FirestoreConstants.war_rules];
    status = documentSnapshot.data()[FirestoreConstants.war_status];
    createdBy = documentSnapshot.data()[FirestoreConstants.created_by];
    isActive = documentSnapshot.data()[FirestoreConstants.is_war_active];
    maxTeamsCount = documentSnapshot.data()[FirestoreConstants.max_teams_count];
    postsCount = documentSnapshot.data()[FirestoreConstants.posts_count];
    perTeamLimit = documentSnapshot.data()[FirestoreConstants.per_team_limit];
    startDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.start_date_time]
        .toDate()
        .toString());
    teamLeadersSelectionEndTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.team_leaders_selection_end_time]
        .toDate()
        .toString());
    teamMembersSelectionEndTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.team_members_selection_end_time]
        .toDate()
        .toString());
    dDayEndTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.d_day_end_time]
        .toDate()
        .toString());
    Map<String, dynamic> winnerTeamMap =
        documentSnapshot.data()[FirestoreConstants.winner_team];
    if (winnerTeamMap != null)
      winnerTeam = WarTeam.fromMap(winnerTeamMap);
    areTeamLeadersSelected =
        documentSnapshot.data()[FirestoreConstants.are_team_leaders_selected];
    teamLeaders = documentSnapshot.data()[FirestoreConstants.team_leaders];
    participants = documentSnapshot.data()[FirestoreConstants.war_participants];
    memeOfTheWar = documentSnapshot.data()[FirestoreConstants.meme_of_the_war];
    cardColor = documentSnapshot.data()[FirestoreConstants.card_color];
  }
}
