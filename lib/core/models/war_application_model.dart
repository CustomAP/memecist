import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';

class WarApplication {
  MemecistUser user;
  DateTime createDateTime;
  String teamID;
  bool isAccepted;

  WarApplication(
      {this.user, this.createDateTime, this.teamID, this.isAccepted});

  WarApplication.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    user = MemecistUser.fromDocumentSnapshot(documentSnapshot);
    isAccepted = documentSnapshot.data()[FirestoreConstants.is_accepted];
    teamID = documentSnapshot.data()[FirestoreConstants.team_id];
    createDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
  }
}
