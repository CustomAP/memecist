class MemecistRemoteConfig {
  String forceUpdateVersionForAndroidString, forceUpdateVersionForIosString;
  bool shouldShowRatingDialog, shouldShowSuggestionsDialog;
  int groupMembersCountUpdateAtRun,
      userFollowersCountUpdateAtRun,
      appAvailabilityUpdateAtRun;

  MemecistRemoteConfig(
      this.forceUpdateVersionForIosString,
      this.forceUpdateVersionForAndroidString,
      this.shouldShowRatingDialog,
      this.shouldShowSuggestionsDialog,
      this.groupMembersCountUpdateAtRun,
      this.userFollowersCountUpdateAtRun,
      this.appAvailabilityUpdateAtRun);
}
