import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';

class Comment {
  String comment, commentID;
  DateTime createDateTime;

  MemecistUser user;

  Comment(this.comment, this.createDateTime, this.user, this.commentID);

  Comment.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    user = MemecistUser.fromDocumentSnapshot(documentSnapshot);
    comment = documentSnapshot.data()[FirestoreConstants.comment];
    createDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
    commentID = documentSnapshot.documentID;
  }
}
