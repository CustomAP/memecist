class IP {
  String ip,
      city,
      region,
      countryCodeIso3,
      countryName,
      postal,
      timezone,
      utcOffset,
      countryCallingCode,
      asn,
      org;

  double latitude, longitude;

  IP(
      this.ip,
      this.city,
      this.region,
      this.countryCodeIso3,
      this.countryName,
      this.postal,
      this.timezone,
      this.utcOffset,
      this.countryCallingCode,
      this.asn,
      this.org,
      this.latitude,
      this.longitude);
}
