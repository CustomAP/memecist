import 'package:memecist/core/constants/firestore_constants.dart';

class TemplateCategory {
  String templateCategoryName, templateCategoryID;
  DateTime createDateTime;
  List<TemplateCategory> subcategories;
  bool isDummy = false;

  TemplateCategory.fromMap(Map<String, dynamic> map) {
    templateCategoryName = map[FirestoreConstants.template_category_name];
    createDateTime = DateTime.parse(
        map[FirestoreConstants.create_date_time].toDate().toString());
    templateCategoryID = map[FirestoreConstants.template_category_id];
  }

  TemplateCategory(
      String templateCategoryName,
      String templateCategoryID,
      DateTime createDateTime,
      bool isDummy,
      List<TemplateCategory> subcategories) {
    this.templateCategoryName = templateCategoryName;
    this.templateCategoryID = templateCategoryID;
    this.createDateTime = createDateTime;
    this.subcategories = subcategories;
    this.isDummy = isDummy;
  }
}
