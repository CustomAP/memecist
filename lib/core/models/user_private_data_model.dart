class UserPrivateData {
  String birthDate, verifyMode, verifyLink;

  bool isPhoneNumberAdded;

  int notificationsCount;

  UserPrivateData(
      {this.birthDate,
      this.verifyMode,
      this.verifyLink,
      this.notificationsCount,
      this.isPhoneNumberAdded});
}
