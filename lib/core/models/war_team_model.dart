import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';

class WarTeam {
  List<MemecistUser> teamMembers;
  MemecistUser teamLeader;
  String teamName, teamPic, warID, teamID;
  int memesCount, membersCount;
  DateTime createDateTime;
  List<dynamic> members;
  double teamRepScore;

  WarTeam.fromMap(Map<String, dynamic> map) {
    teamLeader = MemecistUser.fromMap(map[FirestoreConstants.team_leader]);
    teamName = map[FirestoreConstants.team_name];
    teamPic = map[FirestoreConstants.team_pic];
    warID = map[FirestoreConstants.war_id];
    memesCount = map[FirestoreConstants.memes_count];
    membersCount = map[FirestoreConstants.members_count];
    if (map[FirestoreConstants.create_date_time] != null)
      createDateTime = DateTime.parse(
          map[FirestoreConstants.create_date_time].toDate().toString());
    members = map[FirestoreConstants.members];
    if (map[FirestoreConstants.team_rep_score] != null)
      teamRepScore = (map[FirestoreConstants.team_rep_score] as num).toDouble();
    teamID = map[FirestoreConstants.team_id];
    teamMembers = [];
    List<dynamic> teamMembersMap = map[FirestoreConstants.team_members];
    if (teamMembersMap != null)
      teamMembersMap.forEach((teamLeaderMap) {
        teamMembers.add(MemecistUser.fromMap(teamLeaderMap));
      });
  }

  WarTeam.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    teamLeader =
        MemecistUser.fromMap(documentSnapshot.data()[FirestoreConstants.team_leader]);
    teamName = documentSnapshot.data()[FirestoreConstants.team_name];
    teamPic = documentSnapshot.data()[FirestoreConstants.team_pic];
    warID = documentSnapshot.data()[FirestoreConstants.war_id];
    memesCount = documentSnapshot.data()[FirestoreConstants.memes_count];
    membersCount = documentSnapshot.data()[FirestoreConstants.members_count];
    createDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
    members = documentSnapshot.data()[FirestoreConstants.members];
    if (documentSnapshot.data()[FirestoreConstants.team_rep_score] != null)
      teamRepScore =
          (documentSnapshot.data()[FirestoreConstants.team_rep_score] as num)
              .toDouble();
    teamID = documentSnapshot.documentID;
    teamMembers = [];
    List<dynamic> teamMembersMap =
        documentSnapshot.data()[FirestoreConstants.team_members];
    if (teamMembersMap != null)
      teamMembersMap.forEach((teamLeaderMap) {
        teamMembers.add(MemecistUser.fromMap(teamLeaderMap));
      });
  }
}
