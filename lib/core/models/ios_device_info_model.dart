class MemecistIOSDeviceInfo {
  String name,
      systemName,
      systemVersion,
      model,
      localizedModel,
      identifierForVendor,
      utsnameSysname,
      utsnameNodename,
      utsnameRelease,
      utsnameVersion,
      utsnameMachine;

  bool isPhysicalDevice;

  MemecistIOSDeviceInfo(
      this.name,
      this.systemName,
      this.systemVersion,
      this.model,
      this.localizedModel,
      this.identifierForVendor,
      this.isPhysicalDevice,
      this.utsnameSysname,
      this.utsnameNodename,
      this.utsnameRelease,
      this.utsnameVersion,
      this.utsnameMachine);
}
