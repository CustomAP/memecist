import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';

class Promo {
  String instagramID;
  String facebookID;
  String memecistID;

  bool isSet = false;

  Promo(this.facebookID, this.instagramID, this.memecistID, this.isSet);

  Promo.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    instagramID = documentSnapshot.data()[FirestoreConstants.instagram];
    facebookID = documentSnapshot.data()[FirestoreConstants.facebook];
    memecistID = documentSnapshot.data()[FirestoreConstants.memecist];
    isSet = true;
  }
}
