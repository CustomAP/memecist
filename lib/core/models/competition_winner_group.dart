import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/competition_winner_model.dart';

class CompetitionWinnerGroup {
  String position;
  int totalWinners, displayOrder;
  List<CompetitionWinner> competitionWinners = [];

  CompetitionWinnerGroup.fromMap(Map<String, dynamic> map) {
    position = map[FirestoreConstants.position];
    displayOrder = map[FirestoreConstants.display_order];
    totalWinners = map[FirestoreConstants.total_winners];
    List<dynamic> winnerMap = map[FirestoreConstants.winners];
    if (winnerMap != null)
      winnerMap.forEach((winner) {
        competitionWinners.add(CompetitionWinner.fromMap(winner));
      });
  }
}
