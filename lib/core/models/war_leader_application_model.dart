import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/models/war_application_model.dart';

class WarLeaderApplication extends WarApplication {
  bool isSelected = false;

  WarLeaderApplication.fromDocumentSnapshot(DocumentSnapshot documentSnapshot)
      : super.fromDocumentSnapshot(documentSnapshot);
}
