import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/models/war_application_model.dart';

class WarMemberApplication extends WarApplication {
  bool isSelected = false;

  WarMemberApplication.fromDocumentSnapshot(DocumentSnapshot documentSnapshot)
      : super.fromDocumentSnapshot(documentSnapshot);
}