import 'package:memecist/core/constants/firestore_constants.dart';

class Badge {
  String name, image;
  int level;

  Badge.fromMap(Map map) {
    name = map[FirestoreConstants.name];
    image = map[FirestoreConstants.image];
    level = map[FirestoreConstants.level];
  }
}
