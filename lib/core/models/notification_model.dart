import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';

class MemecistNotification {
  String message, type, id, leadingImage, actionImage;
  DateTime createDateTime;

  MemecistNotification.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    createDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
    type = documentSnapshot.data()[FirestoreConstants.type];
    id = documentSnapshot.data()[FirestoreConstants.id];
    message = documentSnapshot.data()[FirestoreConstants.message];
    leadingImage = documentSnapshot.data()[FirestoreConstants.leading_image];
    actionImage = documentSnapshot.data()[FirestoreConstants.action_image];
  }
}
