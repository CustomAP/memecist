import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';

class Message {
  String content, type, messageID;
  MemecistUser sender;
  DateTime createDateTime;

  Message(
      {this.content,
      this.type,
      this.messageID,
      this.sender,
      this.createDateTime});

  Message.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    content = documentSnapshot.data()[FirestoreConstants.content];
    type = documentSnapshot.data()[FirestoreConstants.type];
    messageID = documentSnapshot.documentID;
    sender = MemecistUser.fromDocumentSnapshot(documentSnapshot);
    createDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
  }
}
