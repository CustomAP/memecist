import 'package:memecist/core/models/badge_model.dart';

class BadgeCategory {
  String name;
  List<Badge> badges;

  BadgeCategory(this.name, this.badges);
}
