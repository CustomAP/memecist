import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';

class Template {
  String templateName, templatePic, categoryName, templateDesc;
  MemecistUser createdBy;
  List<dynamic> searchKeywords;
  bool isApproved;
  DateTime createDateTime;

  Template.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    templateName = documentSnapshot.data()[FirestoreConstants.template_name];
    templateDesc = documentSnapshot.data()[FirestoreConstants.template_desc];
    templatePic = documentSnapshot.data()[FirestoreConstants.template_pic];
    isApproved = documentSnapshot.data()[FirestoreConstants.is_approved];
    categoryName =
        documentSnapshot.data()[FirestoreConstants.template_category_name];
    searchKeywords =
        documentSnapshot.data()[FirestoreConstants.search_keywords];
    createDateTime = DateTime.parse(documentSnapshot
        .data()[FirestoreConstants.create_date_time]
        .toDate()
        .toString());
    createdBy = MemecistUser.fromMap(documentSnapshot.data());
  }

  Template(String categoryName, String templateName, String userID,
      String userName, String templatePic) {
    this.createdBy = MemecistUser();
    this.categoryName = categoryName;
    this.templateName = templateName;
    this.createdBy.userID = userID;
    this.createdBy.userName = userName;
    this.templatePic = templatePic;
  }
}
