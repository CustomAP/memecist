import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget memecistInputField(
    String hint, TextEditingController controller, bool autofocus,
    {Color borderColor,
    FormFieldValidator validator,
    String errorText,
    List<TextInputFormatter> inputFormatter}) {
  return TextFormField(
      autofocus: autofocus,
      controller: controller,
      validator: validator,
      textCapitalization: TextCapitalization.sentences,
      inputFormatters: inputFormatter,
      style: TextStyle(
        fontSize: 16.0.sp
      ),
      decoration: InputDecoration(
          hintStyle: TextStyle(fontSize: 14.0.sp),
          errorText: errorText,
          hintText: hint,
          contentPadding: EdgeInsets.only(
              left: 10.0.w, right: 10.0.w, top: 10.0.h, bottom: 10.0.h),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0.w),
              borderSide: BorderSide(
                  color: MemecistColors.primaryColor, width: 2.25.w)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0.w),
              borderSide: BorderSide(color: borderColor ?? Colors.white))));
}

Widget circularImageButton(String imageUrl, {double size, double radius}) {
  return Container(
    width: size ?? 36.0.w,
    height: size ?? 36.0.w,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius ?? 18.0.w),
        image: DecorationImage(
            image: CachedNetworkImageProvider(
              imageUrl,
            ),
            fit: BoxFit.cover)),
  );
}

Widget verifiedBadge({size}) {
  return Container(
      width: size ?? 20.0.w,
      height: size ?? 20.0.w,
      alignment: Alignment.center,
      child: Icon(
        Icons.check_circle,
        color: MemecistColors.primaryColor,
        size: size ?? 16.0.w,
      ));
}

Widget actionButton(BuildContext context, String title, Function() action,
    {double width, double height}) {
  return Material(
    color: Colors.transparent,
    child: InkWell(
      onTap: action,
      child: Container(
        height:
            MediaQuery.of(context).size.height > StringConstants.referenceHeight
                ? (height != null
                    ? (height.h + (height - height.h) / 2)
                    : 40.0.h + (40.0 - 40.0.h) / 2)
                : (height != null ? height.h : 40.0.h),
        width: width != null ? width.w : 125.0.w,
        decoration: BoxDecoration(
            color: Colors.lightBlueAccent[100].withOpacity(0.5),
            borderRadius: BorderRadius.circular(10.0.w)),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                fontSize: 14.0.sp,
                fontFamily: "Quicksand",
                fontWeight: FontWeight.bold,
                color: Colors.blue),
          ),
        ),
      ),
    ),
  );
}

Widget greyedOutButton(BuildContext context, String title, Function() action,
    {double width, double height}) {
  return InkWell(
    onTap: action,
    child: Container(
        height:
            MediaQuery.of(context).size.height > StringConstants.referenceHeight
                ? (height != null
                    ? (height.h + (height - height.h) / 2)
                    : 40.0.h + (40.0 - 40.0.h) / 2)
                : (height != null ? height.h : 40.0.h),
        width: width != null ? width.w : 125.0.w,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            border: Border.all(), borderRadius: BorderRadius.circular(10.0.w)),
        child: Text(title,
            style: TextStyle(
                fontSize: 14.0.sp,
                fontWeight: FontWeight.bold,
                fontFamily: 'Quicksand'))),
  );
}

Widget dialogButton(String text, Function() action, {Color color}) {
  return FlatButton(
    child: Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontSize: 16.0.sp, color: color ?? MemecistColors.primaryColor),
    ),
    onPressed: action,
  );
}

AppBar appBarWithBackButton(BuildContext context) {
  return AppBar(
    centerTitle: true,
    brightness:
        Platform.isAndroid ? AppBarTheme().brightness : Brightness.light,
    title: Image.asset(
      'assets/mc.png',
      height: 40.0.w,
      width: 40.0.w,
    ),
    backgroundColor: Colors.white,
    elevation: 0.0,
    leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
        ),
        onPressed: () => Navigator.of(context).pop()),
  );
}

AppBar appBarWithBackAndActionButton(
    BuildContext context, Widget actionWidget) {
  return AppBar(
    backgroundColor: Colors.white,
    brightness:
        Platform.isAndroid ? AppBarTheme().brightness : Brightness.light,
    elevation: 0.0,
    centerTitle: true,
    title: Image.asset(
      'assets/mc.png',
      height: 40.0.w,
      width: 40.0.w,
    ),
    leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
        ),
        onPressed: () => Navigator.of(context).pop()),
    actions: <Widget>[actionWidget],
  );
}

AppBar appBarWithBackButtonAndTitle(String title, BuildContext context) {
  return AppBar(
    backgroundColor: Colors.white,
    brightness:
        Platform.isAndroid ? AppBarTheme().brightness : Brightness.light,
    elevation: 0.0,
    centerTitle: true,
    leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
        ),
        onPressed: () => Navigator.of(context).pop()),
    title: Text(
      title,
      style: TextStyle(
          fontSize: 22.0.sp,
          fontFamily: 'Quicksand',
          color: MemecistColors.primaryColor),
    ),
  );
}

Widget userRowItem(MemecistUser user, bool isBig) {
  double picSize = 36.w;
  double nameSize = 16.0.sp;
  double userNameSize = 14.0.sp;
  if (isBig) {
    picSize = 50.0.w;
    nameSize = 18.0.sp;
    userNameSize = 16.0.sp;
  }

  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
      circularImageButton(user.profilePic, size: picSize, radius: picSize / 2),
      SizedBox(
        width: 10.0.w,
      ),
      Flexible(
        /*All flexibles used here required for autosizetext to work*/
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
              child: AutoSizeText(
                "${user.firstName} ${user.lastName}",
                maxLines: 2,
                minFontSize: 12.0.sp,
                stepGranularity: 12.0.sp / 12,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: nameSize,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Flexible(
              child: AutoSizeText(
                "@${user.userName}",
                maxLines: 1,
                minFontSize: 12.0.sp,
                stepGranularity: 12.0.sp / 12,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontSize: userNameSize,
                ),
              ),
            )
          ],
        ),
      ),
      user.isVerified ? Container(child: verifiedBadge()) : Container(),
    ],
  );
}
