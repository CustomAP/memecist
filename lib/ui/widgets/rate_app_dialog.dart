import 'package:flutter/material.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RateAppDialog extends StatefulWidget {
  @override
  _RateAppDialogState createState() => _RateAppDialogState();
}

class _RateAppDialogState extends State<RateAppDialog> {
  @override
  Widget build(BuildContext context) {
    String title = "We need your support!";
    String message =
        "\tWe work hard daily just to bring smile on your faces with the best memes.\n\n\tYou can bring a smile on our faces too, simply by giving us a 5 Star!";
    String btnLabel = "Rate 5 Star";
    String btnLabelCancel = "Nah, never";
    return AlertDialog(
      title: Text(
        title,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontFamily: 'Quicksand',
            color: MemecistColors.primaryColor,
            fontSize: 16.0.sp),
      ),
      content: Text(
        message,
        style: TextStyle(fontFamily: 'Quicksand', fontSize: 15.0.sp),
      ),
      actions: <Widget>[
        FlatButton(
          textColor: MemecistColors.primaryColor,
          child: Text(
            btnLabelCancel,
            style: TextStyle(fontSize: 16.0.sp),
          ),
          onPressed: () => dismissRateAppDialog(),
        ),
        FlatButton(
          textColor: MemecistColors.primaryColor,
          child: Text(
            btnLabel,
            style: TextStyle(fontSize: 16.0.sp),
          ),
          onPressed: () => rateApp(),
        )
      ],
    );
  }

  rateApp() async {
//    if (Platform.isAndroid) {
//      launchURL(StringConstants.play_store_url);
//    } else if (Platform.isIOS) {
//      launchURL(StringConstants.app_store_url);
//    }

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.rate_app);
  }

  void launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  dismissRateAppDialog() {
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.dismiss_rate_app_prompt);
    Navigator.of(context).pop();
  }
}
