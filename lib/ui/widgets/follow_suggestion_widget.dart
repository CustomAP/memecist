import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:memecist/core/blocs/home/home_bloc.dart';
import 'package:memecist/core/blocs/home/home_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/blocs/widgets/follow_suggestion_bloc.dart';
import 'package:memecist/core/blocs/widgets/follow_suggestion_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FollowSuggestionWidget extends StatefulWidget {
  final MemecistUser user;

  FollowSuggestionWidget(this.user);

  @override
  _FollowSuggestionWidgetState createState() =>
      _FollowSuggestionWidgetState(user);
}

class _FollowSuggestionWidgetState extends State<FollowSuggestionWidget> {
  final MemecistUser user;
  FollowSuggestionBloc _followSuggestionBloc;
  HomeBloc _homeBloc;
  User currentUser;

  _FollowSuggestionWidgetState(this.user);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _followSuggestionBloc = FollowSuggestionBlocProvider.of(context);
    _homeBloc = HomeBlocProvider.of(context);
    _followSuggestionBloc.initializeFollowSuggestionsBloc(user);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: 8.0.h, left: 10.0.w, right: 10.0.w, bottom: 8.0.h),
      child: Container(
        width: 200.0.w,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(7.0.w),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  blurRadius: 3.0.w,
                  spreadRadius: 3.0.w)
            ]),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: openProfile,
            child: Padding(
              padding: EdgeInsets.only(left: 5.0.w, right: 5.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  circularImageButton(user.profilePic,
                      size: 60.0.w, radius: 30.0.w),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: AutoSizeText(
                          '${user.firstName} ${user.lastName}',
                          maxLines: 2,
                          minFontSize: 12.0.sp,
                          stepGranularity: 12.0.sp / 12,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 16.0.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        width: 5.0.w,
                      ),
                      user.isVerified
                          ? verifiedBadge(size: 15.0.w)
                          : Container()
                    ],
                  ),
                  SizedBox(
                    height: 5.0.h,
                  ),
                  Flexible(
                    child: AutoSizeText(
                      '@${user.userName}',
                      maxLines: 1,
                      minFontSize: 12.0.sp,
                      stepGranularity: 12.0.sp / 12,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: Colors.black,
                          fontSize: 14.0.sp),
                    ),
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  Text(
                    '${NumberFormat.compact().format(user.followersCount)} followers',
                    style: TextStyle(
                        color: MemecistColors.greyText, fontSize: 13.0.sp),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  StreamBuilder(
                      stream: _followSuggestionBloc.following,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data) {
                            return greyedOutButton(context,
                                "Following", () => unFollowUser(),
                                width: 100.0, height: 35.0);
                          } else {
                            return StreamBuilder(
                                stream: _followSuggestionBloc.currentUser,
                                builder: (context,
                                    AsyncSnapshot<User>
                                        currentUserSnapshot) {
                                  if (currentUserSnapshot.hasData) {
                                    if (currentUserSnapshot.data.uid ==
                                        user.userID) {
                                      return Container();
                                    } else {
                                      return actionButton(
                                          context, "Follow", () => followUser(),
                                          width: 100.0, height: 35.0);
                                    }
                                  } else {
                                    return actionButton(
                                        context, "Follow", () => followUser(),
                                        width: 100.0, height: 35.0);
                                  }
                                });
                          }
                        } else {
                          return actionButton(
                              context, "Follow", () => followUser(),
                              width: 100.0, height: 35.0);
                        }
                      }),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void openProfile() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(user.userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }

  void followUser() {
    _followSuggestionBloc.follow();
    _homeBloc.setInitialRunDone();
  }

  void unFollowUser() {
    _followSuggestionBloc.unFollow();
  }
}
