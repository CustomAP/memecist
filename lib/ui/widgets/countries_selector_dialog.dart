import 'package:flutter/material.dart';
import 'package:memecist/core/models/country_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CountriesSelectorDialog {
  List<Country> countryList;

  CountriesSelectorDialog(this.countryList);

  Widget searchPickCountryDialog(TextEditingController controller,
          Function(Country country) selectCountry, Stream stream) =>
      WillPopScope(
        onWillPop: () => Future.value(false),
        child: Dialog(
          key: Key('SearchCountryDialog'),
          elevation: 8.0.w,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0.w)),
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(5.0.w),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        left: 8.0.w, top: 8.0.h, bottom: 2.0.h, right: 8.0.w),
                    child: Card(
                      child: TextFormField(
                        autofocus: true,
                        controller: controller,
                        style: TextStyle(
                          fontSize: 16.0.sp
                        ),
                        decoration: InputDecoration(
                            hintText: 'Search your country',
                            contentPadding: EdgeInsets.only(
                                left: 5.0.w,
                                right: 5.0.w,
                                top: 10.0.h,
                                bottom: 10.0.h),
                            border: InputBorder.none),
                      ),
                    ),
                  ),
                  SizedBox(
                      height: 300.0.h,
                      child: StreamBuilder(
                          stream: stream,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              countryList = snapshot.data;
                              return ListView.builder(
                                shrinkWrap: true,
                                itemCount: countryList.length,
                                itemBuilder: (BuildContext context, int i) =>
                                    selectableWidget(countryList[i],
                                        (Country c) => selectCountry(c)),
                              );
                            } else {
                              return Container();
                            }
                          }))
                ],
              ),
            ),
          ),
        ),
      );

  static Widget selectableWidget(
          Country country, Function(Country country) selectCountry) =>
      Material(
        color: Colors.white,
        type: MaterialType.canvas,
        child: InkWell(
          onTap: () => selectCountry(country),
          child: Padding(
            padding: EdgeInsets.only(
                left: 10.0.w, right: 10.0.w, top: 10.0.h, bottom: 10.0.h),
            child: Text(
              "  " +
                  country.flag +
                  "  " +
                  country.name +
                  " (" +
                  country.dialCode +
                  ")",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0.sp,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
      );
}
