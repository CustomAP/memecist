import 'package:flutter/material.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memecist/ui/constants/colors.dart';

class ColorPickerDialog {
  Widget colorPickerDialog(Function(String color) selectColor) {
    List<String> colors = MemecistColors.card_colors;
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        elevation: 8.0.w,
        insetPadding: EdgeInsets.all(10.0.w),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0.w)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "Select a color",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0.sp,
                    fontFamily: 'Quicksand',
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Padding(
                padding: EdgeInsets.all(5.0.w),
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4),
                  itemBuilder: (context, index) {
                    return selectableWidget(
                        colors[index], (String color) => selectColor(color));
                  },
                  shrinkWrap: true,
                  itemCount: colors.length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Widget selectableWidget(
          String color, Function(String color) selectColor) =>
      Material(
        color: Colors.white,
        type: MaterialType.canvas,
        child: InkWell(
            onTap: () => selectColor(color),
            child: Container(
              color: Color(
                  int.parse(color.substring(1, 7), radix: 16) + 0xFF000000),
            )),
      );
}
