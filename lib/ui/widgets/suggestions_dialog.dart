import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SuggestionsDialog extends StatefulWidget {
  @override
  _SuggestionsDialogState createState() => _SuggestionsDialogState();
}

class _SuggestionsDialogState extends State<SuggestionsDialog> {
  TextEditingController _suggestionsController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    String title = "Memecist is made for YOU!!";
    String message =
        "You can type in any feedback/suggestion you want and it will directly reach us!\nLet's build the strongest meme community!";
    String btnLabel = "Send";
    String btnLabelCancel = "Nah, I don't have any";
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        elevation: 8.0.w,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0.w)),
        child: Padding(
          padding: EdgeInsets.all(10.0.w),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: MemecistColors.primaryColor,
                      fontSize: 17.0.sp),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  message,
                  style: TextStyle(fontFamily: 'Quicksand', fontSize: 14.0.sp),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                TextField(
                  keyboardType: TextInputType.multiline,
                  controller: _suggestionsController,
                  maxLines: 5,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                      hintText: "Suggestions/Feedback..",
                      hintStyle: TextStyle(fontSize: 14.0.sp),
                      contentPadding: EdgeInsets.only(
                          left: 10.0.w,
                          right: 10.0.w,
                          top: 10.0.w,
                          bottom: 10.0.w),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0.w),
                          borderSide: BorderSide(
                              color: MemecistColors.primaryColor,
                              width: 2.25.w)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0.w),
                          borderSide: BorderSide(color: Colors.white))),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    FlatButton(
                      child: Text(
                        btnLabelCancel,
                        style: TextStyle(fontSize: 16.0.sp),
                      ),
                      textColor: MemecistColors.primaryColor,
                      onPressed: () => dismissSuggestionDialog(),
                    ),
                    FlatButton(
                        textColor: MemecistColors.primaryColor,
                        child: Text(
                          btnLabel,
                          style: TextStyle(fontSize: 16.0.sp),
                        ),
                        onPressed: () => sendSuggestion()),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  dismissSuggestionDialog() {
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.dismiss_suggestion_prompt);
    Navigator.of(context).pop();
  }

  sendSuggestion() async {
    String suggestion = _suggestionsController.text;
    User user = await repository.getLoggedInUser();
    repository.sendSuggestion(user.uid, suggestion);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.suggestion_send);

    Fluttertoast.showToast(msg: "Thank you so much for your valuable feedback");
    Navigator.of(context).pop();
  }
}
