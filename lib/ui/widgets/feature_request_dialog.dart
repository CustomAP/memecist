import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FeatureRequestDialog extends StatefulWidget {
  @override
  _FeatureRequestDialogState createState() => _FeatureRequestDialogState();
}

class _FeatureRequestDialogState extends State<FeatureRequestDialog> {
  TextEditingController _featureRequestController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    String title = "Want to see something more?";
    String message =
        "We believe in giving what's wanted! Let us know what you'll love to see in this app. We are all ears!";
    String btnLabel = "Send";
    String btnLabelCancel = "Cancel";
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        elevation: 8.0.w,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0.w)),
        child: Padding(
          padding: EdgeInsets.all(10.0.w),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: MemecistColors.primaryColor,
                      fontSize: 17.0.sp),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  message,
                  style: TextStyle(fontFamily: 'Quicksand', fontSize: 14.0.sp),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                TextField(
                  keyboardType: TextInputType.multiline,
                  controller: _featureRequestController,
                  maxLines: 5,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(fontSize: 16.0.sp),
                  decoration: InputDecoration(
                      hintText: "Describe the feature...",
                      hintStyle: TextStyle(fontSize: 14.0.sp),
                      contentPadding: EdgeInsets.only(
                          left: 10.0.w,
                          right: 10.0.w,
                          top: 10.0.h,
                          bottom: 10.0.h),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0.w),
                          borderSide: BorderSide(
                              color: MemecistColors.primaryColor,
                              width: 2.25.w)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0.w),
                          borderSide: BorderSide(color: Colors.white))),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    FlatButton(
                      child: Text(
                        btnLabelCancel,
                        style: TextStyle(fontSize: 16.0.sp),
                      ),
                      textColor: MemecistColors.primaryColor,
                      onPressed: () => dismissFeatureRequestDialog(),
                    ),
                    FlatButton(
                        textColor: MemecistColors.primaryColor,
                        child: Text(
                          btnLabel,
                          style: TextStyle(fontSize: 16.0.sp),
                        ),
                        onPressed: () => sendFeatureRequest()),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  dismissFeatureRequestDialog() {
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.dismiss_feature_req_prompt);
    Navigator.of(context).pop();
  }

  sendFeatureRequest() async {
    String suggestion = _featureRequestController.text;
    User user = await repository.getLoggedInUser();
    repository.featureRequest(user.uid, suggestion);

    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.feature_request);

    Fluttertoast.showToast(msg: "We'll think it over!\nThank you so much :)");
    Navigator.of(context).pop();
  }
}
