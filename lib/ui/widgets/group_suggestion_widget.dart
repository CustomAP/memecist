import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:memecist/core/blocs/home/home_bloc.dart';
import 'package:memecist/core/blocs/home/home_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/widgets/group_suggestion_bloc.dart';
import 'package:memecist/core/blocs/widgets/group_suggestion_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GroupSuggestionWidget extends StatefulWidget {
  final Group group;

  GroupSuggestionWidget(this.group);

  @override
  _GroupSuggestionWidgetState createState() =>
      _GroupSuggestionWidgetState(group);
}

class _GroupSuggestionWidgetState extends State<GroupSuggestionWidget> {
  final Group group;

  GroupSuggestionBloc _groupSuggestionBloc;
  HomeBloc _homeBloc;

  _GroupSuggestionWidgetState(this.group);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _groupSuggestionBloc = GroupSuggestionBlocProvider.of(context);
    _homeBloc = HomeBlocProvider.of(context);

    _groupSuggestionBloc.initializeGroupSuggestionsBloc(group);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: openGroupDetailsScreen,
        child: Padding(
          padding: EdgeInsets.only(
              top: 8.0.h, left: 10.0.w, right: 10.0.w, bottom: 8.0.h),
          child: Container(
            width: 200.0.w,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(7.0.w),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      blurRadius: 3.0.w,
                      spreadRadius: 3.0.w)
                ]),
            child: Padding(
              padding: EdgeInsets.only(left: 5.0.w, right: 5.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  circularImageButton(group.groupProfilePic,
                      size: 60.0.w, radius: 30.0.w),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  Flexible(
                    child: AutoSizeText(
                      '${group.groupName}',
                      maxLines: 2,
                      minFontSize: 12.0.sp,
                      stepGranularity: 12.0.sp / 12,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  Text(
                    '${NumberFormat.compact().format(group.membersCount)} members',
                    style: TextStyle(
                        color: MemecistColors.greyText, fontSize: 13.0.sp),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  StreamBuilder(
                      stream: _groupSuggestionBloc.member,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data) {
                            return greyedOutButton(context, "Joined", () => leaveGroup(),
                                width: 100.0, height: 35.0);
                          } else {
                            return actionButton(
                                context, "Join", () => joinGroup(),
                                width: 100.0, height: 35.0);
                          }
                        } else {
                          return actionButton(
                              context, "Join", () => joinGroup(),
                              width: 100.0, height: 35.0);
                        }
                      }),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void openGroupDetailsScreen() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return GroupDetailsBlocProvider(
            child: GroupDetailsScreen(group.groupID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.group_details_screen)));
  }

  void joinGroup() {
    _groupSuggestionBloc.joinGroup();
    _homeBloc.setInitialRunDone();
  }

  void leaveGroup() {
    _groupSuggestionBloc.leaveGroup();
  }
}
