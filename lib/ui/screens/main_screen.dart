import 'package:firebase_analytics/observer.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/meme_post/create_meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/templates/templates_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/core/blocs/main_bloc.dart';
import 'package:memecist/core/blocs/main_bloc_provider.dart';
import 'package:memecist/ui/screens/explore/explore_screen.dart';
import 'package:memecist/core/blocs/home/home_bloc_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/blocs/explore/explore_bloc_provider.dart';
import 'package:memecist/core/models/user_model.dart';

import 'package:memecist/ui/screens/home/home_screen.dart';
import 'package:memecist/ui/screens/meme_post/create_meme_post_screen.dart';
import 'package:memecist/ui/screens/templates/templates_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'meme_world/meme_world_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with RouteAware {
  MainBloc _mainBloc;
  List<Widget> bodyList = [];
  bool isFirstRun = true;
  FirebaseAnalyticsObserver observer;
  String screenName = "";
  DateTime currentBackPressTime;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  _MainScreenState() {
    bodyList.add(HomeBlocProvider(
      child: HomeScreen(),
    ));
    bodyList.add(ExploreBlocProvider(
      child: ExploreScreen(),
    ));
    bodyList.add(CreateMemePostBlocProvider(
      child: CreateMemePostScreen(),
    ));
    bodyList.add(TemplatesBlocProvider(child: TemplatesScreen()));
    bodyList.add(MemeWorldScreen());
  }

  final pageController = PageController();

  int currentIndex = 0;

  @override
  void didPush() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    _mainBloc.dispose();
    repository.cancelIOSSubscription();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    repository.initializeFirebaseMessaging(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: MemecistColors.backgroundColor,
        body: PageView(
          controller: pageController,
          children: bodyList,
          physics: NeverScrollableScrollPhysics(),
        ),
        bottomNavigationBar: StreamBuilder(
            stream: _mainBloc.currentIndex,
            builder: (context, snapshot) {
              return CurvedNavigationBar(
                items: <Widget>[
                  Icon(
                    Icons.home,
                    size: 30.0.w,
                  ),
                  Icon(
                    Icons.search,
                    size: 30.0.w,
                  ),
                  Icon(
                    Icons.add,
                    size: 30.0.w,
                  ),
                  Icon(
                    Icons.sentiment_very_satisfied,
                    size: 30.0.w,
                  ),
                  Icon(
                    Icons.local_play,
                    size: 30.0.w,
                  )
                ],
                animationDuration: Duration(milliseconds: 200),
                animationCurve: Curves.easeIn,
                backgroundColor: Colors.transparent,
                height: MediaQuery.of(context).size.height > 683
                    ? 55.0 + (55.0.h - 55.0) / 2
                    : 55.0.h,
                onTap: onTap,
                index: currentIndex,
              );
            }),
      ),
    );
  }

  void onTap(int index) {
    pageController.jumpToPage(index);
    switch (index) {
      case 0:
        screenName = FirebaseAnalyticsConstants.home_screen;
        break;
      case 1:
        screenName = FirebaseAnalyticsConstants.explore_screen;
        break;
      case 2:
        screenName = FirebaseAnalyticsConstants.create_meme_post_screen;
        break;
      case 3:
        screenName = FirebaseAnalyticsConstants.templates_screen;
        break;
      default:
        break;
    }

    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      scaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(milliseconds: 500),
          content: Text(
            "Press back again to exit",
            style: TextStyle(
              color: Colors.white,
              fontSize: 15.0.sp,
              fontFamily: 'Quicksand',
            ),
          )));
      return Future.value(false);
    }
    return Future.value(true);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _mainBloc = MainBlocProvider.of(context);
      observer = ObserverProvider.of(context);
      observer.subscribe(this, ModalRoute.of(context));

      _mainBloc.getUserIpDetails();
      isFirstRun = false;

      screenName = FirebaseAnalyticsConstants.home_screen;
      _sendCurrentTabToAnalytics();
    }
  }

  void _sendCurrentTabToAnalytics() {
    observer.analytics.setCurrentScreen(
      screenName: screenName,
    );
  }
}
