import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memecist/ui/constants/colors.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 30.0.h),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "Memes are ❤️",
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0.sp,
                    color: MemecistColors.greyText),
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Image.asset(
              'assets/mc.png',
              height: 70.0.w,
              width: 70.0.w,
            ),
          ),
        ],
      ),
    );
  }
}
