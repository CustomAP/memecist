import 'package:firebase_analytics/observer.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_detail_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:memecist/ui/screens/main_screen.dart';
import 'package:memecist/ui/screens/login/login_screen.dart';
import 'package:memecist/core/blocs/main_bloc_provider.dart';
import 'package:memecist/core/blocs/login/login_bloc_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/core/blocs/login/initialize_profile_info_bloc_provider.dart';
import 'package:memecist/ui/screens/login/intialize_profile_info_screen.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_detail_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/screens/splash_screen.dart';

class Wrapper extends StatefulWidget {
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> with RouteAware {
  int streamCounter = 0;
  FirebaseAnalyticsObserver observer;
  String screenName = "";

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    observer = ObserverProvider.of(context);
    observer.subscribe(this, ModalRoute.of(context));
  }

  @override
  void didPush() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        width: StringConstants.referenceWidth,
        height: StringConstants.referenceHeight,
        allowFontScaling: false);
    fetchLinkData(context);
    return StreamBuilder(
      stream: repository.onLoggedInUserAuthChange(),
      builder: (context, AsyncSnapshot<User> asyncSnapshot) {
        if (asyncSnapshot.hasData) {
          if (asyncSnapshot.data != null) {
            return StreamBuilder(
                stream: repository.getUserStream(asyncSnapshot.data.uid),
                builder: (context, AsyncSnapshot<MemecistUser> userSnapshot) {
                  if (userSnapshot.hasData) {
                    if (userSnapshot.data.userName !=
                        userSnapshot.data.userID) {
                      return MainBlocProvider(
                        child: MainScreen(),
                      );
                    } else {
                      screenName = FirebaseAnalyticsConstants
                          .initialize_profile_info_screen;
                      _sendCurrentTabToAnalytics();
                      return InitializeProfileInfoBlocProvider(
                        child: InitializeProfileInfoScreen(),
                      );
                    }
                  } else {
                    return SplashScreen();
                  }
                });
          } else {
            screenName = FirebaseAnalyticsConstants.login_screen;
            _sendCurrentTabToAnalytics();
            return LoginBlocProvider(child: LoginScreen());
          }
        } else {
          if (streamCounter == 0) {
            // Enters this only for first time.
            streamCounter++;
            return SplashScreen();
          }
          screenName = FirebaseAnalyticsConstants.login_screen;
          _sendCurrentTabToAnalytics();
          return LoginBlocProvider(child: LoginScreen());
        }
      },
    );
  }

  void fetchLinkData(BuildContext context) async {
    var link = await FirebaseDynamicLinks.instance.getInitialLink();

    await handleLinkData(link, context);

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      await handleLinkData(dynamicLink, context);
    });
  }

  handleLinkData(PendingDynamicLinkData data, BuildContext context) async {
    final Uri uri = data?.link;
    if (uri != null) {
      LoggerUtil.log("Wrapper", "Handle Link Data", "link : " + uri.toString());
      final queryParams = uri.queryParameters;

      if (uri.toString().contains("posts")) {
        if (queryParams.length > 0) {
          String postID = queryParams["postID"];
          LoggerUtil.log("Wrapper", "Handle Link Data", "postID : " + postID);
          User currentUser = await repository.getLoggedInUser();
          if (currentUser != null) {
//            MemePost memePost =
//                await repository.getMemePost(postID, currentUser.uid);
            LoggerUtil.log(
                "Wrapper", "Handle Link Data", "UserID : " + currentUser.uid);
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return MemePostDetailBlocProvider(
                  child: MemePostDetailScreen(
                true,
                postID: postID,
              ));
            }));
          } else {
            LoggerUtil.log("Wrapper", "Handle Link Data", "UserID is null");
            //TODO handle this and supply params at the end.
          }
        }
      } else if (uri.toString().contains("groups")) {
        if (queryParams.length > 0) {
          String groupID = queryParams["groupID"];
          LoggerUtil.log("Wrapper", "Handle Link Data", "groupID : " + groupID);
          User currentUser = await repository.getLoggedInUser();
          if (currentUser != null) {
            LoggerUtil.log(
                "Wrapper", "Handle Link Data", "UserID : " + currentUser.uid);
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return GroupDetailsBlocProvider(
                  child: GroupDetailsScreen(groupID));
            }));
          } else {
            LoggerUtil.log("Wrapper", "Handle Link Data", "UserID is null");
            //TODO handle this and supply params at the end.
          }
        }
      }
    }
  }

  void _sendCurrentTabToAnalytics() {
    observer.analytics.setCurrentScreen(
      screenName: screenName,
    );
  }
}
