import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/edit_profile_info_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_verification_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/view_followers_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/badge_category.dart';
import 'package:memecist/core/models/badge_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/trouble_shoot_info_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc.dart';
import 'package:memecist/ui/screens/profile/edit_profile_info_screen.dart';
import 'package:memecist/ui/screens/profile/profile_verification_screen.dart';
import 'package:memecist/ui/screens/profile/view_followers_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/ui/widgets/feature_request_dialog.dart';
import 'package:memecist/ui/widgets/rate_app_dialog.dart';
import 'package:memecist/ui/widgets/suggestions_dialog.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfileScreen extends StatefulWidget {
  final String _userID;

  ProfileScreen(this._userID);

  @override
  _ProfileScreenState createState() => _ProfileScreenState(this._userID);
}

class _ProfileScreenState extends State<ProfileScreen> {
  String _userID;
  bool isOwnProfile = false;
  ProfileBloc _profileScreenBloc;

  TextEditingController _bioController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreMemePosts = true;
  bool _isLoading = false;

  MemecistUser user;
  String _model;

  _ProfileScreenState(String userID) {
    this._userID = userID;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _profileScreenBloc = ProfileBlocProvider.of(context);

    if (isFirstRun) {
      //kept here to reflect changes when profile edited
      _profileScreenBloc.initializeProfileScreenBloc(_userID);
      _profileScreenBloc.getUser();
      _profileScreenBloc.isFollowing();
      _profileScreenBloc.determineIfOwnProfile();
      _profileScreenBloc.fetchUserMemes();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = MediaQuery
            .of(context)
            .size
            .height * 0.20;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreMemePosts &&
            !_isLoading) {
          _profileScreenBloc.fetchUserMemes();
        }
      });

      _profileScreenBloc.hasMoreMemes.listen((hasMoreData) {
        _hasMoreMemePosts = hasMoreData;
      });

      _profileScreenBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      _profileScreenBloc.model.listen((model) {
        _model = model;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _profileScreenBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _profileScreenBloc.isOwnProfile,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            isOwnProfile = snapshot.data;
            return Scaffold(
              endDrawer: settingsDrawer(),
              backgroundColor: MemecistColors.backgroundColor,
              appBar: isOwnProfile
                  ? appBarForOwnProfile()
                  : appBarForOthersProfile(),
              body: StreamBuilder(
                stream: _profileScreenBloc.user,
                builder: (context, AsyncSnapshot<MemecistUser> asyncSnapshot) {
                  if (asyncSnapshot.hasData) {
                    user = asyncSnapshot.data;
                    return profileView(asyncSnapshot.data);
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  Widget profileView(MemecistUser user) {
    return ListView(
      controller: _scrollController,
      children: <Widget>[
        Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              color: Colors.white,
              alignment: Alignment.topCenter,
              child: Padding(
                  padding: EdgeInsets.only(left: 5.0.w, right: 5.0.w),
                  child: profileInfo(user)),
            ),
            Positioned(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              bottom: -40.0.h,
              child: Container(
                margin: EdgeInsets.fromLTRB(25.0.w, 0, 25.0.w, 0.0.h),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0.w),
                    boxShadow: [
                      BoxShadow(blurRadius: 0.5.w, color: Colors.grey)
                    ]),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(10.0.w, 15.0.h, 10.0.w, 15.0.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      countWidget("MEMES", user.memesCount.toString(), null),
                      countWidget(
                          "FOLLOWERS",
                          NumberFormat.compact().format(user.followersCount),
                              () => openViewFollowersScreen()),
                      countWidget("REP", user.rep.toStringAsPrecision(3), null)
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        Padding(
          padding: EdgeInsets.only(left: 10.0.w, top: 60.0.h),
          child: Text(
            "MEMES",
            style: TextStyle(
                fontFamily: "Quicksand",
                fontSize: 14.0.sp,
                fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          height: 10.0.h,
        ),
        StreamBuilder(
            stream: _profileScreenBloc.memesList,
            builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data.length == 0) {
                  return Center(
                    child: Text(
                      "NO MEMES POSTED YET!",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                }
                return userPostsList(asyncSnapshot.data);
              } else
                return Container();
            }),
        StreamBuilder(
            stream: _profileScreenBloc.isLoading,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING MEMES...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
      ],
    );
  }

  Widget userPostsList(List<MemePost> memesList) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: memesList.length,
        itemBuilder: (context, index) {
          return MemePostBlocProvider(
            child: MemePostWidget(
              memesList[index],
              key: ObjectKey(memesList[index].postID),
              onDeleteClicked: () => onDeletePost(memesList[index]),
            ),
          );
        });
  }

  Widget countWidget(String name, String count, Function() onTapFunction) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTapFunction,
        child: Column(
          children: <Widget>[
            Text(
              name,
              style: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'QuickSand',
                  fontSize: 14.0.sp),
            ),
            SizedBox(
              height: 5.0.h,
            ),
            Text(
              count,
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'QuickSand',
                  fontSize: 20.0.sp),
            )
          ],
        ),
      ),
    );
  }

  Widget profileInfo(MemecistUser user) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: 20.0.h,
        ),
        InkWell(
          onTap: isOwnProfile ? () => openEditProfileScreen() : null,
          child: Container(
            width: 100.0.w,
            height: 100.0.w,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(50.0.w)),
                image: DecorationImage(
                    image: CachedNetworkImageProvider(user.profilePic),
                    fit: BoxFit.cover)),
          ),
        ),
        SizedBox(
          height: 10.0.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: AutoSizeText(
                '${user.firstName} ${user.lastName}',
                maxLines: 1,
                textAlign: TextAlign.center,
                stepGranularity: 8.0.sp / 8,
                minFontSize: 8.0.sp,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    fontSize: 23.0.sp,
                    color: Colors.black),
              ),
            ),
            SizedBox(
              width: 5.0.w,
            ),
            user.isVerified ? verifiedBadge(size: 23.0.w) : Container()
          ],
        ),
        SizedBox(
          height: 5.0.h,
        ),
        Flexible(
          child: AutoSizeText(
            '@${user.userName}',
            textAlign: TextAlign.center,
            maxLines: 1,
            minFontSize: 12.0.sp,
            stepGranularity: 12.0.sp / 12,
            style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 15.0.sp,
                color: Colors.black),
          ),
        ),
        SizedBox(
          height: 20.0.h,
        ),
        AutoSizeText(
          user.bio,
          textAlign: TextAlign.center,
          maxLines: 5,
          minFontSize: 12.0.sp,
          stepGranularity: 12.0.sp / 12,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 14.0.sp,
              color: MemecistColors.greyText),
        ),
        SizedBox(
          height: 20.0.h,
        ),
        isOwnProfile
            ? actionButtonsForOwnProfile()
            : actionButtonsForOthersProfile(),
        badgeBar(user.badges),
        SizedBox(
          height: 20.0,
        ),
        SizedBox(
          //needed because positioned stack is -40.0.h
          height: 40.0.h,
        )
      ],
    );
  }

  Widget actionButtonsForOwnProfile() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        actionButton(context, "Edit Profile", () => openEditProfileScreen()),
        SizedBox(
          width: 10.0.w,
        ),
        actionButton(context, "Copy Link", () => copyLink()),
      ],
    );
  }

  Widget actionButtonsForOthersProfile() {
    return StreamBuilder(
        stream: _profileScreenBloc.follow,
        builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
          if (asyncSnapshot.hasData) {
            if (asyncSnapshot.data)
              return greyedOutButton(
                  context, "Following", () => unFollowUser());
            else
              return actionButton(context, "Follow", () => followUser());
          } else {
            return Container();
          }
        });
  }

  Widget appBarForOwnProfile() {
    return appBarWithBackAndActionButton(context, settingsDrawerHandle());
  }

  Widget settingsDrawerHandle() {
    return Builder(
      builder: (context) {
        return IconButton(
          icon: Icon(
            Icons.menu,
            color: Colors.black,
          ),
          onPressed: () => Scaffold.of(context).openEndDrawer(),
        );
      },
    );
  }

  Widget settingsDrawer() {
    return Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Text(""),
              decoration: BoxDecoration(
                  color: MemecistColors.primaryColor,
                  image: DecorationImage(
                      image: AssetImage('assets/drawer_background.jpg'),
                      fit: BoxFit.cover)),
            ),
            ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Update Bio',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Quicksand',
                        fontSize: 16.0.sp),
                  ),
                  SizedBox(
                    height: 5.0.h,
                  ),
                  Text(
                    '\tExpress yourself better',
                    style: TextStyle(
                        color: MemecistColors.greyText,
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp),
                  ),
                ],
              ),
              onTap: () => showUpdateBioDialog(),
            ),
            ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Rate App',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Quicksand',
                        fontSize: 16.0.sp),
                  ),
                  SizedBox(
                    height: 5.0.h,
                  ),
                  Text(
                    "\tRate us a 5-star! We feel loved❤️",
                    style: TextStyle(
                        color: MemecistColors.greyText,
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp),
                  ),
                ],
              ),
              onTap: () => showRateAppDialog(),
            ),
            ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Request A Feature',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Quicksand',
                        fontSize: 16.0.sp),
                  ),
                  SizedBox(
                    height: 5.0.h,
                  ),
                  Text(
                    "\tYeah, you heard it right!",
                    style: TextStyle(
                        color: MemecistColors.greyText,
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp),
                  ),
                ],
              ),
              onTap: () => showFeatureRequestDialog(),
            ),
            ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Suggestions?',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Quicksand',
                        fontSize: 16.0.sp),
                  ),
                  SizedBox(
                    height: 5.0.h,
                  ),
                  Text(
                    "\tWe're happy to hear",
                    style: TextStyle(
                        color: MemecistColors.greyText,
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp),
                  ),
                ],
              ),
              onTap: () => showSuggestionsDialog(),
            ),
            ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Get Verified',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Quicksand',
                        fontSize: 16.0.sp),
                  ),
                  SizedBox(
                    height: 5.0.h,
                  ),
                  Text(
                    "\tWant a blue tick?",
                    style: TextStyle(
                        color: MemecistColors.greyText,
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp),
                  ),
                ],
              ),
              onTap: () => openProfileVerificationScreen(),
            ),
            ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Terms and Privacy',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Quicksand',
                        fontSize: 16.0.sp),
                  ),
                  SizedBox(
                    height: 5.0.h,
                  ),
                  Text(
                    "\tSome legal stuff!",
                    style: TextStyle(
                        color: MemecistColors.greyText,
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp),
                  ),
                ],
              ),
              onTap: () => openPrivacyAndTerms(),
            ),
            StreamBuilder(
              stream: _profileScreenBloc.showTroubleShoot,
              builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data) {
                    return ListTile(
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'TroubleShoot',
                            style: TextStyle(
                                color: Colors.orange[700],
                                fontFamily: 'Quicksand',
                                fontSize: 16.0.sp),
                          ),
                          SizedBox(
                            height: 5.0.h,
                          ),
                          Text(
                            "\tNot receiving notification?",
                            style: TextStyle(
                                color: MemecistColors.greyText,
                                fontFamily: 'Quicksand',
                                fontSize: 14.0.sp),
                          ),
                        ],
                      ),
                      onTap: () {
                        showTroubleShootDialog();
                      },
                    );
                  } else {
                    return Container();
                  }
                } else {
                  return Container();
                }
              },
            ),
            ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Logout',
                    style: TextStyle(
                        color: Colors.red,
                        fontFamily: 'Quicksand',
                        fontSize: 16.0.sp),
                  ),
                  SizedBox(
                    height: 5.0.h,
                  ),
                  Text(
                    "\tGot another account?",
                    style: TextStyle(
                        color: MemecistColors.greyText,
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp),
                  ),
                ],
              ),
              onTap: () {
                logout();
              },
            )
          ],
        ));
  }

  Widget appBarForOthersProfile() {
    return AppBar(
      centerTitle: true,
      brightness:
      Platform.isAndroid ? AppBarTheme().brightness : Brightness.light,
      title: Image.asset(
        'assets/mc.png',
        height: 40.0.w,
        width: 40.0.w,
      ),
      backgroundColor: Colors.white,
      elevation: 0.0,
      leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () => Navigator.of(context).pop()),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.link,
            color: Colors.black,
          ),
          onPressed: () => copyLink(),
        )
      ],
    );
  }

  Widget badgeBar(List badges) {
    return InkWell(
      onTap: displayBadgeInfo,
      child: Padding(
        padding: EdgeInsets.only(top: 20.0.h),
        child: Container(
          alignment: Alignment.center,
          height: 30.0.h,
          child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: badges.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.only(left: 2.5.w, right: 2.5.w),
                  child: CachedNetworkImage(
                    imageUrl: badges[index].toString(),
                  ),
                );
              }),
        ),
      ),
    );
  }

  Widget logoutButton() {
    return FlatButton(
      child: Text(
        "LOGOUT",
        style: TextStyle(color: Colors.red, fontSize: 14.0.sp),
      ),
      onPressed: () => processLogout(),
    );
  }

  Widget cancelButton() {
    return FlatButton(
      child: Text(
        "CANCEL",
        style: TextStyle(fontSize: 14.0.sp),
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

  Widget badgeCategoryItem(BadgeCategory badgeCategory) {
    return ListView(
      primary: false,
      shrinkWrap: true,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 10.0.w),
          child: Text(
            badgeCategory.name,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15.0.sp,
                fontFamily: 'Quicksand'),
          ),
        ),
        allBadgesList(badgeCategory.badges)
      ],
    );
  }

  Widget allBadgesList(List<Badge> badges) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: badges.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.all(10.0.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 25.0.h,
                    child: CachedNetworkImage(
                      imageUrl: badges[index].image,
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    badges[index].name,
                    style:
                    TextStyle(fontSize: 14.0.sp, fontFamily: 'Quicksand'),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      badges[index].level.toString(),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0.sp,
                          fontFamily: 'Quicksand'),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget badgeDialog() {
    return Dialog(
      key: Key('badgeDialog'),
      elevation: 8.0.w,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0.w)),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 10.0.w,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Earned Badges",
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    color: MemecistColors.primaryColor,
                    fontSize: 16.0.sp,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Text(
                      "Badge",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      "Title",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Level",
                        style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 16.0.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            ),
            StreamBuilder(
                stream: _profileScreenBloc.badgesList,
                builder: (context, AsyncSnapshot<List<Badge>> asyncSnapshot) {
                  if (asyncSnapshot.hasData) {
                    return allBadgesList(asyncSnapshot.data);
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
            greyedOutButton(context, "View Badges Board", () {
              Navigator.of(context).pop();
              return showDialog(
                  context: context,
                  builder: (BuildContext context) => badgeBoardDialog(),
                  barrierDismissible: true);
            }, width: 200.0),
            SizedBox(
              height: 20.0.h,
            )
          ],
        ),
      ),
    );
  }

  Widget badgeBoardDialog() {
    return Dialog(
      key: Key('badgeBoardDialog'),
      elevation: 8.0.w,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0.w)),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(10.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Text(
                      "Badge",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      "Title",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Level",
                        style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 16.0.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            ),
            StreamBuilder(
                stream: _profileScreenBloc.badgeBoardList,
                builder: (context,
                    AsyncSnapshot<List<BadgeCategory>> asyncSnapshot) {
                  if (asyncSnapshot.hasData) {
                    return ListView.builder(
                        primary: false,
                        shrinkWrap: true,
                        itemCount: asyncSnapshot.data.length,
                        itemBuilder: (context, index) {
                          return badgeCategoryItem(asyncSnapshot.data[index]);
                        });
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }

  Widget updateBioDialog() {
    String title = "Update Bio";
    String btnLabel = "Update";
    String btnLabelCancel = "Cancel";
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        elevation: 8.0.w,
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0.w)),
        child: Padding(
          padding: EdgeInsets.all(10.0.w),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: MemecistColors.primaryColor,
                      fontSize: 17.0.sp),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                TextField(
                  keyboardType: TextInputType.multiline,
                  controller: _bioController,
                  inputFormatters: [LengthLimitingTextInputFormatter(150)],
                  maxLines: 4,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(fontSize: 16.0.sp),
                  decoration: InputDecoration(
                      hintText: "Bio",
                      contentPadding: EdgeInsets.only(
                          left: 10.0.w,
                          right: 10.0.w,
                          top: 10.0.w,
                          bottom: 10.0.w),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0.w),
                          borderSide: BorderSide(
                              color: MemecistColors.primaryColor,
                              width: 2.25.w)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0.w),
                          borderSide: BorderSide(color: Colors.white))),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    FlatButton(
                      child: Text(
                        btnLabelCancel,
                        style: TextStyle(fontSize: 16.0.sp),
                      ),
                      textColor: MemecistColors.primaryColor,
                      onPressed: () => dismissUpdateBioDialog(),
                    ),
                    FlatButton(
                        textColor: MemecistColors.primaryColor,
                        child: Text(
                          btnLabel,
                          style: TextStyle(fontSize: 16.0.sp),
                        ),
                        onPressed: () => updateBio()),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  dismissUpdateBioDialog() {
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.dismiss_update_bio_prompt);
    Navigator.of(context).pop();
  }

  updateBio() async {
    String newBio = _bioController.text;
    if (newBio.length > 0) {
      _profileScreenBloc.updateBio(newBio);
      Fluttertoast.showToast(msg: "Bio Updated");
    } else {
      Fluttertoast.showToast(msg: "Bio cannot be empty!");
    }
    Navigator.of(context).pop();
  }

  void displayBadgeInfo() {
    _profileScreenBloc.fetchBadgesList();
    showDialog(
        context: context,
        builder: (BuildContext context) => badgeDialog(),
        barrierDismissible: true);
  }

  void followUser() async {
    await _profileScreenBloc.followUser();
  }

  void unFollowUser() async {
    await _profileScreenBloc.unFollowUser();
  }

  void openViewFollowersScreen() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ViewFollowersBlocProvider(
            child: ViewFollowersScreen(_userID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.view_followers_screen)));
  }

  void logout() async {
    showDialog(
        context: context,
        builder: (_) =>
        new AlertDialog(
          title: new Text(
            "Logout",
            style: TextStyle(fontSize: 16.0.sp),
          ),
          content: new Text(
            "Are you sure?",
            style: TextStyle(fontSize: 14.0.sp),
          ),
          actions: <Widget>[
            logoutButton(),
            cancelButton(),
          ],
        ));
  }

  Future<void> showTroubleShootDialog() async {
    _profileScreenBloc.getTroubleShootInfo(_model);
    return await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
            elevation: 8.0.w,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0.w)),
            child: StreamBuilder(
              stream: _profileScreenBloc.troubleShootInfo,
              builder: (context, AsyncSnapshot<TroubleShootInfo> snapshot) {
                if (snapshot.hasData) {
                  return Padding(
                    padding: EdgeInsets.all(10.0.w),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          height: 10.0.h,
                        ),
                        Text(
                          "Troubleshoot Notifications",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Quicksand',
                              color: MemecistColors.primaryColor,
                              fontSize: 17.0.sp),
                        ),
                        SizedBox(
                          height: 10.0.h,
                        ),
                        Text(
                          snapshot.data.info,
                          style: TextStyle(
                              fontFamily: 'Quicksand', fontSize: 15.0.sp),
                        ),
                        SizedBox(
                          height: 10.0.h,
                        ),
                        dialogButton(
                            "MORE INFO", () => openLink(snapshot.data.link))
                      ],
                    ),
                  );
                } else {
                  return Container(
                    height: 70.0.w,
                    width: 70.0.w,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                    color: Colors.white.withOpacity(0.8),
                  );
                }
              },
            ));
      },
    );
  }

  Future<void> showRateAppDialog() async {
    return await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return RateAppDialog();
      },
    );
  }

  Future<void> showSuggestionsDialog() async {
    return await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SuggestionsDialog();
      },
    );
  }

  Future<void> showFeatureRequestDialog() async {
    return await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return FeatureRequestDialog();
      },
    );
  }

  Future<void> openLink(url) async {
    if (await (canLaunch(url))) {
      launch(url);
    }
  }

  Future<void> processLogout() async {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          title: Text(
            'Logging Out',
            style: TextStyle(fontSize: 16.0.sp),
          ),
          content: Text(
            "Please wait...",
            style: TextStyle(fontSize: 14.0.sp),
          ),
        );
      },
    );
    await _profileScreenBloc.logout();
    Navigator.of(context).popUntil((route) => route.isFirst);
  }

  Future<void> showUpdateBioDialog() async {
    _bioController.text = user.bio;
    return await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return updateBioDialog();
      },
    );
  }

  void openProfileVerificationScreen() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileVerificationBlocProvider(
            child: ProfileVerificationScreen(),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.profile_verification_screen)));
  }

  void onDeletePost(MemePost memePost) async {
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.delete_post,
        parameters: {
          FirebaseAnalyticsConstants.source:
          FirebaseAnalyticsConstants.profile_screen
        });
    _profileScreenBloc.deletePost(memePost);
  }

  void openEditProfileScreen() {
    Navigator.of(context)
        .push(MaterialPageRoute(
        builder: (context) {
          return EditProfileInfoBlocProvider(
            child: EditProfileInfoScreen(user),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.edit_profile_info_screen)))
        .then((value) =>
      //Takes time to update, hence the delay
        Future.delayed(Duration(milliseconds: 500), () {
          _profileScreenBloc.getUser();
        }));
  }

  void copyLink() {
    _profileScreenBloc.copyLink();
  }

  void openPrivacyAndTerms() async {
    const url = StringConstants.privacyAndTermsUrl;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
