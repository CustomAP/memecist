import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/blocs/profile/profile_verification_bloc.dart';
import 'package:memecist/core/blocs/profile/profile_verification_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:memecist/core/blocs/profile/profile_verification_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfileVerificationScreen extends StatefulWidget {
  @override
  _ProfileVerificationScreenState createState() =>
      _ProfileVerificationScreenState();
}

class _ProfileVerificationScreenState extends State<ProfileVerificationScreen> {
  ProfileVerificationBloc _profileVerificationBloc;

  bool isFirstRun = true;

  GlobalKey _globalKey = GlobalKey();

  TextEditingController _userNameCardLink1Controller = TextEditingController();
  TextEditingController _userNameCardLink2Controller = TextEditingController();

  TextEditingController _bioLink1Controller = TextEditingController();
  TextEditingController _bioLink2Controller = TextEditingController();

  TextEditingController _postLink1Controller = TextEditingController();
  TextEditingController _postLink2Controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWithBackButton(context),
      backgroundColor: MemecistColors.backgroundColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10.0.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 20.0.h,
              ),
              Text(
                "Are you famous on Facebook or Instagram?",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "OR",
                style: TextStyle(
                    fontSize: 14.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.greyText),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "Do you post the dopest ORIGINAL memes on Facebook or Instagram?",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "Ofcourse, you can apply for a verified badge then!",
                style: TextStyle(
                    fontSize: 14.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.primaryColor),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20.0.h,
              ),
              Text(
                "METHOD 1",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "Share this card on your instagram/facebook page and copy paste the post link to verify you own the page.",
                style: TextStyle(
                    fontSize: 14.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.greyText),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              shareUserNameCardWidget(),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0.w)),
                child: memecistInputField(
                    "Shared card link 1", _userNameCardLink1Controller, false),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0.w)),
                child: memecistInputField(
                    "Shared card link 2", _userNameCardLink2Controller, false),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              UnconstrainedBox(
                  child:
                      actionButton(context, "SUBMIT LINKS", () => submitLinksForCard())),
              SizedBox(
                height: 20.0.h,
              ),
              Text(
                "METHOD 2",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "Add memecist id on your instagram/facebook bio and copy paste your profile link here!",
                style: TextStyle(
                    fontSize: 14.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.greyText),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0.w)),
                child: memecistInputField(
                    "Profile link 1", _bioLink1Controller, false),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0.w)),
                child: memecistInputField(
                    "Profile link 2", _bioLink2Controller, false),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              UnconstrainedBox(
                  child:
                      actionButton(context, "SUBMIT LINKS", () => submitLinksForBio())),
              SizedBox(
                height: 20.0.h,
              ),
              Text(
                "METHOD 3",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "After posting meme on memecist, share the meme with the complete memecist frame with your id visible.\nThen copy paste the link here",
                style: TextStyle(
                    fontSize: 14.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.greyText),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0.w)),
                child: memecistInputField(
                    "Post link 1", _postLink1Controller, false),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0.w)),
                child: memecistInputField(
                    "Post link 2", _postLink2Controller, false),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              UnconstrainedBox(
                  child:
                      actionButton(context, "SUBMIT LINKS", () => submitLinksForPost())),
              SizedBox(
                height: 10.0.h,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget shareUserNameCardWidget() {
    return StreamBuilder(
        stream: _profileVerificationBloc.user,
        builder: (context, AsyncSnapshot<MemecistUser> asyncSnapshot) {
          if (asyncSnapshot.hasData) {
            MemecistUser user = asyncSnapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 10.0.h, bottom: 10.0.h),
                  child: RepaintBoundary(
                    key: _globalKey,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0.w)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                blurRadius: 3.0.w,
                                spreadRadius: 3.0.w)
                          ]),
                      child: Padding(
                        padding: EdgeInsets.all(30.0.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            circularImageButton(user.profilePic,
                                size: 80.0.w, radius: 40.0.w),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Flexible(
                              child: AutoSizeText(
                                "${user.firstName} ${user.lastName}",
                                maxLines: 2,
                                minFontSize: 12.0.sp,
                                stepGranularity: 12.0.sp/12,
                                style: TextStyle(
                                    fontSize: 30.0.sp,
                                    fontFamily: 'Quicksand',
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Text(
                              "is now on Memecist",
                              style: TextStyle(
                                fontSize: 25.0.sp,
                                fontFamily: 'Quicksand',
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Text(
                              "Follow @${user.userName} for the best memes",
                              style: TextStyle(
                                fontSize: 20.0.sp,
                                fontFamily: 'Quicksand',
                                color: MemecistColors.greyText,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => shareUserName(),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                "SHARE",
                                style: TextStyle(
                                    fontFamily: "Quicksand",
                                    fontSize: 14.0.sp,
                                    color: MemecistColors.primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 5.0.w,
                              ),
                              CachedNetworkImage(
                                height: 20.0.w,
                                width: 20.0.w,
                                imageUrl:
                                    FirebaseStorageConstants.instagramLogo,
                              ),
                              SizedBox(
                                width: 5.0.w,
                              ),
                              CachedNetworkImage(
                                height: 20.0.w,
                                width: 20.0.w,
                                imageUrl: FirebaseStorageConstants.facebookLogo,
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            );
          } else {
            return Container();
          }
        });
  }

  void shareUserName() {
    _profileVerificationBloc.shareUserNameCard(_globalKey);
  }

  void submitLinksForCard() {
    _profileVerificationBloc.submitLinks(
        _userNameCardLink1Controller.text,
        _userNameCardLink2Controller.text,
        FirestoreConstants.user_name_card_share);
    Fluttertoast.showToast(
        msg: "Links submitted.\nPlease give us 1-2 weeks for verification");
  }

  void submitLinksForBio() {
    _profileVerificationBloc.submitLinks(_bioLink1Controller.text,
        _bioLink2Controller.text, FirestoreConstants.bio_share);
    Fluttertoast.showToast(
        msg: "Links submitted.\nPlease give us 1-2 weeks for verification");
  }

  void submitLinksForPost() {
    _profileVerificationBloc.submitLinks(_postLink1Controller.text,
        _postLink2Controller.text, FirestoreConstants.post_share);
    Fluttertoast.showToast(
        msg: "Links submitted.\nPlease give us 1-2 weeks for verification");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _profileVerificationBloc = ProfileVerificationBlocProvider.of(context);

    if (isFirstRun) {
      _profileVerificationBloc.getUser();
      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _profileVerificationBloc.dispose();
    super.dispose();
  }
}
