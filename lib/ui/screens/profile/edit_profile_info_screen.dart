import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memecist/core/blocs/login/initialize_profile_info_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/edit_profile_info_bloc.dart';
import 'package:memecist/core/blocs/profile/edit_profile_info_bloc_provider.dart';
import 'package:memecist/core/utils/lower_case_formatter.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:memecist/core/blocs/login/initialize_profile_info_bloc.dart';
import 'package:intl/intl.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/utils/regex_validator.dart';
import 'package:memecist/core/models/user_private_data_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class EditProfileInfoScreen extends StatefulWidget {
  final MemecistUser _user;

  EditProfileInfoScreen(this._user);

  @override
  _EditProfileInfoScreenState createState() =>
      _EditProfileInfoScreenState(this._user);
}

class _EditProfileInfoScreenState extends State<EditProfileInfoScreen> {
  EditProfileInfoBloc _editProfileInfoBloc;

  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _userNameController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  FormFieldValidator nameFieldValidator;
  FormFieldValidator userNameFieldValidator;
  File imageFile;

  bool isFirstRun = true;

  MemecistUser user;

  _EditProfileInfoScreenState(this.user);

  @override
  Widget build(BuildContext context) {
    nameFieldValidator = (val) {
      return val.length == 0 ? "Value cannot be blank" : null;
    };

    userNameFieldValidator = (val) {
      return val.length < 4
          ? "Minimum length should be 4"
          : (!RegexValidator.isStringValidForUserName(val)
              ? "Only lowercase characters, numbers, . and _ allowed"
              : null);
    };

    return GestureDetector(
      onTap: () => removeFocus(),
      child: Scaffold(
        backgroundColor: MemecistColors.backgroundColor,
        appBar: appBarWithBackButton(context),
        body: Container(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 40.0.h,
                    ),
                    Text(
                      "Edit Profile!",
                      style: TextStyle(
                          fontSize: 35.0.sp,
                          fontFamily: 'Quicksand',
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    StreamBuilder(
                        stream: _editProfileInfoBloc.imageFile,
                        builder: (context, AsyncSnapshot<File> asyncSnapshot) {
                          if (asyncSnapshot.hasData) {
                            if (asyncSnapshot.data == null)
                              return chooseImagePlaceHolder();

                            imageFile = asyncSnapshot.data;

                            return imageAvatar();
                          } else {
                            return chooseImagePlaceHolder();
                          }
                        }),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                          "First Name", _firstNameController, false,
                          validator: nameFieldValidator,
                          inputFormatter: [
                            LengthLimitingTextInputFormatter(20),
                          ]),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                          "Last Name", _lastNameController, false,
                          validator: nameFieldValidator,
                          inputFormatter: [
                            LengthLimitingTextInputFormatter(20),
                          ]),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    StreamBuilder(
                        stream: _editProfileInfoBloc.isUserNameTaken,
                        builder: (context, AsyncSnapshot<bool> snapshot) {
                          if (snapshot.hasData) {
                            if (snapshot.data) {
                              return userNameField(
                                  errorText: "username already taken");
                            } else {
                              return userNameField();
                            }
                          } else {
                            return userNameField();
                          }
                        }),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    StreamBuilder(
                      stream: _editProfileInfoBloc.showLoading,
                      builder: (context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return submitButton();
                          }
                        } else {
                          return submitButton();
                        }
                      },
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Text(
                      "PS: You can edit Profile at most once a month",
                      style: TextStyle(
                          fontSize: 14.0.sp,
                          fontFamily: 'Quicksand',
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget userNameField({String errorText}) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10.0.w)),
      child: memecistInputField(
        "Pick a username",
        _userNameController,
        false,
        validator: userNameFieldValidator,
        errorText: errorText,
        inputFormatter: [
          LowerCaseFormatter(),
          TextInputFormatter.withFunction((oldValue, newValue) =>
              RegExp(r'^[a-z0-9._]+$').hasMatch(newValue.text)
                  ? newValue
                  : oldValue),
          LengthLimitingTextInputFormatter(20),
        ],
      ),
    );
  }

  Widget submitButton() {
    return actionButton(context, "SUBMIT", submit);
  }

  submit() {
    if (_formKey.currentState.validate()) {
      _editProfileInfoBloc.editProfileDetails(
          imageFile,
          _firstNameController.text,
          _lastNameController.text,
          _userNameController.text);
    }
  }

  Widget imageAvatar() {
    return Container(
      width: 100.0.w,
      height: 100.0.w,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.0.w),
          border: Border.all(color: Colors.grey),
          image:
              DecorationImage(image: FileImage(imageFile), fit: BoxFit.cover)),
    );
  }

  Widget chooseImagePlaceHolder() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: pickImage,
        child:
            circularImageButton(user.profilePic, size: 100.0.w, radius: 50.0.w),
      ),
    );
  }

  pickImage() async {
    try {
      var selectedImage =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      _editProfileInfoBloc.setImageFile(selectedImage);
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _editProfileInfoBloc = EditProfileInfoBlocProvider.of(context);

      _editProfileInfoBloc.initializeEditProfileInfoBloc(user);
      _firstNameController.text = user.firstName;
      _lastNameController.text = user.lastName;
      _userNameController.text = user.userName;

      _editProfileInfoBloc.isProfileInfoEdited.listen((result) {
        if (!result) {
          Fluttertoast.showToast(msg: "Some error occured");
        } else {
          Fluttertoast.showToast(msg: "Profile edited successfully");
          Navigator.of(context).pop();
        }
      });

      _editProfileInfoBloc.canEditProfile.listen((canEditProfile) {
        if(!canEditProfile) {
          Fluttertoast.showToast(msg: "Sorry, you can edit profile only once a month! Try again later");
        }
       });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _editProfileInfoBloc.dispose();
    super.dispose();
  }
}
