import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/view_followers_bloc.dart';
import 'package:memecist/core/blocs/profile/view_followers_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViewFollowersScreen extends StatefulWidget {
  final String _userID;

  ViewFollowersScreen(this._userID);

  @override
  _ViewFollowersScreenState createState() =>
      _ViewFollowersScreenState(this._userID);
}

class _ViewFollowersScreenState extends State<ViewFollowersScreen> {
  String _userID;

  _ViewFollowersScreenState(this._userID);

  ViewFollowersBloc _viewFollowersBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreFollowers = true;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWithBackButtonAndTitle("Followers", context),
      body: ListView(
        controller: _scrollController,
        children: <Widget>[
          SizedBox(
            height: 20.0.h,
          ),
          StreamBuilder(
              stream: _viewFollowersBloc.usersList,
              builder: (context, AsyncSnapshot<List<MemecistUser>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO FOLLOWERS YET!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return usersList(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _viewFollowersBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING FOLLOWERS...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget usersList(List<MemecistUser> users) {
    return ListView.builder(
        itemCount: users.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () => openProfile(users[index].userID),
            child: Padding(
              padding:
                  EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 15.0.h),
              child: userRowItem(users[index], false),
            ),
          );
        });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _viewFollowersBloc = ViewFollowersBlocProvider.of(context);
      _viewFollowersBloc.initializeViewFollowersBloc(_userID);

      _viewFollowersBloc.fetchFollowers();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = MediaQuery.of(context).size.height * 0.20;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreFollowers &&
            !_isLoading) {
          _viewFollowersBloc.fetchFollowers();
        }
      });

      _viewFollowersBloc.hasMoreFollowers.listen((hasMoreData) {
        _hasMoreFollowers = hasMoreData;
      });

      _viewFollowersBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _viewFollowersBloc.dispose();
    super.dispose();
  }

  void openProfile(String userID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }
}
