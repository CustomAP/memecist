import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:memecist/core/blocs/explore/explore_bloc.dart';
import 'package:memecist/core/blocs/explore/explore_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ExploreTab extends StatefulWidget {
  final int tabNumber;
  final ScrollController scrollController;
  final AnimationController _hideFabAnimation;
  final Function() refreshScreen;

  ExploreTab(this.tabNumber, this.scrollController, this._hideFabAnimation,
      this.refreshScreen);

  @override
  _ExploreTabState createState() => _ExploreTabState(this.tabNumber,
      this.scrollController, this._hideFabAnimation, this.refreshScreen);
}

class _ExploreTabState extends State<ExploreTab>
    with AutomaticKeepAliveClientMixin {
  int tabNumber;
  ScrollController _scrollController;
  ExploreBloc _exploreBloc;
  AnimationController _hideFabAnimation;
  Function() refreshScreen;

  _ExploreTabState(this.tabNumber, this._scrollController,
      this._hideFabAnimation, this.refreshScreen);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return NotificationListener<ScrollNotification>(
      onNotification: _handleScrollNotification,
      child: LiquidPullToRefresh(
        showChildOpacityTransition: false,
        animSpeedFactor: 2.0,
        color: MemecistColors.refreshBackgroundColor,
        onRefresh: () => refreshScreen(),
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              StreamBuilder(
                stream: _exploreBloc.memeLists[tabNumber],
                builder:
                    (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
                  if (asyncSnapshot.hasData) {
                    return ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: asyncSnapshot.data.length,
                      itemBuilder: (context, index) {
                        return MemePostBlocProvider(
                          child: MemePostWidget(
                            asyncSnapshot.data[index],
                            key: ObjectKey(asyncSnapshot.data[index].postID),
                            onDeleteClicked: () => onDeletePost(
                                asyncSnapshot.data[index], tabNumber),
                          ),
                        );
                      },
                    );
                  } else {
                    return Container();
                  }
                },
              ),
              SizedBox(
                height: 10.0.h,
              ),
              StreamBuilder(
                  stream: _exploreBloc.isLoadingMemes[tabNumber],
                  builder: (context, AsyncSnapshot<bool> snapshot) {
                    if (snapshot.hasData && snapshot.data) {
                      return Center(
                        child: Text(
                          "LOADING MEMES...",
                          style: TextStyle(
                              fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                        ),
                      );
                    } else {
                      return Container();
                    }
                  }),
              SizedBox(
                height: 10.0.h,
              )
            ],
          ),
        ),
      ),
    );
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    if (notification.depth == 0) {
      if (notification is UserScrollNotification) {
        final UserScrollNotification userScroll = notification;
        switch (userScroll.direction) {
          case ScrollDirection.forward:
            if (userScroll.metrics.maxScrollExtent !=
                userScroll.metrics.minScrollExtent) {
              _hideFabAnimation.forward();
            }
            break;
          case ScrollDirection.reverse:
            if (userScroll.metrics.maxScrollExtent !=
                userScroll.metrics.minScrollExtent) {
              _hideFabAnimation.reverse();
            }
            break;
          case ScrollDirection.idle:
            break;
        }
      }
    }
    return false;
  }

  void onDeletePost(MemePost memePost, int tabIndex) async {
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.delete_post,
        parameters: {
          FirebaseAnalyticsConstants.source:
              FirebaseAnalyticsConstants.explore_screen
        });
    _exploreBloc.deletePost(memePost, tabIndex);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _exploreBloc = ExploreBlocProvider.of(context);
  }

  @override
  bool get wantKeepAlive => true;
}
