import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/explore/search_bloc.dart';
import 'package:memecist/core/blocs/explore/search_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with SingleTickerProviderStateMixin {
  ScrollController _memesScrollController = ScrollController();
  TextEditingController _searchController = TextEditingController();
  TabController _tabController;

  String _searchQuery = "";

  bool _hasMoreMemes = true;
  bool _isLoadingMemes = false;

  SearchBloc _searchBloc;
  int selectedIndex = 0;

  bool isFirstRun = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: Container(
                child: searchTabs(),
              ),
            ),
            appBarRow(),
          ],
        ),
      ),
    );
  }

  Widget searchTabs() {
    List<Widget> tabIndicators = [
      Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.w),
            border: Border.all(color: Colors.grey[600])),
        child: Padding(
          padding: EdgeInsets.only(
              left: 5.0.w, right: 5.0.w, top: 7.0.h, bottom: 7.0.h),
          child: Text(
            "Memes",
            style: TextStyle(fontSize: 15.0.sp),
          ),
        ),
      ),
      Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: Colors.grey[600])),
        child: Padding(
          padding: EdgeInsets.only(
              left: 5.0.w, right: 5.0.w, top: 7.0.h, bottom: 7.0.h),
          child: Text(
            "Groups",
            style: TextStyle(fontSize: 15.0.sp),
          ),
        ),
      ),
      Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: Colors.grey[600])),
        child: Padding(
          padding: EdgeInsets.only(
              left: 5.0.w, right: 5.0.w, top: 7.0.h, bottom: 7.0.h),
          child: Text(
            "Users",
            style: TextStyle(fontSize: 15.0.sp),
          ),
        ),
      ),
    ];

    return Padding(
      padding: EdgeInsets.only(top: 65.0.h),
      child: Container(
        height: double.maxFinite,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TabBar(
                isScrollable: true,
                controller: _tabController,
                unselectedLabelColor: Colors.grey[600],
                indicatorSize: TabBarIndicatorSize.label,
                labelPadding: EdgeInsets.only(left: 5.0.w, right: 5.0.w),
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.w),
                    color: Colors.grey[600]),
                tabs: tabIndicators),
            SizedBox(
              height: 10.0.h,
            ),
            Flexible(
              fit: FlexFit.loose,
              child: TabBarView(
                controller: _tabController,
                children: [
                  memesList(),
                  groupsList(),
                  usersList(),
                ],
                physics: NeverScrollableScrollPhysics(),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget memesList() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          StreamBuilder(
            stream: _searchBloc.memesList,
            builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
              if (asyncSnapshot.hasData &&
                  asyncSnapshot.data != null &&
                  asyncSnapshot.data.length != 0) {
                return ListView.builder(
                  shrinkWrap: true,
                  controller: _memesScrollController,
                  itemCount: asyncSnapshot.data.length,
                  itemBuilder: (context, index) {
                    return MemePostBlocProvider(
                      child: MemePostWidget(
                        asyncSnapshot.data[index],
                        key: ObjectKey(asyncSnapshot.data[index].postID),
                        onDeleteClicked: () =>
                            onDeletePost(asyncSnapshot.data[index]),
                      ),
                    );
                  },
                );
              } else if (asyncSnapshot.data == null) {
                return StreamBuilder(
                    stream: _searchBloc.isLoadingMemes,
                    builder: (context, AsyncSnapshot<bool> snapshot) {
                      if (snapshot.hasData) {
                        if (snapshot.data) {
                          return Container();
                        } else {
                          return Align(
                            alignment: Alignment.center,
                            child: actionButton(context, 
                              "SEARCH",
                              () => _searchBloc.searchMemes(
                                  _searchController.text, true),
                            ),
                          );
                        }
                      } else {
                        return Align(
                          alignment: Alignment.center,
                          child: actionButton(context, 
                            "SEARCH",
                            () => _searchBloc.searchMemes(
                                _searchController.text, true),
                          ),
                        );
                      }
                    });
              } else {
                return Align(
                  alignment: Alignment.center,
                  child: Text(
                    "NO RESULTS FOUND!",
                    style: TextStyle(
                        fontSize: 16.0.sp,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Quicksand',
                        color: Colors.black),
                  ),
                );
              }
            },
          ),
          SizedBox(
            height: 10.0.h,
          ),
          StreamBuilder(
              stream: _searchBloc.isLoadingMemes,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "SEARCHING MEMES...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
          SizedBox(
            height: 10.0.h,
          )
        ],
      ),
    );
  }

  Widget groupsList() {
    return StreamBuilder(
        stream: _searchBloc.groupsList,
        builder: (context, AsyncSnapshot<List<Group>> snapshot) {
          if (snapshot.hasData &&
              snapshot.data != null &&
              snapshot.data.length != 0) {
            return ListView.builder(
                shrinkWrap: true,
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return groupItem(snapshot.data[index]);
                });
          } else if (snapshot.data != null && snapshot.data.length == 0) {
            return Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    "NO RESULTS FOUND!",
                    style: TextStyle(
                        fontSize: 16.0.sp,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Quicksand',
                        color: Colors.black),
                  ),
                ),
              ],
            );
          } else {
            return Container();
          }
        });
  }

  Widget groupItem(Group group) {
    return Padding(
      padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 5.0.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: () => openGroupDetails(group.groupID),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                circularImageButton(group.groupProfilePic),
                SizedBox(
                  width: 10.0.w,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "${group.groupName}",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "${group.membersCount} members",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp,
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget usersList() {
    return StreamBuilder(
        stream: _searchBloc.usersList,
        builder: (context, AsyncSnapshot<List<MemecistUser>> snapshot) {
          if (snapshot.hasData &&
              snapshot.data != null &&
              snapshot.data.length != 0) {
            return ListView.builder(
                shrinkWrap: true,
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return userItem(snapshot.data[index]);
                });
          } else if (snapshot.data != null && snapshot.data.length == 0) {
            return Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    "NO RESULTS FOUND!",
                    style: TextStyle(
                        fontSize: 16.0.sp,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Quicksand',
                        color: Colors.black),
                  ),
                ),
              ],
            );
          } else {
            return Container();
          }
        });
  }

  Widget userItem(MemecistUser user) {
    return Padding(
      padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 5.0.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: () => openProfile(user.userID),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                circularImageButton(user.profilePic),
                SizedBox(
                  width: 10.0.w,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "${user.firstName} ${user.lastName}",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "@${user.userName}",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 14.0.sp,
                      ),
                    )
                  ],
                ),
                user.isVerified
                    ? Container(child: verifiedBadge())
                    : Container()
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget appBarRow() {
    return Container(
      height: 55.0.h,
      color: Colors.white,
      padding: EdgeInsets.only(
          left: 10.0.w, right: 10.0.w, top: 5.0.h, bottom: 5.0.h),
      child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: Container(
                height: 35.0.h,
                decoration: BoxDecoration(
                    color: MemecistColors.backgroundColor,
                    borderRadius: BorderRadius.all(Radius.circular(30.0.w))),
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
                  child: TextFormField(
                    autofocus: true,
                    controller: _searchController,
                    textInputAction: TextInputAction.done,
                    onFieldSubmitted: (text) =>
                        _searchBloc.searchMemes(_searchQuery, true),
                    style: TextStyle(
                        fontSize: 16.0.sp
                    ),
                    decoration: InputDecoration(
                        hintText: "Search",
                        hintStyle: TextStyle(fontSize: 14.0.sp),
                        border: InputBorder.none),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10.0.w,
            ),
            Icon(
              Icons.search,
              size: 30.0.w,
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }

  void openGroupDetails(String groupID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return GroupDetailsBlocProvider(
            child: GroupDetailsScreen(groupID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.group_details_screen)));
  }

  void openProfile(String userID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }

  void onDeletePost(MemePost memePost) async {
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.delete_post,
        parameters: {
          FirebaseAnalyticsConstants.source:
              FirebaseAnalyticsConstants.search_screen
        });
    _searchBloc.deletePost(memePost);
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _searchBloc = SearchBlocProvider.of(context);

      _searchController.addListener(() {
        bool isNewQuery = false;

        if (_searchQuery != _searchController.text) isNewQuery = true;

        _searchQuery = _searchController.text;

        if (isNewQuery)
          switch (selectedIndex) {
            case 0:
              _searchBloc.setMemesList(null);
              break;
            case 1:
              _searchBloc.searchGroups(_searchQuery);
              break;
            case 2:
              _searchBloc.searchUsers(_searchQuery);
              break;
          }
      });

      _memesScrollController.addListener(() {
        double maxScroll = _memesScrollController.position.maxScrollExtent;
        double currentScroll = _memesScrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreMemes &&
            !_isLoadingMemes) {
          _searchBloc.searchMemes(_searchQuery, false);
        }
      });

      _searchBloc.hasMoreMemes.listen((hasMoreData) {
        _hasMoreMemes = hasMoreData;
      });

      _searchBloc.isLoadingMemes.listen((isLoading) {
        _isLoadingMemes = isLoading;
      });

      _tabController.addListener(() {
        if (selectedIndex != _tabController.index) {
          selectedIndex = _tabController.index;
          switch (selectedIndex) {
            case 0:
              _searchBloc.searchMemes(_searchQuery, true);
              break;
            case 1:
              _searchBloc.searchGroups(_searchQuery);
              break;
            case 2:
              _searchBloc.searchUsers(_searchQuery);
              break;
          }
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _searchController.dispose();
    _memesScrollController.dispose();
    super.dispose();
  }
}
