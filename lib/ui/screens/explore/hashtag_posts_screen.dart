import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/explore/hashtag_posts_bloc.dart';
import 'package:memecist/core/blocs/explore/hashtag_posts_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HashtagPostsScreen extends StatefulWidget {
  final String _hashtag;

  HashtagPostsScreen(this._hashtag);

  @override
  _HashtagPostsScreenState createState() => _HashtagPostsScreenState(_hashtag);
}

class _HashtagPostsScreenState extends State<HashtagPostsScreen> {
  final String _hashtag;

  _HashtagPostsScreenState(this._hashtag);

  HashtagPostsBloc _hashtagPostsBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreMemes = true;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          _hashtag,
          style: TextStyle(
              fontFamily: 'Quicksand', color: MemecistColors.primaryColor),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () => Navigator.of(context).pop(),
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
        ),
      ),
      body: ListView(
        controller: _scrollController,
        children: <Widget>[
          SizedBox(
            height: 20.0.h,
          ),
          StreamBuilder(
              stream: _hashtagPostsBloc.memesList,
              builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO MEMES YET!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return memesList(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _hashtagPostsBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING MEMES...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget memesList(List<MemePost> memePosts) {
    return ListView.builder(
        itemCount: memePosts.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return MemePostBlocProvider(
            child: MemePostWidget(
              memePosts[index],
              key: ObjectKey(memePosts[index].postID),
              onDeleteClicked: () => onDeletePost(memePosts[index]),
            ),
          );
        });
  }

  void onDeletePost(MemePost memePost) async {
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.delete_post,
        parameters: {
          FirebaseAnalyticsConstants.source:
              FirebaseAnalyticsConstants.hashtag_posts_screen
        });
    _hashtagPostsBloc.deletePost(memePost);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _hashtagPostsBloc = HashtagPostsBlocProvider.of(context);

    if (isFirstRun) {
      _hashtagPostsBloc.fetchMemes(_hashtag);

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreMemes &&
            !_isLoading) {
          _hashtagPostsBloc.fetchMemes(_hashtag);
        }
      });

      _hashtagPostsBloc.hasMoreMemes.listen((hasMoreData) {
        _hasMoreMemes = hasMoreData;
      });

      _hashtagPostsBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _hashtagPostsBloc.dispose();
    super.dispose();
  }
}
