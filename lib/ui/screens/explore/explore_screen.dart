import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/explore/search_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/explore/explore_tab.dart';
import 'package:memecist/ui/screens/explore/search_screen.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/core/blocs/explore/explore_bloc_provider.dart';
import 'package:memecist/core/blocs/explore/explore_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ExploreScreen extends StatefulWidget {
  @override
  _ExploreScreenState createState() => _ExploreScreenState();
}

class _ExploreScreenState extends State<ExploreScreen>
    with
        AutomaticKeepAliveClientMixin,
        SingleTickerProviderStateMixin,
        TickerProviderStateMixin {
  ExploreBloc _exploreBloc;
  bool isFirstRun = true;
  List<ScrollController> _scrollController = [];

  List<String> categories = [];

  List<bool> _hasMoreMemes = [];
  List<bool> _isLoadingMemes = [];

  TabController _tabController;
  int selectedIndex = 0;

  AnimationController _hideFabAnimation;

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _exploreBloc = ExploreBlocProvider.of(context);

    if (isFirstRun) {
      _exploreBloc.initializeExploreBloc(categories);
      _exploreBloc.getFeed(0);

      for (int i = 0; i < categories.length; i++) {
        _hasMoreMemes.add(true);
        _isLoadingMemes.add(false);
        _scrollController.add(ScrollController());
      }

      for (int i = 0; i < categories.length; i++) {
        _scrollController[i].addListener(() {
          double maxScroll = _scrollController[i].position.maxScrollExtent;
          double currentScroll = _scrollController[i].position.pixels;
          double delta =
              (maxScroll - MediaQuery.of(context).size.height) * 0.40;
          if (maxScroll - currentScroll <= delta &&
              _hasMoreMemes[i] &&
              !_isLoadingMemes[i]) {
            _exploreBloc.getFeed(i);
          }
        });

        _exploreBloc.isLoadingMemes[i].listen((isLoading) {
          _isLoadingMemes[i] = isLoading;
        });

        _exploreBloc.hasMorePosts[i].listen((hasMore) {
          if (!hasMore) _hasMoreMemes[i] = hasMore;
        });
      }

      isFirstRun = false;
    }
  }

  @override
  void initState() {
    super.initState();
    categories = StringConstants.categoriesForExplore;
    categories.insert(0, "Trending");
    _tabController = TabController(length: categories.length, vsync: this);

    _hideFabAnimation =
        AnimationController(vsync: this, duration: kThemeAnimationDuration);

    _tabController.addListener(() {
      if (selectedIndex != _tabController.index) {
        selectedIndex = _tabController.index;
        if (_hasMoreMemes[selectedIndex] &&
            !_isLoadingMemes[selectedIndex] &&
            _exploreBloc.memePostsLists[selectedIndex].isEmpty) {
          _exploreBloc.getFeed(selectedIndex);
        }
      }
    });
  }

  @override
  void dispose() {
    _exploreBloc.dispose();
    _hideFabAnimation.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      floatingActionButton: ScaleTransition(
        scale: _hideFabAnimation,
        alignment: Alignment.bottomCenter,
        child: Container(
          height: 30.0.w,
          width: 30.0.w,
          child: FittedBox(
            child: FloatingActionButton(
              backgroundColor: Colors.white,
              heroTag: "heroTagExplore",
              elevation: 8.0.w,
              onPressed: () => _scrollController[selectedIndex].animateTo(0.0,
                  duration: Duration(milliseconds: 300), curve: Curves.easeOut),
              child: Icon(
                Icons.keyboard_arrow_up,
                color: Colors.black,
              ),
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(
                  height: AppBar().preferredSize.height,
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Expanded(child: exploreTabs()),
              ],
            ),
            getAppBarRow(),
          ],
        ),
      ),
    );
  }

  Widget exploreTabs() {
    List<Widget> tabsIndicators = [];
    List<Widget> tabs = [];

    for (int i = 0; i < categories.length; i++) {
      tabsIndicators.add(Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.w),
            border: Border.all(color: Colors.grey[600])),
        child: Padding(
          padding: EdgeInsets.only(
              left: 5.0.w, right: 5.0.w, top: 7.0.h, bottom: 7.0.h),
          child: Text(
            categories[i],
            style: TextStyle(fontSize: 15.0.sp),
          ),
        ),
      ));

      tabs.add(getExploreList(i));
    }

    return Container(
      height: double.maxFinite,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TabBar(
              isScrollable: true,
              controller: _tabController,
              unselectedLabelColor: Colors.grey[600],
              indicatorSize: TabBarIndicatorSize.label,
              labelPadding: EdgeInsets.only(left: 5.0.w, right: 5.0.w),
              indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.w),
                  color: Colors.grey[600]),
              tabs: tabsIndicators),
          SizedBox(
            height: 10.0.h,
          ),
          Flexible(
            fit: FlexFit.loose,
            child: TabBarView(
              controller: _tabController,
              children: tabs,
              physics: NeverScrollableScrollPhysics(),
            ),
          )
        ],
      ),
    );
  }

  Widget getExploreList(int i) {
    return ExploreTab(
        i, _scrollController[i], _hideFabAnimation, () => refreshScreen(i));
  }

  Widget getAppBarRow() {
    return Container(
      height: AppBar().preferredSize.height,
      color: Colors.white,
      padding: EdgeInsets.only(
          left: 10.0.w, right: 10.0.w, top: 5.0.h, bottom: 5.0.h),
      child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: Container(
                height: 35.0.h,
                decoration: BoxDecoration(
                    color: MemecistColors.backgroundColor,
                    borderRadius: BorderRadius.all(Radius.circular(30.0.w))),
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
                  child: InkWell(
                    onTap: openSearchScreen,
                    child: IgnorePointer(
                      child: TextField(
                        decoration: InputDecoration(
                            hintText: "Search memes, groups, users...",
                            hintStyle: TextStyle(fontSize: 14.0.sp),
                            border: InputBorder.none),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10.0.w,
            ),
            Icon(
              Icons.search,
              size: 30.0.w,
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> refreshScreen(int index) async {
    _hasMoreMemes[index] = true;
    _isLoadingMemes[index] = false;

    _exploreBloc.refreshScreen(index);
    _exploreBloc.getFeed(index);
  }

  @override
  bool get wantKeepAlive => true;

  void openSearchScreen() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return SearchBlocProvider(
                child: SearchScreen(),
              );
            },
            settings:
                RouteSettings(name: FirebaseAnalyticsConstants.search_screen)));
  }
}
