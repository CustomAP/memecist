import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc_provider.dart';
import 'package:memecist/core/blocs/notifications/notifications_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/blocs/widgets/follow_suggestion_bloc_provider.dart';
import 'package:memecist/core/blocs/widgets/group_suggestion_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/memecist_remote_config.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/ui/animations/particle.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/blocs/home/home_bloc.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/blocs/home/home_bloc_provider.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memecist/ui/widgets/follow_suggestion_widget.dart';
import 'package:memecist/ui/screens/meme_world/competitions/competition_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_details_screen.dart';
import 'package:memecist/ui/screens/notifications/notifications_screen.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:memecist/ui/widgets/group_suggestion_widget.dart';
import 'package:memecist/ui/widgets/rate_app_dialog.dart';
import 'package:memecist/ui/widgets/suggestions_dialog.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  HomeBloc _homeBloc;
  GlobalKey _globalKey = GlobalKey();

  bool isFirstRun = true;

  bool _hasMoreMemes = true;
  bool _isLoadingMemes = false;

  bool _hasMoreGroupsSuggestions = true;
  bool _isLoadingGroupsSuggestions = false;

  bool _hasMoreFollowSuggestions = true;
  bool _isLoadingFollowSuggestions = false;

  MemecistRemoteConfig _memecistRemoteConfig;

  int _appRunCount = 0;

  ScrollController _feedScrollController = ScrollController();
  ScrollController _followSuggestionsScrollController = ScrollController();
  ScrollController _groupsSuggestionsScrollController = ScrollController();

  AnimationController _hideFabAnimation;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return NotificationListener<ScrollNotification>(
      onNotification: _handleScrollNotification,
      child: Scaffold(
        backgroundColor: MemecistColors.backgroundColor,
        floatingActionButton: ScaleTransition(
          scale: _hideFabAnimation,
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 30.0.w,
            width: 30.0.w,
            child: FittedBox(
              child: FloatingActionButton(
                heroTag: "heroTagHome",
                backgroundColor: Colors.white,
                elevation: 8.0.w,
                onPressed: () => _feedScrollController.animateTo(0.0,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeOut),
                child: Icon(
                  Icons.keyboard_arrow_up,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ),
        appBar: AppBar(
          centerTitle: true,
          brightness:
              Platform.isAndroid ? AppBarTheme().brightness : Brightness.light,
          title: InkWell(
              child: Image.asset(
            'assets/mc.png',
            height: 40.0.w,
            width: 40.0.w,
          )),
          leading: StreamBuilder(
              stream: _homeBloc.hasNewNotifications,
              builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data) {
                    return Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: openNotifications,
                              child: Icon(
                                Icons.notifications,
                                size: 30.0.w,
                                color: Colors.black,
                              ),
                            )),
                        Positioned(
                          right: 15.0.w,
                          top: 15.0.h,
                          child: Container(
                            width: 10.w,
                            height: 10.h,
                            child: Icon(
                              Icons.fiber_manual_record,
                              size: 5.w,
                              color: Colors.red,
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: Colors.red),
                          ),
                        )
                      ],
                    );
                  } else {
                    return Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: openNotifications,
                          child: Icon(
                            Icons.notifications,
                            size: 30.0.w,
                            color: Colors.black,
                          ),
                        ));
                  }
                } else {
                  return Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: openNotifications,
                        child: Icon(
                          Icons.notifications,
                          size: 30.0.w,
                          color: Colors.black,
                        ),
                      ));
                }
              }),
          actions: <Widget>[
            StreamBuilder(
                stream: _homeBloc.user,
                builder: (context, AsyncSnapshot<MemecistUser> snapshot) {
                  if (snapshot.hasData) {
                    return InkWell(
                        onTap: () => openProfile(snapshot.data.userID),
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: circularImageButton(snapshot.data.profilePic),
                        ));
                  } else {
                    return InkWell(
                      onTap: () => openProfile(snapshot.data.userID),
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        child: circularImageButton(
                            FirebaseStorageConstants.defaultProfilePicUrl),
                      ),
                    );
                  }
                }),
            SizedBox(
              width: 10.0.w,
            )
          ],
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        body: LiquidPullToRefresh(
          color: MemecistColors.refreshBackgroundColor,
          animSpeedFactor: 2.0,
          showChildOpacityTransition: false,
          onRefresh: () => refreshScreen(),
          child: ListView(
            controller: _feedScrollController,
            children: <Widget>[
              SizedBox(
                height: 10.0.h,
              ),
              competitionsList(),
              SizedBox(
                height: 10.0.h,
              ),
              StreamBuilder(
                stream: _homeBloc.isInitialRunDone,
                builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
                  if (asyncSnapshot.hasData) {
                    if (asyncSnapshot.data) {
                      return normalView();
                    } else {
                      return initialView();
                    }
                  } else {
                    return Container();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget initialView() {
    return ListView(
      primary: false,
      shrinkWrap: true,
      children: <Widget>[
        shareUserNameCardWidget(),
        SizedBox(
          height: 10.0.h,
        ),
        followSuggestionsList(),
        SizedBox(
          height: 10.0.h,
        ),
        groupSuggestionsList(),
        SizedBox(
          height: 10.0.h,
        ),
      ],
    );
  }

  Widget normalView() {
    return ListView(
      primary: false,
      shrinkWrap: true,
      children: <Widget>[
        warsList(),
        SizedBox(
          height: 10.0.h,
        ),
        suggestionsList(),
        SizedBox(
          height: 10.0.h,
        ),
        memesList(),
      ],
    );
  }

  Widget groupSuggestionsList() {
    return StreamBuilder(
      stream: _homeBloc.groupSuggestions,
      builder: (context, AsyncSnapshot<List<Group>> asyncSnapshot) {
        if (asyncSnapshot.hasData) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0.w),
                  child: Text(
                    "GROUPS TO JOIN",
                    style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 14.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                controller: _groupsSuggestionsScrollController,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height >
                              StringConstants.referenceHeight
                          ? 250.0 + (250.h - 250.0) / 2
                          : 250.0.h,
                      child: ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          itemCount: asyncSnapshot.data.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return GroupSuggestionBlocProvider(
                              child: GroupSuggestionWidget(
                                  asyncSnapshot.data[index]),
                            );
                          }),
                    ),
                    StreamBuilder(
                        stream: _homeBloc.isLoadingGroupsSuggestions,
                        builder: (context, AsyncSnapshot<bool> snapshot) {
                          if (snapshot.hasData && snapshot.data) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return Container();
                          }
                        }),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0.h,
              )
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }

  Widget shareUserNameCardWidget() {
    return StreamBuilder(
        stream: _homeBloc.user,
        builder: (context, AsyncSnapshot<MemecistUser> asyncSnapshot) {
          if (asyncSnapshot.hasData) {
            MemecistUser user = asyncSnapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10.0.w),
                  child: Text(
                    "WELCOME TO MEMECIST ❤",
                    style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 14.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                  child: Text(
                    "We've created this card that you can easily share to let your followers/friends know you're on Memecist️",
                    style:
                        TextStyle(fontFamily: "Quicksand", fontSize: 14.0.sp),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0.w, right: 20.0.w, top: 10.0.h, bottom: 10.0.h),
                  child: RepaintBoundary(
                    key: _globalKey,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0.w)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                blurRadius: 3.0.w,
                                spreadRadius: 3.0.w)
                          ]),
                      child: Padding(
                        padding: EdgeInsets.all(30.0.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            circularImageButton(user.profilePic,
                                size: 80.0.w, radius: 40.0.w),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Flexible(
                              child: AutoSizeText(
                                "${user.firstName} ${user.lastName}",
                                maxLines: 2,
                                minFontSize: 12.0.sp,
                                stepGranularity: 12.0.sp / 12,
                                style: TextStyle(
                                    fontSize: 30.0.sp,
                                    fontFamily: 'Quicksand',
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Text(
                              "is now on Memecist",
                              style: TextStyle(
                                fontSize: 25.0.sp,
                                fontFamily: 'Quicksand',
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Text(
                              "Follow @${user.userName} for the best memes",
                              style: TextStyle(
                                fontSize: 20.0.sp,
                                fontFamily: 'Quicksand',
                                color: MemecistColors.greyText,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => shareUserName(),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                "SHARE",
                                style: TextStyle(
                                    fontFamily: "Quicksand",
                                    fontSize: 14.0.sp,
                                    color: MemecistColors.primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 5.0.w,
                              ),
                              CachedNetworkImage(
                                height: 20.0.h,
                                width: 20.0.w,
                                imageUrl:
                                    FirebaseStorageConstants.instagramLogo,
                              ),
                              SizedBox(
                                width: 5.0.w,
                              ),
                              CachedNetworkImage(
                                height: 20.0.h,
                                width: 20.0.w,
                                imageUrl: FirebaseStorageConstants.facebookLogo,
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            );
          } else {
            return Container();
          }
        });
  }

  Widget followSuggestionsList() {
    return StreamBuilder(
      stream: _homeBloc.followSuggestions,
      builder: (context, AsyncSnapshot<List<MemecistUser>> asyncSnapshot) {
        if (asyncSnapshot.hasData) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0.w),
                  child: Text(
                    "MEMERS TO FOLLOW",
                    style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 14.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                controller: _followSuggestionsScrollController,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height >
                              StringConstants.referenceHeight
                          ? 270.0 + (270.h - 270.0) / 2
                          : 270.0.h,
                      child: ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          itemCount: asyncSnapshot.data.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return FollowSuggestionBlocProvider(
                              child: FollowSuggestionWidget(
                                  asyncSnapshot.data[index]),
                            );
                          }),
                    ),
                    StreamBuilder(
                        stream: _homeBloc.isLoadingFollowSuggestions,
                        builder: (context, AsyncSnapshot<bool> snapshot) {
                          if (snapshot.hasData && snapshot.data) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return Container();
                          }
                        }),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0.h,
              )
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }

  Widget warsList() {
    return StreamBuilder(
      stream: _homeBloc.groups,
      builder: (context, AsyncSnapshot<List<Group>> asyncSnapshot) {
        if (asyncSnapshot.hasData) {
          if (asyncSnapshot.data.length == 0) return Container();
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 10.0.w),
                child: Text(
                  "WARS",
                  style: TextStyle(
                      fontFamily: "Quicksand",
                      fontSize: 14.0.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height >
                        StringConstants.referenceHeight
                    ? 180.0 + (180.h - 180.0) / 2
                    : 180.0.h,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: asyncSnapshot.data.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return warItem(asyncSnapshot.data[index]);
                    }),
              ),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }

  Widget memesList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        StreamBuilder(
          stream: _homeBloc.memePosts,
          builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
            if (asyncSnapshot.hasData) {
              if (asyncSnapshot.data.length == 0) return Container();
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 10.0.w),
                    child: Text(
                      "FEED",
                      style: TextStyle(
                          fontFamily: "Quicksand",
                          fontSize: 14.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: asyncSnapshot.data.length,
                    itemBuilder: (context, index) {
                      return MemePostBlocProvider(
                        child: MemePostWidget(
                          asyncSnapshot.data[index],
                          key: ObjectKey(asyncSnapshot.data[index].postID),
                          onDeleteClicked: () =>
                              onDeletePost(asyncSnapshot.data[index]),
                        ),
                      );
                    },
                  ),
                ],
              );
            } else {
              return Container();
            }
          },
        ),
        StreamBuilder(
            stream: _homeBloc.isLoadingMemes,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING FEED...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
        SizedBox(
          height: 10.0.h,
        )
      ],
    );
  }

  Widget suggestionsList() {
    return Column(
      children: [followSuggestionsList(), groupSuggestionsList()],
    );
  }

  Widget competitionsList() {
    return StreamBuilder(
      stream: _homeBloc.competitions,
      builder: (context, AsyncSnapshot<List<Competition>> asyncSnapshot) {
        if (asyncSnapshot.hasData) {
          if (asyncSnapshot.data.length == 0) return Container();
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 10.0.w),
                child: Text(
                  "COMPETITIONS",
                  style: TextStyle(
                      fontFamily: "Quicksand",
                      fontSize: 14.0.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height >
                        StringConstants.referenceHeight
                    ? 180.0 + (180.h - 180.0) / 2
                    : 180.0.h,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: asyncSnapshot.data.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return competitionItem(asyncSnapshot.data[index]);
                    }),
              ),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }

  Widget warItem(Group group) {
    return InkWell(
      onTap: () => openWarDetailsScreen(group.warID),
      child: Container(
        width: 250.0.w,
        padding: EdgeInsets.only(left: 30.0.w, right: 30.0.w),
        margin: EdgeInsets.all(10.0.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0.w)),
          color: Color(
              int.parse(group.warCardColor.substring(1, 7), radix: 16) +
                  0xFF000000),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                width: 40.0.w,
                height: 40.0.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(30.0.w)),
                    image: DecorationImage(
                        image: CachedNetworkImageProvider(group.warPic),
                        fit: BoxFit.cover))),
            SizedBox(
              height: 10.0.h,
            ),
            Text(
              group.warName,
              style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: Colors.white,
                  fontSize: 16.0.sp,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10.0.h,
            ),
            Text(
              group.warStatus,
              style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: Colors.white,
                  fontSize: 13.0.sp,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  Widget competitionItem(Competition competition) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () => openCompetitionDetailsScreen(competition),
        child: Container(
          width: 250.0.w,
          padding: EdgeInsets.only(left: 30.0.w, right: 30.0.w),
          margin: EdgeInsets.all(10.0.w),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0.w)),
            color: Color(
                int.parse(competition.cardColor.substring(1, 7), radix: 16) +
                    0xFF000000),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  width: 40.0.w,
                  height: 40.0.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30.0.w)),
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(
                              competition.competitionPic),
                          fit: BoxFit.cover))),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                competition.competitionName,
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontSize: 16.0.sp,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                competition.prizesInShort,
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontSize: 13.0.sp,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void shareUserName() {
    _homeBloc.shareUserNameCard(_globalKey);
  }

  void openProfile(String userID) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return ProfileBlocProvider(child: ProfileScreen(userID));
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.profile_screen)));
  }

  void openCompetitionDetailsScreen(Competition competition) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return CompetitionDetailsBlocProvider(
                  child: CompetitionDetailsScreen(competition));
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.competition_details_screen)));
  }

  void openWarDetailsScreen(String warID) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return WarDetailsBlocProvider(child: WarDetailsScreen(warID));
    }));
  }

  void versionCheck(context) async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    double currentVersion =
        double.parse(info.version.trim().replaceAll(".", ""));

    print("current version : " + currentVersion.toString());

    try {
      String forceUpdateVersionForAndroidString =
          _memecistRemoteConfig.forceUpdateVersionForAndroidString;
      String forceUpdateVersionForIosString =
          _memecistRemoteConfig.forceUpdateVersionForIosString;

      double newAndroidVersion = double.parse(
          forceUpdateVersionForAndroidString.trim().replaceAll(".", ""));
      double newIosVersion = double.parse(
          forceUpdateVersionForIosString.trim().replaceAll(".", ""));

      if (Platform.isAndroid && newAndroidVersion > currentVersion) {
        updateApp(context);
      } else if (Platform.isIOS && newIosVersion > currentVersion) {
        showUpdateDialog(context);
      }
    } on FetchThrottledException catch (exception) {
      print(exception);
    } catch (exception) {
      print(exception);
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }
  }

  showUpdateDialog(BuildContext context) async {
    return await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "New Update Available";
        String message =
            "There is a newer version of app available please update it now.";
        String btnLabel = "Update Now";
        String btnLabelCancel = "Later";
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(btnLabel),
              onPressed: () => launchURL(StringConstants.app_store_url),
            ),
            FlatButton(
              child: Text(btnLabelCancel),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  updateApp(BuildContext context) async {
    return InAppUpdate.checkForUpdate().then((info) {
      InAppUpdate.performImmediateUpdate().catchError((e) =>
          Fluttertoast.showToast(msg: "Some error occured :" + e.toString()));
    }).catchError((e) =>
        Fluttertoast.showToast(msg: "Some error occured :" + e.toString()));
  }

  void launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void openNotifications() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return NotificationsBlocProvider(
            child: NotificationsScreen(),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.notifications_screen)));
  }

  rateAppAndSuggestionsCheck() async {
    if (_memecistRemoteConfig.shouldShowRatingDialog) {
      _homeBloc.isRateAppDialogShown.listen((isShown) async {
        if (!isShown) {
          _homeBloc.updateIsRateAppDialogShown();
          return await showDialog<String>(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return RateAppDialog();
            },
          );
        } else {
          return "";
        }
      });
    } else if (_memecistRemoteConfig.shouldShowSuggestionsDialog) {
      _homeBloc.isSuggestionsDialogShown.listen((isShown) async {
        if (!isShown) {
          _homeBloc.updateIsSuggestionsDialogShown();
          return await showDialog<String>(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return SuggestionsDialog();
            },
          );
        } else {
          return "";
        }
      });
    }
  }

  fetchRemoteConfig() async {
    _memecistRemoteConfig = await repository.fetchRemoteConfig();
    //TODO check if this working after uploading on store
//      versionCheck(context);
    rateAppAndSuggestionsCheck();
    _homeBloc.updateRunCount(_memecistRemoteConfig);
  }

  Future<void> refreshScreen() async {
    await _homeBloc.refreshScreen(_appRunCount++);

    _hasMoreMemes = true;
    _isLoadingMemes = false;

    _hasMoreGroupsSuggestions = true;
    _isLoadingGroupsSuggestions = false;

    _hasMoreFollowSuggestions = true;
    _isLoadingFollowSuggestions = false;

    _homeBloc.initializeHomeBloc();
  }

  void onDeletePost(MemePost memePost) async {
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post, parameters: {
      FirebaseAnalyticsConstants.source: FirebaseAnalyticsConstants.home_screen
    });
    _homeBloc.deletePost(memePost);
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    if (notification.depth == 0) {
      if (notification is UserScrollNotification) {
        final UserScrollNotification userScroll = notification;
        switch (userScroll.direction) {
          case ScrollDirection.forward:
            if (userScroll.metrics.maxScrollExtent !=
                userScroll.metrics.minScrollExtent) {
              _hideFabAnimation.forward();
            }
            break;
          case ScrollDirection.reverse:
            if (userScroll.metrics.maxScrollExtent !=
                userScroll.metrics.minScrollExtent) {
              _hideFabAnimation.reverse();
            }
            break;
          case ScrollDirection.idle:
            break;
        }
      }
    }
    return false;
  }

  @override
  void initState() {
    super.initState();
    _hideFabAnimation =
        AnimationController(vsync: this, duration: kThemeAnimationDuration);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _homeBloc = HomeBlocProvider.of(context);

    if (isFirstRun) {
      _homeBloc.initializeHomeBloc();
      _homeBloc.getIsRateAppDialogShown();
      _homeBloc.getIsSuggestionsDialogShown();
      fetchRemoteConfig();

      _homeBloc.isInitialRunDone.listen((isInitialRunDone) {
        if (isInitialRunDone) {
          _homeBloc.getWars();
          _homeBloc.getFeed();

          _feedScrollController.addListener(() {
            double maxScroll = _feedScrollController.position.maxScrollExtent;
            double currentScroll = _feedScrollController.position.pixels;
            double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
            if (maxScroll - currentScroll <= delta &&
                _hasMoreMemes &&
                !_isLoadingMemes) {
              _homeBloc.getFeed();
            }
          });

          _homeBloc.hasMoreMemes.listen((hasMoreData) {
            _hasMoreMemes = hasMoreData;
          });

          _homeBloc.isLoadingMemes.listen((isLoading) {
            _isLoadingMemes = isLoading;
          });
        } else {
          _homeBloc.getFollowSuggestions();
          _homeBloc.getGroupSuggestions();
        }
      });

      _groupsSuggestionsScrollController.addListener(() {
        double maxScroll =
            _groupsSuggestionsScrollController.position.maxScrollExtent;
        double currentScroll =
            _groupsSuggestionsScrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.width) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreGroupsSuggestions &&
            !_isLoadingGroupsSuggestions) {
          _homeBloc.getGroupSuggestions();
        }
      });

      _homeBloc.hasMoreGroupsSuggestions.listen((hasMoreData) {
        _hasMoreGroupsSuggestions = hasMoreData;
      });

      _homeBloc.isLoadingGroupsSuggestions.listen((isLoading) {
        _isLoadingGroupsSuggestions = isLoading;
      });

      _followSuggestionsScrollController.addListener(() {
        double maxScroll =
            _followSuggestionsScrollController.position.maxScrollExtent;
        double currentScroll =
            _followSuggestionsScrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.width) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreFollowSuggestions &&
            !_isLoadingFollowSuggestions) {
          _homeBloc.getFollowSuggestions();
        }
      });

      _homeBloc.hasMoreFollowSuggestions.listen((hasMoreData) {
        _hasMoreFollowSuggestions = hasMoreData;
      });

      _homeBloc.isLoadingFollowSuggestions.listen((isLoading) {
        _isLoadingFollowSuggestions = isLoading;
      });

      _homeBloc.appRunCount.listen((count) {
        _appRunCount = count;
        if (count % 2 == 0) {
          _homeBloc.getFollowSuggestions();
        } else {
          _homeBloc.getGroupSuggestions();
        }
      });

      _homeBloc.memePosts.listen((memes) {
        if (memes.length == 0) {
          if (_appRunCount % 2 != 0) {
            _homeBloc.getFollowSuggestions();
          } else {
            _homeBloc.getGroupSuggestions();
          }
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _feedScrollController.dispose();
    _followSuggestionsScrollController.dispose();
    _groupsSuggestionsScrollController.dispose();
    _homeBloc.dispose();
    _hideFabAnimation.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}
