import 'package:app_settings/app_settings.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memecist/core/blocs/templates/help_memecist_grow_bloc.dart';
import 'package:memecist/core/blocs/templates/help_memecist_grow_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HelpMemecistGrowScreen extends StatefulWidget {
  @override
  _HelpMemecistGrowScreenState createState() => _HelpMemecistGrowScreenState();
}

class _HelpMemecistGrowScreenState extends State<HelpMemecistGrowScreen> {
  GlobalKey _globalKey = GlobalKey();

  HelpMemecistGrowBloc _helpMemecistGrowBloc;

  bool isFirstRun = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWithBackButtonAndTitle("Unlock Feature", context),
      backgroundColor: MemecistColors.backgroundColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 5.0.w, right: 5.0.w),
          child: Column(
            children: [
              SizedBox(
                height: 20.0.h,
              ),
              Text(
                "Download UNLIMITED HD templates WITHOUT watermark!",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "It's free!",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: Colors.green[700]),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "Help us reach more people, that's all we ask ❤️️",
                style: TextStyle(
                    fontSize: 14.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20.0.h,
              ),
              Text(
                "Share this as your Instagram story and tag '@memecistapp' OR as whatsapp status.\nCome back here to upload the screenshot once it's seen by atleast 100 people.",
                style: TextStyle(
                    fontSize: 14.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.greyText),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20.0.h,
              ),
              UnconstrainedBox(
                  child: actionButton(context, "SHARE", () => shareImage())),
              SizedBox(
                height: 10.0.h,
              ),
              sharableImage(),
              SizedBox(
                height: 20.0.h,
              ),
              StreamBuilder(
                  stream: _helpMemecistGrowBloc.showLoading,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        return UnconstrainedBox(
                            child: actionButton(context, "UPLOAD SCREENSHOT",
                                () => uploadScreenshot(),
                                width: 200.0));
                      }
                    } else {
                      return UnconstrainedBox(
                          child: actionButton(context, "UPLOAD SCREENSHOT",
                              () => uploadScreenshot(),
                              width: 200.0));
                    }
                  }),
              SizedBox(
                height: 20.0.h,
              ),
              Text(
                "PS: Make sure the number of views are visible.\nOur system will verify the screenshot authenticity automatically within 48 hours.\nDo not crop or edit the screenshot. Verification will fail in such cases.",
                style: TextStyle(
                    fontSize: 14.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.greyText),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0.h,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget sharableImage() {
    return Padding(
      padding: EdgeInsets.only(top: 10.0.h, bottom: 10.0.h),
      child: RepaintBoundary(
        key: _globalKey,
        child: Container(
          height: 200.0.h,
          width: MediaQuery.of(context).size.width,
          child: CachedNetworkImage(
            imageUrl: FirebaseStorageConstants.memecistStoryShare,
            placeholder: (context, _) {
              return Image.asset('assets/loading.png');
            },
            errorWidget: (context, _, __) {
              return Image.asset('assets/sad_emoji.png');
            },
          ),
        ),
      ),
    );
  }

  shareImage() {
    _helpMemecistGrowBloc.shareImage();
  }

  uploadScreenshot() async {
    try {
      var selectedImage =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      Fluttertoast.showToast(msg: "Uploading, please wait...");
      _helpMemecistGrowBloc.uploadScreenshot(selectedImage);
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _helpMemecistGrowBloc = HelpMemecistGrowBlocProvider.of(context);
    if (isFirstRun) {
      _helpMemecistGrowBloc.isSSUploaded.listen((isSSUploaded) {
        if (isSSUploaded) {
          Fluttertoast.showToast(
              msg: "Screen shot uploaded\nYou'll be notified when verified");
          Navigator.of(context).pop();
        }
      });
      isFirstRun = false;
    }
  }
}
