import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memecist/core/blocs/templates/help_memecist_grow_bloc_provider.dart';
import 'package:memecist/core/blocs/templates/templates_bloc.dart';
import 'package:memecist/core/blocs/templates/templates_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/template_category_model.dart';
import 'package:memecist/core/models/template_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/templates/help_memecist_grow_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TemplatesScreen extends StatefulWidget {
  @override
  _TemplatesScreenState createState() => _TemplatesScreenState();
}

class _TemplatesScreenState extends State<TemplatesScreen>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  TemplatesBloc _templatesBloc;
  TextEditingController _categoriesSearchController = TextEditingController();
  TextEditingController _templatesSearchController = TextEditingController();
  ScrollController _scrollController = ScrollController();

  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  GlobalKey _templateImageKey = GlobalKey();

  bool isFirstRun = true;
  bool isFirstRunForFetchingTemplates = true;

  bool _hasMoreTemplates = true;
  bool _isLoading = false;
  bool _isSearching = false;

  String _searchTerm = "";
  String _categoryID;

  File imageFile;

  AnimationController _hideFabAnimation;

  TextEditingController _templateNameController = new TextEditingController();
  TextEditingController _categoryNameController = new TextEditingController();
  TextEditingController _subCategoryNameController =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return NotificationListener<ScrollNotification>(
      onNotification: _handleScrollNotification,
      child: GestureDetector(
        onTap: () => removeFocus(),
        child: Scaffold(
          key: _drawerKey,
          backgroundColor: MemecistColors.backgroundColor,
          floatingActionButton: ScaleTransition(
            scale: _hideFabAnimation,
            alignment: Alignment.bottomCenter,
            child: FloatingActionButton(
              heroTag: "heroTagTemplate",
              backgroundColor: Colors.white,
              elevation: 8.0.w,
              onPressed: showUploadTemplateDialog,
              child: Icon(
                Icons.add,
                color: Colors.black,
              ),
            ),
          ),
          drawer: Drawer(
            child: StreamBuilder(
                stream: _templatesBloc.categories,
                builder: (context,
                    AsyncSnapshot<List<TemplateCategory>> asyncSnapshot) {
                  if (asyncSnapshot.hasData) {
                    return templateCategoryDrawer(asyncSnapshot.data);
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
          ),
          appBar: AppBar(
              centerTitle: true,
              brightness: Platform.isAndroid
                  ? AppBarTheme().brightness
                  : Brightness.light,
              iconTheme: new IconThemeData(color: Colors.black),
              backgroundColor: Colors.white,
              elevation: 0.0,
              title: Text(
                "Templates",
                style: TextStyle(
                    fontSize: 22.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.primaryColor),
              )),
          body: StreamBuilder(
              stream: _templatesBloc.templatesList,
              builder: (context, AsyncSnapshot<List<Template>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  return templatesList(asyncSnapshot.data);
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }),
        ),
      ),
    );
  }

  Widget templatesList(List<Template> templates) {
    return SingleChildScrollView(
      controller: _scrollController,
      child: Column(
        children: <Widget>[
          Container(
            height: 55.0.h,
            color: Colors.white,
            padding: EdgeInsets.only(
                left: 10.0.w, right: 10.0.w, top: 5.0.h, bottom: 5.0.h),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: Container(
                    height: 35.0.h,
                    decoration: BoxDecoration(
                        color: MemecistColors.backgroundColor,
                        borderRadius:
                            BorderRadius.all(Radius.circular(30.0.w))),
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
                      child: TextField(
                        controller: _templatesSearchController,
                        style: TextStyle(fontSize: 16.0.sp),
                        decoration: InputDecoration(
                            hintText: "Search",
                            hintStyle: TextStyle(fontSize: 14.0.sp),
                            border: InputBorder.none),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10.0.w,
                ),
                Icon(
                  Icons.search,
                  size: 30.0.w,
                  color: Colors.grey,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10.0.h,
          ),
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: openDrawer,
              child: Padding(
                padding: EdgeInsets.only(right: 10.0.w),
                child: Text(
                  "ALL CATEGORIES",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontFamily: 'Quicksand',
                      fontSize: 15.0.sp),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10.0.h,
          ),
          GridView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: templates.length,
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2),
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => viewTemplate(templates[index]),
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Expanded(
                          child: CachedNetworkImage(
                            imageUrl: templates[index].templatePic,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Center(
                          child: Padding(
                            padding: EdgeInsets.all(5.0.w),
                            child: Text(
                              templates[index].templateName,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontFamily: 'Quicksand',
                                  fontSize: 16.0.sp),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
          SizedBox(
            height: 10.0.h,
          ),
          StreamBuilder(
              stream: _templatesBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING TEMPLATES...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
          SizedBox(
            height: 10.0.h,
          ),
        ],
      ),
    );
  }

  Widget templateCategoryDrawer(List<TemplateCategory> templateCategories) {
    List<TemplateCategory> dummyCategories = [];
    for (int i = 0; i < templateCategories.length; i++) {
      TemplateCategory templateCategory = templateCategories[i];
      if (templateCategory.templateCategoryName.contains("-")) {
        String categoryName = templateCategory.templateCategoryName
            .substring(0, templateCategory.templateCategoryName.indexOf("-"));

        templateCategory.templateCategoryName = templateCategory
            .templateCategoryName
            .substring(templateCategory.templateCategoryName.indexOf("-") + 1);

        bool found = false;
        for (TemplateCategory dummyCategory in dummyCategories) {
          if (dummyCategory.templateCategoryName == categoryName) {
            dummyCategory.subcategories.add(templateCategory);
            found = true;
            break;
          }
        }

        if (!found) {
          dummyCategories.add(TemplateCategory(
              categoryName, "dummy", DateTime.now(), true, [templateCategory]));
        }
        templateCategories.remove(templateCategory);
        i--;
      }
    }

    dummyCategories.forEach((category) {
      templateCategories.add(category);
    });

    templateCategories.sort(
        (TemplateCategory category1, TemplateCategory category2) => category1
            .templateCategoryName
            .compareTo(category2.templateCategoryName));

    return ListView(
      primary: false,
      children: <Widget>[
        DrawerHeader(
          child: Text(
            "",
          ),
          decoration: BoxDecoration(
              color: MemecistColors.primaryColor,
              image: DecorationImage(
                  image: AssetImage('assets/drawer_background.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          height: 55.0.h,
          color: Colors.white,
          padding: EdgeInsets.only(
              left: 10.0.w, right: 10.0.w, top: 5.0.h, bottom: 5.0.h),
          child: Row(
            children: <Widget>[
              Flexible(
                child: Container(
                  height: 35.0.h,
                  decoration: BoxDecoration(
                      color: MemecistColors.backgroundColor,
                      borderRadius: BorderRadius.all(Radius.circular(30.0.w))),
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
                    child: TextField(
                      controller: _categoriesSearchController,
                      style: TextStyle(fontSize: 16.0.sp),
                      decoration: InputDecoration(
                          hintText: "Search",
                          hintStyle: TextStyle(fontSize: 14.0.sp),
                          border: InputBorder.none),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10.0.w,
              ),
              Icon(
                Icons.search,
                size: 30.0.w,
                color: Colors.grey,
              ),
            ],
          ),
        ),
        ListView.builder(
            shrinkWrap: true,
            itemCount: templateCategories.length,
            itemBuilder: (context, index) {
              return templateCategories[index].isDummy
                  ? dummyCategoryItem(templateCategories[index])
                  : templateCategoryItem(templateCategories[index]);
            })
      ],
    );
  }

  Widget pickImageBox() {
    return Padding(
      padding: EdgeInsets.all(10.0.w),
      child: UnconstrainedBox(
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: pickImage,
            child: Container(
              height: 100.0.w,
              width: 100.0.w,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: MemecistColors.backgroundColor,
                  borderRadius: BorderRadius.all(Radius.circular(10.0.w))),
              child: Icon(
                Icons.add,
                color: Colors.black,
              ),
            ),
          ),
        ),
      ),
    );
  }

  pickImage() async {
    try {
      var selectedImage =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      _templatesBloc.setImageFile(selectedImage);
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }

  void routeSaveImageWithoutWatermark(Template template) async {
    if (await repository.getCanDownloadTemplateWithoutWatermark()) {
      saveImageWithoutWatermark(template);
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) {
                return HelpMemecistGrowBlocProvider(
                    child: HelpMemecistGrowScreen());
              },
              settings: RouteSettings(
                  name: FirebaseAnalyticsConstants.help_memecist_grow_screen)));
    }
  }

  Future<void> saveImageWithoutWatermark(Template template) async {
    bool success = await _templatesBloc.saveImageWithoutWatermark(template);
    if (success) {
      Fluttertoast.showToast(msg: "Image saved to gallery");
    }
  }

  Future<void> saveImageWithWatermark() async {
    bool success =
        await _templatesBloc.saveImageWithWatermark(_templateImageKey);
    if (success) {
      Fluttertoast.showToast(msg: "Image saved to gallery");
    }
  }

  Widget templateCategoryItem(TemplateCategory templateCategory) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () => getTemplates(templateCategory.templateCategoryID),
        child: Padding(
          padding: EdgeInsets.only(
              left: 10.0.w, top: 5.0.h, right: 10.0.w, bottom: 5.0.h),
          child: Text(
            templateCategory.templateCategoryName,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 15.0.sp,
            ),
          ),
        ),
      ),
    );
  }

  Widget dummyCategoryItem(TemplateCategory templateCategory) {
    return ExpandablePanel(
        headerAlignment: ExpandablePanelHeaderAlignment.center,
        header: Padding(
          padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
          child: Text(
            templateCategory.templateCategoryName,
            style: TextStyle(
              fontFamily: 'Quicksand',
              fontSize: 15.0.sp,
            ),
          ),
        ),
        expanded: ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemBuilder: (context, index) {
            return templateCategoryItem(templateCategory.subcategories[index]);
          },
          itemCount: templateCategory.subcategories.length,
        ));
  }

  void viewTemplate(Template template) {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 8.0.w,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0.w)),
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(5.0.w),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0.w),
                      child: RepaintBoundary(
                        key: _templateImageKey,
                        child: Stack(
                          children: [
                            CachedNetworkImage(
                              imageUrl: template.templatePic,
                            ),
                            Align(
                                alignment: Alignment.bottomRight,
                                child: Padding(
                                  padding: EdgeInsets.all(5.0.w),
                                  child: Text(
                                    "Memecist App",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12.0.sp),
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
                    Text(
                      template.templateName,
                      style: TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Quicksand',
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Text(
                      template.createdBy.userName,
                      style:
                          TextStyle(fontSize: 14.0.sp, fontFamily: 'Quicksand'),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        dialogButton(
                            "DOWNLOAD", () => saveImageWithWatermark()),
                        dialogButton("DOWNLOAD WITHOUT WATERMARK",
                            () => routeSaveImageWithoutWatermark(template))
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  void openDrawer() {
    _drawerKey.currentState.openDrawer();
  }

  void getTemplates(String categoryID) {
    _categoryID = categoryID;
    _templatesBloc.getTemplates(categoryID, isNewCategory: true);
    Navigator.of(context).pop();
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    if (notification.depth == 0) {
      if (notification is UserScrollNotification) {
        final UserScrollNotification userScroll = notification;
        switch (userScroll.direction) {
          case ScrollDirection.forward:
          case ScrollDirection.reverse:
            _hideFabAnimation.reverse();
            break;
          case ScrollDirection.idle:
            _hideFabAnimation.forward();
            break;
        }
      }
    }
    return false;
  }

  Future<void> showUploadTemplateDialog() async {
    return await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "Contribute Template";
        String btnLabel = "Upload";
        String btnLabelCancel = "Cancel";
        return AlertDialog(
          title: Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: 'Quicksand',
                color: MemecistColors.primaryColor,
                fontSize: 16.0.sp),
          ),
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                memecistInputField(
                    "Category name", _categoryNameController, false),
                SizedBox(
                  height: 5.0.h,
                ),
                memecistInputField("Subcategory name (optional)",
                    _subCategoryNameController, false),
                SizedBox(
                  height: 5.0.h,
                ),
                memecistInputField(
                    "Template name", _templateNameController, false),
                SizedBox(
                  height: 5.0.h,
                ),
                StreamBuilder(
                    stream: _templatesBloc.imageFile,
                    builder: (context, AsyncSnapshot<File> asyncSnapshot) {
                      if (asyncSnapshot.hasData) {
                        if (asyncSnapshot.data == null) return pickImageBox();
                        imageFile = asyncSnapshot.data;
                        return Image.file(
                          imageFile,
                          fit: BoxFit.cover,
                        );
                      } else {
                        return pickImageBox();
                      }
                    }),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              textColor: MemecistColors.primaryColor,
              child: Text(
                btnLabelCancel,
                style: TextStyle(fontSize: 16.0.sp),
              ),
              onPressed: () => dismissUploadTemplateDialog(),
            ),
            FlatButton(
              textColor: MemecistColors.primaryColor,
              child: Text(
                btnLabel,
                style: TextStyle(fontSize: 16.0.sp),
              ),
              onPressed: () => uploadTemplate(),
            )
          ],
        );
      },
    );
  }

  Future<void> showUploadProgressDialog() async {
    return await showDialog<String>(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              "Uploading",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Quicksand',
                  color: MemecistColors.primaryColor,
                  fontSize: 16.0.sp),
            ),
            content: StreamBuilder(
              stream: _templatesBloc.imageUploadProgress,
              builder: (context, AsyncSnapshot<double> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  return Column(
                    children: <Widget>[
                      LinearProgressIndicator(value: asyncSnapshot.data ?? 0),
                      SizedBox(
                        height: 5.0.h,
                      ),
                      Center(
                        child: Text(
                          '${((asyncSnapshot.data ?? 0) * 100).toStringAsFixed(2)} % ',
                          style: TextStyle(fontSize: 14.0.sp),
                        ),
                      ),
                    ],
                  );
                } else {
                  return Container();
                }
              },
            ),
          );
        });
  }

  dismissUploadTemplateDialog() {
    imageFile = null;
    _templatesBloc.setImageFile(null);
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.dismiss_rate_app_prompt);
    Navigator.of(context).pop();
  }

  uploadTemplate() {
    String categoryName = _categoryNameController.text;
    String subCategoryName = _subCategoryNameController.text;
    String templateName = _templateNameController.text;

    if (imageFile != null) {
      if (categoryName != null && categoryName.length > 0) {
        if (templateName != null && templateName.length > 0) {
          _templatesBloc.uploadTemplate(
              imageFile, categoryName, subCategoryName, templateName);
        } else {
          Fluttertoast.showToast(msg: "Template name cannot be blank");
        }
      } else {
        Fluttertoast.showToast(msg: "Category Name cannot be blank");
      }
    } else {
      Fluttertoast.showToast(msg: "Please select image");
    }
  }

  @override
  void initState() {
    super.initState();
    _categoriesSearchController.addListener(() {
      _templatesBloc.searchCategories(_categoriesSearchController.text);
    });

    _templatesSearchController.addListener(() {
      _searchTerm = _templatesSearchController.text;
      _templatesBloc.searchTemplates(_searchTerm);
    });

    _hideFabAnimation =
        AnimationController(vsync: this, duration: kThemeAnimationDuration);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (isFirstRun) {
      _hideFabAnimation.forward();
      _templatesBloc = TemplatesBlocProvider.of(context);

      _templatesBloc.getCategories();
      _templatesBloc.categories.listen((event) {
        if (isFirstRunForFetchingTemplates) {
          _categoryID = event[0].templateCategoryID;
          _templatesBloc.getTemplates(event[0].templateCategoryID);
          isFirstRunForFetchingTemplates = false;
        }
      });

      _templatesBloc.isSearching.listen((isSearching) {
        _isSearching = isSearching;
      });

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreTemplates &&
            !_isLoading) {
          if (_isSearching)
            _templatesBloc.searchTemplates(_searchTerm, firstRun: false);
          else
            _templatesBloc.getTemplates(_categoryID);
        }
      });

      _templatesBloc.hasMoreTemplates.listen((hasMoreData) {
        _hasMoreTemplates = hasMoreData;
      });

      _templatesBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _templatesBloc.dispose();
    _hideFabAnimation.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}
