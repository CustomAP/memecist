import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_detail_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc_provider.dart';
import 'package:memecist/core/blocs/notifications/notifications_bloc_provider.dart';
import 'package:memecist/core/blocs/notifications/notifications_bloc.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_messaging_constants.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/notification_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_detail_screen.dart';
import 'package:memecist/ui/screens/meme_world/competitions/competition_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_details_screen.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  NotificationsBloc _notificationsBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreNotifications = true;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWithBackButtonAndTitle("Notifications", context),
      body: ListView(
        controller: _scrollController,
        children: <Widget>[
          SizedBox(
            height: 20.0.h,
          ),
          StreamBuilder(
              stream: _notificationsBloc.notificationsList,
              builder: (context,
                  AsyncSnapshot<List<MemecistNotification>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO NOTIFICATIONS YET!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return notificationsList(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _notificationsBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING NOTIFICATIONS...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget notificationsList(List<MemecistNotification> notifications) {
    return ListView.builder(
        itemCount: notifications.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
            padding:
                EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 15.0.h),
            child: InkWell(
              onTap: () => routeNotification(notifications[index]),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      circularImageButton(notifications[index].leadingImage),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      Expanded(
                        child: Text(
                          notifications[index].message,
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 16.0.sp,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      Text(
                        timeago.format(notifications[index].createDateTime,
                            locale: 'en_short'),
                        style: TextStyle(fontSize: 14.0.sp),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> routeNotification(MemecistNotification notification) async {
    switch (notification.type) {
      case FirebaseMessagingConstants.post:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) {
                  return MemePostDetailBlocProvider(
                    child: MemePostDetailScreen(
                      true,
                      postID: notification.id,
                    ),
                  );
                },
                settings: RouteSettings(
                    name: FirebaseAnalyticsConstants.meme_post_detail_screen)));
        break;
      case FirebaseMessagingConstants.user:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) {
                  return ProfileBlocProvider(
                    child: ProfileScreen(notification.id),
                  );
                },
                settings: RouteSettings(
                    name: FirebaseAnalyticsConstants.profile_screen)));
        break;
      case FirebaseMessagingConstants.war:
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return WarDetailsBlocProvider(
            child: WarDetailsScreen(notification.id),
          );
        }));
        break;
      case FirebaseMessagingConstants.competition:
        Competition competition =
            await repository.getCompetition(notification.id);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) {
              return CompetitionDetailsBlocProvider(
                child: CompetitionDetailsScreen(competition),
              );
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.competition_details_screen)));
        break;
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _notificationsBloc = NotificationsBlocProvider.of(context);

      _notificationsBloc.initializeNotificationsBloc();
      _notificationsBloc.fetchNotifications();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = MediaQuery.of(context).size.height * 0.20;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreNotifications &&
            !_isLoading) {
          _notificationsBloc.fetchNotifications();
        }
      });

      _notificationsBloc.hasMoreNotifications.listen((hasMoreData) {
        _hasMoreNotifications = hasMoreData;
      });

      _notificationsBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _notificationsBloc.dispose();
    super.dispose();
  }
}
