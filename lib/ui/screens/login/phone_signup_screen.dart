import 'package:flutter/material.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/core/blocs/login/phone_signup_bloc.dart';
import 'package:memecist/core/blocs/login/phone_signup_bloc_provider.dart';
import 'package:memecist/core/models/country_model.dart';
import 'package:memecist/ui/widgets/countries_selector_dialog.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:memecist/ui/screens/login/otp_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PhoneSignupScreen extends StatefulWidget {
  @override
  _PhoneSignupScreenState createState() => _PhoneSignupScreenState();
}

class _PhoneSignupScreenState extends State<PhoneSignupScreen> {
  PhoneSignUpBloc _phoneSignUpBloc;

  bool isFirstRun = true;

  List<Country> countryList = [];

  int _selectedCountryIndex = 31;

  TextEditingController _searchCountryController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();

  CountriesSelectorDialog _countriesSelectorDialog;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.primaryColorAccent,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: MemecistColors.primaryColor,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 50.0.h,
                ),
                Icon(
                  Icons.phone_iphone,
                  color: Colors.black,
                  size: 70.0.w,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  "Mobile Number",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 30.0.sp,
                  ),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  "We need to send OTP to verify your number",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    color: Colors.black,
                    fontSize: 17.0.sp,
                  ),
                ),
                SizedBox(
                  height: 30.0.h,
                ),
                StreamBuilder(
                  stream: _phoneSignUpBloc.countryCodesList,
                  builder: (BuildContext context,
                      AsyncSnapshot<Future<List<Country>>> snapshot) {
                    if (snapshot.hasData) {
                      return FutureBuilder(
                        future: snapshot.data,
                        builder: (context,
                            AsyncSnapshot<List<Country>> itemSnapshot) {
                          print(itemSnapshot.data);
                          if (itemSnapshot.hasData) {
                            countryList = itemSnapshot.data;
                            _countriesSelectorDialog =
                                CountriesSelectorDialog(countryList);
                            return getPhoneNumberContainer();
                          } else {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        },
                      );
                    } else if (snapshot.hasError) {
                      return Center(
                        child: Text("Error occured\nPlease try again."),
                      );
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getPhoneNumberContainer() {
    return StreamBuilder(
        stream: _phoneSignUpBloc.getPhoneAuthState,
        builder: (context, snapshot) {
          return Container(
            height: MediaQuery.of(context).size.height - 200.h,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(topLeft: Radius.circular(70.0.w)),
              color: Colors.white,
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 80.0.w, right: 80.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 40.0.h,
                  ),
                  Text(
                    "Select your country",
                    style: TextStyle(
                      fontSize: 15.0.sp,
                      fontFamily: 'Montserrat',
                    ),
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  StreamBuilder(
                      stream: _phoneSignUpBloc.getSelectedCountryIndex,
                      builder: (context, AsyncSnapshot<int> snapshot) {
                        if (snapshot.hasData)
                          _selectedCountryIndex = snapshot.data;
                        return InkWell(
                          onTap: () => showCountries(),
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: 4.0.w,
                                right: 4.0.w,
                                top: 8.0.h,
                                bottom: 8.0.h),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                    child: Text(
                                  ' ${countryList[_selectedCountryIndex].flag}  ${countryList[_selectedCountryIndex].name} ',
                                  style: TextStyle(fontSize: 16.0.sp),
                                )),
                                Icon(Icons.arrow_drop_down, size: 24.0.sp)
                              ],
                            ),
                          ),
                        );
                      }),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Text(
                    "Enter your phone number",
                    style: TextStyle(
                      fontSize: 15.0.sp,
                      fontFamily: 'Montserrat',
                    ),
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  StreamBuilder(
                    stream: _phoneSignUpBloc.getSelectedCountryIndex,
                    builder: (context, snapshot) {
                      if(snapshot.hasData)
                        _selectedCountryIndex = snapshot.data;
                      return TextField(
                          autofocus: false,
                          keyboardType: TextInputType.phone,
                          controller: _phoneNumberController,
                          style: TextStyle(
                            fontSize: 16.0.sp
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                gapPadding: 0.0,
                                borderRadius: BorderRadius.circular(25.0.w),
                                borderSide:
                                    BorderSide(color: MemecistColors.primaryColor)),
                            errorMaxLines: 1,
                            fillColor: Colors.white,
                            prefix: Text("  " +
                                countryList[_selectedCountryIndex]
                                    .dialCode
                                    .toString() +
                                "  "),
                          ));
                    }
                  ),
                  SizedBox(
                    height: 30.0.h,
                  ),
                  StreamBuilder(
                      stream: _phoneSignUpBloc.getPhoneAuthState,
                      builder:
                          (context, AsyncSnapshot<PhoneAuthState> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data == PhoneAuthState.CodeSent ||
                              snapshot.data == PhoneAuthState.CodeResent) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return sendOtpButton();
                          }
                        } else {
                          return sendOtpButton();
                        }
                      })
                ],
              ),
            ),
          );
        });
  }

  Widget sendOtpButton() {
    return SizedBox(
      width: double.infinity,
      child: FlatButton(
        padding: EdgeInsets.only(top: 15.0.h, bottom: 15.0.h),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0.w)),
        onPressed: () => sendOTP(),
        color: MemecistColors.primaryColor,
        child: Text(
          "Send OTP",
          style: TextStyle(
              fontSize: 17.0.sp,
              color: Colors.white,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (isFirstRun) {
      _phoneSignUpBloc = PhoneSignUpBlocProvider.of(context);
      _phoneSignUpBloc.fetchCountryCodesList(context);

      _phoneSignUpBloc.getPhoneAuthState
          .listen((PhoneAuthState phoneAuthState) {
        switch (phoneAuthState) {
          case PhoneAuthState.Error:
            Fluttertoast.showToast(msg: "Some error occured, please try again");
            break;
          case PhoneAuthState.AutoRetrievalTimeout:
//          Fluttertoast.showToast(msg: "OTP not detected");
            break;
          case PhoneAuthState.CodeSent:
          case PhoneAuthState.CodeResent:
            Fluttertoast.showToast(msg: "OTP sent to phone");
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return OtpScreen(
                  countryList[_selectedCountryIndex].dialCode +
                      _phoneNumberController.text,
                  _phoneSignUpBloc);
            }));
            break;
          case PhoneAuthState.Failed:
            Fluttertoast.showToast(msg: "Some error occured, please try again");
            break;
          default:
            break;
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _phoneSignUpBloc.dispose();
    super.dispose();
  }

  showCountries() {
    _searchCountryController.addListener(searchCountries);
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            _countriesSelectorDialog.searchPickCountryDialog(
                _searchCountryController,
                selectCountry,
                _phoneSignUpBloc.getCountries),
        barrierDismissible: false);
  }

  searchCountries() {
    String query = _searchCountryController.text;
    if (query.length == 0 || query.length == 1) {
      _phoneSignUpBloc.addCountries(countryList);
    } else if (query.length >= 2 && query.length <= 5) {
      List<Country> searchResults = [];
      searchResults.clear();
      countryList.forEach((Country c) {
        if (c.toString().toLowerCase().contains(query.toLowerCase()))
          searchResults.add(c);
      });
      _phoneSignUpBloc.addCountries(searchResults);
    } else {
      List<Country> searchResults = [];
      _phoneSignUpBloc.addCountries(searchResults);
    }
  }

  selectCountry(Country country) {
    _searchCountryController.clear();
    _phoneSignUpBloc.selectCountryIndex(countryList.indexOf(country));
    Navigator.of(context).pop();
  }

  sendOTP() {
    String phoneNumber = countryList[_selectedCountryIndex].dialCode +
        _phoneNumberController.text;
    try {
      _phoneSignUpBloc.startPhoneAuth(phoneNumber);
    } catch (e) {
      LoggerUtil.log("Phone Sign Up", "send OTP", "error occured :" + e);
      //toast error
    }
  }
}
