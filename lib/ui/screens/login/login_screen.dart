import 'package:flutter/material.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memecist/core/blocs/login/phone_signup_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/core/blocs/login/login_bloc.dart';
import 'package:memecist/core/blocs/login/login_bloc_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/ui/screens/login/phone_signup_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _loginBloc;

  bool firstRun = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 20.0.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 25.0.w),
              child: Text(
                "Welcome!",
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 40.0.sp,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ),
            SizedBox(
              height: 5.0.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 25.0.w),
              child: Text(
                "Greetings from Memecist",
                style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 20.0.sp,
                    fontWeight: FontWeight.bold,
                    color: MemecistColors.primaryColor),
              ),
            ),
            SizedBox(
              height: 5.0.h,
            ),
            Center(
              child: Container(
                padding: EdgeInsets.all(5.0.w),
                height: 250.0.h,
                child: Image.asset(
                  "assets/smiley_people.png",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: 15.0.h,
            ),
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50.0.w),
                        topRight: Radius.circular(50.0.w)),
                    color: MemecistColors.primaryColor),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(3.0.w),
                      width: 300.0.w,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0.w),
                      ),
                      child: FlatButton(
                        onPressed: () => openPhoneSignupScreen(),
                        color: Colors.transparent,
                        child: Text(
                          "Login with Phone Number",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 17.0.sp),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5.0.h,
                    ),
                    Center(
                      child: Text(
                        "OR",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0.sp),
                      ),
                    ),
                    SizedBox(
                      height: 5.0.h,
                    ),
                    StreamBuilder(
                      stream: _loginBloc.loginState,
                      builder:
                          (context, AsyncSnapshot<LoginStates> asyncSnapshot) {
                        if (asyncSnapshot.hasData) {
                          print("here");
                          print("state :" + asyncSnapshot.data.toString());
                          if (asyncSnapshot.data ==
                              LoginStates.GoogleSignInRequestSent) {
                            return Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            );
                          } else {
                            return googleSignInButton();
                          }
                        } else {
                          return googleSignInButton();
                        }
                      },
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
                      child: InkWell(
                        onTap: () => openPrivacyAndTerms(),
                        child: ParsedText(
                          text:
                              "By proceeding, you agree to our Terms\nand that you have read our Privacy Policy",
                          parse: <MatchText>[
                            MatchText(
                              pattern: "Terms",
                              style: TextStyle(
                                color: Colors.black,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                            MatchText(
                              pattern: "Privacy Policy",
                              style: TextStyle(
                                color: Colors.black,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ],
                          style: TextStyle(color: Colors.white, fontSize: 14.0),
                          alignment: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).viewInsets.bottom,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget googleSignInButton() {
    return Container(
      padding: EdgeInsets.all(3.0.w),
      width: 300.0.w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20.0.w),
      ),
      child: FlatButton(
        onPressed: () => googleSignIn(),
        color: Colors.transparent,
        child: Text(
          "SignIn with Google",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0.sp),
        ),
      ),
    );
  }

  googleSignIn() async {
    _loginBloc.googleSignIn();
  }

  void openPhoneSignupScreen() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return PhoneSignUpBlocProvider(
            child: PhoneSignupScreen(),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.phone_sign_up_screen)));
  }

  void openPrivacyAndTerms() async {
    const url = StringConstants.privacyAndTermsUrl;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if(firstRun) {
      _loginBloc = LoginBlocProvider.of(context);
      firstRun = false;
    }
  }

  @override
  void dispose() async {
    _loginBloc.dispose();
    super.dispose();
  }
}
