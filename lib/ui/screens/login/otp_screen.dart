import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/core/blocs/login/phone_signup_bloc.dart';
import 'package:memecist/core/utils/logger.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OtpScreen extends StatefulWidget {
  String _phoneNumber;
  PhoneSignUpBloc _phoneSignUpBloc;

  OtpScreen(this._phoneNumber, this._phoneSignUpBloc);

  @override
  _OtpScreenState createState() =>
      _OtpScreenState(_phoneNumber, _phoneSignUpBloc);
}

class _OtpScreenState extends State<OtpScreen> {
  String _phoneNumber;
  PhoneSignUpBloc _phoneSignUpBloc;

  _OtpScreenState(this._phoneNumber, this._phoneSignUpBloc);

  TextEditingController textEditingController1 = TextEditingController();
  TextEditingController textEditingController2 = TextEditingController();
  TextEditingController textEditingController3 = TextEditingController();
  TextEditingController textEditingController4 = TextEditingController();
  TextEditingController textEditingController5 = TextEditingController();
  TextEditingController textEditingController6 = TextEditingController();

  TextEditingController currController = new TextEditingController();

  String code = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.primaryColorAccent,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: MemecistColors.primaryColor,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 50.0.h,
                ),
                Icon(
                  Icons.phone_iphone,
                  color: Colors.black,
                  size: 70.0.w,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  "One Time Password",
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 30.0.sp,
                  ),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                SizedBox(
                  height: 30.0.h,
                ),
                otpContainer()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget inputContainer() {
    List<Widget> widgetList = [
      Padding(
        padding: EdgeInsets.only(left: 0.0, right: 2.0.w),
        child: new Container(
          color: Colors.transparent,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: 2.0.w, left: 2.0.w),
        child: new Container(
            alignment: Alignment.center,
            decoration: new BoxDecoration(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                border: new Border.all(
                    width: 1.0.w, color: Color.fromRGBO(0, 0, 0, 0.1)),
                borderRadius: new BorderRadius.circular(4.0.w)),
            child: new TextField(
              inputFormatters: [
                LengthLimitingTextInputFormatter(1),
              ],
              enabled: false,
              controller: textEditingController1,
              autofocus: false,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 24.0.sp, color: Colors.black),
            )),
      ),
      Padding(
        padding: EdgeInsets.only(right: 2.0.w, left: 2.0.w),
        child: new Container(
          alignment: Alignment.center,
          decoration: new BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.1),
              border: new Border.all(
                  width: 1.0.w, color: Color.fromRGBO(0, 0, 0, 0.1)),
              borderRadius: new BorderRadius.circular(4.0.w)),
          child: new TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(1),
            ],
            controller: textEditingController2,
            autofocus: false,
            enabled: false,
            keyboardType: TextInputType.number,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24.0.sp, color: Colors.black),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: 2.0.w, left: 2.0.w),
        child: new Container(
          alignment: Alignment.center,
          decoration: new BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.1),
              border: new Border.all(
                  width: 1.0.w, color: Color.fromRGBO(0, 0, 0, 0.1)),
              borderRadius: new BorderRadius.circular(4.0.w)),
          child: new TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(1),
            ],
            keyboardType: TextInputType.number,
            controller: textEditingController3,
            textAlign: TextAlign.center,
            autofocus: false,
            enabled: false,
            style: TextStyle(fontSize: 24.0.sp, color: Colors.black),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: 2.0.w, left: 2.0.w),
        child: new Container(
          alignment: Alignment.center,
          decoration: new BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.1),
              border: new Border.all(
                  width: 1.0.w, color: Color.fromRGBO(0, 0, 0, 0.1)),
              borderRadius: new BorderRadius.circular(4.0.w)),
          child: new TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(1),
            ],
            textAlign: TextAlign.center,
            controller: textEditingController4,
            autofocus: false,
            enabled: false,
            style: TextStyle(fontSize: 24.0.sp, color: Colors.black),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: 2.0.w, left: 2.0.w),
        child: new Container(
          alignment: Alignment.center,
          decoration: new BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.1),
              border: new Border.all(
                  width: 1.0.w, color: Color.fromRGBO(0, 0, 0, 0.1)),
              borderRadius: new BorderRadius.circular(4.0.w)),
          child: new TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(1),
            ],
            textAlign: TextAlign.center,
            controller: textEditingController5,
            autofocus: false,
            enabled: false,
            style: TextStyle(fontSize: 24.0.sp, color: Colors.black),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: 2.0.w, left: 2.0.w),
        child: new Container(
          alignment: Alignment.center,
          decoration: new BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.1),
              border: new Border.all(
                  width: 1.0.w, color: Color.fromRGBO(0, 0, 0, 0.1)),
              borderRadius: new BorderRadius.circular(4.0.w)),
          child: new TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(1),
            ],
            textAlign: TextAlign.center,
            controller: textEditingController6,
            autofocus: false,
            enabled: false,
            style: TextStyle(fontSize: 24.0.sp, color: Colors.black),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(left: 2.0.w, right: 0.0.w),
        child: new Container(
          color: Colors.transparent,
        ),
      ),
    ];

    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
      GridView.count(
          crossAxisCount: 8,
          mainAxisSpacing: 10.0.w,
          shrinkWrap: true,
          primary: false,
          scrollDirection: Axis.vertical,
          children: List<Container>.generate(
              8, (int index) => Container(child: widgetList[index]))),
      Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
            child: Padding(
              padding: EdgeInsets.only(
                  left: 8.0.w, top: 16.0.w, right: 8.0.w, bottom: 0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("1");
                    },
                    child: Text("1",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("2");
                    },
                    child: Text("2",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("3");
                    },
                    child: Text("3",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                ],
              ),
            ),
          ),
          new Container(
            child: Padding(
              padding: EdgeInsets.only(
                  left: 8.0.w, top: 4.0.w, right: 8.0.w, bottom: 0.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("4");
                    },
                    child: Text("4",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("5");
                    },
                    child: Text("5",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("6");
                    },
                    child: Text("6",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                ],
              ),
            ),
          ),
          new Container(
            child: Padding(
              padding: EdgeInsets.only(
                  left: 8.0.w, top: 4.0.w, right: 8.0.w, bottom: 0.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("7");
                    },
                    child: Text("7",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("8");
                    },
                    child: Text("8",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("9");
                    },
                    child: Text("9",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                ],
              ),
            ),
          ),
          new Container(
            child: Padding(
              padding: EdgeInsets.only(
                  left: 8.0.w, top: 4.0.w, right: 8.0.w, bottom: 0.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  MaterialButton(
                      onPressed: () {
                        deleteText();
                      },
                      child: Icon(
                        Icons.backspace,
                        size: 25.0.sp,
                      )),
                  MaterialButton(
                    onPressed: () {
                      inputTextToField("0");
                    },
                    child: Text("0",
                        style: TextStyle(
                            fontSize: 25.0.sp, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center),
                  ),
                  submitButton(),
                ],
              ),
            ),
          ),
        ],
      ),
    ]);
  }

  Widget otpContainer() {
    return Container(
        height: MediaQuery.of(context).size.height - 200.h,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(70.0.w)),
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 40.0.h,
                ),
                Center(
                  child: Text(
                    "Please enter the OTP recieved on \n" + _phoneNumber,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      color: Colors.black,
                      fontSize: 17.0.sp,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40.0.h,
                ),
                inputContainer()
              ]),
        ));
  }

  Widget submitButton() {
    return FlatButton(
      padding: EdgeInsets.only(top: 15.0.h, bottom: 15.0.h),
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0.w)),
      onPressed: () => submitOtp(),
      color: MemecistColors.primaryColor,
      child: Text(
        "Submit",
        style: TextStyle(
            fontSize: 17.0.sp,
            color: Colors.white,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  void submitOtp() {
    String code = textEditingController1.text +
        textEditingController2.text +
        textEditingController3.text +
        textEditingController4.text +
        textEditingController5.text +
        textEditingController6.text;

    LoggerUtil.log("OTP Screen", "submit otp", "code" + code);
    if (code.length != 6) {
      Fluttertoast.showToast(msg: "Please enter complete OTP");
    } else {
      _phoneSignUpBloc.signInWithPhoneNumber(code);
    }
  }

  void inputTextToField(String str) {
    //Edit first textField
    if (currController == textEditingController1) {
      textEditingController1.text = str;
      currController = textEditingController2;
    }

    //Edit second textField
    else if (currController == textEditingController2) {
      textEditingController2.text = str;
      currController = textEditingController3;
    }

    //Edit third textField
    else if (currController == textEditingController3) {
      textEditingController3.text = str;
      currController = textEditingController4;
    }

    //Edit fourth textField
    else if (currController == textEditingController4) {
      textEditingController4.text = str;
      currController = textEditingController5;
    }

    //Edit fifth textField
    else if (currController == textEditingController5) {
      textEditingController5.text = str;
      currController = textEditingController6;
    }

    //Edit sixth textField
    else if (currController == textEditingController6) {
      textEditingController6.text = str;
      currController = textEditingController6;
    }
  }

  void deleteText() {
    if (currController.text.length == 0) {
    } else {
      currController.text = "";
      currController = textEditingController5;
      return;
    }

    if (currController == textEditingController1) {
      textEditingController1.text = "";
    } else if (currController == textEditingController2) {
      textEditingController1.text = "";
      currController = textEditingController1;
    } else if (currController == textEditingController3) {
      textEditingController2.text = "";
      currController = textEditingController2;
    } else if (currController == textEditingController4) {
      textEditingController3.text = "";
      currController = textEditingController3;
    } else if (currController == textEditingController5) {
      textEditingController4.text = "";
      currController = textEditingController4;
    } else if (currController == textEditingController6) {
      textEditingController5.text = "";
      currController = textEditingController5;
    }
  }

  @override
  void initState() {
    super.initState();
    currController = textEditingController1;
    _phoneSignUpBloc.getPhoneAuthState.listen((PhoneAuthState phoneAuthState) {
      switch (phoneAuthState) {
        case PhoneAuthState.Verified:
          Fluttertoast.showToast(msg: "Phone Number Verified");
          Navigator.of(context).popUntil((route) => route.isFirst);
          //Solved bug : pushnamedandpopuntill will push another instance of main screen which is already put by wrapper
          break;
        case PhoneAuthState.Error:
          Fluttertoast.showToast(msg: "Some error occured, please try again");
          break;
        case PhoneAuthState.AutoRetrievalTimeout:
          break;
        case PhoneAuthState.Failed:
          Fluttertoast.showToast(msg: "Some error occured, please try again");
          break;
        default:
          break;
      }
    });
  }

  @override
  void dispose() {
    PhoneSignUpBloc.setPhoneAuthState(PhoneAuthState.Failed);
    super.dispose();
    textEditingController1.dispose();
    textEditingController2.dispose();
    textEditingController3.dispose();
    textEditingController4.dispose();
    textEditingController5.dispose();
    textEditingController6.dispose();
  }
}
