import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:memecist/core/blocs/login/initialize_profile_info_bloc_provider.dart';
import 'package:memecist/core/utils/lower_case_formatter.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:memecist/core/blocs/login/initialize_profile_info_bloc.dart';
import 'package:intl/intl.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/utils/regex_validator.dart';
import 'package:memecist/core/models/user_private_data_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InitializeProfileInfoScreen extends StatefulWidget {
  @override
  _InitializeProfileInfoScreenState createState() =>
      _InitializeProfileInfoScreenState();
}

class _InitializeProfileInfoScreenState
    extends State<InitializeProfileInfoScreen> {
  InitializeProfileInfoBloc _initializeProfileInfoBloc;

  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _userNameController = TextEditingController();

  String _birthDate = "";

  final _formKey = GlobalKey<FormState>();

  FormFieldValidator nameFieldValidator;
  FormFieldValidator userNameFieldValidator;

  @override
  Widget build(BuildContext context) {
    nameFieldValidator = (val) {
      return val.length == 0 ? "Value cannot be blank" : null;
    };

    userNameFieldValidator = (val) {
      return val.length < 4
          ? "Minimum length should be 4"
          : (!RegexValidator.isStringValidForUserName(val)
              ? "Only lowercase characters, numbers, . and _ allowed"
              : null);
    };

    return GestureDetector(
      onTap: () => removeFocus(),
      child: Scaffold(
        backgroundColor: MemecistColors.backgroundColor,
        appBar: AppBar(
          centerTitle: true,
          brightness:
              Platform.isAndroid ? AppBarTheme().brightness : Brightness.light,
          title: Image.asset(
            'assets/mc.png',
            height: 40.0.w,
            width: 40.0.w,
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 40.0.h,
                    ),
                    Text(
                      "One step more!",
                      style: TextStyle(
                          fontSize: 35.0.sp,
                          fontFamily: 'Quicksand',
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                          "First Name", _firstNameController, false,
                          validator: nameFieldValidator,
                          inputFormatter: [
                            LengthLimitingTextInputFormatter(20),
                          ]),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                          "Last Name", _lastNameController, false,
                          validator: nameFieldValidator,
                          inputFormatter: [
                            LengthLimitingTextInputFormatter(20),
                          ]),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    StreamBuilder(
                        stream: _initializeProfileInfoBloc
                            .initializeProfileInfoState,
                        builder: (context,
                            AsyncSnapshot<InitializeProfileInfoStates>
                                snapshot) {
                          if (snapshot.hasData) {
                            if (snapshot.data ==
                                InitializeProfileInfoStates.UserNameTaken) {
                              return userNameField(
                                  errorText: "username already taken");
                            } else {
                              return userNameField();
                            }
                          } else {
                            return userNameField();
                          }
                        }),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      width: 250.w,
                      decoration: BoxDecoration(
                          border: Border.all(),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      padding: EdgeInsets.only(
                          left: 30.0.w,
                          right: 30.0.w,
                          top: 10.0.h,
                          bottom: 10.0.h),
                      child: StreamBuilder(
                          stream: _initializeProfileInfoBloc.birthDate,
                          builder: (context, AsyncSnapshot<String> snapshot) {
                            if (snapshot.hasData) {
                              _birthDate = snapshot.data;
                            }
                            return Center(
                              child: InkWell(
                                onTap: () => pickBirthDate(),
                                child: AutoSizeText(
                                  snapshot.data == null
                                      ? "Pick your Birthdate"
                                      : _birthDate,
                                  maxLines: 1,
                                  minFontSize: 12.0.sp,
                                  stepGranularity: 12.0.sp / 12,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Quicksand',
                                      fontSize: 18.0.sp),
                                ),
                              ),
                            );
                          }),
                    ),
                    SizedBox(
                      height: 30.h,
                    ),
                    StreamBuilder(
                      stream:
                          _initializeProfileInfoBloc.initializeProfileInfoState,
                      builder: (context,
                          AsyncSnapshot<InitializeProfileInfoStates> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data ==
                              InitializeProfileInfoStates.RequestSent) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return submitButton();
                          }
                        } else {
                          return submitButton();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget userNameField({String errorText}) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10.0.w)),
      child: memecistInputField(
        "Pick a username",
        _userNameController,
        false,
        validator: userNameFieldValidator,
        errorText: errorText,
        inputFormatter: [
          LowerCaseFormatter(),
          TextInputFormatter.withFunction((oldValue, newValue) =>
              RegExp(r'^[a-z0-9._]+$').hasMatch(newValue.text)
                  ? newValue
                  : oldValue),
          LengthLimitingTextInputFormatter(20),
        ],
      ),
    );
  }

  Widget submitButton() {
    return actionButton(context, "SUBMIT", submit);
  }

  submit() {
    if (_formKey.currentState.validate()) {
      if (_birthDate == "") {
        Fluttertoast.showToast(msg: "Please select your birthdate");
        return;
      }
      try {
        _initializeProfileInfoBloc.submitProfileInfo(MemecistUser(
            firstName: _firstNameController.text,
            lastName: _lastNameController.text,
            userName: _userNameController.text,
            userPrivateData: UserPrivateData(birthDate: _birthDate)));
      } catch (e) {
        Fluttertoast.showToast(msg: e.toString());
      }
    }
  }

  pickBirthDate() {
    DateTime today = DateTime.now();
    showDatePicker(
            context: context,
            initialDate: DateTime(today.year - 13, today.month, today.day),
            firstDate: DateTime(1970),
            lastDate: DateTime(today.year - 13, today.month, today.day))
        .then((DateTime value) => _initializeProfileInfoBloc
            .setBirthDate(DateFormat("yyyy-MM-dd").format(value)));
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _initializeProfileInfoBloc = InitializeProfileInfoBlocProvider.of(context);

//    _initializeProfileInfoBloc.initializeProfileInfoState
//        .listen((InitializeProfileInfoStates initializeProfileInfoStates) {
//      if (initializeProfileInfoStates ==
//          InitializeProfileInfoStates.RequestSuccess) {
//        Navigator.of(context).pushReplacementNamed('/main_screen');
//      }
//    });
  }

  @override
  void dispose() {
    _initializeProfileInfoBloc.dispose();
    super.dispose();
  }
}
