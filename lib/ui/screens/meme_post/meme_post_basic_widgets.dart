import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget memeMedia(String url, String postType) {
  if (postType == FirestoreConstants.image)
    return imageMemePost(url);
  else
    return videoMemePost(url);
}

Widget videoMemePost(String url) {
  return Container();
}

Widget imageMemePost(String url) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(10.0.w),
    child: Align(
      /*
        Hack : width factor and height factor avoids the bottom and right black lines due to cropping
       */
      widthFactor: 0.99,
      heightFactor: 0.99,
      child: CachedNetworkImage(
        imageUrl: url,
        placeholder: (context, _) {
          return Image.asset('assets/loading.png');
        },
        errorWidget: (context, _, __) {
          return Image.asset('assets/sad_emoji.png');
        },
      ),
    ),
  );
}

Widget headerRow(MemePost memePost, Function() openProfilePage,
    {Function() openGroupPage,
    Function() openCompetitionPage,
    Function() openWarPage,
    bool showDate = true}) {
  return Material(
    color: Colors.transparent,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
              child: InkWell(
                onTap: () => openProfilePage(),
                child: userRowItem(memePost.user, false),
              ),
            ),
            showDate
                ? Text(
                    timeago.format(memePost.createDateTime, locale: 'en_short'),
                    style: TextStyle(
                      fontSize: 14.0.sp,
                    ),
                  )
                : Container()
          ],
        ),
        SizedBox(
          height: 5.0.h,
        ),
        postedForDetails(memePost,
            onTapCompetition: openCompetitionPage,
            onTapWar: openWarPage,
            onTapGroup: openGroupPage),
      ],
    ),
  );
}

Widget postedForDetails(MemePost memePost,
    {Function() onTapCompetition, Function() onTapWar, Function() onTapGroup}) {
  if (memePost.isForCompetition) {
    return InkWell(
      onTap: onTapCompetition,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              width: 20.0.w,
              height: 20.0.w,
              child: CachedNetworkImage(
                  imageUrl: FirebaseStorageConstants.competitionIdentifierUrl)),
          SizedBox(
            width: 5.0.w,
          ),
          AutoSizeText(memePost.competitionName,
              maxLines: 1,
              minFontSize: 12.0.sp,
              stepGranularity: 12.0.sp/12,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Quicksand',
                  fontSize: 15.0.sp,
                  fontWeight: FontWeight.bold))
        ],
      ),
    );
  } else if (memePost.isForWar) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        InkWell(
          onTap: onTapGroup,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  width: 20.0.w,
                  height: 20.0.w,
                  child: CachedNetworkImage(
                      imageUrl: FirebaseStorageConstants.groupIdentifierUrl)),
              SizedBox(
                width: 5.0.w,
              ),
              Flexible(
                child: AutoSizeText(memePost.groupName,
                    maxLines: 1,
                    minFontSize: 12.0.sp,
                    stepGranularity: 12.0.sp/12,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Quicksand',
                        fontSize: 15.0.sp,
                        fontWeight: FontWeight.bold)),
              )
            ],
          ),
        ),
        SizedBox(
          height: 5.0.h,
        ),
        InkWell(
          onTap: onTapWar,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  width: 20.0.w,
                  height: 20.0.w,
                  child: CachedNetworkImage(
                      imageUrl: FirebaseStorageConstants.warIdentifierUrl)),
              SizedBox(
                width: 5.0.w,
              ),
              Flexible(
                child: AutoSizeText.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                          text: memePost.warName,
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Quicksand',
                              fontSize: 15.0.sp,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: " ‣ ",
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0.sp)),
                      TextSpan(
                          text: memePost.teamName,
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Quicksand',
                              fontSize: 15.0.sp,
                              fontWeight: FontWeight.bold))
                    ],
                  ),
                  maxLines: 2,
                  minFontSize: 12.0.sp,
                  stepGranularity: 12.0.sp/12,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  } else if (memePost.isForGroup) {
    return InkWell(
      onTap: onTapGroup,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              width: 20.0.w,
              height: 20.0.w,
              child: CachedNetworkImage(
                  imageUrl: FirebaseStorageConstants.groupIdentifierUrl)),
          SizedBox(
            width: 5.0.w,
          ),
          Flexible(
            child: AutoSizeText(memePost.groupName,
                maxLines: 1,
                minFontSize: 12.0.sp,
                stepGranularity: 12.0.sp/12,
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Quicksand',
                    fontSize: 15.0.sp,
                    fontWeight: FontWeight.bold)),
          )
        ],
      ),
    );
  } else {
    return Container();
  }
}
