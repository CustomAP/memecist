import 'package:flutter/material.dart';

class BounceWidget extends StatefulWidget {
  final Function onDoubleTap;
  final Widget child;

  BounceWidget({Key key, @required this.onDoubleTap, @required this.child})
      : super(key: key);

  @override
  _BounceWidgetState createState() => _BounceWidgetState();
}

class _BounceWidgetState extends State<BounceWidget>
    with SingleTickerProviderStateMixin {
  double _scale;
  final Duration duration = Duration(milliseconds: 200);
  AnimationController _animationController;

  //Getting onDoubleTap Callback
  VoidCallback get onDoubleTap => widget.onDoubleTap;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this, duration: duration, lowerBound: 0.0, upperBound: 0.1)
      ..addListener(() {
        setState(() {});
      });

    super.initState();
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _animationController.value;
    return GestureDetector(
        onDoubleTap: _onDoubleTap,
        child: Transform.scale(scale: _scale, child: widget.child));
  }

  // Triggering the onPressed callback with a check
  void _onTapTrigger() {
    if (onDoubleTap != null) {
      onDoubleTap();
    }
  }

//  //Start the animation
//  _onTapDown(TapDownDetails details) {
//    _animationController.forward();
//  }
//
//  // We revise the animation and notify the user of the event
//  _onTapUp(TapUpDetails details) {
//    Future.delayed(Duration(milliseconds: 100), () {
//      _animationController.reverse();
//    });
//
//    //Finally calling the callback function
//    _onTapTrigger();
//  }

  _onDoubleTap() {
    _animationController.forward();
    Future.delayed(Duration(milliseconds: 100), () {
      _animationController.reverse();
    });

    //Finally calling the callback function
    _onTapTrigger();
  }
}
