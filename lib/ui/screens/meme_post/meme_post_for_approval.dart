import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_basic_widgets.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MemePostForApprovalWidget extends StatefulWidget {
  final MemePost _memePost;
  final Function() _approve;
  final Function() _reject;

  MemePostForApprovalWidget(this._memePost, this._approve, this._reject);

  @override
  _MemePostForApprovalWidgetState createState() =>
      _MemePostForApprovalWidgetState(_memePost, _approve, _reject);
}

class _MemePostForApprovalWidgetState extends State<MemePostForApprovalWidget> {
  final MemePost _memePost;
  final Function() _approve;
  final Function() _reject;

  _MemePostForApprovalWidgetState(this._memePost, this._approve, this._reject);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 20.0..h),
      padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 10.0.h),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0.w))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 10.0.h,
          ),
          headerRow(_memePost, openProfilePage),
          SizedBox(
            height: 10.0.h,
          ),
          Text(
            _memePost.caption,
            style: TextStyle(
              fontSize: 15.0.sp,
            ),
          ),
          SizedBox(
            height: 10.0.h,
          ),
          memeMedia(_memePost.mediaUrl, _memePost.postType),
          SizedBox(
            height: 10.0.h,
          ),
          actionRow()
        ],
      ),
    );
  }

  Widget actionRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        actionButton(context, "Reject", () => _reject()),
        actionButton(context, "Approve", () => _approve())
      ],
    );
  }

  void openProfilePage() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(_memePost.user.userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }
}
