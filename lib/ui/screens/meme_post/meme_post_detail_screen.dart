import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_detail_bloc.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_detail_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/view_reacts_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/comments_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/ui/screens/meme_post/view_reacts_screen.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MemePostDetailScreen extends StatefulWidget {
  final MemePost memePost;
  final String postID;
  final bool _toLoad;

  MemePostDetailScreen(this._toLoad, {this.memePost, this.postID});

  @override
  _MemePostDetailScreenState createState() =>
      _MemePostDetailScreenState(this.memePost, this.postID, this._toLoad);
}

class _MemePostDetailScreenState extends State<MemePostDetailScreen> {
  MemePost _memePost;
  MemePostDetailBloc _memePostDetailBloc;

  bool isFirstRun = true;
  bool _hasMoreComments = true;
  bool _isLoading = false;

  final String _postID;
  final bool _toLoad;

  ScrollController _scrollController = ScrollController();
  TextEditingController _commentController = TextEditingController();

  _MemePostDetailScreenState(this._memePost, this._postID, this._toLoad);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _memePostDetailBloc = MemePostDetailBlocProvider.of(context);

    if (isFirstRun) {
      //TODO while fetching if failed show post not available yet
      if (_toLoad) {
        _memePostDetailBloc.fetchMemePost(_postID);
      } else {
        _memePostDetailBloc.initializeMemePostDetailBloc(_memePost);
        _memePostDetailBloc.fetchComments();
      }

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreComments &&
            !_isLoading) {
          _memePostDetailBloc.fetchComments();
        }
      });

      _memePostDetailBloc.hasMoreComments.listen((hasMoreData) {
        _hasMoreComments = hasMoreData;
      });

      _memePostDetailBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _memePostDetailBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButton(context),
      body: StreamBuilder(
          stream: _memePostDetailBloc.memePost,
          builder: (context, AsyncSnapshot<MemePost> asyncSnapshot) {
            if (asyncSnapshot.hasData) {
              if (asyncSnapshot.data == null) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                _memePost = asyncSnapshot.data;
                return memePostDetails();
              }
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }

  Widget memePostDetails() {
    return Stack(children: <Widget>[
      Padding(
        padding: EdgeInsets.only(bottom: 70.0.h),
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10.0.h,
              ),
              MemePostBlocProvider(
                child: MemePostWidget(
                  _memePost,
                  showDetails: false,
                  showActions: false,
                ),
              ),
              SizedBox(
                height: 5.0.h,
              ),
              viewReactButton(),
              SizedBox(
                height: 10.0.h,
              ),
              StreamBuilder(
                  stream: _memePostDetailBloc.commentList,
                  builder:
                      (context, AsyncSnapshot<List<Comment>> asyncSnapshot) {
                    if (asyncSnapshot.hasData) {
                      if (asyncSnapshot.data.length == 0) {
                        return Center(
                          child: Text(
                            "BE THE FIRST TO COMMENT",
                            style: TextStyle(
                                fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                          ),
                        );
                      }
                      return commentList(asyncSnapshot.data);
                    } else
                      return Container();
                  }),
              StreamBuilder(
                  stream: _memePostDetailBloc.isLoading,
                  builder: (context, AsyncSnapshot<bool> snapshot) {
                    if (snapshot.hasData && snapshot.data) {
                      return Center(
                        child: Text(
                          "LOADING COMMENTS...",
                          style: TextStyle(
                              fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                        ),
                      );
                    } else {
                      return Container();
                    }
                  }),
              SizedBox(
                height: 5.0.h,
              )
            ],
          ),
        ),
      ),
      Align(alignment: Alignment.bottomCenter, child: getCommentRow())
    ]);
  }

  Widget commentList(List<Comment> comments) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: comments.length,
        itemBuilder: (context, index) {
          return commentItem(comments[index]);
        });
  }

  Widget commentItem(Comment comment) {
    return InkWell(
      onLongPress: () => showDeleteCommentDialog(comment),
      child: Padding(
        padding: EdgeInsets.only(
            left: 10.0.w, top: 5.0.h, bottom: 5.0.h, right: 10.0.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            userInfoRow(comment),
            Padding(
              padding: EdgeInsets.only(
                  left: 46.0.w, right: 10.0.w, top: 5.0.h, bottom: 5.0.h),
              child: Text(
                comment.comment,
                style: TextStyle(fontSize: 15.0.sp),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget userInfoRow(Comment comment) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
          child: InkWell(
              onTap: () => openProfile(comment.user.userID),
              child: userRowItem(comment.user, false)),
        ),
        Text(
          timeago.format(comment.createDateTime, locale: 'en_short'),
          style: TextStyle(
            fontSize: 14.0.sp,
          ),
        )
      ],
    );
  }

  Widget viewReactButton() {
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) {
                return ViewReactsBlocProvider(
                  child: ViewReactsScreen(_memePost),
                );
              },
              settings: RouteSettings(
                  name: FirebaseAnalyticsConstants.view_reacts_screen))),
      child: Container(
        margin: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
        height: 30.0.h,
        alignment: Alignment.centerLeft,
        child: Text(
          "VIEW REACTS",
          style: TextStyle(
              fontSize: 14.0.sp,
              fontFamily: 'Quicksand',
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget getCommentRow() {
    return Padding(
      padding: EdgeInsets.only(top: 10.0.h, bottom: 10.0.h),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 10.0.w,
          ),
          Container(
            width: MediaQuery.of(context).size.width - 76.w,
            height: 46.0.h,
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 15.0.w, right: 15.0.w),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(30.0.w))),
            child: TextField(
              textCapitalization: TextCapitalization.sentences,
              controller: _commentController,
              style: TextStyle(fontSize: 16.0.sp),
              decoration: InputDecoration(
                  hintText: "Start typing your comment..",
                  hintStyle: TextStyle(fontSize: 14.0.sp),
                  border: InputBorder.none),
            ),
          ),
          SizedBox(
            width: 10.0.w,
          ),
          InkWell(
            onTap: commentOnPost,
            child: Container(
                height: 46.0.w,
                width: 46.0.w,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(23.0.w))),
                child: Icon(
                  Icons.send,
                  color: Colors.black,
                  size: 25.0.w,
                )),
          ),
          SizedBox(
            width: 10.0.w,
          )
        ],
      ),
    );
  }

  void commentOnPost() {
    String comment = _commentController.text.trim();
    if (comment == "") {
      Fluttertoast.showToast(msg: "Comment cannot be blank");
      return;
    }
    _memePostDetailBloc.comment(comment);

    _commentController.text = "";

    FocusScope.of(context).requestFocus(new FocusNode());

    Fluttertoast.showToast(msg: "Comment Posted");
  }

  Future<void> showDeleteCommentDialog(Comment comment) async {
    HapticFeedback.vibrate();

    User user = await repository.getLoggedInUser();

    if (comment.user.userID == user.uid) {
      return await showDialog<String>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          String title = "Delete Comment?";
          String message = "This action cannot be undone!";
          String btnLabel = "Delete";
          String btnLabelCancel = "Cancel";
          return WillPopScope(
            onWillPop: () => Future.value(false),
            child: Dialog(
              elevation: 8.0.w,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0.w)),
              child: Padding(
                padding: EdgeInsets.all(10.0.w),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.0.h,
                      ),
                      Text(
                        title,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Quicksand',
                            color: MemecistColors.primaryColor,
                            fontSize: 17.0.sp),
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      Text(
                        message,
                        style: TextStyle(
                            fontFamily: 'Quicksand', fontSize: 14.0.sp),
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          FlatButton(
                            child: Text(btnLabelCancel, style: TextStyle(fontSize: 16.0.sp),),
                            textColor: MemecistColors.primaryColor,
                            onPressed: () => dismissDeleteCommentDialog(),
                          ),
                          FlatButton(
                              textColor: MemecistColors.primaryColor,
                              child: Text(btnLabel, style: TextStyle(fontSize: 16.0.sp),),
                              onPressed: () => deleteComment(comment)),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
    }
  }

  void openProfile(String userID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }

  dismissDeleteCommentDialog() {
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.dismiss_delete_comment_prompt);
    Navigator.of(context).pop();
  }

  deleteComment(Comment comment) async {
    _memePostDetailBloc.deleteComment(comment);

    Fluttertoast.showToast(msg: "Comment deleted");
    Navigator.of(context).pop();
  }
}
