import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_post/view_reacts_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/blocs/meme_post/view_reacts_bloc.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViewReactsScreen extends StatefulWidget {
  final MemePost _memePost;

  ViewReactsScreen(this._memePost);

  @override
  _ViewReactsScreenState createState() => _ViewReactsScreenState(_memePost);
}

class _ViewReactsScreenState extends State<ViewReactsScreen> {
  MemePost _memePost;

  _ViewReactsScreenState(this._memePost);

  ViewReactsBloc _viewReactsBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreReacts = true;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButtonAndTitle("Reacts", context),
      body: ListView(
        controller: _scrollController,
        children: <Widget>[
          SizedBox(
            height: 20.0.h,
          ),
          StreamBuilder(
              stream: _viewReactsBloc.usersList,
              builder: (context, AsyncSnapshot<List<MemecistUser>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO REACTS YET!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return reactList(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _viewReactsBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING REACTS...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget reactList(List<MemecistUser> users) {
    return ListView.builder(
        itemCount: users.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
            padding:
                EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 15.0.h),
            child: InkWell(
              onTap: () => openProfile(users[index].userID),
              child: userRowItem(users[index], false),
            ),
          );
        });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _viewReactsBloc = ViewReactsBlocProvider.of(context);
      _viewReactsBloc.initializeViewReactsBloc(_memePost);

      _viewReactsBloc.fetchReacts();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = MediaQuery.of(context).size.height * 0.20;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreReacts &&
            !_isLoading) {
          _viewReactsBloc.fetchReacts();
        }
      });

      _viewReactsBloc.hasMoreReacts.listen((hasMoreData) {
        _hasMoreReacts = hasMoreData;
      });

      _viewReactsBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _viewReactsBloc.dispose();
    super.dispose();
  }

  void openProfile(String userID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }
}
