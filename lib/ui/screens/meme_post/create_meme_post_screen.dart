import 'dart:io';
import 'package:app_settings/app_settings.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memecist/core/blocs/meme_post/add_links_for_competition_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/create_meme_post_bloc.dart';
import 'package:memecist/core/blocs/meme_post/create_meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_details_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/promo_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/add_links_for_competition_screen.dart';
import 'package:memecist/ui/screens/meme_world/competitions/competition_details_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'meme_post_widget.dart';

class CreateMemePostScreen extends StatefulWidget {
  final String groupID, warID, competitionID, teamID;
  final String groupName, warName, competitionName, teamName;
  final bool isGroupAdmin, areExternalLinksAllowed, areExternalLinksCompulsory;

  CreateMemePostScreen(
      {this.groupID,
      this.competitionID,
      this.warID,
      this.groupName,
      this.competitionName,
      this.warName,
      this.isGroupAdmin,
      this.teamID,
      this.teamName,
      this.areExternalLinksAllowed,
      this.areExternalLinksCompulsory});

  @override
  _CreateMemePostScreenState createState() => _CreateMemePostScreenState(
      groupID: groupID,
      competitionID: competitionID,
      warID: warID,
      groupName: groupName,
      competitionName: competitionName,
      warName: warName,
      isGroupAdmin: isGroupAdmin,
      teamID: teamID,
      teamName: teamName,
      areExternalLinksAllowed: areExternalLinksAllowed,
      areExternalLinksCompulsory: areExternalLinksCompulsory);
}

class _CreateMemePostScreenState extends State<CreateMemePostScreen>
    with AutomaticKeepAliveClientMixin {
  CreateMemePostBloc _createMemePostBloc;
  File imageFile;

  String groupID, competitionID, warID, teamID;
  String groupName, competitionName, warName, teamName;
  bool isGroupAdmin, areExternalLinksAllowed, areExternalLinksCompulsory;
  bool isForGroup, isForCompetition;
  bool showPromo = false;
  bool isFirstRun = true;
  String _category = "";

  double padding = 0;
  double sizedBoxHeight = 10.0.h;

  GlobalKey _imageKey = GlobalKey();
  GlobalKey _memePostKey = GlobalKey();

  TextEditingController _captionController = TextEditingController();

  TextEditingController _facebookTextController = TextEditingController();
  TextEditingController _instagramTextController = TextEditingController();

  TextEditingController _externalLink1Controller = TextEditingController();
  TextEditingController _externalLink2Controller = TextEditingController();

  _CreateMemePostScreenState(
      {this.groupID,
      this.competitionID,
      this.warID,
      this.groupName,
      this.warName,
      this.competitionName,
      this.isGroupAdmin,
      this.teamName,
      this.teamID,
      this.areExternalLinksAllowed,
      this.areExternalLinksCompulsory});

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _createMemePostBloc = CreateMemePostBlocProvider.of(context);

      _createMemePostBloc.initializeCreateMemePostBloc(
          groupID: groupID,
          competitionID: competitionID,
          warID: warID,
          warName: warName,
          groupName: groupName,
          competitionName: competitionName,
          isGroupAdmin: isGroupAdmin,
          teamName: teamName,
          teamID: teamID);

      groupID == null ? isForGroup = false : isForGroup = true;
      competitionID == null
          ? isForCompetition = false
          : isForCompetition = true;

      if (isForCompetition) {
        _createMemePostBloc.postID.listen((postID) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) {
                return AddLinksForCompetitionBlocProvider(
                  child: AddLinksForCompetitionScreen(
                      postID, competitionID, areExternalLinksCompulsory),
                );
              },
              settings: RouteSettings(
                  name: FirebaseAnalyticsConstants
                      .add_links_for_competition_screen)));
        });
      } else {
        _createMemePostBloc.postID.listen((postID) {
          _createMemePostBloc.fetchMeme(postID);
        });
      }

      _createMemePostBloc.getPromos();

      _createMemePostBloc.memecistSocialCompetition.listen((competition) {
        if (competition != null) {
          Navigator.of(context).pop();
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) {
                return CompetitionDetailsBlocProvider(
                  child: CompetitionDetailsScreen(competition),
                );
              },
              settings: RouteSettings(
                  name:
                      FirebaseAnalyticsConstants.competition_details_screen)));
        } else {
          Navigator.of(context).pop();
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _createMemePostBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onTap: () => removeFocus(),
      child: Scaffold(
          backgroundColor: MemecistColors.backgroundColor,
          appBar: AppBar(
            backgroundColor: Colors.white,
            brightness: Platform.isAndroid
                ? AppBarTheme().brightness
                : Brightness.light,
            elevation: 0.0,
            centerTitle: true,
            title: Text(
              "Upload Meme",
              style: TextStyle(
                  fontSize: 22.0.sp,
                  fontFamily: 'Quicksand',
                  color: MemecistColors.primaryColor),
            ),
          ),
          body: StreamBuilder(
              stream: _createMemePostBloc.memePost,
              builder: (context, AsyncSnapshot<MemePost> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  return memePostPreviewWidget(asyncSnapshot.data);
                } else {
                  return createMemePostWidget();
                }
              })),
    );
  }

  Widget memePostPreviewWidget(MemePost memePost) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
        child: Column(
          children: [
            SizedBox(
              height: 20.0.h,
            ),
            Text(
              "Meme Posted!",
              style: TextStyle(
                  fontSize: 16.0.sp,
                  fontFamily: 'Quicksand',
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20.0.h,
            ),
            Text(
              "Participate in Memecist Social?\nShare this post on Facebook/Instagram and copy paste those links below!",
              style: TextStyle(
                  fontSize: 14.0.sp,
                  fontFamily: 'Quicksand',
                  color: MemecistColors.greyText),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10.0.h,
            ),
            FlatButton(
              onPressed: () => openCompetitionDetails(),
              child: Text(
                "MORE INFO",
                style: TextStyle(
                    fontSize: 13.0.sp,
                    fontFamily: 'Quicksand',
                    color: MemecistColors.primaryColor),
              ),
            ),
            SizedBox(
              height: 10.0.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FlatButton(
                  child: Text(
                    "CREATE NEW",
                    style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 14.0.sp,
                        color: MemecistColors.primaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                  textColor: MemecistColors.primaryColor,
                  onPressed: () => dismissSharePostDialog(),
                ),
                InkWell(
                  onTap: () => sharePost(memePost),
                  child: Row(
                    children: [
                      Text(
                        "SHARE",
                        style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 14.0.sp,
                            color: MemecistColors.primaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 5.0.w,
                      ),
                      CachedNetworkImage(
                        height: 20.0.w,
                        width: 20.0.w,
                        imageUrl: FirebaseStorageConstants.instagramLogo,
                      ),
                      SizedBox(
                        width: 5.0.w,
                      ),
                      CachedNetworkImage(
                        height: 20.0.w,
                        width: 20.0.w,
                        imageUrl: FirebaseStorageConstants.facebookLogo,
                      )
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0.h,
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0.w)),
              child: memecistInputField(
                  "Shared post link 1", _externalLink1Controller, false),
            ),
            SizedBox(
              height: 10.0.h,
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0.w)),
              child: memecistInputField(
                  "Shared post link 2", _externalLink2Controller, false),
            ),
            SizedBox(
              height: 10.0.h,
            ),
            submitLinksButton(),
            SizedBox(
              height: 20.0.h,
            ),
            StreamBuilder(
                stream: _createMemePostBloc.sizedBoxHeight,
                builder: (context, asyncSnapshot) {
                  if (asyncSnapshot.hasData)
                    sizedBoxHeight = asyncSnapshot.data;
                  return StreamBuilder(
                      stream: _createMemePostBloc.padding,
                      builder: (context, paddingSnapshot) {
                        if (paddingSnapshot.hasData)
                          padding = paddingSnapshot.data;
                        return dynamicSizedPost(memePost);
                      });
                }),
            SizedBox(
              height: 10.0.h,
            ),
          ],
        ),
      ),
    );
  }

  Widget dynamicSizedPost(MemePost memePost) {
    return RepaintBoundary(
      key: _memePostKey,
      child: MemePostBlocProvider(
        child: MemePostWidget(
          memePost,
          showActions: false,
          showDetails: false,
          forSharing: true,
          padding: padding,
          sizedBoxHeight: sizedBoxHeight,
        ),
      ),
    );
  }

  Widget createMemePostWidget() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        SizedBox(
          height: 10.0.h,
        ),
        captionWidget(),
        SizedBox(
          height: 10.0.h,
        ),
        StreamBuilder(
            stream: _createMemePostBloc.imageFile,
            builder: (context, AsyncSnapshot<File> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data == null) return pickImageBox();
                imageFile = asyncSnapshot.data;
                return imageBox();
              } else {
                return pickImageBox();
              }
            }),
        SizedBox(
          height: 10.0.h,
        ),
        StreamBuilder(
            stream: _createMemePostBloc.category,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return categorySelector(snapshot.data);
              } else {
                return categorySelector(null);
              }
            }),
        SizedBox(
          height: 20.0.h,
        ),
        StreamBuilder(
            stream: _createMemePostBloc.memePostStatus,
            builder: (context, AsyncSnapshot<MemePostStatus> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data == MemePostStatus.Complete) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  _createMemePostBloc.setImageFile(null);
                  return postButton();
                } else {
                  return StreamBuilder(
                    stream: _createMemePostBloc.imageUploadProgress,
                    builder: (context, AsyncSnapshot<double> snapshot) {
                      return progressBar(snapshot);
                    },
                  );
                }
              } else {
                return postButton();
              }
            }),
        SizedBox(
          height: 20.0.h,
        )
      ],
    );
  }

  Widget categorySelector(String category) {
    List<String> categories = StringConstants.categoriesForCreateMemePost;

    categories.sort();

    return UnconstrainedBox(
      child: Container(
        width: 300.0.w,
        child: DropdownButtonFormField(
          hint: Text(
            "Select category",
            style: TextStyle(fontSize: 16.0.sp),
          ),
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 2.0.w)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 2.0.w))),
          value: category,
          items: categories.map((category) {
            return DropdownMenuItem(
                value: category,
                child: Text(
                  category,
                  style: TextStyle(fontSize: 14.0.sp),
                ));
          }).toList(),
          onChanged: (String value) {
            _createMemePostBloc.setCategory(value);
            _category = value;
          },
        ),
      ),
    );
  }

  Widget progressBar(AsyncSnapshot<double> snapshot) {
    return UnconstrainedBox(
      child: Container(
        width: 300.w,
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            LinearProgressIndicator(value: snapshot.data ?? 0),
            SizedBox(
              height: 10.0.h,
            ),
            Center(
              child: Text(
                '${((snapshot.data ?? 0) * 100).toStringAsFixed(2)} % ',
                style: TextStyle(fontSize: 14.0.sp),
              ),
            ),
            SizedBox(
              height: 10.0.h,
            ),
            Center(
              child: Text(
                'Posting your meme',
                style: TextStyle(fontSize: 14.0.sp),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget captionWidget() {
    return Padding(
      padding: EdgeInsets.all(10.0.w),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10.0.w))),
        child: TextField(
          keyboardType: TextInputType.multiline,
          controller: _captionController,
          textCapitalization: TextCapitalization.sentences,
          maxLines: 3,
          style: TextStyle(fontSize: 16.0.sp),
          decoration: InputDecoration(
              hintText: "Start typing caption...",
              hintStyle: TextStyle(fontSize: 16.0.sp),
              contentPadding: EdgeInsets.only(
                  left: 10.0.w, right: 10.0.w, top: 10.0.h, bottom: 10.0.h),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0.w),
                  borderSide: BorderSide(
                      color: MemecistColors.primaryColor, width: 2.25.w)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0.w),
                  borderSide: BorderSide(color: Colors.white))),
        ),
      ),
    );
  }

  Widget postButton() {
    return UnconstrainedBox(
        child: actionButton(context, "POST", () => postMeme()));
  }

  Widget imageBox() {
    return Padding(
      padding: EdgeInsets.all(10.0.w),
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: pickImage,
            child: RepaintBoundary(
              key: _imageKey,
              child: Column(
                children: <Widget>[
                  Image.file(
                    imageFile,
                    fit: BoxFit.cover,
                  ),
                  StreamBuilder(
                      stream: _createMemePostBloc.showPromoBar,
                      builder: (context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData) {
                          showPromo = snapshot.data;
                          return showPromo
                              ? StreamBuilder(
                                  stream: _createMemePostBloc.promo,
                                  builder: (context,
                                      AsyncSnapshot<Promo> asyncSnapshot) {
                                    if (asyncSnapshot.hasData) {
                                      if (!asyncSnapshot.data.isSet) {
                                        WidgetsBinding.instance
                                            .addPostFrameCallback(
                                                (_) => promoEditDialog());
                                        return Container();
                                      } else {
                                        return Container(
                                            color: Colors.grey[100],
                                            child: promoWidget(
                                                asyncSnapshot.data));
                                      }
                                    } else {
                                      return Container();
                                    }
                                  },
                                )
                              : Container();
                        } else {
                          return Container();
                        }
                      }),
                ],
              ),
            ),
          ),
          StreamBuilder(
              stream: _createMemePostBloc.showPromoBar,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  showPromo = snapshot.data;
                }
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Add Promo bar",
                          style: TextStyle(fontSize: 16.0.sp),
                        ),
                        Switch(
                          value: showPromo,
                          onChanged: (value) {
                            _createMemePostBloc.setShowPromoBar(!showPromo);
                          },
                          activeTrackColor: MemecistColors.primaryColorAccent,
                          activeColor: MemecistColors.primaryColor,
                        ),
                        SizedBox(
                          width: 10.0.w,
                        ),
                        InkWell(
                            onTap: promoEditDialog,
                            child: Container(
                                width: 50.0.w,
                                alignment: Alignment.center,
                                height: 20.0.h,
                                decoration: BoxDecoration(
                                    border: Border.all(),
                                    borderRadius: BorderRadius.circular(5.0.w)),
                                child: Text("Edit",
                                    style: TextStyle(
                                        fontSize: 12.0.sp,
                                        fontFamily: 'Quicksand'))))
                      ],
                    ),
                    !showPromo
                        ? Text(
                            "Promo Bar helps you create an identity among viewers!",
                            style: TextStyle(fontSize: 14.0.sp),
                            textAlign: TextAlign.center)
                        : Container()
                  ],
                );
              })
        ],
      ),
    );
  }

  promoEditDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return WillPopScope(
            onWillPop: () => Future.value(false),
            child: Dialog(
              elevation: 8.0.w,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0.w)),
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(5.0.w),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 5.0.w, right: 5.0.w),
                            child: CachedNetworkImage(
                              width: 50.0.w,
                              height: 50.0.w,
                              imageUrl: FirebaseStorageConstants.instagramLogo,
                            ),
                          ),
                          Expanded(
                            child: memecistInputField("Instagram ID",
                                _instagramTextController, false),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 5.0.w, right: 5.0.w),
                            child: CachedNetworkImage(
                              width: 50.0.w,
                              height: 50.0.w,
                              imageUrl: FirebaseStorageConstants.facebookLogo,
                            ),
                          ),
                          Expanded(
                            child: memecistInputField(
                                "Facebook ID", _facebookTextController, false),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          greyedOutButton(context, "Cancel", () {
                            _createMemePostBloc.setShowPromoBar(false);
                            Navigator.of(context).pop();
                          }),
                          actionButton(
                              context, "Save", () => savePromo(context))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  Widget promoWidget(Promo promo) {
    List<String> ids = [];
    List<String> urls = [];

    if (promo.memecistID != "") {
      ids.add(promo.memecistID);
      urls.add(FirebaseStorageConstants.memecistLogo);
    }
    if (promo.instagramID != "") {
      ids.add(promo.instagramID);
      urls.add(FirebaseStorageConstants.instagramLogo);
    }
    if (promo.facebookID != "") {
      ids.add(promo.facebookID);
      urls.add(FirebaseStorageConstants.facebookLogo);
    }

    return promoRow(urls, ids);

    //for more handles in future
//    switch (ids.length) {
//      case 1:
//        return promoRow(urls, ids);
//        break;
//      case 2:
//        return promoRow(urls, ids);
//        break;
//      case 3:
//        return promoRow(urls, ids);
//        break;
//      case 4:
//        List<String> urls1 = [];
//        urls1.add(urls[0]);
//        urls1.add(urls[1]);
//
//        List<String> ids1 = [];
//        ids1.add(ids[0]);
//        ids1.add(ids[1]);
//
//        List<String> urls2 = [];
//        urls2.add(urls[2]);
//        urls2.add(urls[3]);
//
//        List<String> ids2 = [];
//        ids2.add(ids[2]);
//        ids2.add(ids[3]);
//
//        return Column(
//          children: <Widget>[
//            promoRow(urls1, ids1),
//            SizedBox(
//              height: 3.0,
//            ),
//            promoRow(urls2, ids2)
//          ],
//        );
//        break;
//      case 5:
//        List<String> urls1 = [];
//        urls1.add(urls[0]);
//        urls1.add(urls[1]);
//
//        List<String> ids1 = [];
//        ids1.add(ids[0]);
//        ids1.add(ids[1]);
//
//        List<String> urls2 = [];
//        urls2.add(urls[2]);
//        urls2.add(urls[3]);
//        urls2.add(urls[4]);
//
//        List<String> ids2 = [];
//        ids2.add(ids[2]);
//        ids2.add(ids[3]);
//        ids2.add(ids[4]);
//
//        return Column(
//          children: <Widget>[
//            promoRow(urls1, ids1),
//            SizedBox(
//              height: 3.0,
//            ),
//            promoRow(urls2, ids2)
//          ],
//        );
//        break;
//      default:
//        return Container();
//    }
  }

  Widget promoRow(List<String> url, List<String> id) {
    List<Widget> widgetList = new List<Widget>();
    for (var i = 0; i < url.length; i++) {
      widgetList.add(Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: url[i],
            width: 20.0.w,
            height: 20.0.w,
          ),
          SizedBox(
            width: 5.0.w,
          ),
          Text(
            id[i],
            style: TextStyle(fontSize: 14.0.sp),
          ),
        ],
      ));
    }
    return Padding(
      padding: EdgeInsets.all(5.0.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: widgetList,
      ),
    );
  }

  Widget pickImageBox() {
    return Padding(
      padding: EdgeInsets.all(10.0.w),
      child: UnconstrainedBox(
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: pickImage,
            child: Container(
              height: 100.0.w,
              width: 100.0.w,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.0.w))),
              child: Icon(
                Icons.add,
                color: MemecistColors.iconGreyBackgroundColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget submitLinksButton() {
    return UnconstrainedBox(
        child: actionButton(context, "SUBMIT LINKS", () => submitLinks()));
  }

  savePromo(BuildContext context) async {
    Promo promo = Promo(
        _facebookTextController.text, _instagramTextController.text, "", true);
    _createMemePostBloc.savePromos(promo);
    Navigator.of(context).pop();
  }

  void postMeme() {
    if (imageFile == null) {
      Fluttertoast.showToast(msg: "Please select image first");
      return;
    }
    _createMemePostBloc.uploadPost(
        _imageKey, _captionController.text, _category, imageFile);
  }

  pickImage() async {
    try {
      var selectedImage =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      _createMemePostBloc.setImageFile(selectedImage);
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }

  sharePost(MemePost memePost) async {
    _createMemePostBloc.resizeImage(_memePostKey);
    await _createMemePostBloc.sharePost(_memePostKey, memePost);
    _createMemePostBloc.resizeImage(_memePostKey);
  }

  dismissSharePostDialog() {
    if (isForGroup || (isForCompetition && !areExternalLinksAllowed)) {
      Navigator.of(context).pop();
    } else {
      _createMemePostBloc.setMemePost(null);
      _captionController.clear();
    }
  }

  openCompetitionDetails() async {
    _createMemePostBloc.fetchMemecistSocialCompetition();
    return await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return StreamBuilder(
            stream: _createMemePostBloc.memecistSocialCompetition,
            builder: (context, AsyncSnapshot<Competition> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                return Container();
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          );
        });
  }

  void submitLinks() async {
    if (_externalLink1Controller.text.trim() == "" &&
        _externalLink2Controller.text.trim() == "") {
      Fluttertoast.showToast(msg: "Atleast one link required");
    } else {
      await _createMemePostBloc.submitLinks(
          _externalLink1Controller.text, _externalLink2Controller.text);
      Fluttertoast.showToast(
          msg:
              "Links submitted!\nYour meme will be evaluated for Memecist social");
      dismissSharePostDialog();
    }
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  bool get wantKeepAlive => true;
}
