import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/blocs/meme_post/add_links_for_competition_bloc.dart';
import 'package:memecist/core/blocs/meme_post/add_links_for_competition_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AddLinksForCompetitionScreen extends StatefulWidget {
  final String competitionID;
  final String postID;
  final bool areExternalLinksCompulsory;

  AddLinksForCompetitionScreen(
      this.postID, this.competitionID, this.areExternalLinksCompulsory);

  @override
  _AddLinksForCompetitionScreenState createState() =>
      _AddLinksForCompetitionScreenState(
          postID, competitionID, areExternalLinksCompulsory);
}

class _AddLinksForCompetitionScreenState
    extends State<AddLinksForCompetitionScreen> {
  final String _competitionID;
  final String _postID;
  final bool _areExternalLinksCompulsory;
  bool isFirstRun = true;

  AddLinksForCompetitionBloc _addLinksForCompetitionBloc;

  double padding = 0;
  double sizedBoxHeight = 10.0.h;

  GlobalKey _memePostKey = GlobalKey();

  TextEditingController _externalLink1Controller = TextEditingController();
  TextEditingController _externalLink2Controller = TextEditingController();
  TextEditingController _externalLink3Controller = TextEditingController();

  _AddLinksForCompetitionScreenState(
      this._postID, this._competitionID, this._areExternalLinksCompulsory);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (isFirstRun) {
      _addLinksForCompetitionBloc =
          AddLinksForCompetitionBlocProvider.of(context);

      _addLinksForCompetitionBloc.getMemePost(_postID);

      _addLinksForCompetitionBloc.initializeAddLinksForCompetitionBloc(
          _competitionID, _postID);

      _addLinksForCompetitionBloc.externalLinksUpdateStatus.listen((event) {
        if (event == ExternalLinksUpdateStatus.Complete) {
          _externalLink1Controller.clear();
          _externalLink2Controller.clear();
          _externalLink3Controller.clear();
          Fluttertoast.showToast(msg: "Links updated");
          Navigator.of(context).pop();
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _addLinksForCompetitionBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => removeFocus(),
      child: Scaffold(
        backgroundColor: MemecistColors.backgroundColor,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          title: Text(
            "Add Links",
            style: TextStyle(
                fontFamily: 'Quicksand', color: MemecistColors.primaryColor),
          ),
        ),
        body: StreamBuilder(
          stream: _addLinksForCompetitionBloc.memePost,
          builder: (context, AsyncSnapshot<MemePost> asyncSnapshot) {
            if (asyncSnapshot.hasData) {
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 20.0.h,
                      ),
                      Text(
                        "To participate in this competition, share this post on other social media and copy paste the links below!",
                        style: TextStyle(
                            fontSize: 14.0.sp,
                            fontFamily: 'Quicksand',
                            color: MemecistColors.greyText),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 20.0.h,
                      ),
                      InkWell(
                        onTap: () => sharePost(asyncSnapshot.data),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "SHARE",
                              style: TextStyle(
                                  fontFamily: "Quicksand",
                                  fontSize: 16.0.sp,
                                  color: MemecistColors.primaryColor,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5.0.w,
                            ),
                            CachedNetworkImage(
                              height: 20.0.w,
                              width: 20.0.w,
                              imageUrl: FirebaseStorageConstants.instagramLogo,
                            ),
                            SizedBox(
                              width: 5.0.w,
                            ),
                            CachedNetworkImage(
                              height: 20.0.w,
                              width: 20.0.w,
                              imageUrl: FirebaseStorageConstants.facebookLogo,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20.0.h,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0.w)),
                        child: memecistInputField("Shared post link 1",
                            _externalLink1Controller, false),
                      ),
                      SizedBox(
                        height: 20.0.h,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0.w)),
                        child: memecistInputField("Shared post link 2",
                            _externalLink2Controller, false),
                      ),
                      SizedBox(
                        height: 20.0.h,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0.w)),
                        child: memecistInputField("Shared post link 3",
                            _externalLink3Controller, false),
                      ),
                      SizedBox(
                        height: 20.0.h,
                      ),
                      submitLinksButton(),
                      SizedBox(
                        height: 20.0.h,
                      ),
                      StreamBuilder(
                          stream: _addLinksForCompetitionBloc.sizedBoxHeight,
                          builder: (context, snapshot) {
                            if (snapshot.hasData)
                              sizedBoxHeight = snapshot.data;
                            return StreamBuilder(
                                stream: _addLinksForCompetitionBloc.padding,
                                builder: (context, paddingSnapshot) {
                                  if (paddingSnapshot.hasData)
                                    padding = paddingSnapshot.data;
                                  return dynamicSizedPost(asyncSnapshot.data);
                                });
                          }),
                      SizedBox(
                        height: 20.0.h,
                      )
                    ],
                  ),
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }

  Widget dynamicSizedPost(MemePost memePost) {
    return RepaintBoundary(
      key: _memePostKey,
      child: MemePostBlocProvider(
        child: MemePostWidget(
          memePost,
          showActions: false,
          showDetails: false,
          forSharing: true,
          padding: padding,
          sizedBoxHeight: sizedBoxHeight,
        ),
      ),
    );
  }

  Widget submitLinksButton() {
    return UnconstrainedBox(
        child: actionButton(context, "SUBMIT LINKS", () => submitLinks()));
  }

  void submitLinks() {
    if (_areExternalLinksCompulsory) {
      if (_externalLink1Controller.text.trim() == "" &&
          _externalLink2Controller.text.trim() == "" &&
          _externalLink3Controller.text.trim() == "") {
        Fluttertoast.showToast(msg: "Atleast one link required");
      } else {
        _addLinksForCompetitionBloc.submitLinks(_externalLink1Controller.text,
            _externalLink2Controller.text, _externalLink3Controller.text);
      }
    } else {
      _addLinksForCompetitionBloc.submitLinks(_externalLink1Controller.text,
          _externalLink2Controller.text, _externalLink3Controller.text);
    }
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  sharePost(MemePost memePost) async {
    _addLinksForCompetitionBloc.resizeImage(_memePostKey);
    await _addLinksForCompetitionBloc.sharePost(_memePostKey, memePost);
    _addLinksForCompetitionBloc.resizeImage(_memePostKey);
  }
}
