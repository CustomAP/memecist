import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_basic_widgets.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MemePostForPendingApprovalWidget extends StatefulWidget {
  final MemePost _memePost;

  MemePostForPendingApprovalWidget(this._memePost);

  @override
  _MemePostForPendingApprovalWidgetState createState() =>
      _MemePostForPendingApprovalWidgetState(_memePost);
}

class _MemePostForPendingApprovalWidgetState
    extends State<MemePostForPendingApprovalWidget> {
  final MemePost _memePost;

  _MemePostForPendingApprovalWidgetState(this._memePost);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 20.0.h),
      padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 10.0.h),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0.w))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 10.0.h,
          ),
          headerRow(_memePost, openProfilePage),
          SizedBox(
            height: 10.0.h,
          ),
          Text(
            _memePost.caption,
            style: TextStyle(
              fontSize: 15.0.sp,
            ),
          ),
          SizedBox(
            height: 10.0.h,
          ),
          memeMedia(_memePost.mediaUrl, _memePost.postType),
          SizedBox(
            height: 10.0.h,
          ),
        ],
      ),
    );
  }

  void openProfilePage() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(_memePost.user.userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }
}
