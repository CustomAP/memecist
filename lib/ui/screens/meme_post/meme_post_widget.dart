import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:memecist/core/blocs/explore/hashtag_posts_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_detail_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/explore/hashtag_posts_screen.dart';
import 'package:memecist/ui/screens/meme_post/bounce_widget.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_basic_widgets.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_detail_screen.dart';
import 'package:memecist/ui/screens/meme_world/competitions/competition_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_details_screen.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MemePostWidget extends StatefulWidget {
  final MemePost _memePost;
  bool showDetails = true;
  bool forSharing = false;
  double padding = 0;
  double sizedBoxHeight = 10.h;
  final VoidCallback onDeleteClicked;
  bool showActions = true;

  MemePostWidget(this._memePost,
      {this.showDetails = true,
      this.forSharing = false,
      this.padding = 0,
      //TODO check for responsiveness of this param and entire sharing for ig flow
      this.sizedBoxHeight = 10,
      this.showActions = true,
      this.onDeleteClicked,
      Key key})
      : super(key: key);

  @override
  _MemePostWidgetState createState() => _MemePostWidgetState(
      _memePost, showDetails, forSharing, padding, sizedBoxHeight);
}

class _MemePostWidgetState extends State<MemePostWidget> {
  MemePost _memePost;
  MemePostBloc _memePostBloc;
  bool _showDetails;
  bool _forSharing;
  GlobalKey _globalKey = new GlobalKey();

  bool isFirstRun = true;

  double _sizedBoxHeight;
  double _padding;

  TextEditingController _reportController = TextEditingController();

  _MemePostWidgetState(this._memePost, this._showDetails, this._forSharing,
      this._padding, this._sizedBoxHeight);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _memePostBloc = MemePostBlocProvider.of(context);
      _memePostBloc.initializeBloc(_memePost);

      _memePostBloc.competition.listen((event) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) {
                  return CompetitionDetailsBlocProvider(
                      child: CompetitionDetailsScreen(event));
                },
                settings: RouteSettings(
                    name: FirebaseAnalyticsConstants
                        .competition_details_screen)));
      });

      _memePostBloc.showMentionDetailsDialog.listen((event) {
        showDialog(
            context: context,
            builder: (context) {
              return new AlertDialog(
                title: Text("Introducing Memention!"),
                content: Text(
                    "Mention your friends in memes by adding your mobile number!\nThey'll receive notification to view this meme instantly :)"),
                actions: <Widget>[
                  dialogButton("ADD NUMBER", () {
                    Navigator.of(context).pop();
                    showDialog(
                        context: context,
                        builder: (context) {
                          return Text("Hey");
                        });
                  }),
                  dialogButton("CANCEL", () {
                    Fluttertoast.showToast(
                        msg:
                            "Ah! Your friends will probably miss this awesome meme :(");
                    Navigator.of(context).pop();
                  }, color: Colors.red)
                ],
              );
            });
      });

      _memePostBloc.memePost.listen((memePost) {
        _memePost = memePost;
      });

      if (_forSharing) {
        _memePostBloc.setShowMemecist(true);
      }

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _memePostBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return memePost();
  }

  Widget memePost() {
    return Padding(
      padding: _forSharing
          ? EdgeInsets.all(0.0)
          : _showDetails
              ? EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 20.0.h)
              : EdgeInsets.only(left: 10.0.h, right: 10.0.w),
      child: InkWell(
        onLongPress: showMenuDialog,
        child: StreamBuilder(
            stream: _memePostBloc.sizedBoxHeight,
            builder: (context, asyncSnapshot) {
              if (asyncSnapshot.hasData) _sizedBoxHeight = asyncSnapshot.data;
              return StreamBuilder(
                  stream: _memePostBloc.padding,
                  builder: (context, paddingSnapshot) {
                    if (paddingSnapshot.hasData)
                      _padding = paddingSnapshot.data;
                    return dynamicSizedPost();
                  });
            }),
      ),
    );
  }

  Widget dynamicSizedPost() {
    String uw = StringConstants.hashtag_regex;
    String rx = "($uw)?#+($uw+)";
    return RepaintBoundary(
      key: _globalKey,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 10.0.h),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: _forSharing
                ? BorderRadius.all(Radius.circular(0))
                : BorderRadius.all(Radius.circular(10.0.w))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 10.0.h,
            ),
            StreamBuilder(
              stream: _memePostBloc.showMemecist,
              builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data) {
                    return headerRow(_memePost, openProfilePage,
                        openGroupPage: openGroupPage,
                        openWarPage: openWarPage,
                        openCompetitionPage: openCompetitionPage,
                        showDate: false);
                  } else {
                    return headerRow(_memePost, openProfilePage,
                        openGroupPage: openGroupPage,
                        openWarPage: openWarPage,
                        openCompetitionPage: openCompetitionPage);
                  }
                } else {
                  return headerRow(_memePost, openProfilePage,
                      openGroupPage: openGroupPage,
                      openWarPage: openWarPage,
                      openCompetitionPage: openCompetitionPage);
                }
              },
            ),
            SizedBox(
              height: _forSharing ? widget.sizedBoxHeight : _sizedBoxHeight,
            ),
            _memePost.caption != ""
                ? Column(
                    children: <Widget>[
                      ParsedText(
                        alignment: TextAlign.start,
                        text: _memePost.caption,
                        style: TextStyle(fontSize: 15.0, color: Colors.black),
                        parse: <MatchText>[
                          MatchText(
                              pattern: rx,
                              style: TextStyle(
                                color: MemecistColors.primaryColor,
                                fontSize: 15.0,
                              ),
                              onTap: (hashtag) => openPostsForHashtag(hashtag)),
                        ],
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                    ],
                  )
                : Container(),
            BounceWidget(
              onDoubleTap: () => react(displayToast: false),
              child: Padding(
                padding: EdgeInsets.only(
                    /*hack : _padding is not set to widget.padding for unknown reason when called from create meme post*/
                    left: _forSharing ? widget.padding : _padding,
                    right: _forSharing ? widget.padding : _padding),
                child: GestureDetector(
                    onTap: _showDetails ? openMemePostDetailPage : null,
                    child: memeMedia(_memePost.mediaUrl, _memePost.postType)),
              ),
            ),
            SizedBox(
              height: _forSharing ? widget.sizedBoxHeight : _sizedBoxHeight,
            ),
            actionRow()
          ],
        ),
      ),
    );
  }

  Widget actionRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        StreamBuilder(
          stream: _memePostBloc.showMemecist,
          builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
            if (asyncSnapshot.hasData) {
              if (asyncSnapshot.data) {
                return Container();
              } else {
                return actions();
              }
            } else {
              return actions();
            }
          },
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            StreamBuilder(
              stream: _memePostBloc.showMemecist,
              builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data) {
                    return memecistWatermark();
                  } else {
                    return Container();
                  }
                } else {
                  return Container();
                }
              },
            ),
            StreamBuilder(
                stream: _memePostBloc.showMemecist,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data) {
                      return Container();
                    } else {
                      return shareButton();
                    }
                  } else {
                    return shareButton();
                  }
                }),
          ],
        )
      ],
    );
  }

  Widget shareButton() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
//        Material(
//          color: Colors.transparent,
//          child: InkWell(
//            onTap: mention,
//            child: Container(
//                height: 25.0,
//                width: 25.0,
//                child: Image.asset('assets/mention.png')),
//          ),
//        ),
//        SizedBox(
//          width: 20.0,
//        ),
        Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: shareImage,
            child: Container(
              padding: EdgeInsets.only(right: 5.0.w),
              height: 25.0.w,
              width: 25.0.w,
              child: Image.asset('assets/share.png'),
            ),
          ),
        ),
      ],
    );
  }

  Widget actions() {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            width: 10.0.w,
          ),
          InkWell(
            onTap: () => react(displayToast: true),
            child: StreamBuilder(
                stream: _memePostBloc.react,
                builder: (context, AsyncSnapshot<bool> snapshot) {
                  if (snapshot.hasData) {
                    _memePost.isReacted = snapshot.data;
                  }
                  return Row(
                    children: <Widget>[
                      Container(
                        child: _memePost.isReacted
                            ? Container(
                                height: 25.0.w,
                                width: 25.0.w,
                                child: Image.asset('assets/haha.png'),
                              )
                            : Icon(
                                Icons.insert_emoticon,
                                color: Colors.grey,
                                size: 25.0.w,
                              ),
                      ),
                      SizedBox(
                        width: 7.0.w,
                      ),
                      Text(
                        NumberFormat.compact().format(_memePost.reactCount),
                        style: TextStyle(fontSize: 14.0.sp),
                      ),
                    ],
                  );
                }),
          ),
          SizedBox(
            width: 20.0.w,
          ),
          InkWell(
            onTap: _showDetails ? openMemePostDetailPage : null,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                    height: 25.0.w,
                    width: 25.0.w,
                    child: Image.asset('assets/comment.png')),
                SizedBox(
                  width: 7.0.w,
                ),
                Text(
                  NumberFormat.compact().format(_memePost.commentsCount),
                  style: TextStyle(fontSize: 14.0.sp),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget memecistWatermark() {
    return Container(
      padding: EdgeInsets.only(right: 10.0.w),
      child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        Image.asset(
          'assets/mc.png',
          height: 25.0.w,
          width: 25.0.w,
        ),
        SizedBox(
          width: 5.0.w,
        ),
        Text("memecist/${_memePost.user.userName}",
            style: TextStyle(
                fontSize: 14.0.sp,
                fontWeight: FontWeight.bold,
                fontFamily: 'Quicksand',
                color: Colors.black)),
      ]),
    );
  }

  void react({bool displayToast}) {
    if (displayToast) {
      Fluttertoast.showToast(
          msg: "You can just double tap the meme to react :P");
    }
    _memePostBloc.reactUnreactToPost(!_memePost.isReacted);
  }

  void openMemePostDetailPage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return MemePostDetailBlocProvider(
                  child: MemePostDetailScreen(
                false,
                memePost: _memePost,
              ));
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.meme_post_detail_screen)));
  }

  void openProfilePage() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(_memePost.user.userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }

  void openGroupPage() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return GroupDetailsBlocProvider(
            child: GroupDetailsScreen(_memePost.groupID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.group_details_screen)));
  }

  void openWarPage() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return WarDetailsBlocProvider(
        child: WarDetailsScreen(_memePost.warID),
      );
    }));
  }

  void openCompetitionPage() {
    _memePostBloc.getCompetition(_memePost.competitionID);
  }

  void mention() {
    _memePostBloc.mention(_memePost.postID);
  }

  Future<void> shareImage() async {
    _memePostBloc.shareImage(_globalKey);
  }

  Future<void> showMenuDialog() async {
    HapticFeedback.vibrate();

    User currentUser = await repository.getLoggedInUser();

    // don't want to show this dialog when post is created and preview is shown
    if (widget.showActions) {
      return await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          String title = "Options";
          String option1 = "Report Meme";
          String option2 = "Delete Meme";
          return Dialog(
            elevation: 8.0.w,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0.w)),
            child: Padding(
              padding: EdgeInsets.all(10.0.w),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Text(
                      title,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Quicksand',
                          color: MemecistColors.primaryColor,
                          fontSize: 16.0.sp),
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Divider(
                      color: Colors.black,
                      thickness: 1.0.w,
                    ),
                    _memePost.user.userID != currentUser.uid
                        ? InkWell(
                            onTap: () => showReportDialog(),
                            child: Padding(
                              //For wider touch area
                              padding: EdgeInsets.only(
                                  top: 10.0.h,
                                  bottom: 10.0.h,
                                  left: 50.0.w,
                                  right: 50.0.w),
                              child: Container(
                                child: Text(
                                  option1,
                                  style: TextStyle(
                                      fontFamily: 'Quicksand',
                                      fontSize: 15.0.sp),
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    _memePost.user.userID == currentUser.uid
                        ? InkWell(
                            onTap: () => showDeletePostDialog(),
                            child: Padding(
                              //For wider touch area
                              padding: EdgeInsets.only(
                                  top: 10.0.h,
                                  bottom: 10.0.h,
                                  left: 50.0.w,
                                  right: 50.0.w),
                              child: Container(
                                child: Text(
                                  option2,
                                  style: TextStyle(
                                      fontFamily: 'Quicksand',
                                      fontSize: 15.0.sp),
                                ),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          );
        },
      );
    }
  }

  Future<void> showReportDialog() async {
    return await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "Report Meme";
        String btnLabel = "Send";
        String btnLabelCancel = "Cancel";
        return WillPopScope(
          onWillPop: () => Future.value(false),
          child: Dialog(
            elevation: 8.0.w,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0.w)),
            child: Padding(
              padding: EdgeInsets.all(10.0.w),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Quicksand',
                          color: MemecistColors.primaryColor,
                          fontSize: 16.0.sp),
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    TextField(
                      keyboardType: TextInputType.multiline,
                      controller: _reportController,
                      maxLines: 5,
                      textCapitalization: TextCapitalization.sentences,
                      style: TextStyle(
                        fontSize: 16.0.sp
                      ),
                      decoration: InputDecoration(
                          hintText: "Reason",
                          hintStyle: TextStyle(fontSize: 14.0.sp),
                          contentPadding: EdgeInsets.only(
                              left: 10.0.w,
                              right: 10.0.w,
                              top: 10.0.h,
                              bottom: 10.0.h),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0.w),
                              borderSide: BorderSide(
                                  color: MemecistColors.primaryColor,
                                  width: 2.25.w)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0.w),
                              borderSide: BorderSide(color: Colors.white))),
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        FlatButton(
                          child: Text(
                            btnLabelCancel,
                            style: TextStyle(fontSize: 16.0.sp),
                          ),
                          textColor: MemecistColors.primaryColor,
                          onPressed: () => dismissReportDialog(),
                        ),
                        FlatButton(
                            textColor: MemecistColors.primaryColor,
                            child: Text(
                              btnLabel,
                              style: TextStyle(fontSize: 16.0.sp),
                            ),
                            onPressed: () => sendReport()),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> showDeletePostDialog() async {
    return await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "Delete Meme?";
        String message = "This action cannot be undone!";
        String btnLabel = "Delete";
        String btnLabelCancel = "Cancel";
        return WillPopScope(
          onWillPop: () => Future.value(false),
          child: Dialog(
            elevation: 8.0.w,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0.w)),
            child: Padding(
              padding: EdgeInsets.all(10.0.w),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Text(
                      title,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Quicksand',
                          color: MemecistColors.primaryColor,
                          fontSize: 17.0.sp),
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Text(
                      message,
                      style:
                          TextStyle(fontFamily: 'Quicksand', fontSize: 14.0.sp),
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        FlatButton(
                          child: Text(
                            btnLabelCancel,
                            style: TextStyle(fontSize: 16.0.sp),
                          ),
                          textColor: MemecistColors.primaryColor,
                          onPressed: () => dismissDeletePostDialog(),
                        ),
                        FlatButton(
                            textColor: MemecistColors.primaryColor,
                            child: Text(
                              btnLabel,
                              style: TextStyle(fontSize: 16.0.sp),
                            ),
                            onPressed: () => deletePost()),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  dismissDeletePostDialog() {
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.dismiss_delete_comment_prompt);
    Navigator.of(context).pop();
  }

  void deletePost() {
    //Analytics event to be handled in callers ondeleteclicked
    widget.onDeleteClicked();
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    Fluttertoast.showToast(msg: "Meme deleted!");
  }

  void sendReport() {
    _memePostBloc.reportMeme(_reportController.text);
    Navigator.of(context).pop();
    Fluttertoast.showToast(msg: "Thank you for letting us know!");
  }

  void dismissReportDialog() {
    Navigator.pop(context);
  }

  void openPostsForHashtag(String hashtag) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return HashtagPostsBlocProvider(
            child: HashtagPostsScreen(hashtag),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.hashtag_posts_screen)));
  }
}
