import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/war_room/war_room_chat_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/war_room/war_rooms_bloc.dart';
import 'package:memecist/core/blocs/meme_world/war_room/war_rooms_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_room_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/war_room/war_room_chat_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WarRoomsScreen extends StatefulWidget {
  @override
  _WarRoomsScreenState createState() => _WarRoomsScreenState();
}

class _WarRoomsScreenState extends State<WarRoomsScreen> {
  WarRoomsBloc _warRoomsBloc;
  bool isFirstRun = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWithBackButtonAndTitle("War Rooms", context),
      backgroundColor: MemecistColors.backgroundColor,
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 15.0.h,
          ),
          StreamBuilder(
              stream: _warRoomsBloc.warRoomsList,
              builder: (context, AsyncSnapshot<List<WarRoom>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "Participate in meme war and discuss strategies here!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return warRoomsList(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _warRoomsBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING WAR ROOMS...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget warRoomsList(List<WarRoom> warRooms) {
    return ListView.builder(
        itemCount: warRooms.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () => openWarRoom(warRooms[index]),
            child: Padding(
              padding: EdgeInsets.only(
                  left: 10.0.w, right: 10.0.w, bottom: 15.0.h, top: 5.0.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  circularImageButton(warRooms[index].teamPic,
                      size: 50.0.w, radius: 25.0.w),
                  SizedBox(
                    width: 10.0.w,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${warRooms[index].warName}",
                          style: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 16.0.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10.0.h,
                        ),
                        warRooms[index].isUnRead
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    child: AutoSizeText(
                                      "${warRooms[index].lastMessage}",
                                      softWrap: true,
                                      maxLines: 1,
                                      minFontSize: 12.0.sp,
                                      stepGranularity: 12.0.sp/12,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Quicksand',
                                          fontSize: 14.0.sp,
                                          color: Colors.black),
                                    ),
                                  ),
                                  Container(
                                    width: 10.w,
                                    height: 10.h,
                                    child: Icon(
                                      Icons.fiber_manual_record,
                                      size: 5.w,
                                      color: Colors.red,
                                    ),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.red),
                                  )
                                ],
                              )
                            : Text(
                                "${warRooms[index].lastMessage}",
                                softWrap: true,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontSize: 14.0.sp,
                                ),
                              )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _warRoomsBloc = WarRoomsBlocProvider.of(context);
      _warRoomsBloc.fetchWarRooms();
      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _warRoomsBloc.dispose();
    super.dispose();
  }

  void openWarRoom(WarRoom warRoom) {
    _warRoomsBloc.warRooms.forEach((warRoomInstance) {
      if (warRoomInstance.warRoomID == warRoom.warRoomID) {
        warRoomInstance.isUnRead = false;
      }
    });
    _warRoomsBloc.setWarRoomsList(_warRoomsBloc.warRooms);

    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return WarRoomChatBlocProvider(
            child: WarRoomChatScreen(warRoom),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.war_room_chat_screen)));
  }
}
