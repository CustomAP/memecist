import 'dart:io';
import 'package:app_settings/app_settings.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/meme_world/war_room/war_room_chat_bloc.dart';
import 'package:memecist/core/blocs/meme_world/war_room/war_room_chat_bloc_provider.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/message_model.dart';
import 'package:memecist/core/models/war_room_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/war_room/full_photo_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WarRoomChatScreen extends StatefulWidget {
  final WarRoom _warRoom;
  bool toLoad;

  WarRoomChatScreen(this._warRoom, {this.toLoad = false});

  @override
  _WarRoomChatScreenState createState() =>
      _WarRoomChatScreenState(_warRoom, toLoad);
}

class _WarRoomChatScreenState extends State<WarRoomChatScreen> {
  WarRoom _warRoom;
  WarRoomChatBloc _warRoomChatBloc;
  TextEditingController _messageController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  List<Message> messages = [];
  User currentUser;
  bool _hasMoreMessages = true;
  bool _toLoad;

  ChatScreenTracker _chatScreenTracker;

  TextEditingController _teamNameController = TextEditingController();

  _WarRoomChatScreenState(this._warRoom, this._toLoad);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _warRoomChatBloc = WarRoomChatBlocProvider.of(context);
      _chatScreenTracker = ObserverProvider.chatScreenTrackerOf(context);
      _chatScreenTracker.chatScreenID = _warRoom.warRoomID;

      if (!_toLoad) {
        _warRoomChatBloc.setWarRoom(_warRoom);
        _warRoomChatBloc.initializeWarRoomChatBloc(_warRoom);
      }

      _warRoomChatBloc.hasMoreMessages.listen((hasMore) {
        _hasMoreMessages = hasMore;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _chatScreenTracker.chatScreenID = "";
    _warRoomChatBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: MemecistColors.backgroundColor,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              onPressed: () => Navigator.of(context).pop()),
          title: StreamBuilder(
              stream: _warRoomChatBloc.warRoom,
              builder: (context, AsyncSnapshot<WarRoom> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  _warRoom = asyncSnapshot.data;
                  return Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      circularImageButton(_warRoom.teamPic,
                          size: 30.0.w, radius: 15.0.w),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      Flexible(
                        child: AutoSizeText(
                          _warRoom.teamName,
                          maxLines: 1,
                          minFontSize: 12.0.sp,
                          stepGranularity: 12.0.sp / 12,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Quicksand',
                              color: MemecistColors.primaryColor),
                        ),
                      ),
                    ],
                  );
                } else {
                  return Text(
                    "War Room",
                    style: TextStyle(
                        fontSize: 16.0.sp,
                        fontFamily: 'Quicksand',
                        color: MemecistColors.primaryColor),
                  );
                }
              }),
          actions: [
            IconButton(
              icon: Icon(
                Icons.edit,
                color: MemecistColors.iconGreyBackgroundColor,
              ),
              onPressed: () => openEditTeamInfoDialog(),
            )
          ],
        ),
        body: StreamBuilder(
            stream: _warRoomChatBloc.warRoom,
            builder: (context, AsyncSnapshot<WarRoom> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                return warRoomWidget();
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }

  Widget warRoomWidget() {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            StreamBuilder(
                stream: _warRoomChatBloc.currentUser,
                builder: (context, AsyncSnapshot<User> asyncSnapshot) {
                  if (asyncSnapshot.hasData) {
                    currentUser = asyncSnapshot.data;
                    return buildListMessage();
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
            buildInput(),
          ],
        ),
        StreamBuilder(
          stream: _warRoomChatBloc.isLoadingForImageUpload,
          builder: (context, AsyncSnapshot<bool> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return Positioned(
                    child: Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                  color: Colors.white.withOpacity(0.8),
                ));
              } else {
                return Container();
              }
            } else {
              return Container();
            }
          },
        )
      ],
    );
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 1.0.w),
              child: new IconButton(
                icon: new Icon(Icons.image),
                onPressed: getImage,
                color: Colors.black,
              ),
            ),
            color: Colors.white,
          ),
          Flexible(
            child: Container(
              child: TextField(
                  controller: _messageController,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(fontSize: 16.0.sp),
                  decoration: InputDecoration.collapsed(
                    hintText: 'Type your message...',
                    hintStyle: TextStyle(color: Colors.grey, fontSize: 14.0.sp),
                  ),
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(2000),
                  ]),
            ),
          ),
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 8.0.w),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: () => sendMessage(_messageController.text, 0),
                color: Colors.black,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 55.0.h,
      decoration: new BoxDecoration(
          border: new Border(
              top: new BorderSide(
                  color: MemecistColors.borderColorGrey, width: 0.5.w)),
          color: Colors.white),
    );
  }

  Widget buildListMessage() {
    return Flexible(
      child: StreamBuilder(
        stream: _warRoomChatBloc.messageList,
        builder: (context, AsyncSnapshot<List<Message>> snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            messages = snapshot.data;
            if (messages.length < 20) _hasMoreMessages = false;
            return ListView.builder(
              padding: EdgeInsets.all(10.0.w),
              itemBuilder: (context, index) {
                if (index == messages.length) {
                  return StreamBuilder(
                      stream: _warRoomChatBloc.isLoadingMoreMessages,
                      builder: (context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data) {
                            return UnconstrainedBox(
                                child: Padding(
                              padding: EdgeInsets.all(10.0.w),
                              child:
                                  greyedOutButton(context, "Loading...", null),
                            ));
                          } else {
                            return _hasMoreMessages
                                ? UnconstrainedBox(
                                    child: Padding(
                                    padding: EdgeInsets.all(10.0.w),
                                    child: greyedOutButton(context,
                                        "Load More Messages", loadMoreMessages,
                                        width: 200.0),
                                  ))
                                : Container();
                          }
                        } else {
                          return _hasMoreMessages
                              ? UnconstrainedBox(
                                  child: Padding(
                                  padding: EdgeInsets.all(10.0.w),
                                  child: greyedOutButton(context,
                                      "Load More Messages", loadMoreMessages,
                                      width: 200.0),
                                ))
                              : Container();
                        }
                      });
                }
                return messageItem(index, snapshot.data[index], currentUser);
              },
              itemCount: snapshot.data.length + 1,
              reverse: true,
              controller: _scrollController,
            );
          }
        },
      ),
    );
  }

  void loadMoreMessages() {
    _warRoomChatBloc.loadMoreMessages();
  }

  void getImage() async {
    try {
      File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (imageFile != null) {
        Fluttertoast.showToast(msg: "Please wait until photo is uploaded");
        _warRoomChatBloc.uploadImage(imageFile);
      }
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }

  sendMessage(String text, int i) async {
    if (_messageController.text.length == 0) {
      return;
    }
    _messageController.clear();
    await _warRoomChatBloc.sendMessage(text, FirestoreConstants.text_message);
    _scrollController.animateTo(0.0,
        duration: Duration(milliseconds: 300), curve: Curves.easeOut);
  }

  Widget messageItem(int index, Message message, User currentUser) {
    if (message.sender.userID == currentUser.uid) {
      return rightMessage(index, message);
    } else {
      return leftMessage(index, message);
    }
  }

  Widget leftMessage(int index, Message message) {
    return Container(
      child: Column(
        children: <Widget>[
          isNextMessageLeftAndFromSameParticipant(index)
              ? Container(
                  child: Text(
                    message.sender.userName,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 13.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                  margin:
                      EdgeInsets.only(left: 50.0.w, top: 5.0.h, bottom: 5.0.h),
                )
              : Container(),
          Row(
            children: <Widget>[
              isLastMessageLeftAndFromDifferentParticipant(index)
                  ? Material(
                      child: CachedNetworkImage(
                        placeholder: (context, url) => Container(
                          child: CircularProgressIndicator(),
                          width: 35.0.w,
                          height: 35.0.w,
                          padding: EdgeInsets.all(10.0.w),
                        ),
                        imageUrl: message.sender.profilePic,
                        width: 35.0.w,
                        height: 35.0.w,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(18.0.w),
                      ),
                      clipBehavior: Clip.hardEdge,
                    )
                  : Container(width: 35.0.w),
              message.type == FirestoreConstants.text_message
                  ? Container(
                      child: textMessage(index, message),
                      margin: EdgeInsets.only(left: 10.0.w),
                    )
                  : message.type == FirestoreConstants.image_message
                      ? Container(
                          child: imageMessage(index, message),
                          margin: EdgeInsets.only(left: 10.0.w),
                        )
                      : Container()
            ],
          ),
          isLastMessageLeftAndFromDifferentParticipant(index)
              ? Container(
                  child: Text(
                    DateFormat('dd MMM kk:mm').format(message.createDateTime),
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 12.0.sp,
                        fontStyle: FontStyle.italic),
                  ),
                  margin:
                      EdgeInsets.only(left: 50.0.w, top: 5.0.h, bottom: 5.0.h),
                )
              : Container()
        ],
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
      margin: EdgeInsets.only(bottom: 10.0.h),
    );
  }

  Widget rightMessage(int index, Message message) {
    return Row(
      children: <Widget>[
        message.type == FirestoreConstants.text_message
            ? Container(
                margin: EdgeInsets.only(
                    bottom: isLastMessageRight(index) ? 20.0.h : 10.0.h,
                    right: 10.0.w),
                child: textMessage(index, message),
              )
            : message.type == FirestoreConstants.image_message
                ? Container(
                    margin: EdgeInsets.only(
                        bottom: isLastMessageRight(index) ? 20.0.h : 10.0.h,
                        right: 10.0.w),
                    child: imageMessage(index, message),
                  )
                : Container()
      ],
      mainAxisAlignment: MainAxisAlignment.end,
    );
  }

  Widget textMessage(int index, Message message) {
    return Container(
      constraints:
          BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 3 / 4),
      child: Text(
        message.content,
        style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
      ),
      padding: EdgeInsets.fromLTRB(15.0.w, 10.0.h, 15.0.w, 10.0.h),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[300]),
          color: Colors.white,
          borderRadius: BorderRadius.circular(20.0.w)),
    );
  }

  Widget imageMessage(int index, Message message) {
    return FlatButton(
      child: Material(
        child: CachedNetworkImage(
          placeholder: (context, url) => Container(
            child: CircularProgressIndicator(),
            width: 200.0.w,
            height: 200.0.w,
            padding: EdgeInsets.all(70.0.w),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(8.0.w),
              ),
            ),
          ),
          imageUrl: message.content,
          width: 200.0.w,
          height: 200.0.w,
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.all(Radius.circular(8.0.w)),
        clipBehavior: Clip.hardEdge,
      ),
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => FullPhotoScreen(url: message.content),
                settings: RouteSettings(
                    name: FirebaseAnalyticsConstants.full_photo_screen)));
      },
      padding: EdgeInsets.all(0),
    );
  }

  Widget editTeamInfoDialog(bool toAllowTeamRename) {
    return Dialog(
        elevation: 8.0.w,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0.w)),
        child: StreamBuilder(
          stream: _warRoomChatBloc.isLoadingForTeamPicUpload,
          builder: (context, AsyncSnapshot<bool> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return Container(
                  height: 70.0.w,
                  width: 70.0.w,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                  color: Colors.white.withOpacity(0.8),
                );
              } else {
                return editTeamInfoDialogContents(toAllowTeamRename);
              }
            } else {
              return editTeamInfoDialogContents(toAllowTeamRename);
            }
          },
        ));
  }

  Widget editTeamInfoDialogContents(bool toAllowTeamRename) {
    String title = "Edit Team Info";
    String message = "PS : You can only rename the team once!";
    String btnLabel = "Rename";
    String btnLabelCancel = "Cancel";
    return Padding(
      padding: EdgeInsets.all(10.0.w),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 10.0.h,
            ),
            Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Quicksand',
                  color: MemecistColors.primaryColor,
                  fontSize: 17.0.sp),
            ),
            SizedBox(
              height: 10.0.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                circularImageButton(_warRoom.teamPic,
                    size: 50.0.w, radius: 25.0.w),
                SizedBox(
                  width: 20.0.w,
                ),
                FlatButton(
                  child: Text(
                    "CHANGE",
                    style: TextStyle(fontSize: 16.0.sp),
                  ),
                  onPressed: () => changeTeamPic(),
                )
              ],
            ),
            SizedBox(
              height: 10.0.h,
            ),
            !toAllowTeamRename
                ? Container()
                : Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        message,
                        style: TextStyle(
                            fontFamily: 'Quicksand', fontSize: 14.0.sp),
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      TextField(
                        controller: _teamNameController,
                        textCapitalization: TextCapitalization.sentences,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(50),
                        ],
                        style: TextStyle(fontSize: 16.0.sp),
                        decoration: InputDecoration(
                            hintText: "Team name",
                            hintStyle: TextStyle(fontSize: 14.0.sp),
                            contentPadding: EdgeInsets.only(
                                left: 10.0.w,
                                right: 10.0.w,
                                top: 10.0.h,
                                bottom: 10.0.h),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0.w),
                                borderSide: BorderSide(
                                    color: MemecistColors.primaryColor,
                                    width: 2.25.w)),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0.w),
                                borderSide: BorderSide(color: Colors.white))),
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          FlatButton(
                            child: Text(
                              btnLabelCancel,
                              style: TextStyle(fontSize: 16.0.sp),
                            ),
                            textColor: MemecistColors.primaryColor,
                            onPressed: () => dismissEditTeamInfoDialog(),
                          ),
                          FlatButton(
                              textColor: MemecistColors.primaryColor,
                              child: Text(
                                btnLabel,
                                style: TextStyle(fontSize: 16.0.sp),
                              ),
                              onPressed: () => renameTeam()),
                        ],
                      )
                    ],
                  ),
          ],
        ),
      ),
    );
  }

  bool isNextMessageLeftAndFromSameParticipant(index) {
    if ((index < messages.length - 1 &&
            messages != null &&
            messages[index].sender.userID !=
                messages[index + 1].sender.userID) ||
        index == messages.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageLeftAndFromDifferentParticipant(index) {
    if ((index > 0 &&
            messages != null &&
            messages[index].sender.userID !=
                messages[index - 1].sender.userID) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 &&
            messages != null &&
            messages[index - 1].sender.userID != currentUser.uid) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> openEditTeamInfoDialog() async {
    _warRoomChatBloc.checkToAllowTeamRename(_warRoom.warID, _warRoom.teamID);
    return await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return StreamBuilder(
          stream: _warRoomChatBloc.toAllowTeamRename,
          builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
            if (asyncSnapshot.hasData) {
              return editTeamInfoDialog(asyncSnapshot.data);
            } else {
              return Dialog(
                  elevation: 8.0.w,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0.w)),
                  child: Padding(
                    padding: EdgeInsets.all(10.0.w),
                    child: Text(
                      "Loading...",
                      style:
                          TextStyle(fontFamily: 'Quicksand', fontSize: 16.0.sp),
                    ),
                  ));
            }
          },
        );
      },
    );
  }

  dismissEditTeamInfoDialog() {
    repository.firebaseAnalytics.logEvent(
        name: FirebaseAnalyticsConstants.dismiss_edit_team_info_prompt);
    Navigator.of(context).pop();
  }

  renameTeam() async {
    String teamName = _teamNameController.text;
    if (teamName.length > 0) {
      if (teamName.length > 50) {
        Fluttertoast.showToast(
            msg: "Team name cannot be greater than 50 characters");
        return;
      }
      _warRoomChatBloc.renameTeam(_warRoom.warID, _warRoom.teamID, teamName);
      Fluttertoast.showToast(msg: "Team renamed!");
      Navigator.of(context).pop();
    }
  }

  changeTeamPic() async {
    try {
      var selectedImage =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      if (selectedImage != null) {
        Fluttertoast.showToast(msg: "Please wait until photo is uploaded");
        _warRoomChatBloc.uploadTeamPic(selectedImage);
      }
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }
}
