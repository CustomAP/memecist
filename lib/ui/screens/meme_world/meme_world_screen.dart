import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_tab_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/groups_tab_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/war_room/war_rooms_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/competitions/competitions_tab_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/groups_tab_screen.dart';
import 'package:memecist/ui/screens/meme_world/war_room/war_rooms_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MemeWorldScreen extends StatefulWidget {
  @override
  _MemeWorldScreenState createState() => _MemeWorldScreenState();
}

class _MemeWorldScreenState extends State<MemeWorldScreen>
    with
        SingleTickerProviderStateMixin,
        AutomaticKeepAliveClientMixin,
        RouteAware {
  TabController _tabController;
  FirebaseAnalyticsObserver observer;
  String screenName = "";
  int selectedIndex = 0;
  bool isFirstRun = true;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      if (selectedIndex != _tabController.index) {
        selectedIndex = _tabController.index;
        switch (selectedIndex) {
          case 0:
            screenName = FirebaseAnalyticsConstants.groups_tab_screen;
            break;
          case 1:
            screenName = FirebaseAnalyticsConstants.competition_tab_screen;
            break;
        }
        if (screenName != "") _sendCurrentTabToAnalytics();
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      observer = ObserverProvider.of(context);
      observer.subscribe(this, ModalRoute.of(context));

      screenName = FirebaseAnalyticsConstants.groups_tab_screen;
      _sendCurrentTabToAnalytics();
      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: AppBar(
        centerTitle: true,
        brightness:
            Platform.isAndroid ? AppBarTheme().brightness : Brightness.light,
        title: Text(
          "Meme World",
          style: TextStyle(
              fontSize: 22.0.sp,
              fontFamily: 'Quicksand',
              color: MemecistColors.primaryColor),
        ),
//        leading: Padding(
//          padding: const EdgeInsets.all(15.0),
//          child: Container(
//            width: 36.0,
//            height: 36.0,
//            child: new Material(
//              color: Colors.transparent,
//              child: InkWell(
//                onTap: openWarRoom,
//                child: Image.asset('assets/mention_black.png'),
//              ),
//            ),
//          ),
//        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 15.0.w),
            child: Container(
              width: 36.0.w,
              height: 36.0.w,
              child: new Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: openWarRoom,
                  child: Image.asset('assets/swords.png'),
                ),
              ),
            ),
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: Column(
        children: <Widget>[
          TabBar(
              controller: _tabController,
              indicatorColor: MemecistColors.primaryColor,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorWeight: 4.0.w,
              labelColor: Colors.black,
              unselectedLabelColor: Colors.black,
              tabs: <Widget>[
                Tab(
                  child: Text(
                    "GROUPS",
                    style:
                        TextStyle(fontFamily: 'Quicksand', fontSize: 15.0.sp),
                  ),
                ),
                Tab(
                  child: Text(
                    "COMPETITIONS",
                    style:
                        TextStyle(fontFamily: 'Quicksand', fontSize: 15.0.sp),
                  ),
                ),
              ]),
          SizedBox(
            height: 10.0.h,
          ),
          Expanded(
            child: TabBarView(controller: _tabController, children: <Widget>[
              GroupsTabBlocProvider(
                child: GroupsTabScreen(),
              ),
              CompetitionsTabBlocProvider(child: CompetitionsTabScreen()),
            ]),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  openWarRoom() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return WarRoomsBlocProvider(
            child: WarRoomsScreen(),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.war_rooms_screen)));
  }

  void _sendCurrentTabToAnalytics() {
    observer.analytics.setCurrentScreen(
      screenName: screenName,
    );
  }
}
