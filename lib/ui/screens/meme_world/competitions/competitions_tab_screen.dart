import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_tab_bloc.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_tab_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/competitions/competition_details_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';

class CompetitionsTabScreen extends StatefulWidget {
  @override
  _CompetitionsTabScreenState createState() => _CompetitionsTabScreenState();
}

class _CompetitionsTabScreenState extends State<CompetitionsTabScreen>
    with AutomaticKeepAliveClientMixin {
  CompetitionsTabBloc _competitionsTabBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreCompetitions = true;
  bool _isLoading = false;

  @override
  void dispose() {
    _scrollController.dispose();
    _competitionsTabBloc.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _competitionsTabBloc = CompetitionsTabBlocProvider.of(context);

      _competitionsTabBloc.fetchCompetitions();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreCompetitions &&
            !_isLoading) {
          _competitionsTabBloc.fetchCompetitions();
        }
      });

      _competitionsTabBloc.hasMoreGroups.listen((hasMoreData) {
        _hasMoreCompetitions = hasMoreData;
      });

      _competitionsTabBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });
      isFirstRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return LiquidPullToRefresh(
      onRefresh: () => refreshScreen(),
      showChildOpacityTransition: false,
      color: MemecistColors.refreshBackgroundColor,
      animSpeedFactor: 2.0,
      child: ListView(
        shrinkWrap: true,
        controller: _scrollController,
        children: <Widget>[
          SizedBox(
            height: 10.0.h,
          ),
          StreamBuilder(
              stream: _competitionsTabBloc.competitionsList,
              builder:
                  (context, AsyncSnapshot<List<Competition>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO COMPETITIONS ACTIVE!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return competitionsList(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _competitionsTabBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING COMPETITIONS...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget competitionsList(List<Competition> groups) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: groups.length,
        itemBuilder: (context, index) {
          return competitionWithHostCard(groups[index]);
        });
  }

  Widget competitionWithHostCard(Competition competition) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: InkWell(
            onTap: () => openProfileScreen(competition.hostedBy.userID),
            child: Container(
              padding: EdgeInsets.only(
                  top: 20.0.h, bottom: 20.0.h, left: 10.0.w, right: 10.0.w),
              child: Column(
                children: <Widget>[
                  Container(
                    width: 60.0.w,
                    height: 60.0.w,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.0.w)),
                        image: DecorationImage(
                            image: CachedNetworkImageProvider(
                                competition.hostedBy.profilePic),
                            fit: BoxFit.cover)),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  AutoSizeText(
                    "${competition.hostedBy.firstName} ${competition.hostedBy.lastName}",
                    maxLines: 2,
                    minFontSize: 12.0.sp,
                    stepGranularity: 12.0.sp/12,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0.sp,
                        color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => openCompetitionDetails(competition),
              child: Container(
                padding: EdgeInsets.all(20.0.w),
                margin: EdgeInsets.all(10.0.w),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0.w)),
                  color: Color(int.parse(competition.cardColor.substring(1, 7),
                          radix: 16) +
                      0xFF000000),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: 40.0.w,
                        height: 40.0.w,
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(30.0.w)),
                            image: DecorationImage(
                                image: CachedNetworkImageProvider(
                                    competition.competitionPic),
                                fit: BoxFit.cover))),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    AutoSizeText(
                      competition.competitionName,
                      maxLines: 1,
                      minFontSize: 12.0.sp,
                      stepGranularity: 12.0.sp/12,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: Colors.white,
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Text(
                      competition.prizesInShort,
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: Colors.white,
                          fontSize: 13.0.sp,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  void openCompetitionDetails(Competition competition) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return CompetitionDetailsBlocProvider(
                  child: CompetitionDetailsScreen(competition));
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.competition_details_screen)));
  }

  void openProfileScreen(String userID) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return ProfileBlocProvider(
                  child: ProfileScreen(userID));
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.competition_details_screen)));
  }

  Future<void> refreshScreen() async {
    _hasMoreCompetitions = true;
    _isLoading = false;
    _competitionsTabBloc.refreshScreen();
    _competitionsTabBloc.fetchCompetitions();
  }

  @override
  bool get wantKeepAlive => true;
}
