import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_post/create_meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_detail_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competition_details_bloc.dart';
import 'package:memecist/core/blocs/meme_world/competitions/competitions_details_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/competition_model.dart';
import 'package:memecist/core/models/competition_winner_group.dart';
import 'package:memecist/core/models/competition_winner_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/create_meme_post_screen.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_detail_screen.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CompetitionDetailsScreen extends StatefulWidget {
  final Competition _competition;

  CompetitionDetailsScreen(this._competition);

  @override
  _CompetitionDetailsScreenState createState() =>
      _CompetitionDetailsScreenState(_competition);
}

class _CompetitionDetailsScreenState extends State<CompetitionDetailsScreen> {
  Competition _competition;
  bool isFirstRun = true;
  List<CompetitionWinnerGroup> winners = [];

  GlobalKey _globalKey = GlobalKey();

  CompetitionDetailsBloc _competitionDetailsBloc;

  _CompetitionDetailsScreenState(this._competition);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _competitionDetailsBloc = CompetitionDetailsBlocProvider.of(context);
    if (isFirstRun &&
        _competition.competitionStatus == FirestoreConstants.winners_declared) {
      _competitionDetailsBloc.initializeCompetitionDetailsBloc(_competition);
      _competitionDetailsBloc.getWinners(_competition.competitionID);
    }
  }

  @override
  void dispose() {
    _competitionDetailsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButton(context),
      body: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          competitionHeader(),
          SizedBox(
            height: 20.0.h,
          ),
          _competition.requirePost && _competition.isCompetitionActive
              ? Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: <Widget>[
                      actionButton(context, "Post Meme", postMeme),
                      SizedBox(
                        height: 20.0.h,
                      ),
                    ],
                  ),
                )
              : Container(),
          _competition.competitionStatus ==
                  FirestoreConstants.waiting_for_results
              ? Center(
                  child: Text(
                    'Winners will be declared soon',
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 18.0.sp,
                        color: MemecistColors.primaryColor),
                  ),
                )
              : Container(),
          _competition.competitionStatus == FirestoreConstants.winners_declared
              ? winnersWidget()
              : Container(),
          _competition.competitionStatus == FirestoreConstants.accepting_memes
              ? Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(left: 10.0.w, bottom: 10.0.h),
                        child: Text(
                          "RULES",
                          style: TextStyle(
                              fontFamily: "Quicksand",
                              fontSize: 14.0.sp,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                      child: rulesWidget(),
                    )
                  ],
                )
              : Container(),
          SizedBox(
            height: 20.0.h,
          )
        ],
      )),
    );
  }

  Widget winnersWidget() {
    return Column(
      children: <Widget>[
        StreamBuilder(
          stream: _competitionDetailsBloc.winnersList,
          builder: (context,
              AsyncSnapshot<List<CompetitionWinnerGroup>> asyncSnapshot) {
            if (asyncSnapshot.hasData) {
              winners = asyncSnapshot.data;
              return Column(
                children: <Widget>[
                  competitionWinnerCard(),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.0.w, bottom: 10.0.h),
                      child: Text(
                        "WINNERS",
                        style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 14.0.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  winnerGroupsList(asyncSnapshot.data)
                ],
              );
            } else {
              return Center(
                child: Text(
                  "LOADING WINNERS...",
                  style: TextStyle(fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                ),
              );
            }
          },
        ),
        SizedBox(
          height: 20.0.h,
        )
      ],
    );
  }

  Widget winnerGroupsList(
      List<CompetitionWinnerGroup> competitionWinnerGroups) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: competitionWinnerGroups.length,
        itemBuilder: (context, index) {
          return Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 10.0.w),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    circularImageButton(
                        FirebaseStorageConstants.competitionWinnerTrophy),
                    SizedBox(
                      width: 10.0.w,
                    ),
                    Flexible(
                      child: AutoSizeText(
                        competitionWinnerGroups[index].position,
                        minFontSize: 12.0.sp,
                        stepGranularity: 12.0.sp/12,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Quicksand',
                            fontSize: 16.0.sp),
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
              ),
              winnersList(competitionWinnerGroups[index].competitionWinners)
            ],
          );
        });
  }

  Widget winnersList(List<CompetitionWinner> competitionWinners) {
    return Card(
      child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: competitionWinners.length,
          itemBuilder: (context, i) {
            return Padding(
              padding: EdgeInsets.all(10.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () => openProfile(competitionWinners[i].user.userID),
                    child: userRowItem(competitionWinners[i].user, true),
                  ),
                  competitionWinners[i].postApplicable
                      ? dialogButton("View Meme",
                          () => fetchMeme(competitionWinners[i].postID))
                      : Container()
                ],
              ),
            );
          }),
      margin: EdgeInsets.all(10.0.w),
    );
  }

  Widget competitionHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20.0.h,
          ),
          Container(
            width: 100.0.w,
            height: 100.0.w,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(50.0.w)),
                image: DecorationImage(
                    image:
                        CachedNetworkImageProvider(_competition.competitionPic),
                    fit: BoxFit.cover)),
          ),
          SizedBox(
            height: 10.0.h,
          ),
          AutoSizeText(
            '${_competition.competitionName}',
            minFontSize: 12.0.sp,
            stepGranularity: 12.0.sp/12,
            maxLines: 1,
            style: TextStyle(
                fontFamily: 'Quicksand', fontSize: 23.0.sp, color: Colors.black),
          ),
          SizedBox(
            height: 10.0.h,
          ),
          InkWell(
            onTap: () => openProfile(_competition.hostedBy.userID),
            child: AutoSizeText(
              '${_competition.hostedBy.firstName} ${_competition.hostedBy.lastName}',
              maxLines: 1,
              minFontSize: 12.0.sp,
              stepGranularity: 12.0.sp/12,
              style: TextStyle(
                  fontFamily: 'Quicksand',
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0.sp,
                  color: MemecistColors.primaryColor),
            ),
          ),
          SizedBox(
            height: 20.0.h,
          ),
          Padding(
            padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
            child: Text(
              '${_competition.info}',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: 'Quicksand', fontSize: 15.0.sp, color: Colors.black),
            ),
          ),
          SizedBox(
            height: 20.0.h,
          ),
          Text(
            "ends ${timeago.format(_competition.endDateTime, locale: 'en', allowFromNow: true)}",
            style: TextStyle(
                fontFamily: 'Quicksand', fontSize: 15.0.sp, color: Colors.red),
          ),
          SizedBox(
            height: 20.0.h,
          ),
          prizesWidget(),
          SizedBox(
            height: 20.0.h,
          )
        ],
      ),
    );
  }

  Widget rulesWidget() {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: _competition.rules.length,
        itemBuilder: (context, index) {
          return Text(
            "\t\t${index + 1}. ${_competition.rules[index]}",
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: 16.0.sp,
              fontFamily: 'Quicksand',
            ),
          );
        });
  }

  Widget prizesWidget() {
    return ListView.builder(
        itemCount: _competition.prizes.length,
        shrinkWrap: true,
        primary: false,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(top: 5.0.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(right: 10.0.w),
                    child: Container(
                      width: 30.0.w,
                      height: 30.0.w,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15.0.w)),
                          image: DecorationImage(
                              image: CachedNetworkImageProvider(
                                  _competition.prizes[index].icon),
                              fit: BoxFit.cover)),
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    '${_competition.prizes[index].position}',
                    style: TextStyle(fontFamily: 'Quicksand', fontSize: 14.0.sp),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    _competition.prizes[index].prize,
                    style: TextStyle(fontFamily: 'Quicksand', fontSize: 14.0.sp),
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget competitionWinnerCard() {
    String position = "";
    int rank = 1;
    return StreamBuilder(
        stream: _competitionDetailsBloc.user,
        builder: (context, AsyncSnapshot<MemecistUser> snapshot) {
          if (snapshot.hasData) {
            winners.forEach((competitionWinnerGroup) {
              competitionWinnerGroup.competitionWinners.forEach((winner) {
                if (winner.user.userID == snapshot.data.userID) {
                  position = competitionWinnerGroup.position;
                  rank = winner.rank;
                }
              });
            });
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10.0.w),
                  child: Text(
                    "HEARTIEST CONGRATULATIONS ❤",
                    style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 14.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                  child: Text(
                    "We've curated this card as a token of appreciation!",
                    style: TextStyle(fontFamily: "Quicksand", fontSize: 14.0),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0.w, right: 20.0.w, top: 10.0.h, bottom: 10.0.h),
                  child: RepaintBoundary(
                    key: _globalKey,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10.0.w)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                blurRadius: 3.0.w,
                                spreadRadius: 3.0.w)
                          ]),
                      child: Padding(
                        padding: EdgeInsets.all(30.0.w),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Flexible(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  circularImageButton(_competition.competitionPic,
                                      size: 80.0.w, radius: 40.0.w),
                                  SizedBox(
                                    width: 30.0.w,
                                  ),
                                  Flexible(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text(
                                          "Competition Winner",
                                          style: TextStyle(
                                            fontSize: 20.0.sp,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Quicksand',
                                            color: MemecistColors.primaryColor,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10.0.h,
                                        ),
                                        Flexible(
                                          child: AutoSizeText(
                                            "#$rank in $position",
                                            maxLines: 1,
                                            minFontSize: 12.0.sp,
                                            stepGranularity: 12.0.sp/12,
                                            style: TextStyle(
                                              fontSize: 20.0.sp,
                                              fontFamily: 'Quicksand',
                                              color: Colors.red,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Flexible(
                              child: AutoSizeText(
                                "${_competition.competitionName}",
                                maxLines: 1,
                                minFontSize: 12.0.sp,
                                stepGranularity: 12.0.sp/12,
                                style: TextStyle(
                                    fontSize: 30.0.sp,
                                    fontFamily: 'Quicksand',
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Text.rich(TextSpan(children: [
                              TextSpan(
                                text: "Memecist congratulates ",
                                style: TextStyle(
                                  fontSize: 18.0.sp,
                                  fontFamily: 'Quicksand',
                                  color: Colors.black,
                                ),
                              ),
                              TextSpan(
                                text:
                                "${snapshot.data.firstName} ${snapshot.data.lastName}",
                                style: TextStyle(
                                  fontSize: 18.0.sp,
                                  fontFamily: 'Quicksand',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              TextSpan(
                                text: " for this remarkable feat!",
                                style: TextStyle(
                                  fontSize: 18.0.sp,
                                  fontFamily: 'Quicksand',
                                  color: Colors.black,
                                ),
                              ),
                            ])),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => shareCard(),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                "SHARE",
                                style: TextStyle(
                                    fontFamily: "Quicksand",
                                    fontSize: 14.0.sp,
                                    color: MemecistColors.primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 5.0.w,
                              ),
                              CachedNetworkImage(
                                height: 20.0.w,
                                width: 20.0.w,
                                imageUrl:
                                    FirebaseStorageConstants.instagramLogo,
                              ),
                              SizedBox(
                                width: 5.0.w,
                              ),
                              CachedNetworkImage(
                                height: 20.0.w,
                                width: 20.0.w,
                                imageUrl: FirebaseStorageConstants.facebookLogo,
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            );
          } else {
            return Container();
          }
        });
  }

  void postMeme() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return CreateMemePostBlocProvider(
            child: CreateMemePostScreen(
              competitionID: _competition.competitionID,
              competitionName: _competition.competitionName,
              areExternalLinksAllowed: _competition.areExternalLinksAllowed,
              areExternalLinksCompulsory:
                  _competition.areExternalLinksCompulsory,
            ),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.create_meme_post_screen)));
  }

  void openProfile(String userID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }

  void fetchMeme(String postID) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return MemePostDetailBlocProvider(
                child: MemePostDetailScreen(
                  true,
                  postID: postID,
                ),
              );
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.meme_post_detail_screen)));
  }

  void shareCard() {
    _competitionDetailsBloc.shareWarWinnerCard(_globalKey);
  }
}
