import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_pending_approvals_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_pending_approvals_bloc_provider.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_for_pending_approval.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';

class ViewPendingApprovalsScreen extends StatefulWidget {
  final String _groupID;

  ViewPendingApprovalsScreen(this._groupID);

  @override
  _ViewPendingApprovalsScreenState createState() =>
      _ViewPendingApprovalsScreenState(_groupID);
}

class _ViewPendingApprovalsScreenState
    extends State<ViewPendingApprovalsScreen> {
  final String _groupID;

  _ViewPendingApprovalsScreenState(this._groupID);

  ViewPendingApprovalsBloc _viewPendingApprovalsBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreReacts = true;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _viewPendingApprovalsBloc = ViewPendingApprovalsBlocProvider.of(context);
      _viewPendingApprovalsBloc.initializeViewPendingApprovalMemes(_groupID);

      _viewPendingApprovalsBloc.fetchUnapprovedMemes();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreReacts &&
            !_isLoading) {
          _viewPendingApprovalsBloc.fetchUnapprovedMemes();
        }
      });

      _viewPendingApprovalsBloc.hasMoreMemes.listen((hasMoreData) {
        _hasMoreReacts = hasMoreData;
      });

      _viewPendingApprovalsBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _viewPendingApprovalsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar:
          appBarWithBackButtonAndTitle("Pending Approvals by admin", context),
      body: ListView(
        controller: _scrollController,
        children: <Widget>[
          SizedBox(
            height: 20.0.h,
          ),
          StreamBuilder(
              stream: _viewPendingApprovalsBloc.memePostList,
              builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO MEMES SUBMITTED FOR APPROVAL!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return memePostList(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _viewPendingApprovalsBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING MEMES...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget memePostList(List<MemePost> memePosts) {
    return ListView.builder(
        itemCount: memePosts.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return MemePostForPendingApprovalWidget(memePosts[index]);
        });
  }
}
