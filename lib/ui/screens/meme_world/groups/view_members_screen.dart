import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_members_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_members_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_member_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViewMembersScreen extends StatefulWidget {
  final String _groupID;

  ViewMembersScreen(this._groupID);

  @override
  _ViewMembersScreenState createState() =>
      _ViewMembersScreenState(this._groupID);
}

class _ViewMembersScreenState extends State<ViewMembersScreen> {
  String _groupID;
  bool isAdmin = false;

  TextEditingController _searchController = TextEditingController();

  _ViewMembersScreenState(this._groupID);

  ViewMembersBloc _viewMembersBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreMembers = true;
  bool _isLoading = false;
  bool _isAdmin = false;

  @override
  void initState() {
    super.initState();
    _searchController.addListener(() {
      if (_searchController.text.length != 0) {
        _viewMembersBloc.searchMembers(_searchController.text);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => removeFocus(),
      child: Scaffold(
        appBar: appBarWithBackButtonAndTitle("Members", context),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              getSearchBar(),
              SizedBox(
                height: 10.0.h,
              ),
              ListView(
                shrinkWrap: true,
                controller: _scrollController,
                children: <Widget>[
                  SizedBox(
                    height: 10.0.h,
                  ),
                  StreamBuilder(
                      stream: _viewMembersBloc.membersList,
                      builder: (context,
                          AsyncSnapshot<List<GroupMember>> asyncSnapshot) {
                        if (asyncSnapshot.hasData) {
                          if (asyncSnapshot.data.length == 0) {
                            return Center(
                              child: Text(
                                "NO MEMBERS!",
                                style: TextStyle(
                                    fontSize: 15.0.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                            );
                          }
                          return membersList(asyncSnapshot.data);
                        } else
                          return Container();
                      }),
                  StreamBuilder(
                      stream: _viewMembersBloc.isLoading,
                      builder: (context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData && snapshot.data) {
                          return Center(
                            child: Text(
                              "LOADING MEMBERS...",
                              style: TextStyle(
                                  fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getSearchBar() {
    return Container(
      height: 55.0.h,
      color: Colors.white,
      padding: EdgeInsets.only(
          left: 10.0.w, right: 10.0.w, top: 5.0.h, bottom: 5.0.h),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Container(
              height: 35.0.h,
              decoration: BoxDecoration(
                  color: MemecistColors.backgroundColor,
                  borderRadius: BorderRadius.all(Radius.circular(30.0.w))),
              child: Padding(
                padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
                child: TextField(
                  controller: _searchController,
                  style: TextStyle(
                    fontSize: 16.0.sp
                  ),
                  decoration: InputDecoration(
                      hintText: "Search",
                      hintStyle: TextStyle(fontSize: 14.0.sp),
                      border: InputBorder.none),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 10.0.w,
          ),
          Icon(
            Icons.search,
            size: 30.0.w,
            color: Colors.grey,
          ),
        ],
      ),
    );
  }

  Widget membersList(List<GroupMember> members) {
    return ListView.builder(
        itemCount: members.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return memberWidget(members[index]);
        });
  }

  Widget memberWidget(GroupMember member) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () => openProfile(member),
        child: Padding(
          padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 15.0.h),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(child: userRowItem(member.user, false)),
              member.isAdmin
                  ? Row(
                      children: <Widget>[
                        Container(
                            child: Text(
                          "ADMIN",
                          style: TextStyle(
                              color: MemecistColors.primaryColor,
                              fontSize: 16.0.sp,
                              fontFamily: 'Quicksand'),
                        )),
                        menuButton(member)
                      ],
                    )
                  : menuButton(member),
            ],
          ),
        ),
      ),
    );
  }

  Widget removeButton(GroupMember member) {
    return FlatButton(
      child: Text(
        "REMOVE",
        style: TextStyle(color: Colors.red, fontSize: 16.0.sp),
      ),
      onPressed: () {
        _viewMembersBloc.remove(member);
        Navigator.of(context).pop();
      },
    );
  }

  Widget cancelButton() {
    return FlatButton(
      child: Text(
        "CANCEL",
        style: TextStyle(fontSize: 16.0.sp),
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

  void removeMember(GroupMember member) async {
    User currentUser = await repository.getLoggedInUser();
    if (_isAdmin && member.user.userID != currentUser.uid) {
      showDialog(
          context: context,
          builder: (_) => new AlertDialog(
                title: new Text(
                  "Remove Member",
                  style: TextStyle(fontSize: 16.0.sp),
                ),
                content: new Text(
                  "Are you sure?",
                  style: TextStyle(fontSize: 14.0.sp),
                ),
                actions: <Widget>[
                  removeButton(member),
                  cancelButton(),
                ],
              ));
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _viewMembersBloc = ViewMembersBlocProvider.of(context);
      _viewMembersBloc.initializeViewMembersBloc(_groupID);

      _viewMembersBloc.fetchMembers();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = MediaQuery.of(context).size.height * 0.20;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreMembers &&
            !_isLoading) {
          _viewMembersBloc.fetchMembers();
        }
      });

      _viewMembersBloc.hasMoreMembers.listen((hasMoreData) {
        _hasMoreMembers = hasMoreData;
      });

      _viewMembersBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      _viewMembersBloc.isAdmin.listen((isAdmin) {
        _isAdmin = isAdmin;
      });

      isFirstRun = false;
    }
  }

  Widget menuButton(GroupMember member) {
    return _isAdmin
        ? PopupMenuButton(
            itemBuilder: (context) => [
              member.isAdmin
                  ? null
                  : PopupMenuItem(
                      value: "Make Admin",
                      child: Text(
                        "Make Admin",
                        style: TextStyle(
                            fontFamily: 'Quicksand', fontSize: 14.0.sp),
                      ),
                    ),
              PopupMenuItem(
                value: "Remove",
                child: Text(
                  "Remove",
                  style: TextStyle(
                      fontFamily: 'Quicksand',
                      color: Colors.red,
                      fontSize: 14.0.sp),
                ),
              )
            ],
            onSelected: (value) {
              if (value == "Make Admin") {
                makeAdmin(member);
              } else if (value == "Remove") {
                removeMember(member);
              }
            },
          )
        : Container();
  }

  void makeAdmin(GroupMember member) {
    _viewMembersBloc.makeAdmin(member);
  }

  openProfile(GroupMember member) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(member.user.userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _viewMembersBloc.dispose();
    super.dispose();
  }
}
