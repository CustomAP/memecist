import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:memecist/core/blocs/meme_post/create_meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/edit_group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_members_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_pending_approvals_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_unapproved_memes_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/start_war_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/create_meme_post_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/edit_group_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/view_members_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/view_pending_approvals_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/view_unapproved_memes_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/start_war_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_details_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GroupDetailsScreen extends StatefulWidget {
  final String _groupID;

  GroupDetailsScreen(this._groupID);

  @override
  _GroupDetailsScreenState createState() => _GroupDetailsScreenState(_groupID);
}

class _GroupDetailsScreenState extends State<GroupDetailsScreen> {
  final String _groupID;
  GroupDetailsBloc _groupDetailsBloc;
  Group _group;

  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreMemePosts = true;
  bool _isLoading = false;
  bool _isAdmin = false;

  _GroupDetailsScreenState(this._groupID);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _groupDetailsBloc = GroupDetailsBlocProvider.of(context);

    if (isFirstRun) {
      _groupDetailsBloc.initializeGroupDetailsBloc(_groupID);
      _groupDetailsBloc.getGroup();
      _groupDetailsBloc.fetchGroupMemes();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreMemePosts &&
            !_isLoading) {
          _groupDetailsBloc.fetchGroupMemes();
        }
      });

      _groupDetailsBloc.hasMoreMemes.listen((hasMoreData) {
        _hasMoreMemePosts = hasMoreData;
      });

      _groupDetailsBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      _groupDetailsBloc.admin.listen((isAdmin) {
        if (isAdmin) {
          _groupDetailsBloc.updateGroupMembersCount(_groupID);
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _groupDetailsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: groupSettingsDrawer(),
      backgroundColor: MemecistColors.backgroundColor,
      appBar:
          appBarWithBackAndActionButton(context, groupSettingsDrawerHandle()),
      body: StreamBuilder(
        stream: _groupDetailsBloc.group,
        builder: (context, AsyncSnapshot<Group> asyncSnapshot) {
          if (asyncSnapshot.hasData) {
            _group = asyncSnapshot.data;
            return groupView(asyncSnapshot.data);
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
    );
  }

  Widget drawerForNonMember() {
    return ListView(
      children: <Widget>[
        DrawerHeader(
          child: Text(""),
          decoration: BoxDecoration(
              color: MemecistColors.primaryColor,
              image: DecorationImage(
                  image: AssetImage('assets/drawer_background.jpg'),
                  fit: BoxFit.cover)),
        ),
        ListTile(
          title: Text(
            'Join Group',
            style: TextStyle(
                color: Colors.green,
                fontFamily: 'Quicksand',
                fontSize: 16.0.sp),
          ),
          onTap: () {
            joinGroup();
            Navigator.pop(context);
          },
        )
      ],
    );
  }

  Widget drawerForMember() {
    return ListView(
      children: <Widget>[
        DrawerHeader(
          child: Text(""),
          decoration: BoxDecoration(
              color: MemecistColors.primaryColor,
              image: DecorationImage(
                  image: AssetImage('assets/drawer_background.jpg'),
                  fit: BoxFit.cover)),
        ),
        ListTile(
          title: Text(
            'View Pending Approvals',
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Quicksand',
                fontSize: 16.0.sp),
          ),
          onTap: () {
            openPendingApprovalsScreen();
          },
        ),
        ListTile(
          title: Text(
            'Leave Group',
            style: TextStyle(
                color: Colors.red, fontFamily: 'Quicksand', fontSize: 16.0.sp),
          ),
          onTap: () {
            leaveGroup();
          },
        )
      ],
    );
  }

  Widget drawerForAdmin() {
    return ListView(
      children: <Widget>[
        DrawerHeader(
          child: Text(
            "",
          ),
          decoration: BoxDecoration(
              color: MemecistColors.primaryColor,
              image: DecorationImage(
                  image: AssetImage('assets/drawer_background.jpg'),
                  fit: BoxFit.cover)),
        ),
        ListTile(
          title: Text(
            'Edit Group Details',
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Quicksand',
                fontSize: 16.0.sp),
          ),
          onTap: () {
            openEditGroupDetailsScreen();
          },
        ),
        ListTile(
          title: Text(
            'Approve Pending Memes',
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Quicksand',
                fontSize: 16.0.sp),
          ),
          onTap: () {
            openApprovalsScreen();
          },
        ),
        StreamBuilder(
            stream: _groupDetailsBloc.group,
            builder: (context, AsyncSnapshot<Group> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.isWarActive) {
                  return ListTile(
                    title: Text(
                      'View Ongoing War',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp),
                    ),
                    onTap: () {
                      //TODO on tap
//                    startWar();
                    },
                  );
                } else {
                  return ListTile(
                    title: Text(
                      'Start War',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp),
                    ),
                    onTap: () {
                      startWar();
                    },
                  );
                }
              } else {
                return Container();
              }
            }),
        ListTile(
          title: Text(
            'Leave Group',
            style: TextStyle(
                color: Colors.red, fontFamily: 'Quicksand', fontSize: 16.0.sp),
          ),
          onTap: () {
            leaveGroup();
            Navigator.pop(context);
          },
        )
      ],
    );
  }

  Widget groupSettingsDrawer() {
    return Drawer(
      child: StreamBuilder(
          stream: _groupDetailsBloc.member,
          builder: (context, memberSnapshot) {
            if (memberSnapshot.hasData) {
              if (memberSnapshot.data) {
                return StreamBuilder(
                  stream: _groupDetailsBloc.admin,
                  builder: (context, AsyncSnapshot<bool> adminSnapshot) {
                    if (adminSnapshot.hasData) {
                      if (adminSnapshot.data) {
                        return drawerForAdmin();
                      } else {
                        return drawerForMember();
                      }
                    } else {
                      return drawerForNonMember();
                    }
                  },
                );
              } else {
                return drawerForNonMember();
              }
            } else {
              return Container();
            }
          }),
    );
  }

  Widget groupView(Group group) {
    return SingleChildScrollView(
      controller: _scrollController,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                alignment: Alignment.topCenter,
                child: groupInfo(group),
              ),
              Positioned(
                width: MediaQuery.of(context).size.width,
                bottom: -40.0.h,
                child: Container(
                  margin: EdgeInsets.fromLTRB(25.0.w, 0.0, 25.0.w, 0.0.h),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0.w),
                      boxShadow: [
                        BoxShadow(blurRadius: 0.5.w, color: Colors.grey)
                      ]),
                  child: Padding(
                    padding:
                        EdgeInsets.fromLTRB(10.0.w, 15.0.h, 10.0.w, 15.0.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        countWidget("MEMES", group.memesCount.toString(), null),
                        countWidget(
                            "MEMBERS",
                            NumberFormat.compact().format(group.membersCount),
                            () => openViewMembersScreen()),
                        countWidget("WARS", group.warsCount.toString(), null)
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 40.0.h,
          ),
          SizedBox(
            height: 10.0.h,
          ),
          StreamBuilder(
            stream: _groupDetailsBloc.admin,
            builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data) {
                  _isAdmin = true;
                  if (_isAdmin && group.unapprovedMemesCount > 0) {
                    return approveMemesCard();
                  } else {
                    return Container();
                  }
                } else {
                  return Container();
                }
              } else {
                return Container();
              }
            },
          ),
          StreamBuilder(
            stream: _groupDetailsBloc.admin,
            builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data) {
                  _isAdmin = true;
                  if (group.membersCount < 100)
                    return introducingMemeWarCard();
                  else if (!group.isWarActive &&
                      group.warsLeftThisMonthCount > 0 &&
                      group.membersCount > 100)
                    return startWarCard();
                  else if (group.isWarActive) return warDetailsCard();
                  return Container();
                } else {
                  if (group.isWarActive)
                    return warDetailsCard();
                  else if (group.membersCount < 100)
                    return introducingMemeWarCard();
                  return Container();
                }
              } else {
                return Container();
              }
            },
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0.w, top: 10.0.w),
            child: Text(
              "MEMES",
              style: TextStyle(
                  fontFamily: "Quicksand",
                  fontSize: 14.0.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 10.0.h,
          ),
          StreamBuilder(
              stream: _groupDetailsBloc.memesList,
              builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO MEMES POSTED YET!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return groupPostsList(asyncSnapshot.data);
                } else {
                  return Container();
                }
              }),
          StreamBuilder(
              stream: _groupDetailsBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING MEMES...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
          SizedBox(
            height: 10.0.h,
          ),
        ],
      ),
    );
  }

  Widget introducingMemeWarCard() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: showWarEligibilityInfo,
        child: Container(
          margin: EdgeInsets.all(10.0.w),
          decoration: BoxDecoration(
              color: MemecistColors.primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(30.0.w))),
          padding: EdgeInsets.all(30.0.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 70.0.w,
                height: 70.0.w,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(35.0.w),
                    ),
                    image: DecorationImage(
                        image: CachedNetworkImageProvider(
                            FirebaseStorageConstants.warIdentifierUrl))),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "Introducing Meme Wars!!",
                style: TextStyle(
                    fontSize: 40.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                _isAdmin
                    ? "You can start a meme war once the group reaches 100 members ;)"
                    : "Invite your friends to group and start war once group reaches 100 members :)",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget approveMemesCard() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: openApprovalsScreen,
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.all(10.0.w),
          decoration: BoxDecoration(
              color: Colors.deepPurple[300],
              borderRadius: BorderRadius.all(Radius.circular(15.0.w))),
          padding: EdgeInsets.fromLTRB(30.0.w, 15.0.h, 30.0.w, 15.0.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  width: 30.0.w,
                  height: 30.0.w,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0.w))),
                  child: Container(
                      width: 30.0.w,
                      height: 30.0.w,
                      child: CachedNetworkImage(
                          imageUrl: FirebaseStorageConstants.stampIconUrl))),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "${_group.unapprovedMemesCount} PENDING MEME APPROVALS",
                style: TextStyle(
                    fontSize: 20.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget warDetailsCard() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: showWarDetails,
        child: Container(
          margin: EdgeInsets.all(10.0.w),
          decoration: BoxDecoration(
              color: Color(
                  int.parse(_group.warCardColor.substring(1, 7), radix: 16) +
                      0xFF000000),
              borderRadius: BorderRadius.all(Radius.circular(30.0.w))),
          padding: EdgeInsets.all(30.0.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                      width: 30.0.w,
                      height: 30.0.w,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(Radius.circular(15.0.w))),
                      child: Container(
                          width: 30.0.w,
                          height: 30.0.w,
                          child: CachedNetworkImage(
                              imageUrl:
                                  FirebaseStorageConstants.warIdentifierUrl))),
                  SizedBox(
                    width: 10.0.w,
                  ),
                  Text(
                    "ONGOING MEME WAR",
                    style: TextStyle(
                        fontSize: 14.0.sp,
                        fontFamily: 'Quicksand',
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                  width: 70.0.w,
                  height: 70.0.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(35.0.w)),
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(_group.warPic),
                          fit: BoxFit.cover))),
              SizedBox(
                height: 10.0.h,
              ),
              AutoSizeText(
                _group.warName,
                maxLines: 2,
                minFontSize: 12.0.sp,
                stepGranularity: 12.0.sp / 12,
                style: TextStyle(
                    fontSize: 40.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                _group.warStatus,
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget startWarCard() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: startWar,
        child: Container(
          margin: EdgeInsets.all(10.0.w),
          decoration: BoxDecoration(
              color: MemecistColors.primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(30.0.w))),
          padding: EdgeInsets.all(30.0.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 70.0.w,
                height: 70.0.w,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(35.0.w),
                    ),
                    image: DecorationImage(
                        image: CachedNetworkImageProvider(
                            FirebaseStorageConstants.warIdentifierUrl))),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "Start a Meme War!!",
                style: TextStyle(
                    fontSize: 40.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Text(
                "The only type of war one should start :P",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Quicksand',
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget groupPostsList(List<MemePost> memesList) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: memesList.length,
        itemBuilder: (context, index) {
          return MemePostBlocProvider(
            child: MemePostWidget(
              memesList[index],
              key: ObjectKey(memesList[index].postID),
              onDeleteClicked: () => onDeletePost(memesList[index]),
            ),
          );
        });
  }

  Widget countWidget(String name, String count, Function() onTapFunction) {
    return InkWell(
      onTap: onTapFunction,
      child: Column(
        children: <Widget>[
          Text(
            name,
            style: TextStyle(
                color: Colors.grey, fontFamily: 'QuickSand', fontSize: 14.0.sp),
          ),
          SizedBox(
            height: 5.0.h,
          ),
          Text(
            count,
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'QuickSand',
                fontSize: 20.0.sp),
          )
        ],
      ),
    );
  }

  Widget groupInfo(Group group) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: 20.0.h,
        ),
        Container(
          width: 100.0.w,
          height: 100.0.w,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(50.0.w)),
              image: DecorationImage(
                  image: CachedNetworkImageProvider(group.groupProfilePic),
                  fit: BoxFit.cover)),
        ),
        SizedBox(
          height: 10.0.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: AutoSizeText(
                '${group.groupName}',
                maxLines: 1,
                minFontSize: 12.0.sp,
                stepGranularity: 12.0.sp / 12,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    fontSize: 23.0.sp,
                    color: Colors.black),
              ),
            ),
            SizedBox(
              width: 5.0.w,
            ),
            group.isVerified ? verifiedBadge(size: 23.0.w) : Container()
          ],
        ),
        SizedBox(
          height: 5.0.h,
        ),
        Flexible(
          child: AutoSizeText(
            '${group.groupDescription}',
            textAlign: TextAlign.center,
            maxLines: 3,
            minFontSize: 12.0.sp,
            stepGranularity: 12.0.sp / 12,
            style: TextStyle(
                fontFamily: 'Quicksand',
                fontSize: 15.0.sp,
                color: MemecistColors.greyText),
          ),
        ),
        SizedBox(
          height: 20.0.h,
        ),
        StreamBuilder(
            stream: _groupDetailsBloc.member,
            builder: (context, AsyncSnapshot<bool> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data)
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      actionButton(context, "Post Meme", () => postMeme()),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      actionButton(context, "Copy Link", () => getInviteLink()),
                    ],
                  );
                else
                  return actionButton(context, "Join", () => joinGroup());
              } else {
                return Container();
              }
            }),
        SizedBox(
          height: 20.0.h,
        ),
        SizedBox(
          //because stack positioned is -40.0.h
          height: 40.0.h,
        )
      ],
    );
  }

  Widget groupSettingsDrawerHandle() {
    return Builder(
      builder: (context) {
        return IconButton(
          icon: Icon(
            Icons.menu,
            color: Colors.black,
          ),
          onPressed: () => Scaffold.of(context).openEndDrawer(),
        );
      },
    );
  }

  Widget leaveButton() {
    return FlatButton(
      child: Text(
        "LEAVE",
        style: TextStyle(color: Colors.red, fontSize: 16.0.sp),
      ),
      onPressed: () {
        _groupDetailsBloc.leaveGroup(_isAdmin);
        Navigator.of(context).pop();
      },
    );
  }

  Widget cancelButton() {
    return FlatButton(
      child: Text(
        "CANCEL",
        style: TextStyle(fontSize: 16.0.sp),
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

  Widget inviteButton() {
    return FlatButton(
      child: Text(
        "INVITE FRIENDS",
        style: TextStyle(fontSize: 16.0.sp),
      ),
      onPressed: () {
        getInviteLink();
      },
    );
  }

  void joinGroup() async {
    await _groupDetailsBloc.joinGroup();
  }

  void getInviteLink() {
    _groupDetailsBloc.getInviteLink(_group.groupName);
  }

  void postMeme() {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) {
                  return CreateMemePostBlocProvider(
                    child: CreateMemePostScreen(
                      groupID: _groupID,
                      groupName: _group.groupName,
                      isGroupAdmin: _isAdmin,
                    ),
                  );
                },
                settings: RouteSettings(
                    name: FirebaseAnalyticsConstants.create_meme_post_screen)))
        .then((value) => Future.delayed(Duration(milliseconds: 500), () {
              _groupDetailsBloc.getGroup();
            }));
  }

  void openViewMembersScreen() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ViewMembersBlocProvider(
            child: ViewMembersScreen(_groupID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.view_members_screen)));
  }

  void startWar() {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) {
                  return StartWarBlocProvider(
                    child: StartWarScreen(_group),
                  );
                },
                settings: RouteSettings(
                    name: FirebaseAnalyticsConstants.start_war_screen)))
        .then((value) => Future.delayed(Duration(milliseconds: 500), () {
              _groupDetailsBloc.getGroup();
            }));
  }

  void openApprovalsScreen() {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) {
                  return ViewUnapprovedMemesBlocProvider(
                      child: ViewUnapprovedMemesScreen(_groupID));
                },
                settings: RouteSettings(
                    name: FirebaseAnalyticsConstants
                        .view_unapproved_memes_screen)))
        .then((value) => Future.delayed(Duration(milliseconds: 500), () {
              _groupDetailsBloc.getGroup();
            }));
  }

  void leaveGroup() {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: new Text(
                "Leave Group",
                style: TextStyle(fontSize: 16.0.sp),
              ),
              content: new Text(
                "Are you sure?",
                style: TextStyle(fontSize: 14.0.sp),
              ),
              actions: <Widget>[
                leaveButton(),
                cancelButton(),
              ],
            ));
  }

  void showWarDetails() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return WarDetailsBlocProvider(child: WarDetailsScreen(_group.warID));
    }));
  }

  void openPendingApprovalsScreen() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return ViewPendingApprovalsBlocProvider(
                child: ViewPendingApprovalsScreen(_groupID),
              );
            },
            settings: RouteSettings(
                name:
                    FirebaseAnalyticsConstants.view_pending_approvals_screen)));
  }

  void showWarEligibilityInfo() {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: new Text(
                "Introducing Meme Wars!",
                style: TextStyle(fontSize: 16.0.sp),
              ),
              content: new Text(
                "Wars can be played within a group on any meme topic you like!\nWars need minimum 100 members in a group.\nInvite your friends to group now to start war!",
                style: TextStyle(fontSize: 14.0.sp),
              ),
              actions: <Widget>[
                inviteButton(),
                cancelButton(),
              ],
            ));
  }

  void onDeletePost(MemePost memePost) async {
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post, parameters: {
      FirebaseAnalyticsConstants.source:
          FirebaseAnalyticsConstants.group_details_screen
    });
    _groupDetailsBloc.deletePost(memePost);
  }

  void openEditGroupDetailsScreen() async {
    Navigator.of(context)
        .push(MaterialPageRoute(
            builder: (context) {
              return EditGroupDetailsBlocProvider(
                child: EditGroupDetailsScreen(_group),
              );
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.edit_group_details_screen)))
        .then((value) => Future.delayed(Duration(milliseconds: 500), () {
              _groupDetailsBloc.getGroup();
            }));
  }
}
