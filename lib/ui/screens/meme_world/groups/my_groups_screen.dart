import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/my_groups_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/my_groups_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'group_details_screen.dart';

class MyGroupsScreen extends StatefulWidget {
  @override
  _MyGroupsScreenState createState() => _MyGroupsScreenState();
}

class _MyGroupsScreenState extends State<MyGroupsScreen> {
  MyGroupsBloc _myGroupsBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreGroups = true;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButtonAndTitle("My Groups", context),
      body: ListView(
        controller: _scrollController,
        children: <Widget>[
          SizedBox(
            height: 20.0.h,
          ),
          StreamBuilder(
              stream: _myGroupsBloc.groupsList,
              builder: (context, AsyncSnapshot<List<Group>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO GROUPS JOINED YET!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return groupsGrid(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _myGroupsBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING GROUPS...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget groupsGrid(List<Group> groups) {
    return GridView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: groups.length,
        padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 10.0.h),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, crossAxisSpacing: 7.0.w, mainAxisSpacing: 7.0.w),
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () => openGroupDetails(groups[index]),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0.w),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10.0.w),
                          topLeft: Radius.circular(10.0.w)),
                      child: CachedNetworkImage(
                        imageUrl: groups[index].groupProfilePic,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.all(5.0.w),
                      child: Text(
                        groups[index].groupName,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontFamily: 'Quicksand',
                            fontSize: 16.0.sp),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  void openGroupDetails(Group group) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return GroupDetailsBlocProvider(
                  child: GroupDetailsScreen(group.groupID));
            },
            settings: RouteSettings(
                name: FirebaseAnalyticsConstants.group_details_screen)));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _myGroupsBloc = MyGroupsBlocProvider.of(context);

      _myGroupsBloc.fetchGroups();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreGroups &&
            !_isLoading) {
          _myGroupsBloc.fetchGroups();
        }
      });

      _myGroupsBloc.hasMoreGroups.listen((hasMoreData) {
        _hasMoreGroups = hasMoreData;
      });

      _myGroupsBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _myGroupsBloc.dispose();
    super.dispose();
  }
}
