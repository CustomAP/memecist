import 'dart:io';
import 'package:app_settings/app_settings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memecist/core/blocs/meme_world/groups/create_group_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/create_group_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CreateGroupScreen extends StatefulWidget {
  @override
  _CreateGroupScreenState createState() => _CreateGroupScreenState();
}

class _CreateGroupScreenState extends State<CreateGroupScreen> {
  CreateGroupBloc _createGroupBloc;

  TextEditingController _groupNameController = TextEditingController();
  TextEditingController _groupDescriptionController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  FormFieldValidator descriptionValidator, nameValidator;

  File imageFile;

  bool isClosed = false;
  bool isFirstRun = true;

  @override
  Widget build(BuildContext context) {
    descriptionValidator = (val) {
      return val.length == 0 ? "Value cannot be blank" : null;
    };

    nameValidator = (val) {
      return val.length < 3
          ? "Group name should be atleast 3 characters"
          : null;
    };

    return GestureDetector(
      onTap: () => removeFocus(),
      child: Scaffold(
        backgroundColor: MemecistColors.backgroundColor,
        appBar: appBarWithBackButton(context),
        body: Container(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 40.0.h,
                    ),
                    Text(
                      "Create a Group!",
                      style: TextStyle(
                          fontSize: 35.0.sp,
                          fontFamily: 'Quicksand',
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    StreamBuilder(
                        stream: _createGroupBloc.imageFile,
                        builder: (context, AsyncSnapshot<File> asyncSnapshot) {
                          if (asyncSnapshot.hasData) {
                            if (asyncSnapshot.data == null)
                              return chooseImagePlaceHolder();

                            imageFile = asyncSnapshot.data;

                            return imageAvatar();
                          } else {
                            return chooseImagePlaceHolder();
                          }
                        }),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                        "Group Name",
                        _groupNameController,
                        false,
                        validator: nameValidator,
                        inputFormatter: [
                          LengthLimitingTextInputFormatter(50),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                        "Group Description",
                        _groupDescriptionController,
                        false,
                        validator: descriptionValidator,
                        inputFormatter: [
                          LengthLimitingTextInputFormatter(150),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
//                  StreamBuilder(
//                      stream: _createGroupBloc.isGroupClosed,
//                      builder: (context, snapshot) {
//                        if (snapshot.hasData) {
//                          isClosed = snapshot.data;
//                        }
//                        return Column(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          children: <Widget>[
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.center,
//                              children: <Widget>[
//                                Text("Closed"),
//                                Switch(
//                                  value: isClosed,
//                                  onChanged: (value) {
//                                    _createGroupBloc
//                                        .setIsGroupClosed(!isClosed);
//                                  },
//                                  activeTrackColor:
//                                      MemecistColors.primaryColorAccent,
//                                  activeColor: MemecistColors.primaryColor,
//                                ),
//                              ],
//                            ),
//                            isClosed
//                                ? Text(
//                                    "Anyone willing to join the group will have to request admin for approval",
//                                    textAlign: TextAlign.center,
//                                  )
//                                : Container()
//                          ],
//                        );
//                      }),
//                  SizedBox(
//                    height: 20.0,
//                  ),
                    StreamBuilder(
                      stream: _createGroupBloc.showLoading,
                      builder: (context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            Fluttertoast.showToast(msg: "Group Created");
                            return submitButton();
                          }
                        } else {
                          return submitButton();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget imageAvatar() {
    return Container(
      width: 100.0.w,
      height: 100.0.w,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.0.w),
          border: Border.all(color: Colors.grey),
          image:
              DecorationImage(image: FileImage(imageFile), fit: BoxFit.cover)),
    );
  }

  Widget chooseImagePlaceHolder() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: pickImage,
        child: Container(
          width: 100.0.w,
          height: 100.0.w,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.0.w),
            border: Border.all(color: Colors.grey),
          ),
          child: Icon(
            Icons.add,
            color: MemecistColors.iconGreyBackgroundColor,
          ),
        ),
      ),
    );
  }

  Widget submitButton() {
    return actionButton(context, "SUBMIT", submit);
  }

  pickImage() async {
    try {
      var selectedImage =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      _createGroupBloc.setImageFile(selectedImage);
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }

  submit() {
    if (_formKey.currentState.validate()) {
      try {
        _createGroupBloc.createGroup(
            imageFile,
            Group(
                groupName: _groupNameController.text,
                groupDescription: _groupDescriptionController.text,
                isClosed: isClosed));
      } catch (e) {
        Fluttertoast.showToast(msg: e.toString());
      }
    }
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _createGroupBloc = CreateGroupBlocProvider.of(context);
      _createGroupBloc.groupID.listen((groupID) {
        if (groupID != null) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) {
                    return GroupDetailsBlocProvider(
                      child: GroupDetailsScreen(groupID),
                    );
                  },
                  settings: RouteSettings(
                      name: FirebaseAnalyticsConstants.group_details_screen)));
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _createGroupBloc.dispose();
    super.dispose();
  }
}
