import 'dart:io';
import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memecist/core/blocs/meme_world/groups/edit_group_details_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/edit_group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class EditGroupDetailsScreen extends StatefulWidget {
  final Group _group;

  EditGroupDetailsScreen(this._group);

  @override
  _EditGroupDetailsScreenState createState() =>
      _EditGroupDetailsScreenState(_group);
}

class _EditGroupDetailsScreenState extends State<EditGroupDetailsScreen> {
  EditGroupDetailsBloc _editGroupDetailsBloc;

  TextEditingController _groupNameController = TextEditingController();
  TextEditingController _groupDescriptionController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  FormFieldValidator descriptionValidator, nameValidator;
  File imageFile;
  bool isClosed = false;
  final Group _group;
  bool isFirstRun = true;

  _EditGroupDetailsScreenState(this._group);

  @override
  Widget build(BuildContext context) {
    descriptionValidator = (val) {
      return val.length == 0 ? "Value cannot be blank" : null;
    };

    nameValidator = (val) {
      return val.length < 3
          ? "Group name should be atleast 3 characters"
          : null;
    };

    return GestureDetector(
      onTap: () => removeFocus(),
      child: Scaffold(
        backgroundColor: MemecistColors.backgroundColor,
        appBar: appBarWithBackButton(context),
        body: Container(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 40.0.h,
                    ),
                    Text(
                      "Edit Group Details!",
                      style: TextStyle(
                          fontSize: 35.0.sp,
                          fontFamily: 'Quicksand',
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    StreamBuilder(
                        stream: _editGroupDetailsBloc.imageFile,
                        builder: (context, AsyncSnapshot<File> asyncSnapshot) {
                          if (asyncSnapshot.hasData) {
                            if (asyncSnapshot.data == null)
                              return chooseImagePlaceHolder();

                            imageFile = asyncSnapshot.data;

                            return imageAvatar();
                          } else {
                            return chooseImagePlaceHolder();
                          }
                        }),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                        "Group Name",
                        _groupNameController,
                        false,
                        validator: nameValidator,
                        inputFormatter: [
                          LengthLimitingTextInputFormatter(50),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                        "Group Description",
                        _groupDescriptionController,
                        false,
                        validator: descriptionValidator,
                        inputFormatter: [
                          LengthLimitingTextInputFormatter(150),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    StreamBuilder(
                      stream: _editGroupDetailsBloc.showLoading,
                      builder: (context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return submitButton();
                          }
                        } else {
                          return submitButton();
                        }
                      },
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Text.rich(TextSpan(children: [
                      TextSpan(
                        text: "PS: You can edit ",
                        style: TextStyle(
                            fontSize: 14.0.sp,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                      TextSpan(
                        text: "group name",
                        style: TextStyle(
                            fontSize: 14.0.sp,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      ),
                      TextSpan(
                        text: " at most once a month",
                        style: TextStyle(
                            fontSize: 14.0.sp,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                    ])),
                    SizedBox(
                      height: 20.0.h,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget imageAvatar() {
    return Container(
      width: 100.0.w,
      height: 100.0.w,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.0.w),
          border: Border.all(color: Colors.grey),
          image:
              DecorationImage(image: FileImage(imageFile), fit: BoxFit.cover)),
    );
  }

  Widget chooseImagePlaceHolder() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: pickImage,
        child: circularImageButton(_group.groupProfilePic,
            size: 100.0.w, radius: 50.0.w),
      ),
    );
  }

  Widget submitButton() {
    return actionButton(context, "SUBMIT", submit);
  }

  pickImage() async {
    try {
      var selectedImage =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      _editGroupDetailsBloc.setImageFile(selectedImage);
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }

  submit() {
    if (_formKey.currentState.validate()) {
      _editGroupDetailsBloc.editGroupDetails(
          imageFile,
          _groupNameController.text == _group.groupName
              ? null
              : _groupNameController.text,
          _groupDescriptionController.text == _group.groupDescription
              ? null
              : _groupDescriptionController.text,
          _group.groupID);
    }
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _editGroupDetailsBloc = EditGroupDetailsBlocProvider.of(context);

      _groupDescriptionController.text = _group.groupDescription;
      _groupNameController.text = _group.groupName;

      _editGroupDetailsBloc.isGroupEdited.listen((isGroupEdited) {
        if (isGroupEdited) {
          Fluttertoast.showToast(msg: "Group Info updated successfully");
          Navigator.of(context).pop();
        } else {
          Fluttertoast.showToast(msg: "Some error occurred");
        }
      });

      _editGroupDetailsBloc.canEditGroupName.listen((canEditGroupName) {
        if (!canEditGroupName) {
          Fluttertoast.showToast(
              msg:
                  "Sorry, you can edit group name only once a month! Try again later");
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _editGroupDetailsBloc.dispose();
    super.dispose();
  }
}
