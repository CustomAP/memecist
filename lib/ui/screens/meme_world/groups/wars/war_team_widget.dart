import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WarTeamWidget extends StatefulWidget {
  final WarTeam _warTeam;

  WarTeamWidget(this._warTeam);

  @override
  _WarTeamWidgetState createState() => _WarTeamWidgetState(_warTeam);
}

class _WarTeamWidgetState extends State<WarTeamWidget> {
  WarTeam _warTeam;

  _WarTeamWidgetState(this._warTeam);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0.w),
      ),
      elevation: 0.0,
      child: Padding(
        padding: EdgeInsets.all(10.0.w),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                circularImageButton(_warTeam.teamPic,
                    size: 50.0.w, radius: 25.0.w),
                SizedBox(
                  width: 10.0.w,
                ),
                Flexible(
                  child: AutoSizeText(
                    "${_warTeam.teamName}",
                    maxLines: 2,
                    minFontSize: 12.0.sp,
                    stepGranularity: 12.0.sp/12,
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 20.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: 20.0.w, top: 20.0.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: InkWell(
                        onTap: () => openProfile(_warTeam.teamLeader.userID),
                        child: userRowItem(_warTeam.teamLeader, false)),
                  ),
                  circularImageButton(
                      FirebaseStorageConstants.teamLeaderIconUrl,
                      size: 30.0.w,
                      radius: 15.w)
                ],
              ),
            ),
            ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: _warTeam.teamMembers.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.only(left: 20.0.w, top: 10.0.h),
                    child: InkWell(
                        onTap: () =>
                            openProfile(_warTeam.teamMembers[index].userID),
                        child: userRowItem(_warTeam.teamMembers[index], false)),
                  );
                })
          ],
        ),
      ),
    );
  }

  void openProfile(String userID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }
}
