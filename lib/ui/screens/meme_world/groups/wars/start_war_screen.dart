import 'dart:io';
import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/start_war_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/start_war_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc_provider.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_faq_widget.dart';
import 'package:memecist/ui/widgets/color_picker_dialog.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StartWarScreen extends StatefulWidget {
  final Group _group;

  StartWarScreen(this._group);

  @override
  _StartWarScreenState createState() => _StartWarScreenState(this._group);
}

class _StartWarScreenState extends State<StartWarScreen> {
  final Group _group;

  StartWarBloc _startWarBloc;

  TextEditingController _warNameController = TextEditingController();
  TextEditingController _warDescriptionController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  FormFieldValidator nameFieldValidator;
  int totalTeams = 3;
  File imageFile;
  bool isFirstRun = true;

  _StartWarScreenState(this._group);

  @override
  Widget build(BuildContext context) {
    nameFieldValidator = (val) {
      return val.length == 0 ? "Value cannot be blank" : null;
    };

    return GestureDetector(
      onTap: () => removeFocus(),
      child: Scaffold(
        backgroundColor: MemecistColors.backgroundColor,
        appBar: appBarWithBackButton(context),
        body: Container(
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 40.0.h,
                  ),
                  Text(
                    "Start a Meme War!",
                    style: TextStyle(
                        fontSize: 35.0.sp,
                        fontFamily: 'Quicksand',
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  StreamBuilder(
                      stream: _startWarBloc.imageFile,
                      builder: (context, AsyncSnapshot<File> asyncSnapshot) {
                        if (asyncSnapshot.hasData) {
                          if (asyncSnapshot.data == null)
                            return chooseImagePlaceHolder();

                          imageFile = asyncSnapshot.data;

                          return imageAvatar();
                        } else {
                          return chooseImagePlaceHolder();
                        }
                      }),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                        "War Name",
                        _warNameController,
                        false,
                        validator: nameFieldValidator,
                        inputFormatter: [
                          LengthLimitingTextInputFormatter(50),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0.w)),
                      child: memecistInputField(
                        "War Description",
                        _warDescriptionController,
                        false,
                        validator: nameFieldValidator,
                        inputFormatter: [
                          LengthLimitingTextInputFormatter(150),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  StreamBuilder(
                      stream: _startWarBloc.totalTeams,
                      builder: (context, AsyncSnapshot<int> snapshot) {
                        if (snapshot.hasData) {
                          totalTeams = snapshot.data;
                          return noOfTeamsSelector(snapshot.data);
                        }
                        return noOfTeamsSelector(totalTeams);
                      }),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  StreamBuilder(
                    stream: _startWarBloc.cardColor,
                    builder: (context, AsyncSnapshot<String> asyncSnapshot) {
                      if (asyncSnapshot.hasData) {
                        return cardColorIndicator(color: asyncSnapshot.data);
                      } else {
                        return cardColorIndicator(
                            color: MemecistColors.card_colors[7]);
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  StreamBuilder(
                    stream: _startWarBloc.showLoading,
                    builder: (context, AsyncSnapshot<bool> snapshot) {
                      if (snapshot.hasData) {
                        if (snapshot.data) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          Fluttertoast.showToast(msg: "War Started");
                          return submitButton();
                        }
                      } else {
                        return submitButton();
                      }
                    },
                  ),
                  WarFAQWidget()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget cardColorIndicator({String color}) {
    return InkWell(
      onTap: () => openColorPickerDialog(color),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 30.0.w,
              width: 30.0.w,
              color: Color(
                  int.parse(color.substring(1, 7), radix: 16) + 0xFF000000),
            ),
            SizedBox(
              width: 10.0.w,
            ),
            Text(
              "War card color",
              style: TextStyle(fontFamily: 'Quicksand', fontSize: 16.0.sp),
            )
          ],
        ),
      ),
    );
  }

  Widget noOfTeamsSelector(int _totalTeams) {
    List<int> totalTeams = [];

    if (_group.membersCount >= 100 && _group.membersCount < 500)
      totalTeams = [3];

    if (_group.membersCount >= 500 && _group.membersCount < 1500)
      totalTeams = [3, 5];

    if (_group.membersCount >= 1500 && _group.membersCount < 4000)
      totalTeams = [3, 5, 7];

    if (_group.membersCount >= 4000 && _group.membersCount < 8000)
      totalTeams = [3, 5, 7, 10];

    if (_group.membersCount >= 8000 && _group.membersCount < 15000)
      totalTeams = [3, 5, 7, 10, 13];

    if (_group.membersCount >= 15000) totalTeams = [3, 5, 7, 10, 13, 15];

    return Container(
      width: 300.0.w,
      child: DropdownButtonFormField(
        style: TextStyle(fontSize: 16.0.sp, color: Colors.black),
        decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 2.0.w)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 2.0.w))),
        value: _totalTeams,
        items: totalTeams.map((totalTeam) {
          return DropdownMenuItem(
              value: totalTeam, child: Text("$totalTeam Teams"));
        }).toList(),
        onChanged: (int value) {
          _startWarBloc.setTotalTeams(value);
        },
      ),
    );
  }

  Widget imageAvatar() {
    return Container(
      width: 100.0.w,
      height: 100.0.w,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.0.w),
          border: Border.all(color: Colors.grey),
          image:
              DecorationImage(image: FileImage(imageFile), fit: BoxFit.cover)),
    );
  }

  Widget chooseImagePlaceHolder() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: pickImage,
        child: Container(
          width: 100.0.w,
          height: 100.0.w,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.0.w),
            border: Border.all(color: Colors.grey),
          ),
          child: Icon(
            Icons.add,
            color: MemecistColors.iconGreyBackgroundColor,
          ),
        ),
      ),
    );
  }

  Widget submitButton() {
    return actionButton(context, "SUBMIT", submit);
  }

  pickImage() async {
    try {
      var selectedImage =
          await ImagePicker.pickImage(source: ImageSource.gallery);
      _startWarBloc.setImageFile(selectedImage);
    } on PlatformException catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: "Please give photo access permissions from Settings App");
      AppSettings.openAppSettings();
    }
  }

  submit() {
    if (_formKey.currentState.validate()) {
      try {
        _startWarBloc.startWar(
            imageFile,
            War(
                warName: _warNameController.text,
                warDescription: _warDescriptionController.text,
                maxTeamsCount: totalTeams,
                warRules: StringConstants.war_rules));
      } catch (e) {
        Fluttertoast.showToast(msg: e.toString());
      }
    }
  }

  Future<void> openColorPickerDialog(String color) async {
    showDialog(
        context: context,
        builder: (BuildContext context) => ColorPickerDialog()
            .colorPickerDialog((color) => selectColor(color)),
        barrierDismissible: false);
  }

  selectColor(String color) {
    _startWarBloc.setColor(color);
    Navigator.of(context).pop();
  }

  removeFocus() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _startWarBloc = StartWarBlocProvider.of(context);
      _startWarBloc.initializeStartWarBloc(_group.groupID);
      _startWarBloc.warID.listen((warID) {
        if (warID != null) {
          Navigator.of(context).pop();
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _startWarBloc.dispose();
    super.dispose();
  }
}
