import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/view_all_teams_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_ended_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_ended_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/view_all_teams_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_info_header_widget.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_team_widget.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WarDismissedScreen extends StatefulWidget {
  final War _war;

  WarDismissedScreen(this._war);

  @override
  _WarDismissedScreenState createState() => _WarDismissedScreenState(_war);
}

class _WarDismissedScreenState extends State<WarDismissedScreen> {
  War _war;

  _WarDismissedScreenState(this._war);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButton(context),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            WarInfoHeaderWidget(_war),
            Padding(
              padding:
                  EdgeInsets.only(left: 10.0.w, top: 10.0.h, bottom: 10.0.h),
              child: Text(
                "NO TEAM LEADERS WERE SELECTED",
                style: TextStyle(
                    fontFamily: "Quicksand",
                    fontSize: 16.0.sp,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            greyedOutButton(context, "War Dismissed", () => null)
          ],
        ),
      ),
    );
  }
}
