import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/constants/string_constants.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memecist/ui/constants/colors.dart';

class WarFAQWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(top: 20.0.h),
      primary: false,
      shrinkWrap: true,
      children: <Widget>[
        FAQCard(
            "What happens in Meme War?", StringConstants.what_happens_in_war),
        FAQCard(
            "How often can a group play a war?", StringConstants.war_frequency),
        FAQCard(
            "Which groups can play wars?", StringConstants.min_eligibility_war),
        FAQCard("What is the team size?", StringConstants.team_size),
        FAQCard("How long does a war last?", StringConstants.how_long),
        FAQCard(
            "What are the different war phases?", StringConstants.war_phases),
        FAQCard("Who approves team leader applications?",
            StringConstants.who_accepts_team_leaders),
        FAQCard("Who approves team members applications?",
            StringConstants.who_accepts_team_members),
        FAQCard("How are all the applications displayed to the team leaders?",
            StringConstants.applications_in_desc_order),
        FAQCard("Can a memer be part of multiple teams?",
            StringConstants.can_be_part_of_multiple_teams),
        FAQCard("How many memes can a team post for a war?",
            StringConstants.max_uploads),
        FAQCard("What if memes are not approved by admins in a war?",
            StringConstants.no_meme_approvals_needed),
        FAQCard("How is the result decided?", StringConstants.result),
        FAQCard("What if no team leaders are selected by the war creator?",
            StringConstants.war_dismissed)
      ],
    );
  }

  Widget FAQCard(String question, String answer) {
    return Card(
      margin: EdgeInsets.only(bottom: 10.0.h, left: 10.0.w, right: 10.0.w),
      elevation: 2.0.w,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(10.0.w),
        child: ExpandablePanel(
          header: Text(
            question,
            style: TextStyle(
                fontFamily: 'Quicksand',
                fontWeight: FontWeight.bold,
                fontSize: 18.0.sp),
          ),
          expanded: Text(
            answer,
            style: TextStyle(
                fontSize: 16.0.sp,
                fontFamily: 'Quicksand',
                color: MemecistColors.greyText),
          ),
        ),
      ),
    );
  }
}
