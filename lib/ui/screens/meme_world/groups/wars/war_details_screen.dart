import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/d_day_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_ended_bloc_provider.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/d_day_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_screen_router.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/team_members_selection/team_members_selection_screen_router.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_dismissed_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_ended_screen.dart';

class WarDetailsScreen extends StatefulWidget {
  final String _warID;

  WarDetailsScreen(this._warID);

  @override
  _WarDetailsScreenState createState() => _WarDetailsScreenState(_warID);
}

class _WarDetailsScreenState extends State<WarDetailsScreen> with RouteAware {
  String _warID;
  WarDetailsBloc _warDetailsBloc;
  bool _firstRun = true;
  War _war;
  String screenName = "";

  FirebaseAnalyticsObserver observer;

  _WarDetailsScreenState(this._warID);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_firstRun) {
      _warDetailsBloc = WarDetailsBlocProvider.of(context);
      observer = ObserverProvider.of(context);
      observer.subscribe(this, ModalRoute.of(context));
      _warDetailsBloc.getWar(_warID);
      _firstRun = false;
    }
  }

  @override
  void dispose() {
    _warDetailsBloc.dispose();
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPopNext() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  void didPush() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: _warDetailsBloc.war,
        builder: (context, AsyncSnapshot<War> asyncSnapshot) {
          if (asyncSnapshot.hasData) {
            _war = asyncSnapshot.data;
            switch (_war.status) {
              case FirestoreConstants.team_leaders_selection:
                return TeamLeadersSelectionScreenRouter(_war);
                break;
              case FirestoreConstants.team_members_selection:
                return TeamMembersSelectionScreenRouter(_war);
                break;
              case FirestoreConstants.d_day:
                screenName = FirebaseAnalyticsConstants.d_day_screen;
                _sendCurrentTabToAnalytics();
                return DDayBlocProvider(child: DDayScreen(_war));
                break;
              case FirestoreConstants.results:
                screenName = FirebaseAnalyticsConstants.war_ended_screen;
                _sendCurrentTabToAnalytics();
                return WarEndedBlocProvider(child: WarEndedScreen(_war));
                break;
              case FirestoreConstants.war_dismissed:
                screenName = FirebaseAnalyticsConstants.war_dismissed_screen;
                _sendCurrentTabToAnalytics();
                return WarDismissedScreen(_war);
              default:
                return Container();
                break;
            }
          } else
            return Scaffold(
                backgroundColor: Colors.white,
                body: Center(
                  child: CircularProgressIndicator(),
                ));
        },
      ),
    );
  }

  void _sendCurrentTabToAnalytics() {
    observer.analytics.setCurrentScreen(
      screenName: screenName,
    );
  }
}
