import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_post/create_meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/d_day_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/d_day_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/create_meme_post_screen.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_info_header_widget.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DDayScreen extends StatefulWidget {
  final War _war;

  DDayScreen(this._war);

  @override
  _DDayScreenState createState() => _DDayScreenState(_war);
}

class _DDayScreenState extends State<DDayScreen> {
  War _war;
  WarTeam _warTeam;

  DDayBloc _dDayBloc;

  _DDayScreenState(this._war);

  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreMemePosts = true;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _dDayBloc = DDayBlocProvider.of(context);
      _dDayBloc.initializeDDayBloc(_war);

      _dDayBloc.fetchWarMemes();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreMemePosts &&
            !_isLoading) {
          _dDayBloc.fetchWarMemes();
        }
      });

      _dDayBloc.hasMoreMemes.listen((hasMoreData) {
        _hasMoreMemePosts = hasMoreData;
      });

      _dDayBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      _dDayBloc.user.listen((User user) {
        if (_war.participants.contains(user.uid)) {
          _dDayBloc.getWarTeam();
        }
      });

      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _dDayBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButton(context),
      body: ListView(
        shrinkWrap: true,
        controller: _scrollController,
        children: <Widget>[
          WarInfoHeaderWidget(_war),
          warParticipantHolder(_war),
          SizedBox(
            height: 10.0.h,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0.w, top: 10.0.h, bottom: 10.0.h),
            child: Text(
              "MEMES",
              style: TextStyle(
                  fontFamily: "Quicksand",
                  fontSize: 14.0.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
          memes(),
        ],
      ),
    );
  }

  Widget memes() {
    return ListView(
      primary: false,
      shrinkWrap: true,
      children: <Widget>[
        StreamBuilder(
            stream: _dDayBloc.memesList,
            builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data.length == 0) {
                  return Center(
                    child: Text(
                      "NO MEMES POSTED YET!",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                }
                return warPostsList(asyncSnapshot.data);
              } else {
                return Container();
              }
            }),
        StreamBuilder(
            stream: _dDayBloc.isLoading,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING MEMES...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
      ],
    );
  }

  Widget warParticipantHolder(War war) {
    return StreamBuilder(
        stream: _dDayBloc.user,
        builder: (context, AsyncSnapshot<User> asyncSnapshot) {
          if (asyncSnapshot.hasData) {
            if (_war.participants.contains(asyncSnapshot.data.uid)) {
              return StreamBuilder(
                stream: _dDayBloc.warTeam,
                builder: (context, AsyncSnapshot<WarTeam> asyncSnapshot) {
                  if (asyncSnapshot.hasData) {
                    _warTeam = asyncSnapshot.data;
                    if (_warTeam.memesCount < 4) {
                      return Column(
                        children: <Widget>[
                          SizedBox(
                            height: 10.0.h,
                          ),
                          actionButton(context, "Post Meme", postMeme),
                          SizedBox(
                            height: 10.0.h,
                          ),
                          Text(
                            "Your team has posted ${_warTeam.memesCount} memes",
                            style: TextStyle(
                                fontSize: 14.0.sp, fontFamily: 'Quicksand'),
                          )
                        ],
                      );
                    } else {
                      return Column(
                        children: [
                          SizedBox(
                            height: 10.0.h,
                          ),
                          Text(
                            "Your team has posted ${_warTeam.memesCount} memes",
                            style: TextStyle(
                                fontSize: 16.0.sp, fontFamily: 'Quicksand'),
                          )
                        ],
                      );
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              );
            } else {
              return Container();
            }
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  Widget warPostsList(List<MemePost> memesList) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: memesList.length,
        itemBuilder: (context, index) {
          return MemePostBlocProvider(
            child: MemePostWidget(
              memesList[index],
              key: ObjectKey(memesList[index].postID),
              onDeleteClicked: () => onDeletePost(memesList[index]),
            ),
          );
        });
  }

  void postMeme() {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) {
                  return CreateMemePostBlocProvider(
                    child: CreateMemePostScreen(
                      groupID: _war.groupID,
                      groupName: _war.groupName,
                      isGroupAdmin: false,
                      warName: _war.warName,
                      warID: _war.warID,
                      teamID: _warTeam.teamID,
                      teamName: _warTeam.teamName,
                    ),
                  );
                },
                settings: RouteSettings(
                    name: FirebaseAnalyticsConstants.create_meme_post_screen)))
        .then((value) => Future.delayed(Duration(milliseconds: 500), () {
              _dDayBloc.getWarTeam();
              _dDayBloc.getWar();

              _dDayBloc.memePosts.clear();
              _hasMoreMemePosts = true;
              _dDayBloc.runCount = 0;
              _dDayBloc.fetchWarMemes();
            }));
  }

  void onDeletePost(MemePost memePost) async {
    repository.firebaseAnalytics
        .logEvent(name: FirebaseAnalyticsConstants.delete_post, parameters: {
      FirebaseAnalyticsConstants.source: FirebaseAnalyticsConstants.d_day_screen
    });
    _dDayBloc.deletePost(memePost);
  }
}
