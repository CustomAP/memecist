import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/view_all_teams_bloc_provider.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/view_all_teams_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WarInfoHeaderWidget extends StatefulWidget {
  final War _war;

  WarInfoHeaderWidget(this._war);

  @override
  _WarInfoHeaderWidgetState createState() => _WarInfoHeaderWidgetState(_war);
}

class _WarInfoHeaderWidgetState extends State<WarInfoHeaderWidget> {
  War _war;

  _WarInfoHeaderWidgetState(this._war);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 20.0.h,
                ),
                Container(
                  width: 100.0.w,
                  height: 100.0.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(50.0.w)),
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(_war.warPic),
                          fit: BoxFit.cover)),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Flexible(
                  child: AutoSizeText(
                    '${_war.warName}',
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    minFontSize: 12.0.sp,
                    stepGranularity: 12.0.sp / 12,
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 23.0.sp,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Flexible(
                  child: InkWell(
                    onTap: () => openGroupDetails(),
                    child: AutoSizeText(
                      '${_war.groupName}',
                      textAlign: TextAlign.center,
                      maxLines: 1,
                      minFontSize: 12.0.sp,
                      stepGranularity: 12.0.sp / 12,
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 16.0.sp,
                          color: Colors.black),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Flexible(
                  child: Text(
                    '${_war.warDescription}',
                    maxLines: 3,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 15.0.sp,
                        color: MemecistColors.greyText),
                  ),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  '${_war.status}',
                  style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontWeight: FontWeight.bold,
                      fontSize: 17.0.sp,
                      color: MemecistColors.primaryColor),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                timeWidget(),
                SizedBox(
                  height: 10.0.h,
                ),
                _war.status == FirestoreConstants.d_day ||
                    _war.status == FirestoreConstants.team_members_selection
                    ? greyedOutButton(
                    context, "VIEW ALL TEAMS", openViewAllTeamsScreen,
                    width: 200.0)
                    : Container(),
                SizedBox(
                  height: 20.0.h,
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: 10.0.h,
        ),
        //detailed view
      ],
    );
  }

  Widget timeWidget() {
    String time = "";

    switch (_war.status) {
      case FirestoreConstants.team_members_selection:
        time =
        "ends ${timeago.format(_war.teamMembersSelectionEndTime, locale: 'en',
            allowFromNow: true)}";
        break;
      case FirestoreConstants.team_leaders_selection:
        time =
        "ends ${timeago.format(_war.teamLeadersSelectionEndTime, locale: 'en',
            allowFromNow: true)}";
        break;
      case FirestoreConstants.d_day:
        time =
        "ends ${timeago.format(
            _war.dDayEndTime, locale: 'en', allowFromNow: true)}";
        break;
      case FirestoreConstants.results:
        time = "ended ${timeago.format(_war.dDayEndTime, locale: 'en')}";
    }
    return Text(
      time,
      style: TextStyle(
        fontFamily: 'Quicksand',
        fontSize: 15.0.sp,
      ),
    );
  }

  void openViewAllTeamsScreen() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ViewAllTeamsBlocProvider(
            child: ViewAllTeamsScreen(_war.warID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.view_all_teams_screen)));
  }

  void openGroupDetails() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return GroupDetailsBlocProvider(
            child: GroupDetailsScreen(_war.groupID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.group_details_screen
        )
    ));
  }
}
