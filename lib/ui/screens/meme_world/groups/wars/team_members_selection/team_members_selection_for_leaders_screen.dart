import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_members_selection/team_members_selection_for_leaders_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_members_selection/team_members_selection_for_leaders_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_application_model.dart';
import 'package:memecist/core/models/war_member_application_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_info_header_widget.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TeamMembersSelectionForLeadersScreen extends StatefulWidget {
  final War _war;

  TeamMembersSelectionForLeadersScreen(this._war);

  @override
  _TeamMembersSelectionForLeadersScreenState createState() =>
      _TeamMembersSelectionForLeadersScreenState(_war);
}

class _TeamMembersSelectionForLeadersScreenState
    extends State<TeamMembersSelectionForLeadersScreen> {
  ScrollController _scrollController = ScrollController();
  TeamMembersSelectionForLeadersBloc _teamMembersSelectionForLeadersBloc;
  bool _firstRun = true;
  War _war;
  WarTeam _warTeam;
  bool hasMoreApplications = true;
  bool _isLoading = false;

  _TeamMembersSelectionForLeadersScreenState(this._war);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_firstRun) {
      _teamMembersSelectionForLeadersBloc =
          TeamMembersSelectionForLeadersBlocProvider.of(context);

      _teamMembersSelectionForLeadersBloc
          .initializeTeamMembersSelectionForLeadersBloc(_war);
      _teamMembersSelectionForLeadersBloc.fetchTeamMembersApplications();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            hasMoreApplications &&
            !_isLoading) {
          _teamMembersSelectionForLeadersBloc.fetchTeamMembersApplications();
        }
      });

      _teamMembersSelectionForLeadersBloc.hasMoreApplications
          .listen((hasMoreData) {
        hasMoreApplications = hasMoreData;
      });

      _teamMembersSelectionForLeadersBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      _firstRun = false;
    }
  }

  @override
  void dispose() {
    _teamMembersSelectionForLeadersBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _teamMembersSelectionForLeadersBloc.warTeam,
        builder: (context, AsyncSnapshot<WarTeam> snapshot) {
          if (snapshot.hasData) {
            _warTeam = snapshot.data;
            return Scaffold(
                backgroundColor: MemecistColors.backgroundColor,
                appBar: appBarWithBackButton(context),
                bottomNavigationBar: BottomAppBar(
                    elevation: 0.0,
                    color: Colors.transparent,
                    child: _warTeam.membersCount < _war.perTeamLimit
                        ? StreamBuilder(
                            stream: _teamMembersSelectionForLeadersBloc
                                .selectedTeamMembersList,
                            builder: (context,
                                AsyncSnapshot<List<WarMemberApplication>>
                                    snapshot) {
                              if (snapshot.hasData) {
                                if (snapshot.data.length > 0) {
                                  return approveSelection(
                                      MemecistColors.primaryColor,
                                      () => approveTeamLeadersSelection(
                                          snapshot.data));
                                } else {
                                  return approveSelection(
                                      Colors.grey, () => null);
                                }
                              } else {
                                return approveSelection(
                                    Colors.grey, () => null);
                              }
                            })
                        : null),
                body: ListView(
                  controller: _scrollController,
                  shrinkWrap: true,
                  children: <Widget>[
                    WarInfoHeaderWidget(_war),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 10.0.w, top: 5.0.h, bottom: 10.0.h),
                      child: Text(
                        "MY TEAM",
                        style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 14.0.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    myTeamHolder(),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 10.0.w, top: 10.0.h, bottom: 10.0.h),
                      child: Text(
                        "APPLICATIONS",
                        style: TextStyle(
                            fontFamily: "Quicksand",
                            fontSize: 14.0.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    warApplicationHolder(),
                  ],
                ));
          } else {
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        });
  }

  Widget myTeamHolder() {
    return ListView(
      shrinkWrap: true,
      primary: false,
      children: <Widget>[
        StreamBuilder(
            stream: _teamMembersSelectionForLeadersBloc.warTeam,
            builder: (context, AsyncSnapshot<WarTeam> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data.teamMembers.length == 0) {
                  return Center(
                    child: Text(
                      "NO TEAM MEMBERS SELECTED YET!",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                }
                return myTeamMembersList(asyncSnapshot.data.teamMembers);
              } else
                return Container();
            }),
        StreamBuilder(
            stream: _teamMembersSelectionForLeadersBloc.isLoading,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING TEAM MEMBERS...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
      ],
    );
  }

  Widget warApplicationHolder() {
    return ListView(
      shrinkWrap: true,
      primary: false,
      children: <Widget>[
        StreamBuilder(
            stream: _teamMembersSelectionForLeadersBloc.warApplicationsList,
            builder: (context,
                AsyncSnapshot<List<WarMemberApplication>> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data.length == 0) {
                  return Center(
                    child: Text(
                      "NO APPLICATIONS YET!",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                }
                return teamMembersApplicationsList(asyncSnapshot.data);
              } else
                return Container();
            }),
        StreamBuilder(
            stream: _teamMembersSelectionForLeadersBloc.isLoading,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING APPLICATIONS...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
      ],
    );
  }

  Widget teamMembersApplicationsList(
      List<WarMemberApplication> warApplications) {
    return ListView.builder(
        itemCount: warApplications.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
            padding:
                EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 10.0.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(child: userItem(warApplications[index].user)),
                warApplications[index].isAccepted
                    ? greyedOutButton(context, "SELECTED", () => null)
                    : warApplications[index].isSelected
                        ? actionButton(context, "UNSELECT",
                            () => unSelectTeamLeader(warApplications[index]))
                        : actionButton(context, "SELECT",
                            () => selectTeamLeader(warApplications[index]))
              ],
            ),
          );
        });
  }

  Widget myTeamMembersList(List<MemecistUser> users) {
    return ListView.builder(
        itemCount: users.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
            padding:
                EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 10.0.h),
            child: userItem(users[index]),
          );
        });
  }

  Widget userItem(MemecistUser user) {
    return InkWell(
      onTap: () => openProfile(user.userID),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 50.0.w,
            height: 50.0.w,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: MemecistColors.borderColorGrey),
                borderRadius: BorderRadius.all(Radius.circular(25.0.w))),
            child: AutoSizeText(
              user.rep.toStringAsPrecision(3),
              style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
              maxLines: 1,
              minFontSize: 10.0.sp,
              stepGranularity: 10.0.sp / 10,
            ),
          ),
          SizedBox(
            width: 10.0.w,
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: AutoSizeText(
                    "${user.firstName} ${user.lastName}",
                    maxLines: 1,
                    minFontSize: 12.0.sp,
                    stepGranularity: 12.0.sp / 12,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontSize: 16.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Flexible(
                  child: AutoSizeText(
                    "@${user.userName}",
                    maxLines: 1,
                    minFontSize: 12.0.sp,
                    stepGranularity: 12.0.sp / 12,
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 14.0.sp,
                    ),
                  ),
                )
              ],
            ),
          ),
          user.isVerified ? Container(child: verifiedBadge()) : Container()
        ],
      ),
    );
  }

  Widget approveSelection(Color color, Function() action) {
    return InkWell(
      onTap: action,
      child: Container(
        alignment: Alignment.center,
        height: 50.0.h,
        color: color,
        child: Text(
          "APPROVE",
          style: TextStyle(
              color: Colors.white,
              fontFamily: 'Quicksand',
              fontSize: 20.0.sp,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  void openProfile(String userID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }

  void unSelectTeamLeader(WarApplication warApplication) {
    _teamMembersSelectionForLeadersBloc.unSelectApplication(warApplication);
  }

  void approveTeamLeadersSelection(List<WarApplication> selectedTeamLeaders) {
    if (selectedTeamLeaders.length + _warTeam.membersCount <=
        _war.perTeamLimit) {
      showDialog(
          context: context,
          builder: (context) {
            return new AlertDialog(
              title: Text(
                "Confirm Team Members?",
                style: TextStyle(fontSize: 16.0.sp),
              ),
              content: Text(
                "Are you sure you want to select these team leaders?\nYou cannot remove someone from your team for a war after you select them",
                style: TextStyle(fontSize: 14.0.sp),
              ),
              actions: <Widget>[
                dialogButton("APPROVE", () {
                  _teamMembersSelectionForLeadersBloc.approveTeamMembers();
                  Navigator.of(context).pop();
                }),
                dialogButton("CANCEL", () => Navigator.of(context).pop(),
                    color: Colors.red)
              ],
            );
          });
    } else {
      Fluttertoast.showToast(
          msg:
              "The number of team members per team cannot be more than ${_war.perTeamLimit}");
    }
  }

  void selectTeamLeader(WarApplication warApplication) {
    _teamMembersSelectionForLeadersBloc.selectApplication(warApplication);
  }
}
