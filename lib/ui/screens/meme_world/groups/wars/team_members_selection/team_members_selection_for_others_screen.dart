import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_members_selection/team_members_selection_for_others_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_members_selection/team_members_selection_for_others_bloc_provider.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_faq_widget.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_info_header_widget.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TeamMembersSelectionForOthersScreen extends StatefulWidget {
  final War _war;

  TeamMembersSelectionForOthersScreen(this._war);

  @override
  _TeamMembersSelectionForOthersScreenState createState() =>
      _TeamMembersSelectionForOthersScreenState(_war);
}

class _TeamMembersSelectionForOthersScreenState
    extends State<TeamMembersSelectionForOthersScreen> {
  TeamMembersSelectionForOthersBloc _teamMembersSelectionForOthersBloc;

  bool _firstRun = true;
  War _war;

  _TeamMembersSelectionForOthersScreenState(this._war);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _teamMembersSelectionForOthersBloc =
        TeamMembersSelectionForOthersBlocProvider.of(context);

    if (_firstRun) {
      _teamMembersSelectionForOthersBloc
          .initializeTeamMembersSelectionForOthersBloc(_war);
      _teamMembersSelectionForOthersBloc.getTeamMemberApplication();
      _firstRun = false;
    }
  }

  @override
  void dispose() {
    _teamMembersSelectionForOthersBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButton(context),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          WarInfoHeaderWidget(_war),
          StreamBuilder(
            stream: _teamMembersSelectionForOthersBloc.isApplied,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data) {
                  return teamMembersApplication(isApplied: true);
                } else {
                  return teamMembersApplication(isApplied: false);
                }
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          )
        ],
      ),
    );
  }

  Widget teamMembersApplication({bool isApplied}) {
    if (isApplied) {
      return StreamBuilder(
        stream: _teamMembersSelectionForOthersBloc.isAccepted,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data) {
              return ListView(
                shrinkWrap: true,
                primary: false,
                children: <Widget>[
                  UnconstrainedBox(
                    child: Container(
                        padding: EdgeInsets.only(top: 10.0.h),
                        child: greyedOutButton(context,
                            "SELECTED AS TEAM MEMBER", () => null,
                            width: 250.0)),
                  ),
                  WarFAQWidget()
                ],
              );
            } else {
              return ListView(
                shrinkWrap: true,
                primary: false,
                children: <Widget>[
                  UnconstrainedBox(
                    child: Container(
                        padding: EdgeInsets.only(top: 10.0.h),
                        child: greyedOutButton(context,
                            "APPLIED FOR TEAM MEMBER", () => null,
                            width: 250.0)),
                  ),
                  WarFAQWidget()
                ],
              );
            }
          } else {
            return ListView(
              shrinkWrap: true,
              primary: false,
              children: <Widget>[
                UnconstrainedBox(
                  child: Container(
                      padding: EdgeInsets.only(top: 10.0.h),
                      child: greyedOutButton(context,
                          "APPLIED FOR TEAM MEMBER", () => null,
                          width: 250.0)),
                ),
                WarFAQWidget()
              ],
            );
          }
        },
      );
    } else {
      return ListView(
        shrinkWrap: true,
        primary: false,
        children: <Widget>[
          UnconstrainedBox(
            child: Container(
              padding: EdgeInsets.only(top: 10.0.h),
              child: actionButton(context, 
                  "APPLY FOR TEAM MEMBER", () => applyAsTeamMember(),
                  width: 250.0),
            ),
          ),
          WarFAQWidget()
        ],
      );
    }
  }

  void applyAsTeamMember() {
    showDialog(
        context: context,
        builder: (context) {
          return new AlertDialog(
            title: Text("Team Member Application", style: TextStyle(fontSize: 16.0.sp),),
            content: Text("Apply for Team Member?", style: TextStyle(fontSize: 14.0.sp),),
            actions: <Widget>[
              dialogButton("APPLY", () {
                _teamMembersSelectionForOthersBloc.applyAsTeamMember();
                Navigator.of(context).pop();
                Fluttertoast.showToast(
                    msg: "You'll be notified if selected as a Team Member");
              }),
              dialogButton("CANCEL", () => Navigator.of(context).pop(),
                  color: Colors.red)
            ],
          );
        });
  }
}
