import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/app.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_others_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_war_creator_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_members_selection/team_members_selection_for_leaders_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_members_selection/team_members_selection_for_others_bloc_provider.dart';
import 'package:memecist/core/constants/firestore_constants.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/core/resources/repository.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_others_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_war_creator_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/team_members_selection/team_members_selection_for_leaders_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/team_members_selection/team_members_selection_for_others_screen.dart';

class TeamMembersSelectionScreenRouter extends StatefulWidget {
  final War _war;

  TeamMembersSelectionScreenRouter(this._war);

  @override
  _TeamMembersSelectionScreenRouterState createState() =>
      _TeamMembersSelectionScreenRouterState(_war);
}

class _TeamMembersSelectionScreenRouterState
    extends State<TeamMembersSelectionScreenRouter> with RouteAware {
  final War _war;
  FirebaseAnalyticsObserver observer;
  String screenName = "";

  bool isFirstRun = true;

  _TeamMembersSelectionScreenRouterState(this._war);

  @override
  void didPush() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    if (screenName != "") _sendCurrentTabToAnalytics();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      observer = ObserverProvider.of(context);
      observer.subscribe(this, ModalRoute.of(context));
      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: repository.getLoggedInUser(),
      builder: (context, AsyncSnapshot<User> asyncSnapshot) {
        if (asyncSnapshot.hasData) {
          if (_war.teamLeaders.contains(asyncSnapshot.data.uid)) {
            screenName = FirebaseAnalyticsConstants
                .team_members_selection_for_leaders_screen;
            _sendCurrentTabToAnalytics();
            return TeamMembersSelectionForLeadersBlocProvider(
              child: TeamMembersSelectionForLeadersScreen(_war),
            );
          } else {
            screenName = FirebaseAnalyticsConstants
                .team_members_selection_for_others_screen;
            _sendCurrentTabToAnalytics();
            return TeamMembersSelectionForOthersBlocProvider(
              child: TeamMembersSelectionForOthersScreen(_war),
            );
          }
        } else {
          return Scaffold(
              backgroundColor: Colors.white,
              body: Center(
                child: CircularProgressIndicator(),
              ));
        }
      },
    );
  }

  void _sendCurrentTabToAnalytics() {
    observer.analytics.setCurrentScreen(
      screenName: screenName,
    );
  }
}
