import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_post/meme_post_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/view_all_teams_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_ended_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_ended_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_widget.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/view_all_teams_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_info_header_widget.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_team_widget.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WarEndedScreen extends StatefulWidget {
  final War _war;

  WarEndedScreen(this._war);

  @override
  _WarEndedScreenState createState() => _WarEndedScreenState(_war);
}

class _WarEndedScreenState extends State<WarEndedScreen> {
  War _war;

  _WarEndedScreenState(this._war);

  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreMemePosts = true;
  bool _isLoading = false;
  WarEndedBloc _warEndedBloc;
  GlobalKey _globalKey = GlobalKey();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _warEndedBloc = WarEndedBlocProvider.of(context);
      _warEndedBloc.initializeDDayBloc(_war);

      _warEndedBloc.fetchWarMemes();
      _warEndedBloc.getMemeOfTheWar();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreMemePosts &&
            !_isLoading) {
          _warEndedBloc.fetchWarMemes();
        }
      });

      _warEndedBloc.hasMoreMemes.listen((hasMoreData) {
        _hasMoreMemePosts = hasMoreData;
      });

      _warEndedBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });
      isFirstRun = false;
    }
  }

  @override
  void dispose() {
    _warEndedBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButton(context),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            WarInfoHeaderWidget(_war),
            warWinnerCard(),
            warResultHolder(_war),
            Padding(
              padding:
                  EdgeInsets.only(left: 10.0.w, top: 10.0.h, bottom: 10.0.h),
              child: Text(
                "MEMES",
                style: TextStyle(
                    fontFamily: "Quicksand",
                    fontSize: 14.0.sp,
                    fontWeight: FontWeight.bold),
              ),
            ),
            memes(),
          ],
        ),
      ),
    );
  }

  Widget viewAllTeamsWidget() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 10.0.h,
        ),
        Center(
            child: greyedOutButton(context, "VIEW ALL TEAMS", openViewAllTeamsScreen,
                width: 200.0)),
        SizedBox(
          height: 10.0.h,
        ),
      ],
    );
  }

  Widget memes() {
    return ListView(
      primary: false,
      shrinkWrap: true,
      children: <Widget>[
        StreamBuilder(
            stream: _warEndedBloc.memesList,
            builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data.length == 0) {
                  return Center(
                    child: Text(
                      "NO MEMES POSTED YET!",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                }
                return warPostsList(asyncSnapshot.data);
              } else {
                return Container();
              }
            }),
        StreamBuilder(
            stream: _warEndedBloc.isLoading,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING MEMES...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
      ],
    );
  }

  Widget warWinnerCard() {
    return StreamBuilder(
        stream: _warEndedBloc.user,
        builder: (context, AsyncSnapshot<MemecistUser> snapshot) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10.0.w),
                  child: Text(
                    "HEARTIEST CONGRATULATIONS ❤",
                    style: TextStyle(
                        fontFamily: "Quicksand",
                        fontSize: 14.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                  child: Text(
                    "We've curated this card as a token of appreciation!",
                    style:
                        TextStyle(fontFamily: "Quicksand", fontSize: 14.0.sp),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0.w, right: 20.0.w, top: 10.0.h, bottom: 10.0.h),
                  child: RepaintBoundary(
                    key: _globalKey,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0.w)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                blurRadius: 3.0.w,
                                spreadRadius: 3.0.w)
                          ]),
                      child: Padding(
                        padding: EdgeInsets.all(30.0.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: [
                                circularImageButton(_war.warPic,
                                    size: 80.0.w, radius: 40.0.w),
                                SizedBox(
                                  width: 30.0.w,
                                ),
                                Flexible(
                                  child: AutoSizeText(
                                    "${_war.warName}",
                                    maxLines: 3,
                                    minFontSize: 12.0.sp,
                                    stepGranularity: 12.0.sp/12,
                                    style: TextStyle(
                                        fontSize: 30.0.sp,
                                        fontFamily: 'Quicksand',
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Text(
                              "War Winner",
                              style: TextStyle(
                                fontSize: 22.0.sp,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Quicksand',
                                color: MemecistColors.primaryColor,
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Text.rich(TextSpan(children: [
                              TextSpan(
                                text: "Memecist congratulates ",
                                style: TextStyle(
                                  fontSize: 18.0.sp,
                                  fontFamily: 'Quicksand',
                                  color: Colors.black,
                                ),
                              ),
                              TextSpan(
                                text:
                                    "${snapshot.data.firstName} ${snapshot.data.lastName}",
                                style: TextStyle(
                                  fontSize: 18.0.sp,
                                  fontFamily: 'Quicksand',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              TextSpan(
                                text: " for this remarkable feat!",
                                style: TextStyle(
                                  fontSize: 18.0.sp,
                                  fontFamily: 'Quicksand',
                                  color: Colors.black,
                                ),
                              ),
                            ])),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0.w, right: 20.0.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => shareCard(),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                "SHARE",
                                style: TextStyle(
                                    fontFamily: "Quicksand",
                                    fontSize: 14.0.sp,
                                    color: MemecistColors.primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 5.0.w,
                              ),
                              CachedNetworkImage(
                                height: 20.0.w,
                                width: 20.0.w,
                                imageUrl:
                                    FirebaseStorageConstants.instagramLogo,
                              ),
                              SizedBox(
                                width: 5.0.w,
                              ),
                              CachedNetworkImage(
                                height: 20.0.w,
                                width: 20.0.w,
                                imageUrl: FirebaseStorageConstants.facebookLogo,
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            );
          } else {
            return Container();
          }
        });
  }

  Widget warResultHolder(War war) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 10.0.h,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10.0.w),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              circularImageButton(FirebaseStorageConstants.warWinnerMedalUrl),
              SizedBox(
                width: 10.0.w,
              ),
              Text(
                "WINNER TEAM",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    fontSize: 16.0.sp),
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 10.0.w, right: 10.0.w, top: 10.0.h),
          child: WarTeamWidget(war.winnerTeam),
        ),
        SizedBox(
          height: 10.0.h,
        ),
        viewAllTeamsWidget(),
        SizedBox(
          height: 20.0.h,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10.0.w),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              circularImageButton(FirebaseStorageConstants.memeOfTheWarUrl),
              SizedBox(
                width: 10.0.w,
              ),
              Text(
                "MEME OF THE WAR",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    fontSize: 16.0.sp),
              )
            ],
          ),
        ),
        SizedBox(
          height: 10.0.h,
        ),
        StreamBuilder(
            stream: _warEndedBloc.memeOfTheWar,
            builder: (context, AsyncSnapshot<MemePost> snapshot) {
              if (snapshot.hasData) {
                return MemePostBlocProvider(
                  child: MemePostWidget(
                    snapshot.data,
                    showActions: false,
                  ),
                );
              } else {
                return Container();
              }
            })
      ],
    );
  }

  Widget warPostsList(List<MemePost> memesList) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: memesList.length,
        itemBuilder: (context, index) {
          return MemePostBlocProvider(
            child: MemePostWidget(
              memesList[index],
              showActions: false,
            ),
          );
        });
  }

  void openViewAllTeamsScreen() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ViewAllTeamsBlocProvider(
            child: ViewAllTeamsScreen(_war.warID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.view_all_teams_screen)));
  }

  void shareCard() {
    _warEndedBloc.shareWarWinnerCard(_globalKey);
  }
}
