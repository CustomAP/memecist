import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_others_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_others_bloc_provider.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_faq_widget.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_info_header_widget.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TeamLeadersSelectionForOthersScreen extends StatefulWidget {
  final War _war;

  TeamLeadersSelectionForOthersScreen(this._war);

  @override
  _TeamLeadersSelectionForOthersScreenState createState() =>
      _TeamLeadersSelectionForOthersScreenState(_war);
}

class _TeamLeadersSelectionForOthersScreenState
    extends State<TeamLeadersSelectionForOthersScreen> {
  TeamLeadersSelectionForOthersBloc _teamLeadersSelectionForOthersBloc;

  bool _firstRun = true;
  War _war;

  _TeamLeadersSelectionForOthersScreenState(this._war);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _teamLeadersSelectionForOthersBloc =
        TeamLeadersSelectionForOthersBlocProvider.of(context);

    if (_firstRun) {
      _teamLeadersSelectionForOthersBloc
          .initializeTeamLeadersSelectionForOthersBloc(_war);
      _teamLeadersSelectionForOthersBloc.getSelfWarLeaderApplication();
      _firstRun = false;
    }
  }

  @override
  void dispose() {
    _teamLeadersSelectionForOthersBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButton(context),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          WarInfoHeaderWidget(_war),
          StreamBuilder(
            stream: _teamLeadersSelectionForOthersBloc.isApplied,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data) {
                  return teamLeadersApplication(isApplied: true);
                } else {
                  return teamLeadersApplication(isApplied: false);
                }
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          )
        ],
      ),
    );
  }

  Widget teamLeadersApplication({bool isApplied}) {
    if (isApplied) {
      return StreamBuilder(
        stream: _teamLeadersSelectionForOthersBloc.isAccepted,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data) {
              return ListView(
                shrinkWrap: true,
                primary: false,
                children: <Widget>[
                  UnconstrainedBox(
                    child: Container(
                        padding: EdgeInsets.only(top: 10.0.h),
                        child: greyedOutButton(context,
                            "SELECTED AS TEAM LEADER", () => null,
                            width: 250.0)),
                  ),
                  WarFAQWidget()
                ],
              );
            } else {
              return ListView(
                shrinkWrap: true,
                primary: false,
                children: <Widget>[
                  UnconstrainedBox(
                    child: Container(
                        padding: EdgeInsets.only(top: 10.0.h),
                        child: greyedOutButton(context,
                            "APPLIED FOR TEAM LEADER", () => null,
                            width: 250.0)),
                  ),
                  WarFAQWidget()
                ],
              );
            }
          } else {
            return ListView(
              shrinkWrap: true,
              primary: false,
              children: <Widget>[
                UnconstrainedBox(
                  child: Container(
                      padding: EdgeInsets.only(top: 10.0.h),
                      child: greyedOutButton(context,
                          "APPLIED FOR TEAM LEADER", () => null,
                          width: 250.0)),
                ),
                WarFAQWidget()
              ],
            );
          }
        },
      );
    } else {
      return ListView(
        shrinkWrap: true,
        primary: false,
        children: <Widget>[
          UnconstrainedBox(
            child: Container(
              padding: EdgeInsets.only(top: 10.0.h),
              child: actionButton(context, 
                  "APPLY FOR TEAM LEADER", () => applyAsTeamLeaders(),
                  width: 250.0),
            ),
          ),
          WarFAQWidget()
        ],
      );
    }
  }

  void applyAsTeamLeaders() {
    showDialog(
        context: context,
        builder: (context) {
          return new AlertDialog(
            title: Text("Team Leader Application", style: TextStyle(fontSize: 16.0.sp),),
            content: Text("Apply for Team Leader?", style: TextStyle(fontSize: 14.0.sp),),
            actions: <Widget>[
              dialogButton("APPLY", () {
                _teamLeadersSelectionForOthersBloc.applyAsTeamLeader();
                Navigator.of(context).pop();
                Fluttertoast.showToast(
                    msg: "You'll be notified if selected as a Team Leader");
              }),
              dialogButton("CANCEL", () => Navigator.of(context).pop(),
                  color: Colors.red)
            ],
          );
        });
  }
}
