import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_war_creator_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/team_leaders_selection/team_leaders_selection_for_war_creator_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/models/war_application_model.dart';
import 'package:memecist/core/models/war_leader_application_model.dart';
import 'package:memecist/core/models/war_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_info_header_widget.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TeamLeadersSelectionForWarCreatorScreen extends StatefulWidget {
  final War _war;

  TeamLeadersSelectionForWarCreatorScreen(this._war);

  @override
  _TeamLeadersSelectionForWarCreatorScreenState createState() =>
      _TeamLeadersSelectionForWarCreatorScreenState(_war);
}

class _TeamLeadersSelectionForWarCreatorScreenState
    extends State<TeamLeadersSelectionForWarCreatorScreen> {
  ScrollController _scrollController = ScrollController();
  TeamLeadersSelectionForWarCreatorBloc _teamLeadersSelectionForWarCreatorBloc;
  bool _firstRun = true;
  War _war;
  bool hasMoreApplications = true;
  bool _isLoading = false;

  _TeamLeadersSelectionForWarCreatorScreenState(this._war);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_firstRun) {
      _teamLeadersSelectionForWarCreatorBloc =
          TeamLeadersSelectionForWarCreatorBlocProvider.of(context);

      _teamLeadersSelectionForWarCreatorBloc
          .initializeTeamLeadersSelectionForWarCreatorBloc(_war);
      if (!_war.areTeamLeadersSelected)
        _teamLeadersSelectionForWarCreatorBloc.fetchTeamLeadersApplications();
      else
        _teamLeadersSelectionForWarCreatorBloc.fetchSelectedTeamLeaders();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            hasMoreApplications &&
            !_isLoading &&
            !_war.areTeamLeadersSelected) {
          _teamLeadersSelectionForWarCreatorBloc.fetchTeamLeadersApplications();
        }
      });

      _teamLeadersSelectionForWarCreatorBloc.hasMoreApplications
          .listen((hasMoreData) {
        hasMoreApplications = hasMoreData;
      });

      _teamLeadersSelectionForWarCreatorBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      _firstRun = false;
    }
  }

  @override
  void dispose() {
    _teamLeadersSelectionForWarCreatorBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _teamLeadersSelectionForWarCreatorBloc.war,
        builder: (context, AsyncSnapshot<War> snapshot) {
          if (snapshot.hasData) {
            _war = snapshot.data;
          }
          return Scaffold(
              backgroundColor: MemecistColors.backgroundColor,
              appBar: appBarWithBackButton(context),
              bottomNavigationBar: BottomAppBar(
                  elevation: 0.0,
                  color: Colors.transparent,
                  child: _war.areTeamLeadersSelected
                      ? null
                      : StreamBuilder(
                          stream: _teamLeadersSelectionForWarCreatorBloc
                              .selectedTeamLeadersList,
                          builder: (context,
                              AsyncSnapshot<List<WarLeaderApplication>>
                                  snapshot) {
                            if (snapshot.hasData) {
                              if (snapshot.data.length > 0) {
                                return approveSelection(
                                    MemecistColors.primaryColor,
                                    () => approveTeamLeadersSelection(
                                        snapshot.data));
                              } else {
                                return approveSelection(
                                    Colors.grey, () => null);
                              }
                            } else {
                              return approveSelection(Colors.grey, () => null);
                            }
                          })),
              body: ListView(
                controller: _scrollController,
                shrinkWrap: true,
                children: <Widget>[
                  WarInfoHeaderWidget(_war),
                  _war.areTeamLeadersSelected
                      ? selectedTeamLeadersHolder()
                      : warApplicationHolder(),
                  SizedBox(
                    height: 10.0.h,
                  ),
                ],
              ));
        });
  }

  Widget selectedTeamLeadersHolder() {
    return ListView(
      shrinkWrap: true,
      primary: false,
      children: <Widget>[
        StreamBuilder(
            stream:
                _teamLeadersSelectionForWarCreatorBloc.selectedTeamLeadersList,
            builder: (context,
                AsyncSnapshot<List<WarLeaderApplication>> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data.length == 0) {
                  return Center(
                    child: Text(
                      "NO LEADERS SELECTED YET!",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                }
                return teamLeadersApplicationsList(asyncSnapshot.data);
              } else
                return Container();
            }),
        StreamBuilder(
            stream: _teamLeadersSelectionForWarCreatorBloc.isLoading,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING TEAM LEADERS...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
      ],
    );
  }

  Widget warApplicationHolder() {
    return ListView(
      shrinkWrap: true,
      primary: false,
      children: <Widget>[
        StreamBuilder(
            stream: _teamLeadersSelectionForWarCreatorBloc.warApplicationsList,
            builder: (context,
                AsyncSnapshot<List<WarLeaderApplication>> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data.length == 0) {
                  return Center(
                    child: Text(
                      "NO APPLICATIONS YET!",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                }
                return teamLeadersApplicationsList(asyncSnapshot.data);
              } else
                return Container();
            }),
        StreamBuilder(
            stream: _teamLeadersSelectionForWarCreatorBloc.isLoading,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING APPLICATIONS...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
      ],
    );
  }

  Widget teamLeadersApplicationsList(
      List<WarLeaderApplication> warApplications) {
    return ListView.builder(
        itemCount: warApplications.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
            padding:
                EdgeInsets.only(left: 10.0.w, right: 10.0.w, bottom: 10.0.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: InkWell(
                    onTap: () =>
                        openProfile(warApplications[index].user.userID),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          width: 50.0.w,
                          height: 50.0.w,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: MemecistColors.borderColorGrey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25.0.w))),
                          child: AutoSizeText(
                            warApplications[index]
                                .user
                                .rep
                                .toStringAsPrecision(3),
                            style: TextStyle(
                                color: Colors.black, fontSize: 14.0.sp),
                            maxLines: 1,
                            minFontSize: 10.0.sp,
                            stepGranularity: 10.0.sp / 10,
                          ),
                        ),
                        SizedBox(
                          width: 10.0.w,
                        ),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Flexible(
                                child: AutoSizeText(
                                  "${warApplications[index].user.firstName} ${warApplications[index].user.lastName}",
                                  maxLines: 1,
                                  minFontSize: 12.0.sp,
                                  stepGranularity: 12.0.sp / 12,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontFamily: 'Quicksand',
                                      fontSize: 16.0.sp,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Flexible(
                                child: AutoSizeText(
                                  "@${warApplications[index].user.userName}",
                                  maxLines: 1,
                                  minFontSize: 12.0.sp,
                                  stepGranularity: 12.0.sp / 12,
                                  style: TextStyle(
                                    fontFamily: 'Quicksand',
                                    fontSize: 14.0.sp,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        warApplications[index].user.isVerified
                            ? Container(child: verifiedBadge())
                            : Container()
                      ],
                    ),
                  ),
                ),
                _war.areTeamLeadersSelected
                    ? (warApplications[index].isAccepted
                        ? greyedOutButton(context, "SELECTED", () => null)
                        : Container())
                    : warApplications[index].isSelected
                        ? greyedOutButton(context, "UNSELECT",
                            () => unSelectTeamLeader(warApplications[index]))
                        : actionButton(context, "SELECT",
                            () => selectTeamLeader(warApplications[index]))
              ],
            ),
          );
        });
  }

  Widget approveSelection(Color color, Function() action) {
    return InkWell(
      onTap: action,
      child: Container(
        alignment: Alignment.center,
        height: 50.0.h,
        color: color,
        child: Text(
          "APPROVE",
          style: TextStyle(
              color: Colors.white,
              fontFamily: 'Quicksand',
              fontSize: 20.0.sp,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  void openProfile(String userID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ProfileBlocProvider(
            child: ProfileScreen(userID),
          );
        },
        settings:
            RouteSettings(name: FirebaseAnalyticsConstants.profile_screen)));
  }

  void unSelectTeamLeader(WarLeaderApplication warApplication) {
    _teamLeadersSelectionForWarCreatorBloc.unSelectApplication(warApplication);
  }

  void approveTeamLeadersSelection(
      List<WarLeaderApplication> selectedTeamLeaders) {
    if (_war.maxTeamsCount == selectedTeamLeaders.length) {
      showDialog(
          context: context,
          builder: (context) {
            return new AlertDialog(
              title: Text(
                "Confirm Team Leaders?",
                style: TextStyle(fontSize: 16.0.sp),
              ),
              content: Text(
                "The decision will be final, it cannot be undone!\nThe next phase of war will only start once the timer ends.",
                style: TextStyle(fontSize: 14.0.sp),
              ),
              actions: <Widget>[
                dialogButton("APPROVE", () {
                  _teamLeadersSelectionForWarCreatorBloc.approveTeamLeaders();
                  Navigator.of(context).pop();
                  Fluttertoast.showToast(msg: "Team Leaders approved!");
                }),
                dialogButton("CANCEL", () => Navigator.of(context).pop(),
                    color: Colors.red)
              ],
            );
          });
    } else {
      Fluttertoast.showToast(
          msg:
              "The number of team leaders selected should be equal to the maximum teams count.\nPlease select exactly ${_war.maxTeamsCount} leaders");
    }
  }

  void selectTeamLeader(WarLeaderApplication warApplication) {
    _teamLeadersSelectionForWarCreatorBloc.selectApplication(warApplication);
  }
}
