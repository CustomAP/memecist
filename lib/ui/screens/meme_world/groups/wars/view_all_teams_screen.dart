import 'package:flutter/material.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/view_all_teams_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/view_all_teams_bloc_provider.dart';
import 'package:memecist/core/models/war_team_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_team_widget.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViewAllTeamsScreen extends StatefulWidget {
  final String _warID;

  ViewAllTeamsScreen(this._warID);

  @override
  _ViewAllTeamsScreenState createState() => _ViewAllTeamsScreenState(_warID);
}

class _ViewAllTeamsScreenState extends State<ViewAllTeamsScreen> {
  ViewAllTeamsBloc _viewAllTeamsBloc;
  bool firstRun = true;
  String _warID;

  _ViewAllTeamsScreenState(this._warID);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _viewAllTeamsBloc = ViewAllTeamsBlocProvider.of(context);
    if (firstRun) {
      _viewAllTeamsBloc.getAllWarTeams(_warID);
    }
  }

  @override
  void dispose() {
    _viewAllTeamsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButtonAndTitle("All Teams", context),
      body: StreamBuilder(
          stream: _viewAllTeamsBloc.warTeams,
          builder: (context, AsyncSnapshot<List<WarTeam>> asyncSnapshot) {
            if (asyncSnapshot.hasData) {
              return ListView.builder(
                  itemCount: asyncSnapshot.data.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.all(10.0.w),
                      child: WarTeamWidget(asyncSnapshot.data[index]),
                    );
                  });
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }
}
