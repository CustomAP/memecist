import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:memecist/core/blocs/meme_world/groups/create_group_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/group_details_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/groups_tab_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/groups_tab_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/my_groups_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/wars/war_details_bloc_provider.dart';
import 'package:memecist/core/constants/firebase_analytics_constants.dart';
import 'package:memecist/core/constants/firebase_storage_constants.dart';
import 'package:memecist/core/models/group_model.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_world/groups/create_group_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/group_details_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/my_groups_screen.dart';
import 'package:memecist/ui/screens/meme_world/groups/wars/war_details_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GroupsTabScreen extends StatefulWidget {
  @override
  _GroupsTabScreenState createState() => _GroupsTabScreenState();
}

class _GroupsTabScreenState extends State<GroupsTabScreen>
    with AutomaticKeepAliveClientMixin {
  GroupsTabBloc _groupsTabBloc;

  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreGroups = true;
  bool _isLoading = false;

  @override
  void dispose() {
    _scrollController.dispose();
    _groupsTabBloc.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _groupsTabBloc = GroupsTabBlocProvider.of(context);

    if (isFirstRun) {
      _groupsTabBloc.fetchGroups();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreGroups &&
            !_isLoading) {
          _groupsTabBloc.fetchGroups();
        }
      });

      _groupsTabBloc.hasMoreGroups.listen((hasMoreData) {
        _hasMoreGroups = hasMoreData;
      });

      _groupsTabBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      isFirstRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return LiquidPullToRefresh(
      onRefresh: () => refreshScreen(),
      animSpeedFactor: 2.0,
      showChildOpacityTransition: false,
      color: MemecistColors.refreshBackgroundColor,
      child: ListView(
        children: <Widget>[
          actionButtonsRow(),
          warsListView(),
        ],
      ),
    );
  }

  Widget warsListView() {
    return ListView(
      shrinkWrap: true,
      controller: _scrollController,
      children: <Widget>[
        SizedBox(
          height: 20.0.h,
        ),
        StreamBuilder(
            stream: _groupsTabBloc.groupsList,
            builder: (context, AsyncSnapshot<List<Group>> asyncSnapshot) {
              if (asyncSnapshot.hasData) {
                if (asyncSnapshot.data.length == 0) {
                  return Center(
                    child: Text(
                      "NO GROUPS WITH ACTIVE WARS!",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                }
                return groupsList(asyncSnapshot.data);
              } else
                return Container();
            }),
        StreamBuilder(
            stream: _groupsTabBloc.isLoading,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return Center(
                  child: Text(
                    "LOADING GROUPS...",
                    style: TextStyle(
                        fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                  ),
                );
              } else {
                return Container();
              }
            }),
      ],
    );
  }

  Widget groupsList(List<Group> groups) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: groups.length,
        itemBuilder: (context, index) {
          return groupWithWarItem(groups[index]);
        });
  }

  Widget groupWithWarItem(Group group) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: InkWell(
            onTap: () => openGroupDetailsScreen(group.groupID),
            child: Container(
              padding: EdgeInsets.only(
                  top: 20.0.h, bottom: 20.0.h, left: 10.0.w, right: 10.0.w),
              child: Column(
                children: <Widget>[
                  Container(
                    width: 60.0.w,
                    height: 60.0.w,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.0.w)),
                        image: DecorationImage(
                            image: CachedNetworkImageProvider(
                                group.groupProfilePic),
                            fit: BoxFit.cover)),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  AutoSizeText(
                    group.groupName,
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    minFontSize: 12.0.sp,
                    stepGranularity: 12.0.sp/12,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0.sp,
                        color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: InkWell(
            onTap: () => openWarDetailsScreen(group.warID),
            child: Container(
              padding: EdgeInsets.all(20.0.w),
              margin: EdgeInsets.all(10.0.w),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20.0.w)),
                color: Color(
                    int.parse(group.warCardColor.substring(1, 7), radix: 16) +
                        0xFF000000),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      width: 40.0.w,
                      height: 40.0.w,
                      decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.all(Radius.circular(30.0.w)),
                          image: DecorationImage(
                              image: CachedNetworkImageProvider(group.warPic),
                              fit: BoxFit.cover))),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  AutoSizeText(
                    group.warName,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    minFontSize: 12.0.sp,
                    stepGranularity: 12.0.sp/12,
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        color: Colors.white,
                        fontSize: 16.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  Text(
                    group.warStatus,
                    style: TextStyle(
                        fontFamily: 'Quicksand',
                        color: Colors.white,
                        fontSize: 13.0.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget actionButtonsRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        actionButton(context, 
          "My Groups",
          () => Navigator.of(context).push(MaterialPageRoute(
              builder: (context) {
                return MyGroupsBlocProvider(child: MyGroupsScreen());
              },
              settings: RouteSettings(
                  name: FirebaseAnalyticsConstants.my_groups_screen))),
          width: 180.0,
        ),
        actionButton(context, 
          "Create Group",
          () => Navigator.of(context).push(MaterialPageRoute(
              builder: (context) {
                return CreateGroupBlocProvider(child: CreateGroupScreen());
              },
              settings: RouteSettings(
                  name: FirebaseAnalyticsConstants.create_group_screen))),
          width: 180.0,
        )
      ],
    );
  }

  Future<void> refreshScreen() async {
    _hasMoreGroups = true;
    _isLoading = false;
    _groupsTabBloc.refreshScreen();
    _groupsTabBloc.fetchGroups();
  }

  void openGroupDetailsScreen(String groupID) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return GroupDetailsBlocProvider(
            child: GroupDetailsScreen(groupID),
          );
        },
        settings: RouteSettings(
            name: FirebaseAnalyticsConstants.group_details_screen)));
  }

  void openWarDetailsScreen(String warID) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return WarDetailsBlocProvider(
        child: WarDetailsScreen(warID),
      );
    }));
  }

  @override
  bool get wantKeepAlive => true;
}
