import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:memecist/core/blocs/meme_post/view_reacts_bloc_provider.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_unapproved_memes_bloc.dart';
import 'package:memecist/core/blocs/meme_world/groups/view_unapproved_memes_bloc_provider.dart';
import 'package:memecist/core/blocs/profile/profile_bloc_provider.dart';
import 'package:memecist/core/models/meme_post_model.dart';
import 'package:memecist/core/models/user_model.dart';
import 'package:memecist/core/blocs/meme_post/view_reacts_bloc.dart';
import 'package:memecist/ui/constants/colors.dart';
import 'package:memecist/ui/screens/meme_post/meme_post_for_approval.dart';
import 'package:memecist/ui/screens/profile/profile_screen.dart';
import 'package:memecist/ui/widgets/common_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViewUnapprovedMemesScreen extends StatefulWidget {
  final String _groupID;

  ViewUnapprovedMemesScreen(this._groupID);

  @override
  _ViewUnapprovedMemesScreenState createState() =>
      _ViewUnapprovedMemesScreenState(_groupID);
}

class _ViewUnapprovedMemesScreenState extends State<ViewUnapprovedMemesScreen> {
  String _groupID;

  _ViewUnapprovedMemesScreenState(this._groupID);

  ViewUnapprovedMemesBloc _viewUnapprovedMemesBloc;
  ScrollController _scrollController = ScrollController();
  bool isFirstRun = true;
  bool _hasMoreReacts = true;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MemecistColors.backgroundColor,
      appBar: appBarWithBackButtonAndTitle("Approve Memes", context),
      body: ListView(
        controller: _scrollController,
        children: <Widget>[
          SizedBox(
            height: 20.0.h,
          ),
          StreamBuilder(
              stream: _viewUnapprovedMemesBloc.memePostList,
              builder: (context, AsyncSnapshot<List<MemePost>> asyncSnapshot) {
                if (asyncSnapshot.hasData) {
                  if (asyncSnapshot.data.length == 0) {
                    return Center(
                      child: Text(
                        "NO MEMES TO APPROVE!",
                        style: TextStyle(
                            fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return memePostList(asyncSnapshot.data);
                } else
                  return Container();
              }),
          StreamBuilder(
              stream: _viewUnapprovedMemesBloc.isLoading,
              builder: (context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  return Center(
                    child: Text(
                      "LOADING MEMES...",
                      style: TextStyle(
                          fontSize: 15.0.sp, fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Container();
                }
              }),
        ],
      ),
    );
  }

  Widget memePostList(List<MemePost> memePosts) {
    return ListView.builder(
        itemCount: memePosts.length,
        primary: false,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return MemePostForApprovalWidget(
              memePosts[index],
              () => approveMeme(memePosts[index]),
              () => rejectMeme(memePosts[index]));
        });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (isFirstRun) {
      _viewUnapprovedMemesBloc = ViewUnapprovedMemesBlocProvider.of(context);
      _viewUnapprovedMemesBloc.initializeViewUnapprovedMemesBloc(_groupID);

      _viewUnapprovedMemesBloc.fetchUnapprovedMemes();

      _scrollController.addListener(() {
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;
        double delta = (maxScroll - MediaQuery.of(context).size.height) * 0.40;
        if (maxScroll - currentScroll <= delta &&
            _hasMoreReacts &&
            !_isLoading) {
          _viewUnapprovedMemesBloc.fetchUnapprovedMemes();
        }
      });

      _viewUnapprovedMemesBloc.hasMoreMemes.listen((hasMoreData) {
        _hasMoreReacts = hasMoreData;
      });

      _viewUnapprovedMemesBloc.isLoading.listen((isLoading) {
        _isLoading = isLoading;
      });

      _viewUnapprovedMemesBloc.approve.listen((memePost) {
        Fluttertoast.showToast(msg: "Meme Approved!");
      });

      _viewUnapprovedMemesBloc.reject.listen((memePost) {
        Fluttertoast.showToast(msg: "Meme Rejected!");
      });

      isFirstRun = false;
    }
  }

  approveMeme(MemePost memePost) {
    _viewUnapprovedMemesBloc.approveMeme(memePost);
  }

  rejectMeme(MemePost memePost) {
    _viewUnapprovedMemesBloc.rejectMeme(memePost);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _viewUnapprovedMemesBloc.dispose();
    super.dispose();
  }
}
